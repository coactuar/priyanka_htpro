<?php
	require_once "config.php";

	if(!isset($_SESSION["employ_id"]))
	{
		header("location: ./");
		exit;
	}

	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $employ_id=$_SESSION["employ_id"];
            $location=$_SESSION["location"];
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where employ_id='$employ_id' and eventname='$eventname'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
			   
            unset($_SESSION['name']);
            unset($_SESSION['employ_id']);
            unset($_SESSION['location']);
            header("location: ./");
            exit;
        }

    }

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BFIL</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body class="video">
<div class="top">
       <!-- <img src="img/top-banner.jpg"  class="img-fluid" alt=""/> -->
</div>

<div class="container-fluid">    
    <div class="row">
	<div class="col-12 col-md-12 text-center mt-2">
            
            
        </div>
         <div class="col-12 col-md-9 text-center">
			<div class="embed-responsive embed-responsive-16by9 mt-2">
			
			
			
            <iframe src="video.php" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="GPJ RALLY 2018 X_1"></iframe>
            </div>
        </div>

        
        
			   
            
       
		<div class="text-right offset-md-1  mt-2">
                Hello <?php echo $_SESSION['name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
             
            </div>
		 </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         async:false,
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);



</script>

</body>
</html>