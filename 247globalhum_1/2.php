<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBI General Insurance :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="bg1">
<div class="container-fliud  navbar navbar-default">
<div class="top-banner mt-2  " >
    <img src="img/24.png" class="img-fluid  w-25" alt=""/> 
</div>

</div>
<div class="top-banner mt-5  text-center " >
    <img src="img/Hum Orange.png"  width="150px" alt=""/> 
</div>
<div class="top-banner mt-5  text-center " >
    <img src="img/Sentance.png"  width="80%" alt=""/> 
</div>

<div class="content-area">	
			<div id="loginForm">
			<form id="login-form" method="post" role="form">
			<!--<div class="offset-3">
			<img src="img/Pahal Logo.png" width=70%  alt=""/>
			</div>	
			<br/>-->
			<div class="backs">	
			  <div id="login-message"></div>
			  <!-- <div class="input-group">
				<input type="text" class="form-control" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
			  </div> -->
			  <div class="input-group">
				<input type="email" class="form-control" placeholder="Enter your email id" name="empid" id="empid" autocomplete="off" required>
			  </div>
			  <div class="input-group">
				<!-- <button type="image" src=" id="login" class="login-button" type="submit">Login</button> -->
				<input type="image" src="img/HUM2.png" alt="Submit" width="100%">
			  </div>
			  </div>
			  <!-- <div class="offset-4">
			  <p style="font-size:24px" id="demo"></p>
			  </div> -->
			</form> 
		  </div> 
	
</div>

<script>
// Set the date we're counting down to
// var countDownDate = new Date("Dec 10, 2020 11:00:00").getTime();

// // Update the count down every 1 second
// var x = setInterval(function() {

//   // Get today's date and time
//   var now = new Date().getTime();
    
//   // Find the distance between now and the count down date
//   var distance = countDownDate - now;
    
//   // Time calculations for days, hours, minutes and seconds
//   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//   var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
//   // Output the result in an element with id="demo"
//   document.getElementById("demo").innerHTML = days + "d " + hours + "h "
//   + minutes + "m " + seconds + "s ";
    
//   // If the count down is over, write some text 
//   if (distance < 0) {
//     clearInterval(x);
//     document.getElementById("demo").innerHTML = "Live Now";
//   }
// }, 1000);
</script>

      
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  



	
  // var phoneNo = document.getElementById('empid');

  // if (phoneNo.value.length < 5 || phoneNo.value.length > 5) {
	//   console.log('Employee ID. is not valid,');
  //   alert("Please Enter The Valid Employee ID Max-Length Should be 5 ");
  //   return false;
  // }


  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
     $('#login').attr('disabled', false);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else if (data == '0')
      {
          $('#login-message').text('Invalid Employee Id. Please try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      $('#login').attr('disabled', false);
  });

  
  return false;
});
</script>
</body>
</html>