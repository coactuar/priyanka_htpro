<?php
require_once "../config.php";

$sql = "SELECT `user_name`,`user_code`,`user_phone`,`user_question`,`asked_at`, `speaker`,`responded`,`doctor`,`diet`,`exercise`,`mentor`,`operations`,`answered`  FROM `tbl_questions` where eventname='$eventname' order by asked_at asc";  
$setRec = mysqli_query($link, $sql); 
$columnHeader = '';  
$columnHeader = "#" . "\t". "Name" . "\t" . "Country Code" . "\t" . "Phone No." . "\t" . "Question" . "\t" . "Asked At" . "\t"  . "To an Expert?" . "\t" . "Responded" . "\t" . "Doctor" . "\t" . "Diet" . "\t" . "Exercise" . "\t" . "Mentor" . "\t" . "Operations" . "\t" .  "Answered?" . "\t";  
$setData = '';  
  $i = 1;
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = '"'.$i.'"' . "\t";  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
    //echo $rowData.'<br>';
    $i = $i + 1;
}  

  
$file = 'FFD_Questions.xls';  
header("Content-Type: application/octet-stream");  
header("Content-Disposition: attachment; filename=".$file);  
header("Pragma: no-cache");  
header("Expires: 0");  


echo ucwords($columnHeader) . "\n" . $setData . "\n";  

?>