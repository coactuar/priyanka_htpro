<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_phone"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where mobile_num='$phone' and eventname='$eventname'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<!-- Pixel Code for https://datastudio.co.in/ -->
<script async src="https://datastudio.co.in/pixel/OGoYoWMIkaEXWgvX"></script>
<!-- END Pixel Code -->
<body>
<div class="container-fluid">
    <div class="row mt-2 mb-2">
        <div class="col-12 col-md-12">
            <img src="img/logo.png" class="img-fluid logo" alt=""/> 
        </div>
    </div>
    <div class="row mt-0 mb-2">
        <div class="col-12 col-md-12 text-center top-img">
            <img src="img/top-heading2.png" class="img-fluid" alt=""/> 
        </div>
    </div>
</div>
<div class="container">    
    <div class="row">
        <div class="col-12 col-md-4 mt-5  text-center">
            <div class="event-info">
                <img src="img/pramod.png" class="img-fluid dr-photo mt-5"  alt=""/>
                <img src="img/20-Nov-webinar-Login-Background_01.jpg" class="img-fluid date" alt=""/>
            </div>
        </div>

        <div class="col-12 col-md-8 text-center">
        <div class="text-right mt-2 mr-3 mt-2">
        Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
               <a href="https://docs.google.com/forms/d/e/1FAIpQLSfwoYpwzGxs9C-5OnIjpL-ZbshIonA0cZnzfVdrF2gKqfF5iw/viewform?gxids=7628" target="_blank" class="btn btn-sm btn-success">Feedback</a>
            </div>
            <div class="col-12 col-md-12  text-center">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
 
  <li class="nav-item">
  <a  class="nav-link text-danger" href="webcast_second.php" >"Trouble Loading Video - Please Switch to Secondary Link" - Click Here.. </a>
  </li>
 

  <!-- viemo video -->
  <div class="embed-responsive embed-responsive-16by9 video-panel">
  
  <!-- <iframe src="https://vimeo.com/event/1031150/embed/aa3862a2d9" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> -->
    <iframe src="https://vimeo.com/event/1031150/embed/aa3862a2d9" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div>
			</div>
  
</div>
        </div>
        <div class="col-12 text-center">
            <div id="question" class="mt-2">
              <div id="question-form" class="panel panel-default">
			   <span style="color:red">You can Submit the questions in the below question box during the Q&A Session.</span>
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="2"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_phone" name="user_phone" value="<?php echo $_SESSION['user_phone']; ?>">
                          <input type="hidden" id="user_code" name="user_code" value="<?php echo $_SESSION['user_code']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                      
                      
                      
                  </form>
              </div>
          </div>
            
        </div>
    </div>
</div>
<div class="container-fluid">    
    <div class="row  mb-1">
        <div class="col-12 text-center">
            <div class="icons ">
            <!-- <span class="badge badge-info"> -></span> -->
            <a href="https://bit.ly/3DgTvAc" target="_blank">Register as Diabetic</a> &nbsp;&nbsp;&nbsp; || &nbsp;&nbsp;&nbsp;<a href="https://bit.ly/30idYX0" target="_blank" class="web">Register as Non-Diabetic</i></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">    
    <div class="row mt-3 mb-1">
        <div class="col-12 text-center">
            <div class="icons bg-black">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$('#pills-profile-tab').on('click', function(){
   
   $('#pills-profile-tab').hide();
});
$(function(){


	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);

function changeVideo(video, l)
{
    var vid = '.'+l;
    $('.vid-link').removeClass('act');
    $(vid).addClass('act');
    
    $('#webcast').attr("src",video);
    
    return false;
    
}

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>