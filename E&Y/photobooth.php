<?php
require_once "logincheck.php";
$curr_room = 'photobooth';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content" class="photobooth">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg" class="photobooth">
            <!-- <img src="assets/images/images.jfif"> -->

            <div id="photobooth">
                <img src="assets/img/awesome.png" class="img-fluid">
                <div id="template">
                    <div id="my_camera"></div>
                    <div id="results"></div>

                    <div id="actions">
                        <a href="#"><img src="assets/img/btn-click.png" width="150" id="take" alt="" onClick="take_snapshot()" style="display:block;" /></a>
                        <a href="#"><img src="assets/img/btn-save.png" width="150" id="save" alt="" onClick="saveSnap()" style="display:none;" /></a>
                        <div id="download-image" style="display:none;">
                            <a href="#" id="link" target="_blank" download><img src="assets/img/btn-download.png" width="150" /></a><br>
                            <small>(Right-click and select Save link as)</small><br><br>
                            <h6 style="font-size: 1em;">Share on Social Media</h6>
                            <a href="#" id="facebook" target="_blank"><i class="fab fa-facebook-square fa-3x"></i></a>
                            <a href="#" id="linkedin" target="_blank"><i class="fab fa-linkedin fa-3x"></i></a>
                            <a href="#" id="twitter" target="_blank"><i class="fab fa-twitter-square fa-3x" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
        <div id="back-button">
            <a href="lobby.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
        </div>
        <?php require_once "commons.php" ?>
    </div>
    <?php require_once "scripts.php" ?>
    <script type="text/javascript" src="assets/js/webcam.min.js"></script>
    <script>
        // Configure a few settings and attach camera
        function configure() {
            Webcam.set({
                width: 367,
                height: 225,
                image_format: 'jpg',
                jpeg_quality: 90
            });
            Webcam.attach('#my_camera');
        }
        // A button for taking snaps
        configure();

        // preload shutter audio clip
        var shutter = new Audio();
        shutter.autoplay = false;
        shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

        function take_snapshot() {
            // play sound effect
            shutter.play();

            // take snapshot and get image data
            Webcam.snap(function(data_uri) {
                // display results in page
                document.getElementById('results').innerHTML =
                    '<img id="imageprev" src="' + data_uri + '"/>';
            });
            document.getElementById('take').style.display = 'none';
            document.getElementById('save').style.display = 'block';



            Webcam.reset();
        }

        function saveSnap() {
            console.log('saving');
            // Get base64 value from <img id='imageprev'> source
            var base64image = document.getElementById("imageprev").src;

            Webcam.upload(base64image, 'upload.php', function(code, text) {
                //console.log('Save successfully');
                console.log(text);
                document.getElementById("link").setAttribute('href', text);
                document.getElementById("facebook").setAttribute('href', 'https://www.facebook.com/sharer.php?u=' + text + '&quote=');
                document.getElementById("linkedin").setAttribute('href', 'https://www.linkedin.com/shareArticle?mini=true&url=' + text + '&summary=%20&source=LinkedIn');
                document.getElementById("twitter").setAttribute('href', 'https://twitter.com/intent/tweet?url=' + text + '&text=');
                document.getElementById("download-image").style.display = 'inline-block';
                document.getElementById('save').style.display = 'none';

            });

        }
    </script>
    <?php require_once "ga.php"; ?>

    <?php require_once 'footer.php';  ?>