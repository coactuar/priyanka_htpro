<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$fname = '';
$lname = '';
$emailid = '';
$mobile = '';


if (isset($_POST['reguser-btn'])) {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Mobile No. is required';
    }


    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];

    if (count($errors) == 0) {
        $newuser = new User();
        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);

        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

?>
<?php require_once 'header.php';  ?>
<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12">
            <img src="assets/img/logo-philips.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row p-2">
        <div class="col-12 text-center">
            <img src="assets/img/registration.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3 p-2">
            <?php
            if (count($errors) > 0) : ?>
                <div class="alert alert-danger alert-msg">
                    <ul class="list-unstyled">
                        <?php foreach ($errors as $error) : ?>
                            <li>
                                <?php echo $error; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if ($succ != '') { ?>
                <div class="alert alert-success alert-msg">
                    <?= $succ ?>
                </div>
            <?php } ?>
            <form method="POST">
                <div class="row mt-2">
                    <div class="col-12 col-md-6">
                        <input type="text" placeholder="First Name" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <input type="text" placeholder="Last Name" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 col-md-6">
                        <input type="email" placeholder="Email ID" id="emailid" name="emailid" class="input" value="<?php echo $emailid; ?>" autocomplete="off">
                    </div>

                    <div class="col-12 col-md-6">
                        <input type="number" placeholder="Mobile No." id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                    </div>
                </div>
                <div class="row mt-5 mb-3">
                    <div class="col-12 text-center">
                        <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-register" value="" />
                    </div>

                </div>
            </form>
        </div>
    </div>
    <div class="row p-2">
        <div class="col-12 text-center">
            <a href="./"><img src="assets/img/mainlogin.png" class="img-fluid" alt=""></a>
        </div>
    </div>
    <div class="row trans-bg">
        <div class="col-12 col-md-8 offset-md-2 text-center">
            <img src="assets/img/sie2020.png" class="img-fluid" alt="">
        </div>
    </div>

</div>
<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>