<nav class="navbar bottom-nav">
  <ul class="nav mr-auto ml-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link" href="auditorium.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li> -->
    <!-- <li class="nav-item">
      <a class="nav-link" href="#" id="selectAudi" title="Go To Meeting Rooms"><i class="fas fa-users"></i><span class="hide-menu">Meeting Rooms</span></a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link" href="auditorium.php"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditroium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="https://coact.live/test/games/5/"><i class="fa fa-gamepad"></i><span class="hide-menu">Games</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link view" href="assets/resources/conf-agenda.jpg" title=""><i class="far fa-list-alt"></i><span class="hide-menu">Agenda</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="photobooth.php"><i class="fas fa-camera"></i><span class="hide-menu">Photo Booth</span></a>
    </li>
    
    <li> <a class="show_talktous" href="#" title="Talk to Us" data-from="<?php echo $_SESSION['userid']; ?>"><i class="fas fa-comment-alt"></i><span class="hide-menu"></span>Talk To Us</a></li>
    <!-- <li class="nav-item">
      <a class="nav-link view" href="assets/resources/help.jpg" title=""><i class="fas fa-question"></i><span class="hide-menu">Help</span></a>
    </li> -->

    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>
<!-- <div id="helpline">
  For assistance:<br>
  <i class="fas fa-phone-square-alt"></i> +917314-855-655
</div> -->