<?php
require_once "logincheck.php";
$curr_room = 'lobby';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/images/EY lobby Email 1 Screen.jpg" usemap="#image-map">
            <!-- <map name="image-map">
                <area target="" alt="Photobooth" title="Photobooth" href="photobooth.php" coords="1210,435,1207,582,1508,598,1510,435" shape="poly">
            </map> -->
            <a class="show_talktous" href="#" title="Talk to Us" data-from="<?php echo $_SESSION['userid']; ?>" id="helpdesk">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="https://player.vimeo.com/video/482186151" id="lobbyVideo" class="viewvideo"></a> -->
            <!-- <a href="assests/images/download.png" id="lobbyVideo" class="view"></a> -->
            <img src="assets/images/download.png" id="lobbyLogo" usemap="#image-map">
            <!-- <a href="room1.php" id="enterDWI">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <a href="room2.php" id="enterLTA">
                <div class="indicator d-6"></div>
            </a>
            <a href="room3.php" id="enterPAM">
                <div class="indicator d-6"></div>
            </a>
            <a href="room4.php" id="enterSF">
                <div class="indicator d-6"></div>
             </a> -->
             <a href="photobooth.php" id="photoboothlobby">
                <div class="indicator d-6"></div>
             </a> 
            <a href="auditorium.php?ses=53e9ad9958955c5bfe9d42f1c527c906b430892b78821e0a1e0cd8d272172a16" id="enterSes1">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="auditorium.php?ses=025b947e2fb0de5299954d9d3b629df9da13731902bb09cf801498cd5b853179" id="enterSes2">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <a href="auditorium.php" id="enterAudi">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <a class="view" href="assets/resources/conf-agenda.jpg" id="showAgenda"></a> -->
            <!-- <a class="showpdf" href="assets/resources/conf-agenda.pdf" id="showProfile"></a> -->
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
   // $(function() {
    //     $.magnificPopup.open({
    //         items: {
    //             src: 'https://player.vimeo.com/video/482186151'
    //         },
    //         type: 'iframe',
    //         iframe: {
    //             markup: '<div class="mfp-iframe-scaler">' +
    //                 '<div class="mfp-close"></div>' +
    //                 '<iframe class="mfp-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>' +
    //                 '<div class="mfp-title"></div>' +
    //                 '</div>'
    //         },
    //         removalDelay: 300,
    //         mainClass: 'mfp-fade'
    //     });
    //     ImageMap('img[usemap]', 500);
    // });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>