<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = 'bf2d7511d57abed9f22f2213a9a449212f15d564962705ab151bae17016f403f';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'room1';
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/img/room.jpg" usemap="#image-map">
            <map name="image-map">
                <area onclick="showAttendees()" alt="Attendee List" title="Attendee List" href="#" coords="323,247,922,357,927,530,331,550" shape="poly">
                <area target="_blank" alt="Join Meeting" title="Join Meeting" href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWZjMTcxMzktNWMzYS00NDhiLWJjYTktMGNlNjJmZDBiYzlk%40thread.v2/0?context=%7b%22Tid%22%3a%225c578eff-4c48-49dd-9195-fdf1a837a69a%22%2c%22Oid%22%3a%220b13691d-0b03-4672-8946-9413a344dcd2%22%7d" coords="1161,394,1156,481,1478,471,1480,369" shape="poly">
                <area target="_blank" alt="Miro Whiteboard" title="Miro Whiteboard" href="https://miro.com/app/board/o9J_kgxYdFg=/" coords="1498,368,1498,473,1702,470,1702,351" shape="poly">
            </map>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>

<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);

    function showAttendees() {
        $.magnificPopup.open({
            items: {
                src: 'assets/resources/list3.jpg'
            },
            type: 'image'
        });
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>