<?php
session_start();
 
// if (isset($_SESSION["authenticated"])) {
//     header("location: widget.php");
//     exit;
// }

// Include config file
require_once "connection.php";
 
// Define variables and initialize with empty values
$token = $username="";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    // Takes raw data from the request
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $data = json_decode($json);

       // $username=$data->username;
        $username=$_SESSION['username'];
       // Prepare an insert statement
       // $sql = "INSERT INTO users (username, token) VALUES (?, ?)";

        $sql = "SELECT id, username, token FROM users WHERE username = ?";
        
        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $token);
                    if (mysqli_stmt_fetch($stmt)) {
                    echo json_encode($token);
                     }
                } else {
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
         
       
    

//}
?>

