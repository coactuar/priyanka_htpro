<?php
session_start();
 
// if (isset($_SESSION["authenticated"])) {
//     header("location: widget.php");
//     exit;
// }

// Include config file
require_once "connection.php";
 
// Define variables and initialize with empty values
$token = $username="";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    // Takes raw data from the request
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $data = json_decode($json);

        $username=$data->username;
        $token=$data->token;
        // Prepare an insert statement
       // $sql = "INSERT INTO users (username, token) VALUES (?, ?)";

          $sql= "UPDATE users SET token=? WHERE username=?";
         
        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_token,$param_username);
            
            // Set parameters
            $param_username = $username;
            $param_token = $token;
            
            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Redirect to login page
                header("location: widget.php");
            } else {
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    

//}
?>

