<?php
require_once "logincheck.php";
$curr_room = 'exhibithall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<link href='assets/css/simplelightbox.min.css' rel='stylesheet' type='text/css'>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/exhibithall.jpg">
            <a href="assets/resources/exhibits/0101.jpg" data-docid="9f23f9255e7ac9c0a5ddff539f5d65e270aeb50494553d55c4018dd6f5d47cb0" class="view resdl" id="exhibit0101">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0102.jpg" data-docid="c908ba4891d8b4e60efd176a4d5919ca9ba04b3cbe56647a1694049060e41be9" class="view resdl" id="exhibit0102">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0103.jpg" data-docid="1e42ea9f6778cb55fb523006dd6cad80679adf00695e12d2f948640b53c278a5" class="view resdl" id="exhibit0103">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0104.jpg" data-docid="ccf6f6e80ff57064532813441de7d5303d75c87a8e8881d6431100257832c234" class="view resdl" id="exhibit0104">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0105.jpg" data-docid="b982f6b288f7132f1ed925fef49bd3eb919f0b45d7a76b6a468372609969ca1e" class="view resdl" id="exhibit0105">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0106.jpg" data-docid="a05ed63fe30edb63b02ea782b069e69fa8c40e25206bbb57494dd2f77d780560" class="view resdl" id="exhibit0106">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0107.jpg" data-docid="27344d07e1468f88d7701b43ecf3f0e70bb5b7aa6511b4edf43810376cfeb146" class="view resdl" id="exhibit0107">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0108.jpg" data-docid="c2e40ce8c12184e4c614841fcec99ff747c2b08a91dd5c168bd814faa10c831e" class="view resdl" id="exhibit0108">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0109.jpg" data-docid="e67fbe7d51ed74d30690ff86c6d26a1fe740c00b5ce18050feddbea8d5f74dcc" class="view resdl" id="exhibit0109">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0201.jpg" data-docid="21b7497f766a0faf203aeab40be51ca3e3998611b3cb64b3e78d78b2ed7f8276" class="view resdl" id="exhibit0201">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0202.jpg" data-docid="efa4c4543418635542b682e7a745336b2fcfccf0df6e016b2c29098a8bf9df49" class="view resdl" id="exhibit0202">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0203.jpg" data-docid="eea172f6ad38748bc7881603bae616ba4cdc05d745332699f8dae0d61306ef31" class="view resdl" id="exhibit0203">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0204.jpg" data-docid="6536d296152d92eac46338064adddfa3b8062e8b0c8537032fa8ba5d3e5b3605" class="view resdl" id="exhibit0204">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0205.jpg" data-docid="7e142956e4e5d06beeb4893a5b5ae2c900733827bcaa762cefc89a2d24ddba64" class="view resdl" id="exhibit0205">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0206.jpg" data-docid="4668a08e92806d6481e231d191382066e6f0715d84a7332a6b8a994fb04b4719" class="view resdl" id="exhibit0206">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0207.jpg" data-docid="56356744809005bc4f5da3f7fd842cbf99543fc174995314e0363925bf31b287" class="view resdl" id="exhibit0207">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0208.jpg" data-docid="8c76693793099bf58f918402f845f886a0718388e7e6514781c4b02c894101a6" class="view resdl" id="exhibit0208">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0209.jpg" data-docid="a786177fae7ded46326192166cec60fa9ea20ef3cc64f66817a3844511a4656f" class="view resdl" id="exhibit0209">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0301.jpg" data-docid="bf0948dcb3806724cb6ac86244ff12871a35192b41e83c7668eb16a379818c88" class="view resdl" id="exhibit0301">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0302.jpg" data-docid="2e8de4f9306d8da91437acf3d080fb82bac3e620ffa8908a625021725558c5da" class="view resdl" id="exhibit0302">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0303.jpg" data-docid="21c9cac25f40e5611024a685bb2a7e440f9fe79df50f495145c520f3bc266162" class="view resdl" id="exhibit0303">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0304.jpg" data-docid="2e00c242acbfb2cbcfa113fcb63056c0ec2cdc507a813f74139dee6d9b2c0b38" class="view resdl" id="exhibit0304">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0305.jpg" data-docid="20dc52f54678ec869fdc163e2c47e65bbba008b8d45526a852bf168349fea95e" class="view resdl" id="exhibit0305">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0306.jpg" data-docid="c9cc70cfd3d0541f0ed0e3af0d62a250ae0ccbc1841a72fc709540651982e0c5" class="view resdl" id="exhibit0306">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0307.jpg" data-docid="44e768e188c0c86260aaf16df4e8f908a3910e0fa2c7bf6c2b4634e31408d953" class="view resdl" id="exhibit0307">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0308.jpg" data-docid="6c81430e70fa3a6f1b93c4e8483dce0e89f90604836b7ab2d22c2273480df909" class="view resdl" id="exhibit0308">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/exhibits/0309.jpg" data-docid="44d29a9bb4762edfbc0bdaf63e1bcf3d19b124021adbe340cf5b4bffc679b515" class="view resdl" id="exhibit0309">
                <div class="indicator d-6"></div>
            </a>



        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>