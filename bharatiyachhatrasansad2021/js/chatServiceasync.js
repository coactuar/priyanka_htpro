const chatServiceasync = (function () {
  $("#empty-chat").hide();
  $("#group-message-holder").hide();
  $("#loading-message-container").hide();
  $("#send-message-spinner").hide();
  $("#comethat").hide();
  let messageArray = [];

  function initializeApp() {
    return new Promise((resolve) => {
      let cometChatAppSetting = new CometChat.AppSettingsBuilder()
        .subscribePresenceForAllUsers()
        .setRegion("US")
        .build();

      CometChat.init(APP_ID, cometChatAppSetting).then(
        () => {
          console.log("Initialization completed successfully");
          resolve();
          this.retrieveUserDetails($("#username").text());
        },
        (error) => {
          console.log("Initialization failed with error:", error);
        }
      );
      resolve();
    });
  }
  function retrieveUserDetails(username) {
    CometChat.getUser(username).then(
      (user) => {
        console.log("User details fetched for user:", user);
        console.log(user.authToken);
        // this.authLoginUser(user.authToken);
        $("#comethat").show();
      },
      (error) => {
        console.log("User details fetching failed with error:", error);
        this.createUserOnCometChat(username);
      }
    );
  }
  function createUserOnCometChat(username) {
    let url = `https://api-us.cometchat.io/v3.0/users`;
    let data = {
      uid: username,
      name: `${username}`,
      avatar:
        "https://data-us.cometchat.io/assets/images/avatars/captainamerica.png",
    };

    fetch(url, {
      method: "POST",
      headers: new Headers({
        appid: APP_ID,
        apikey: REST_API_KEY,
        "Content-Type": "application/json",
      }),
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        this.addUserToAGroup(result.data.uid);
        console.log(result, "User created");
      })
      .catch((error) => console.log(error));
  }

  function addUserToAGroup(uid) {
    let url = `https://api-us.cometchat.io/v3.0/groups/supergroup/members`;
    let data = {
      participants: [uid],
    };

    fetch(url, {
      method: "POST",
      headers: new Headers({
        appid: APP_ID,
        apikey: REST_API_KEY,
        "Content-Type": "application/json",
      }),
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        this.generateAuthToken(uid);
        console.log(result, "User added to a group");
      });
  }

  function generateAuthToken(uid) {
    let url = `https://api-us.cometchat.io/v3.0/users/${uid}/auth_tokens`;

    fetch(url, {
      method: "POST",
      headers: new Headers({
        appid: APP_ID,
        apikey: REST_API_KEY,
        "Content-Type": "application/json",
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result, "Token generated");
        this.authLoginUser(result.data.authToken);
        url = `authtoken.php`;
        fetch(url, {
          method: "POST",
          body: JSON.stringify({
            username: result.data.uid,
            token: result.data.authToken,
          }),
          headers: new Headers({
            "Content-type": "application/json; charset=UTF-8",
          }),
        });
      });
  }

  function authLoginUser(token) {
    $("#loading-message-container").show();
    console.log(token);
    CometChat.login(token).then(
      (User) => {
        console.log("Login successfully");
        console.log(token);
        $("#comethat").show();
        // this.getLoggedInUser();
      },
      (error) => {
        alert(
          "Whops. Something went wrong. This commonly happens when you enter a username that doesn't exist. Check the console for more information"
        );
        console.log("Login failed with error:", error);
      }
    );
  }

  function getLoggedInUser() {
    CometChat.getLoggedinUser().then(
      (user) => {
        $("#username").text(user.name);
        $("#loggedInUserAvatar").attr("src", user.avatar);
        $("#loggedInUID").val(user.uid);

        $("#loading-message-container").hide();

        this.fetchMessages();
      },
      (error) => {
        console.log(error);
      }
    );
  }
})();
