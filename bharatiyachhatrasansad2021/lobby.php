<?php
require_once "logincheck.php";
$curr_room = 'lobby';
$curr_session = "Lobby";
$date = getdate();
//var_dump($date);
$day_img = '';
if ($date['mon'] == '9') {
    if ($date['mday'] == '23') {
        $day_img = '23sep.jpg';
    }
    if ($date['mday'] == '24') {
        $day_img = '24sep.jpg';
    }
    if ($date['mday'] == '25') {
        $day_img = '25sep.jpg';
    }
    if ($date['mday'] == '26') {
        $day_img = '26sep.jpg';
    }
    if ($date['mday'] == '27') {
        $day_img = '27sep.jpg';
    }
    if ($date['mday'] == '28') {
        $day_img = '28sep.jpg';
    }
}
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/lobby.jpg">
            <div id="lobbyVideo">
                <iframe src="https://player.vimeo.com/video/608739324?autoplay=1&loop=1&muted=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
            </div>
            <!-- <a href="#" id="lobbyVideo" onclick="javascript:alert('Will Activate Shortly')">
            </a>-->
            <a href="youthconnect.php" id="enterYC">
                <div class="indicator d-6"></div>
            </a>
            <a href="exhibithall.php" id="enterHall">
                <div class="indicator d-6"></div>
            </a>
            <a href="selfiezone.php" id="enterSZ">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" onclick="enterAudi()" id="enterAudi">
                <div class="indicator d-6"></div>
            </a>
            <a href="bcsjourney.php" id="enterBcs">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/Schedule.pdf" data-docid="2651306eff4222195ac248112d34fe7ddfd3b8e685a8b0685c3b1186caadbc95" class="showpdf resdl" id="showAgenda">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-01.jpg" data-docid="1ff2261bbc1d9997d62e4e51d9002383ba41ce8f259a0b18fbab6e24f14170c7" class="view resdl" id="banner01">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-02.jpg" data-docid="b4461de2694ab2221fa3819f633e60531c1ba7f5818327acfcee643954289c72" class="view resdl" id="banner02">
                <div class="indicator d-6"></div>
            </a>
            </a>
            <a href="assets/resources/banner-03.jpg" data-docid="416dd176a7af6b29912541dc66e84940628499940e67427da86cb7fa394ef7ee" class="view resdl" id="banner03">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-04.jpg" data-docid="da4a04edd33cd773243f35fd57b411b5da13846d9bf8d45510012b26a605f5eb" class="view resdl" id="banner04">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-05.jpg" data-docid="54129688187e6595a9dc5eb0e377178c76bd9ade056ac2c99985c380713806e9" class="view resdl" id="banner05">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-06.jpg" data-docid="d6e48e311a00a245f3ae209ec364dd4df0e7eb321856bbcae8d0072b0c1dfc02" class="view resdl" id="banner06">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/banner-07.jpg" data-docid="9615d0a60ebbd7b52326641633afe508aae88ab241c5bddc19442f6ba6de1250" class="view resdl" id="banner07">
                <div class="indicator d-6"></div>
            </a>

            <?php if ($day_img != '') { ?>
                <a href="assets/img/<?= $day_img ?>" data-docid="45ef4356ff6995b4989a9f0f8d6b4de7c0f807bce0464518582371420a372976" class="view resdl" id="banner08">
                    <img src="assets/img/<?= $day_img ?>" alt="">
                <?php } else { ?>
                    <a href="assets/resources/banner-08.jpg" data-docid="45ef4356ff6995b4989a9f0f8d6b4de7c0f807bce0464518582371420a372976" class="view resdl" id="banner08">
                    <?php } ?>
                    <div class="indicator d-6"></div>
                    </a>


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php";            ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<section class="videotoplay" id="gotoaudi" style="display:none;">
    <iframe id="gotoaudivideo" src="https://player.vimeo.com/video/610895828?h=67bba02c3a&autoplay=0&controls=0" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

    <a href="auditorium.php" class="skip">SKIP</a>
</section>

<?php require_once "scripts.php" ?>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>

</script>
<script>
    function enterAudi() {
        $('#content').css('display', 'none');
        $('#gotoaudi').css('display', 'block');
        var iframe = document.querySelector('iframe#gotoaudivideo');
        var player = new Vimeo.Player(iframe);
        player.on('ended', function(data) {
            location.href = "auditorium.php";
        });
        player.play();

    }
</script>

<script>

async function fnAsync() {
await abc();
launchwidget();
}

fnAsync();

</script>

<div id="cometchat"></div>
<script>
     console.log("Before launchwidget function definition");
function launchwidget() {
    console.log("Inside launchwidget()");
// window.addEventListener('DOMContentLoaded', (event) => {
    CometChatWidget.init({
        "appID": "1932479da3e900b5",
        "appRegion": "us",
    }).then(response => {
        console.log("Initialization completed successfully");

    url = `getauthtoken.php`;
      fetch(url, {
        method: "POST",
        body: JSON.stringify({
        username: '',
        }),
        headers: new Headers({
          "Content-type": "application/json; charset=UTF-8",
        }),
      })
    .then((response) => response.json())
    .then((token) => {
      
      console.log(token, "Token received");
   


        //You can now call login function.
        CometChatWidget.login({
            "authToken": token
        }).then(response => {
            CometChatWidget.launch({
                "widgetID": "0b575598-0210-412b-add2-b9e52bca48eb",
                "target": "#cometchat",
                "roundedCorners": "true",
                "height": "600px",
                "width": "800px",
                "defaultID": 'superhero1', //default UID (user) or GUID (group) to show,
                "defaultType": 'user' //user or group
            });
        }, error => {
            console.log("User login failed with error:", error);
            //Check the reason for error and take appropriate action.
        });
     })
    .catch((error) => console.log(error));
    
    
    }, error => {
        console.log("Initialization failed with error:", error);
        //Check the reason for error and take appropriate action.
    });
// });
console.log("Exiting launchwidget()");
}
</script>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>
