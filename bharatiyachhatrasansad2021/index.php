<?php require 'functions.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $event_title ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-0">
                <img src="assets/img/top-banner.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-5 offset-lg-1 p-2 p-lg-4">
                <img src="assets/img/left-banner.png" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-md-7 col-lg-5 p-2 p-lg-4">
                <div class="row">
                    <div class="col-9 col-md-8 ms-auto text-center text-md-right">
                        <img src="assets/img/logo-isp.png" class="img-fluid logo-isp" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 col-md-10 ms-auto">
                        <div class="login-wrapper">
                            <div id="message" class="" role="alert"></div>
                            <form id="form-login" method="post" action="">
                                <div class="mb-3">
                                    <input type="text" class="form-control input" id="userEmail" name="userEmail" placeholder="Email ID">
                                    <span id="email-error" class="text-danger text-error"></span>
                                </div>
                                <input type="image" src="assets/img/btn-login.png" id="btn-login" value="submit" alt="">
                            </form>
                        </div>
                        <div class="text-center mt-2">
                            For assistance: +917314-855-655
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="art-top-right">
        <img src="assets/img/art-top-right.png" class="img-fluid" alt="">
    </div>

    <video autoplay muted loop id="bg-video">
        <source src="assets/videos/bg.mp4" type="video/mp4">
    </video>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#form-login').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: 'submit-login.php',
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#btn-login').attr('disabled', 'disabled');
                    },
                    success: function(data) {
                        $('#btn-login').attr('disabled', false);
                        $('.text-error').text('');
                        $('#message').text('');
                        $('#message').removeClass();
                        if (data.success) {
                            $('#form-login')[0].reset();
                            location.href = 'enter.php';

                        } else {
                            if (data.error) {
                                $('#message').removeClass().addClass('alert alert-danger');
                            }
                            $('#message').text(data.error);
                            $('#email-error').text(data.email_error);
                        }
                    },
                    error: function() {
                        $('#btn-login').attr('disabled', false);
                    }
                });
            })
        })
    </script>
</body>

</html>