!(function (e, t) {
  if ("object" == typeof exports && "object" == typeof module)
    module.exports = t();
  else if ("function" == typeof define && define.amd) define([], t);
  else {
    var n = t();
    for (var o in n) ("object" == typeof exports ? exports : e)[o] = n[o];
  }
})(window, function () {
  return (function (n) {
    var o = {};
    function s(e) {
      if (o[e]) return o[e].exports;
      var t = (o[e] = { i: e, l: !1, exports: {} });
      return n[e].call(t.exports, t, t.exports, s), (t.l = !0), t.exports;
    }
    return (
      (s.m = n),
      (s.c = o),
      (s.d = function (e, t, n) {
        s.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: n });
      }),
      (s.r = function (e) {
        "undefined" != typeof Symbol &&
          Symbol.toStringTag &&
          Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
          Object.defineProperty(e, "__esModule", { value: !0 });
      }),
      (s.t = function (t, e) {
        if ((1 & e && (t = s(t)), 8 & e)) return t;
        if (4 & e && "object" == typeof t && t && t.__esModule) return t;
        var n = Object.create(null);
        if (
          (s.r(n),
          Object.defineProperty(n, "default", { enumerable: !0, value: t }),
          2 & e && "string" != typeof t)
        )
          for (var o in t)
            s.d(
              n,
              o,
              function (e) {
                return t[e];
              }.bind(null, o)
            );
        return n;
      }),
      (s.n = function (e) {
        var t =
          e && e.__esModule
            ? function () {
                return e.default;
              }
            : function () {
                return e;
              };
        return s.d(t, "a", t), t;
      }),
      (s.o = function (e, t) {
        return Object.prototype.hasOwnProperty.call(e, t);
      }),
      (s.p = ""),
      s((s.s = 36))
    );
  })([
    function (e, r, t) {
      "use strict";
      r.__esModule = !0;
      var o = t(12),
        i = t(1),
        s = t(6),
        a = t(2),
        E = t(16),
        c = t(17),
        u = t(19),
        S = t(3),
        n = t(14);
      function l(e) {
        return (
          null != e &&
            ("string" == typeof e && (e = e.trim()),
            "object" == typeof e &&
              0 === Object.keys(e).length &&
              (e = void 0)),
          ["", 0, "0", !1, null, "null", void 0, "undefined"].includes(e)
        );
      }
      function p(e) {
        for (var o = [], t = 1; t < arguments.length; t++)
          o[t - 1] = arguments[t];
        return e.split("%s").reduce(function (e, t, n) {
          return e + t + (o[n] || "");
        }, "");
      }
      (r.getChatHost = function (e) {
        return e[i.APP_SETTINGS.KEYS.CHAT_HOST_OVERRIDE]
          ? e[i.APP_SETTINGS.KEYS.CHAT_HOST_OVERRIDE]
          : e[i.APP_SETTINGS.KEYS.CHAT_HOST_APP_SPECIFIC]
          ? e[i.APP_SETTINGS.KEYS.CHAT_HOST_APP_SPECIFIC]
          : e[i.APP_SETTINGS.KEYS.CHAT_HOST];
      }),
        (r.getJidHost = function (e) {
          return e[i.APP_SETTINGS.KEYS.JID_HOST_OVERRIDE]
            ? e[i.APP_SETTINGS.KEYS.JID_HOST_OVERRIDE]
            : e[i.APP_SETTINGS.KEYS.CHAT_HOST];
        }),
        (r.isObject = function (e) {
          return e instanceof Object && e.constructor === Object;
        }),
        (r.isTruthy = function (e) {
          return [!0, 1, "1", "true", "TRUE"].includes(e);
        }),
        (r.isFalsy = l),
        (r.getOrdinalSuffix = function (e) {
          var t = e % 10,
            n = e % 100;
          return 1 == t && 11 != n
            ? e + "st"
            : 2 == t && 12 != n
            ? e + "nd"
            : 3 == t && 13 != n
            ? e + "rd"
            : e + "th";
        }),
        (r.format = p),
        (r.createUidFromJid = function (e) {
          return e.substring(e.lastIndexOf("]") + 1, e.lastIndexOf("@"));
        });
      var C = (function () {
        function e() {}
        return (
          (e.log = function (e, t) {}),
          (e.error = function (e, t) {}),
          (e.info = function (e, t) {}),
          e
        );
      })();
      function T() {
        return new Promise(function (t, n) {
          s.makeApiCall("appSettings").then(
            function (e) {
              o.LocalStorage.getInstance().set(
                i.LOCAL_STORE.KEY_APP_SETTINGS,
                e.data
              ),
                e.data.MODE && S.CometChat.setMode(e.data.MODE),
                t(e.data);
            },
            function (e) {
              n(new a.CometChatException(e.error));
            }
          );
        });
      }
      (r.Logger = C),
        (r.getCurrentTime = function () {
          return new Date().getTime();
        }),
        (r.getAppSettings = function () {
          return new Promise(function (t, n) {
            o.LocalStorage.getInstance()
              .get(i.LOCAL_STORE.KEY_APP_SETTINGS)
              .then(
                function (e) {
                  l(e)
                    ? T().then(
                        function (e) {
                          t(e);
                        },
                        function (e) {
                          n(e);
                        }
                      )
                    : t(e);
                },
                function (e) {
                  n(e);
                }
              );
          });
        }),
        (r.getUpdatedSettings = T),
        (r.isImage = function (e) {
          var t;
          return (
            e.type && e.type.toLowerCase().includes("image") && (t = !0), t
          );
        }),
        (r.isVideo = function (e) {
          var t;
          return (
            e.type && e.type.toLowerCase().includes("video") && (t = !0), t
          );
        }),
        (r.isAudio = function (e) {
          var t;
          return (
            e.type && e.type.toLowerCase().includes("audio") && (t = !0), t
          );
        }),
        (r.validateScope = function (e) {
          return typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "SCOPE",
                    "SCOPE",
                    "Scope"
                  )
                )
              )
            : l(e)
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID),
                    "SCOPE",
                    "SCOPE",
                    "scope",
                    "scope"
                  )
                )
              )
            : e != S.CometChat.GROUP_MEMBER_SCOPE.ADMIN &&
              e != S.CometChat.GROUP_MEMBER_SCOPE.MODERATOR &&
              e != S.CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT
            ? new a.CometChatException(i.GroupErrors.INVALID_SCOPE)
            : void 0;
        }),
        (r.validateUpdateGroup = function (e) {
          return e.hasOwnProperty(i.GroupConstants.KEYS.GUID)
            ? typeof e[i.GroupConstants.KEYS.GUID] !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                      "GUID",
                      "GUID",
                      "GUID"
                    )
                  )
                )
              : l(e[i.GroupConstants.KEYS.GUID])
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                      "GUID",
                      "GUID",
                      "GUID",
                      "GUID"
                    )
                  )
                )
              : e.hasOwnProperty(i.GroupConstants.KEYS.NAME) &&
                "" === e[i.GroupConstants.KEYS.NAME]
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                      "GROUP_NAME",
                      "GROUP_NAME",
                      "Group name"
                    )
                  )
                )
              : void 0
            : new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                    "GUID",
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
        }),
        (r.validateJoinGroup = function (e, t, n) {
          if (typeof e == i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.OBJECT) {
            if (!e.hasOwnProperty(i.GroupConstants.KEYS.GUID))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                    "GUID",
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
            if (
              typeof e[i.GroupConstants.KEYS.GUID] !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            )
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
            if (l(e[i.GroupConstants.KEYS.GUID]))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                    "GUID",
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
            if (!e.hasOwnProperty(i.GroupConstants.KEYS.TYPE))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                    "GROUP_TYPE",
                    "GROUP_TYPE",
                    "Group type",
                    "Group type"
                  )
                )
              );
            if (
              typeof e[i.GroupConstants.KEYS.TYPE] !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            )
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "GROUP_TYPE",
                    "GROUP_TYPE",
                    "Group type"
                  )
                )
              );
            if (l(e[i.GroupConstants.KEYS.TYPE]))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                    "GROUP_TYPE",
                    "GROUP_TYPE",
                    "type",
                    "type"
                  )
                )
              );
            if (
              e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
                S.CometChat.GROUP_TYPE.PUBLIC &&
              e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
                S.CometChat.GROUP_TYPE.PASSWORD &&
              e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
                S.CometChat.GROUP_TYPE.PROTECTED &&
              e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
                S.CometChat.GROUP_TYPE.PRIVATE
            )
              return new a.CometChatException(i.GroupErrors.INVALID_GROUP_TYPE);
            if (
              e[i.GroupConstants.KEYS.TYPE].toLowerCase() ==
              S.CometChat.GROUP_TYPE.PASSWORD
            ) {
              if (!e.hasOwnProperty(i.GroupConstants.KEYS.PASSWORD))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.PASSWORD_COMPULSORY),
                      "PASSWORD",
                      "PASSWORD"
                    )
                  )
                );
              if (
                typeof e[i.GroupConstants.KEYS.PASSWORD] !==
                i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
              )
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                      "PASSWORD",
                      "PASSWORD",
                      "Password"
                    )
                  )
                );
              if (l(e[i.GroupConstants.KEYS.PASSWORD]))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                      "PASSWORD",
                      "PASSWORD",
                      "password",
                      "password"
                    )
                  )
                );
            }
          } else {
            if (void 0 !== e) {
              if (typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING)
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                      "GUID",
                      "GUID",
                      "GUID"
                    )
                  )
                );
              if (l(e))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                      "GUID",
                      "GUID",
                      "GUID",
                      "GUID"
                    )
                  )
                );
            }
            if (void 0 !== t) {
              if (typeof t !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING)
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                      "GROUP_TYPE",
                      "GROUP_TYPE",
                      "Group type"
                    )
                  )
                );
              if (l(t))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                      "GROUP_TYPE",
                      "GROUP_TYPE",
                      "type",
                      "type"
                    )
                  )
                );
              if (
                t.toLowerCase() != S.CometChat.GROUP_TYPE.PUBLIC &&
                t.toLowerCase() != S.CometChat.GROUP_TYPE.PASSWORD &&
                t.toLowerCase() != S.CometChat.GROUP_TYPE.PROTECTED &&
                t.toLowerCase() != S.CometChat.GROUP_TYPE.PRIVATE
              )
                return new a.CometChatException(
                  i.GroupErrors.INVALID_GROUP_TYPE
                );
              if (t.toLowerCase() == S.CometChat.GROUP_TYPE.PASSWORD) {
                if (
                  typeof n !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new a.CometChatException(
                    JSON.parse(
                      p(
                        JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                        "PASSWORD",
                        "PASSWORD",
                        "Password"
                      )
                    )
                  );
                if (l(n))
                  return new a.CometChatException(
                    JSON.parse(
                      p(
                        JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                        "PASSWORD",
                        "PASSWORD",
                        "password",
                        "password"
                      )
                    )
                  );
              }
            }
          }
        }),
        (r.validateCreateGroup = function (e) {
          if (!e.hasOwnProperty(i.GroupConstants.KEYS.GUID))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                  "GUID",
                  "GUID",
                  "GUID",
                  "GUID"
                )
              )
            );
          if (
            typeof e[i.GroupConstants.KEYS.GUID] !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "GUID",
                  "GUID",
                  "GUID"
                )
              )
            );
          if (l(e[i.GroupConstants.KEYS.GUID]))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                  "GUID",
                  "GUID",
                  "GUID",
                  "GUID"
                )
              )
            );
          if (!e.hasOwnProperty(i.GroupConstants.KEYS.NAME))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                  "GROUP_NAME",
                  "GROUP_NAME",
                  "Group name",
                  "Group name"
                )
              )
            );
          if (
            typeof e[i.GroupConstants.KEYS.NAME] !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "GROUP_NAME",
                  "GROUP_NAME",
                  "Group name"
                )
              )
            );
          if (l(e[i.GroupConstants.KEYS.NAME]))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                  "GROUP_NAME",
                  "GROUP_NAME",
                  "name",
                  "name"
                )
              )
            );
          if (!e.hasOwnProperty(i.GroupConstants.KEYS.TYPE))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                  "GROUP_TYPE",
                  "GROUP_TYPE",
                  "Group type",
                  "Group type"
                )
              )
            );
          if (
            typeof e[i.GroupConstants.KEYS.TYPE] !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "GROUP_TYPE",
                  "GROUP_TYPE",
                  "Group type"
                )
              )
            );
          if (l(e[i.GroupConstants.KEYS.TYPE]))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                  "GROUP_TYPE",
                  "GROUP_TYPE",
                  "type",
                  "type"
                )
              )
            );
          if (
            e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
              S.CometChat.GROUP_TYPE.PUBLIC &&
            e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
              S.CometChat.GROUP_TYPE.PASSWORD &&
            e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
              S.CometChat.GROUP_TYPE.PROTECTED &&
            e[i.GroupConstants.KEYS.TYPE].toLowerCase() !=
              S.CometChat.GROUP_TYPE.PRIVATE
          )
            return new a.CometChatException(i.GroupErrors.INVALID_GROUP_TYPE);
          if (
            e[i.GroupConstants.KEYS.TYPE].toLowerCase() ==
            S.CometChat.GROUP_TYPE.PASSWORD
          ) {
            if (!e.hasOwnProperty(i.GroupConstants.KEYS.PASSWORD))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.PASSWORD_COMPULSORY),
                    "PASSWORD",
                    "PASSWORD"
                  )
                )
              );
            if (
              typeof e[i.GroupConstants.KEYS.PASSWORD] !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            )
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "PASSWORD",
                    "PASSWORD",
                    "Password"
                  )
                )
              );
            if (l(e[i.GroupConstants.KEYS.PASSWORD]))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID_GROUP_PROPERTY),
                    "PASSWORD",
                    "PASSWORD",
                    "password",
                    "password"
                  )
                )
              );
          }
          if (e.hasOwnProperty(i.GroupConstants.KEYS.TAGS)) {
            if (!Array.isArray(e[i.GroupConstants.KEYS.TAGS]))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_AN_ARRAY),
                    "GROUP_TAGS",
                    "GROUP_TAGS",
                    "Group tags"
                  )
                )
              );
            if (0 === e[i.GroupConstants.KEYS.TAGS].length)
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.EMPTY_ARRAY),
                    "GROUP_TAGS",
                    "GROUP_TAGS",
                    "Group tags"
                  )
                )
              );
          }
        }),
        (r.validateId = function (e, t) {
          if ("user" === t) {
            if (typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING)
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "UID",
                    "UID",
                    "UID"
                  )
                )
              );
            if (l(e))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID),
                    "UID",
                    "UID",
                    "UID",
                    "UID"
                  )
                )
              );
          }
          if ("group" === t) {
            if (typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING)
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
            if (l(e))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID),
                    "GUID",
                    "GUID",
                    "GUID",
                    "GUID"
                  )
                )
              );
          }
        }),
        (r.validateHideMessagesFromBlockedUsers = function (e) {
          if (typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN)
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_BOOLEAN),
                  "HIDE_MESSAGES_FROM_BLOCKED_USERS",
                  "HIDE_MESSAGES_FROM_BLOCKED_USERS",
                  "hideMessagesFromBlockedUsers"
                )
              )
            );
        }),
        (r.validateArray = function (e, t) {
          var n = "List should be an array.";
          return (
            (n =
              "blockUsers" === t
                ? "blockUsers() method accepts an array of users."
                : "unblockUsers" === t
                ? "unblockUsers() method accepts an array of users."
                : "groupMembers" === t
                ? "addMembersToGroup() method accepts members list as an array of users."
                : "addMembersToGroup() method accepts bannedMembers list as an array of users."),
            typeof e != i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.OBJECT
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_ARRAY),
                      "USER_LIST",
                      "USER_LIST",
                      n
                    )
                  )
                )
              : Array.isArray(e)
              ? void 0
              : new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_ARRAY),
                      "USER_LIST",
                      "USER_LIST",
                      n
                    )
                  )
                )
          );
        }),
        (r.validateMsgId = function (e) {
          return isNaN(e)
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_NUMBER),
                    "MESSAGE_ID",
                    "MESSAGE_ID",
                    "Message Id"
                  )
                )
              )
            : l(e)
            ? new a.CometChatException(n.ERRORS.PARAMETER_MISSING)
            : void 0;
        }),
        (r.validateChatType = function (e) {
          return typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "RECEIVER_TYPE",
                    "RECEIVER_TYPE",
                    "Receiver type"
                  )
                )
              )
            : l(e)
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID),
                    "RECEIVER_TYPE",
                    "RECEIVER_TYPE",
                    "receiver type",
                    "receiver type"
                  )
                )
              )
            : e != i.MessageConstatnts.RECEIVER_TYPE.GROUP &&
              e != i.MessageConstatnts.RECEIVER_TYPE.USER
            ? new a.CometChatException(i.MessageErrors.INVALID_RECEIVER_TYPE)
            : void 0;
        }),
        (r.validateMessage = function (e) {
          var t = e;
          if (
            typeof t.getReceiverId() !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "RECEIVER_ID",
                  "RECEIVER_ID",
                  "Receiver Id"
                )
              )
            );
          if (l(t.getReceiverId()))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID),
                  "RECEIVER_ID",
                  "RECEIVER_ID",
                  "receiver id",
                  "receiver id"
                )
              )
            );
          if (
            typeof t.getReceiverType() !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "RECEIVER_TYPE",
                  "RECEIVER_TYPE",
                  "Receiver Type"
                )
              )
            );
          if (l(t.getReceiverType()))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID),
                  "RECEIVER_TYPE",
                  "RECEIVER_TYPE",
                  "receiver type",
                  "receiver type"
                )
              )
            );
          if (
            t.getReceiverType() != i.MessageConstatnts.RECEIVER_TYPE.GROUP &&
            t.getReceiverType() != i.MessageConstatnts.RECEIVER_TYPE.USER
          )
            return new a.CometChatException(
              i.MessageErrors.INVALID_RECEIVER_TYPE
            );
          if (e instanceof E.TextMessage) {
            var n = e;
            if (
              typeof n.getText() !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            )
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "MESSAGE_TEXT",
                    "MESSAGE_TEXT",
                    "Message text"
                  )
                )
              );
            if ("" === n.getText().trim())
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                    "MESSAGE_TEXT",
                    "MESSAGE_TEXT",
                    "Message text"
                  )
                )
              );
          }
          if (e instanceof c.MediaMessage) {
            var o = e;
            if (o.getData() && o.getData().hasOwnProperty("attachments")) {
              var s = o.getAttachment();
              if (!s.getExtension())
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MISSING_KEY),
                      "extension",
                      "Attachment"
                    )
                  )
                );
              if (!s.getMimeType())
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MISSING_KEY),
                      "mimeType",
                      "Attachment"
                    )
                  )
                );
              if (!s.getName())
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MISSING_KEY),
                      "name",
                      "Attachment"
                    )
                  )
                );
              if (!s.getUrl())
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MISSING_KEY),
                      "url",
                      "Attachment"
                    )
                  )
                );
            } else {
              if (!(o.file instanceof Blob))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_BLOB),
                      "MEDIA_OBJECT",
                      "MEDIA_OBJECT",
                      "Media object"
                    )
                  )
                );
              if ("image" == o.getType()) {
                if (!r.isImage(o.file))
                  return new a.CometChatException(
                    JSON.parse(
                      p(
                        JSON.stringify(i.GENERAL_ERROR.INVALID_MEDIA_FILE),
                        "IMAGE_FILE",
                        "IMAGE_FILE"
                      )
                    )
                  );
              } else if ("video" == o.getType()) {
                if (!r.isVideo(o.file))
                  return new a.CometChatException(
                    JSON.parse(
                      p(
                        JSON.stringify(i.GENERAL_ERROR.INVALID_MEDIA_FILE),
                        "VIDEO_FILE",
                        "VIDEO_FILE"
                      )
                    )
                  );
              } else if ("audio" == o.getType() && !r.isAudio(o.file))
                return new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_MEDIA_FILE),
                      "AUDIO_FILE",
                      "AUDIO_FILE"
                    )
                  )
                );
            }
          }
          if (e instanceof u.CustomMessage && l(e.getCustomData()))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID),
                  "CUSTOM_DATA",
                  "CUSTOM_DATA",
                  "custom data",
                  "custom data"
                )
              )
            );
        }),
        (r.validateCreateUser = function (e) {
          if (!e.hasOwnProperty(i.UserConstants.UID))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                  "UID",
                  "UID",
                  "UID",
                  "UID"
                )
              )
            );
          if (
            typeof e[i.UserConstants.UID] !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "UID",
                  "UID",
                  "UID"
                )
              )
            );
          if (l(e[i.UserConstants.UID]))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID_USER_PROPERTY),
                  "UID",
                  "UID",
                  "UID",
                  "UID"
                )
              )
            );
          if (!e.hasOwnProperty(i.UserConstants.NAME))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                  "USER_NAME",
                  "USER_NAME",
                  "User name",
                  "User name"
                )
              )
            );
          if (
            typeof e[i.UserConstants.NAME] !==
            i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                  "USER_NAME",
                  "USER_NAME",
                  "User name"
                )
              )
            );
          if (l(e[i.UserConstants.NAME]))
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.INVALID_USER_PROPERTY),
                  "USER_NAME",
                  "USER_NAME",
                  "name",
                  "name"
                )
              )
            );
          if (
            e.hasOwnProperty(i.UserConstants.AVATAR) &&
            "" === e[i.UserConstants.AVATAR]
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                  "USER_AVATAR",
                  "USER_AVATAR",
                  "User avatar"
                )
              )
            );
          if (
            e.hasOwnProperty(i.UserConstants.META_DATA) &&
            "" === e[i.UserConstants.META_DATA]
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                  "USER_METADATA",
                  "USER_METADATA",
                  "User metadata"
                )
              )
            );
          if (
            e.hasOwnProperty(i.UserConstants.LINK) &&
            "" === e[i.UserConstants.LINK]
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                  "USER_LINK",
                  "USER_LINK",
                  "User link"
                )
              )
            );
          if (
            e.hasOwnProperty(i.UserConstants.STATUS_MESSAGE) &&
            "" === e[i.UserConstants.STATUS_MESSAGE]
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                  "USER_STATUS_MESSAGE",
                  "USER_STATUS_MESSAGE",
                  "User status message"
                )
              )
            );
          if (
            e.hasOwnProperty(i.UserConstants.ROLE) &&
            "" === e[i.UserConstants.ROLE]
          )
            return new a.CometChatException(
              JSON.parse(
                p(
                  JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                  "USER_ROLE",
                  "USER_ROLE",
                  "User role"
                )
              )
            );
          if (e.hasOwnProperty(i.UserConstants.TAGS)) {
            if (!Array.isArray(e[i.UserConstants.TAGS]))
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_AN_ARRAY),
                    "USER_TAGS",
                    "USER_TAGS",
                    "User tags"
                  )
                )
              );
            if (0 === e[i.UserConstants.TAGS].length)
              return new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.EMPTY_ARRAY),
                    "USER_TAGS",
                    "USER_TAGS",
                    "User tags"
                  )
                )
              );
          }
        }),
        (r.validateUpdateUser = function (e) {
          return e.hasOwnProperty(i.UserConstants.UID)
            ? typeof e[i.UserConstants.UID] !==
              i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                      "UID",
                      "UID",
                      "UID"
                    )
                  )
                )
              : l(e[i.UserConstants.UID])
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.INVALID_USER_PROPERTY),
                      "UID",
                      "UID",
                      "UID",
                      "UID"
                    )
                  )
                )
              : e.hasOwnProperty(i.UserConstants.NAME) &&
                "" === e[i.UserConstants.NAME]
              ? new a.CometChatException(
                  JSON.parse(
                    p(
                      JSON.stringify(i.GENERAL_ERROR.EMPTY_STRING),
                      "USER_NAME",
                      "USER_NAME",
                      "User name"
                    )
                  )
                )
              : void 0
            : new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.PARAMETER_COMPULSORY),
                    "UID",
                    "UID",
                    "UID",
                    "UID"
                  )
                )
              );
        }),
        (r.validateConversationType = function (e) {
          return typeof e !== i.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.MUST_BE_A_STRING),
                    "CONVERSATION_TYPE",
                    "CONVERSATION_TYPE",
                    "Conversation type"
                  )
                )
              )
            : l(e)
            ? new a.CometChatException(
                JSON.parse(
                  p(
                    JSON.stringify(i.GENERAL_ERROR.INVALID),
                    "CONVERSATION_TYPE",
                    "CONVERSATION_TYPE",
                    "conversation type",
                    "conversation type"
                  )
                )
              )
            : (e = e.toLowerCase()) != S.CometChat.RECEIVER_TYPE.USER &&
              e != S.CometChat.RECEIVER_TYPE.GROUP
            ? new a.CometChatException(
                i.ConversationErrors.INVALID_CONVERSATION_TYPE
              )
            : void 0;
        });
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o,
        s,
        r,
        i = n(2);
      (t.constants = {
        DEFAULT_STORE: "cometchat",
        MSG_VER_PRE: "store-ver-pre",
        MSG_VER_POST: "store-ver-post",
      }),
        (t.DEFAULT_VALUES = {
          ZERO: 0,
          MSGS_LIMIT: 30,
          MSGS_MAX_LIMIT: 100,
          USERS_LIMIT: 30,
          USERS_MAX_LIMIT: 100,
          GROUPS_LIMIT: 30,
          GROUPS_MAX_LIMIT: 100,
          CONVERSATION_MAX_LIMIT: 50,
          CALL_TIMEOUT: 45,
          DEFAULT_MSG_ID: 0,
          DEFAULT_MAX_TYPING_INDICATOR_LIMIT: 5,
          REGION_DEFAULT: "eu",
          REGION_DEFAULT_EU: "eu",
          REGION_DEFAULT_US: "us",
          REGION_DEFAULT_IN: "in",
          REGION_DEFAULT_PRIVATE: "private",
        }),
        (t.CALLING_COMPONENT_VERSION = 4),
        ((o = t.GroupType || (t.GroupType = {})).Public = "public"),
        (o.Private = "private"),
        (o.Protected = "protected"),
        (o.Password = "password"),
        (t.GROUP_TYPE = {
          PUBLIC: "public",
          PRIVATE: "private",
          PROTECTED: "password",
          PASSWORD: "password",
        }),
        ((s = t.GroupMemberScope || (t.GroupMemberScope = {})).Admin = "admin"),
        (s.Moderator = "moderator"),
        (s.Member = "member"),
        (t.GROUP_MEMBER_SCOPE = {
          ADMIN: "admin",
          MODERATOR: "moderator",
          PARTICIPANT: "participant",
        }),
        (t.APPINFO = {
          platform: "WEB",
          sdkVersion: "v3.0.3",
          apiVersion: "v3.0",
          sdkVersionWithUnderScore: "3_0_3",
        }),
        (t.SDKHeader = {
          platform: "javascript",
          sdkVersion: "3.0.3",
          sdk: "%s@%s",
        }),
        (t.WS = {
          CONVERSATION: { TYPE: { CHAT: "chat", GROUP_CHAT: "groupchat" } },
        }),
        (t.ANALYTICS = {
          analyticsHost: "metrics-%s.cometchat.io",
          analyticsVersion: "v1",
        }),
        (t.LOCAL_STORE = {
          COMMON_STORE: "common_store",
          MESSAGE_LISTENERS_LIST: "message_listeners_list",
          USERS_STORE: "users_store",
          MESSAGES_STORE: "messages_store",
          KEYS_STORE: "keys_store",
          STORE_STRING: "%s:%s",
          KEY_STRING: "%s/%s",
          KEY_USER: "user",
          KEY_APP_SETTINGS: "app_settings",
          KEY_APP_ID: "appId",
          KEY_DEVICE_ID: "deviceId",
          KEY_MESSAGE_LISTENER_LIST: "all",
        }),
        (t.ResponseConstants = {
          RESPONSE_KEYS: {
            KEY_DATA: "data",
            KEY_META: "meta",
            KEY_CURSOR: "cursor",
            KEY_ACTION: "action",
            KEY_MESSAGE: "message",
            KEY_ERROR: "error",
            KEY_ERROR_DETAILS: "details",
            KEY_ERROR_CODE: "code",
            KEY_ERROR_MESSAGE: "message",
            KEY_AUTH_TOKEN: "authToken",
            KEY_WS_CHANNEL: "wsChannel",
            KEY_IDENTITY: "identity",
            KEY_SERVICE: "identity",
            KEY_ENTITIES: "entities",
            KEY_ENTITITY: "entity",
            KEY_ENTITYTYPE: "entityType",
            KEY_ATTACHMENTS: "attachments",
            CODE_REQUEST_OK: 200,
            CODE_BAD_REQUEST: 401,
            UNREAD_UNDELIVERED_KEYS: {
              ENTITY: "entity",
              ENTITY_TYPE: "entityType",
              ENTITY_Id: "entityId",
              COUNT: "count",
            },
            GROUP_MEMBERS_RESPONSE: {
              SUCCESS: "success",
              ERROR: "error",
              MESSAGE: "message",
            },
            KEY_ENTITY_TYPE: { USER: "user", GROUP: "group" },
          },
        }),
        (t.DELIVERY_RECEIPTS = {
          RECEIVER_ID: "receiverId",
          RECEIVER_TYPE: "type",
          RECIPIENT: "recipient",
          MESSAGE_ID: "messageId",
          RECEIVED: "delivered",
          DELIVERED_AT: "deliveredAt",
          ID: "id",
          TIME: "time",
          DELIVERED_TO_ME_AT: "deliveredToMeAt",
        }),
        (t.READ_RECEIPTS = {
          RECEIVER_ID: "receiverId",
          RECEIVER_TYPE: "type",
          RECIPIENT: "recipient",
          MESSAGE_ID: "messageId",
          READ: "read",
          READ_AT: "readAt",
          ID: "id",
          TIME: "time",
          READ_BY_ME_AT: "readByMeAt",
        }),
        (t.MessageConstatnts = {
          TYPE: {
            TEXT: "text",
            MEDIA: "media",
            IMAGE: "image",
            VIDEO: "video",
            AUDIO: "audio",
            FILE: "file",
            CUSTOM: "custom",
          },
          CATEGORY: {
            MESSAGE: "message",
            ACTION: "action",
            CALL: "call",
            CUSTOM: "custom",
          },
          RECEIVER_TYPE: { USER: "user", GROUP: "group" },
          KEYS: {
            ATTATCHMENT: "attatchment",
            ATTATCHMENTS: "attachments",
            ACTION: "action",
            TYPE: "type",
            DATA: "data",
            ID: "id",
            MUID: "muid",
            SENDER: "sender",
            RECEIVER: "receiver",
            RECEIVER_ID: "receiverId",
            CATEGORY: "category",
            RECEIVER_TYPE: "receiverType",
            SENT_AT: "sentAt",
            STATUS: "status",
            TEXT: "text",
            URL: "url",
            METADATA: "metadata",
            RECEIPTS: "receipts",
            MY_RECEIPTS: "myReceipt",
            CUSTOM_DATA: "customData",
            CUSTOM_SUB_TYPE: "subType",
            RESOURCE: "resource",
          },
          KNOWN_MEDIA_TYPE: { IMAGE: [], VIDEO: [], AUDIO: [], FILE: [] },
          PAGINATION: {
            AFFIX: { APPEND: "append", PREPEND: "prepend" },
            CURSOR_FILEDS: { ID: "id", SENT_AT: "sentAt" },
            CURSOR_AFFIX_DEFAULT: "prepend",
            CURSOR_FIELD_DEFAULT: "sentAt",
            KEYS: {
              PER_PAGE: "per_page",
              CURSOR_AFFIX: "cursorAffix",
              AFFIX: "affix",
              CURSOR_FIELD: "cursorField",
              CURSOR_VALUE: "cursorValue",
              UID: "uid",
              SENT_AT: "sentAt",
              ID: "id",
              CURRENT_PAGE: "page",
              UNREAD: "unread",
              HIDE_MESSAGES_FROM_BLOCKED_USER: "hideMessagesFromBlockedUsers",
              SEARCH_KEY: "searchKey",
              ONLY_UPDATES: "onlyUpdates",
              UPDATED_AT: "updatedAt",
              CATEGORY: "category",
              CATEGORIES: "categories",
              TYPE: "type",
              TYPES: "types",
              HIDE_REPLIES: "hideReplies",
              HIDE_DELETED_MESSAGES: "hideDeleted",
            },
          },
        }),
        (t.ATTACHMENTS_CONSTANTS = {
          KEYS: {
            EXTENSION: "extension",
            MIME_TYPE: "mimeType",
            NAME: "name",
            SIZE: "size",
            URL: "url",
          },
        }),
        ((r = t.MessageCategory || (t.MessageCategory = {})).ACTION = "action"),
        (r.MESSAGE = "message"),
        (r.CALL = "call"),
        (r.CUSTOM = "custom"),
        (t.TYPING_NOTIFICATION = {
          RECEIVER_ID: "receiverId",
          RECEIVER_TYPE: "receiverType",
          META: "metadata",
          KEYS: {
            TYPING_NOTIFICATION: "typingNotification",
            TIMESTAMP: "timestamp",
          },
          ACTIONS: { STARTED: "started", ENDED: "ended" },
        }),
        (t.ActionConstatnts = {
          ACTION_SUBJECTS: {
            ACTION_ON: "on",
            ACTION_BY: "by",
            ACTION_FOR: "for",
          },
          ACTION_ENTITY_TYPE: {
            GROUP_USER: "groupuser",
            USER: "user",
            GROUP: "group",
            MESSAGE: "message",
          },
          ACTION_KEYS: {
            ACTION_CREATED: "created",
            ACTION_UPDATED: "updated",
            ACTION_DELETED: "deleted",
            ENTITIES: "entities",
            ENTITY: "entity",
            ENTITY_TYPE: "entityType",
            TYPE_MEMBER_JOINED: "joined",
            TYPE_MEMBER_LEFT: "left",
            TYPE_MEMBER_KICKED: "kicked",
            TYPE_MEMBER_BANNED: "banned",
            TYPE_MEMBER_UNBANNED: "unbanned",
            TYPE_MEMBER_INVITED: "invited",
            TYPE_MEMBER_ADDED: "added",
            ACTION_SCOPE_CHANGED: "scopeChanged",
            ACTION_TYPE_USER: "user",
            ACTION_TYPE_GROUP: "group",
            ACTION_TYPE_GROUP_MEMBER: "groupMember",
            TYPE_MESSAGE_EDITED: "edited",
            TYPE_MESSAGE_DELETED: "deleted",
            ACTION_TYPE_CALL: "call",
            EXTRAS: "extras",
            SCOPE: "scope",
            NEW: "new",
            OLD: "old",
          },
          ActionMessages: {
            ACTION_GROUP_JOINED_MESSAGE: "%s joined",
            ACTION_GROUP_LEFT_MESSAGE: "%s left",
            ACTION_MEMBER_KICKED_MESSAGE: "%s kicked %s",
            ACTION_MEMBER_BANNED_MESSAGE: "%s banned %s",
            ACTION_MEMBER_UNBANNED_MESSAGE: "%s unbanned %s",
            ACTION_MEMBER_INVITED_MESSAGE: "%s banned %s",
            ACTION_MESSAGE_EDITED_MESSAGE: " Message Edited",
            ACTION_MESSAGE_DELETED_MESSAGE: "Message Deleted",
            ACTION_MEMBER_SCOPE_CHANGED: "%s made %s %s",
            ACTION_MEMBER_ADDED_TO_GROUP: "%s added %s",
          },
          ACTION_TYPE: {
            TYPE_MEMBER_JOINED: "joined",
            TYPE_MEMBER_LEFT: "left",
            TYPE_MEMBER_KICKED: "kicked",
            TYPE_MEMBER_BANNED: "banned",
            TYPE_MEMBER_UNBANNED: "unbanned",
            TYPE_MEMBER_INVITED: "invited",
            TYPE_MEMBER_SCOPE_CHANGED: "scopeChanged",
            TYPE_MESSAGE: "message",
            TYPE_MESSAGE_EDITED: "edited",
            TYPE_MESSAGE_DELETED: "deleted",
            TYPE_MEMBER_ADDED: "added",
          },
          ACTIONS: {
            MEMBER_ADDED: "added",
            MEMBER_JOINED: "joined",
            MEMBER_LEFT: "left",
            MEMBER_KICKED: "kicked",
            MEMBER_BANNED: "banned",
            MEMBER_UNBANNED: "unbanned",
            MEMBER_INVITED: "invited",
            MEMBER_SCOPE_CHANGED: "scopeChanged",
            MESSAGE_EDITED: "edited",
            MESSSAGE_DELETED: "deleted",
            TYPE_USER: "user",
            TYPE_GROUP: "group",
            TYPE_GROUP_MEMBER: "groupMember",
          },
        }),
        (t.BlockedUsersConstants = {
          REQUEST_KEYS: {
            DIRECTIONS: {
              BOTH: "both",
              HAS_BLOCKED_ME: "hasBlockedMe",
              BLOCKED_BY_ME: "blockedByMe",
            },
          },
        }),
        (t.CallConstants = {
          CALL_MODE: {
            DEFAULT: "DEFAULT",
            SPOTLIGHT: "SPOTLIGHT",
            SINGLE: "SINGLE",
            TILE: "TILE",
            GRID: "GRID",
          },
          CALL_TYPE: { AUDIO: "audio", VIDEO: "video" },
          RECEIVER_TYPE_GROUP: "group",
          RECEIVER_TYPE_USER: "user",
          CALL_KEYS: {
            CALL_DATA: "data",
            CALL_ID: "id",
            CALL_SESSION_ID: "sessionid",
            CALL_RECEIVER: "receiver",
            CALL_SENDER: "sender",
            CALL_RECEIVER_TYPE: "receiverType",
            CALL_STATUS: "status",
            CALL_TYPE: "type",
            CALL_INITIATED_AT: "initiatedAt",
            CALL_JOINED_AT: "joinedAt",
            CALL_LEFT_AT: "leftAt",
            CALL_METADATA: "metadata",
            CALL_ENTITIES: "entities",
            CALL_ENTITY_TYPE: "entityType",
            CALL_ENTITY: "entity",
            CALL_ENTITY_USER: "user",
            CALL_ENTITY_GROUP: "group",
          },
          CALL_STATUS: {
            INITIATED: "initiated",
            ONGOING: "ongoing",
            UNANSWERED: "unanswered",
            REJECTED: "rejected",
            BUSY: "busy",
            CANCELLED: "cancelled",
            ENDED: "ended",
          },
          AUDIO_INPUT_DEVICES: "audioInputDevices",
          AUDIO_OUTPUT_DEVICES: "audioOutputDevices",
          VIDEO_INPUT_DEVICES: "videoInputDevices",
          POST_MESSAGES: {
            TYPES: {
              ACTION_MESSAGE: "cometchat_action_message",
              HANGUP: "hangup",
              COMETCHAT_RTC_SETTINGS: "cometchat_rtc_settings",
            },
            ACTIONS: {
              USER_JOINED: "onUserJoined",
              USER_LEFT: "onUserLeft",
              USER_LIST_CHANGED: "onUserListChanged",
              INITIAL_DEVICE_LIST: "initialDeviceList",
              DEVICE_CHANGE: "onDeviceChange",
              LOAD: "LOAD",
              CHANGE_AUDIO_INPUT: "changeAudioInput",
              CHANGE_AUDIO_OUTPUT: "changeAudioOutput",
              CHANGE_VIDEO_INPUT: "changeVideoInput",
              MUTE_AUDIO: "muteAudio",
              UNMUTE_AUDIO: "unMuteAudio",
              PAUSE_VIDEO: "pauseVideo",
              UNPAUSE_VIDEO: "unPauseVideo",
              SWITCH_MODE: "switchMode",
              START_SCREENSHARE: "startScreenShare",
              STOP_SCREENSHARE: "stopScreenShare",
              END_CALL: "endCall",
              START_RECORDING: "startRecording",
              STOP_RECORDING: "stopRecording",
              RECORDING_TOGGLED: "onRecordingToggled",
              USER_MUTED: "onUserMuted",
              SCREEN_SHARE_STARTED: "SCREEN_SHARE_STARTED",
              SCREEN_SHARE_STOPPED: "SCREEN_SHARE_ENDED",
            },
          },
          MEDIA_DEVICE: { ID: "id", NAME: "name", ACTIVE: "active" },
        }),
        (t.GroupConstants = {
          KEYS: {
            NAME: "name",
            GUID: "guid",
            TYPE: "type",
            PASSWORD: "password",
            ICON: "icon",
            DESCRIPTION: "description",
            OWNER: "owner",
            METADATA: "metadata",
            CREATED_AT: "createdAt",
            UPDATED_AT: "updatedAt",
            HAS_JOINED: "hasJoined",
            WS_CHANNEL: "wsChannel",
            TAGS: "tags",
          },
        }),
        (t.GroupMemersConstans = {
          KEYS: {
            SCOPE: "scope",
            UID: "uid",
            GUID: "guid",
            USER: "user",
            NAME: "name",
          },
        }),
        (t.UserConstants = {
          UID: "uid",
          NAME: "name",
          AUTH_TOKEN: "authToken",
          AVATAR: "avatar",
          LAST_ACTIVE_AT: "lastActiveAt",
          LINK: "link",
          META_DATA: "metadata",
          ROLE: "role",
          STATUS: "status",
          STATUS_MESSAGE: "statusMessage",
          USER_NAME: "user_name",
          TAGS: "tags",
        }),
        (t.Errors = {
          ERROR_IO_EXCEPTION: "ERROR_IO_EXCEPTION",
          ERROR_JSON_EXCEPTION: "ERROR_JSON_EXCEPTION",
          ERROR_PASSWORD_MISSING: "ERROR_PASSWORD_MISSING",
          ERROR_LIMIT_EXCEEDED: "ERROR_LIMIT_EXCEEDED",
          ERROR_USER_NOT_LOGGED_IN: "ERROR_USER_NOT_LOGGED_IN",
          ERROR_INVALID_GUID: "ERROR_INVALID_GUID",
          ERROR_PASSWORD_MISSING_MESSAGE:
            "Password is mandatory for a password group",
          ERROR_LIMIT_EXCEEDED_MESSAGE: "Limit Exceeded Max limit of %s",
          ERROR_USER_NOT_LOGGED_IN_MESSAGE:
            "Please log in to CometChat before calling this method",
          ERROR_INVALID_GUID_MESSAGE: "Please provide a valid GUID",
          ERROR_DEFAULT_MESSAGE: "Something went wrong",
          ERR_SETTINGS_HASH_OUTDATED: "ERR_SETTINGS_HASH_OUTDATED",
          ERR_NO_AUTH: "ERR_NO_AUTH",
        }),
        (t.CALL_ERROR = {
          CALL_ALREADY_INITIATED: {
            code: "CALL_ALREADY_INITIATED",
            name: "CALL_ALREADY_INITIATED",
            message: "There is already call in progress",
            details: {},
          },
          ERROR_IN_CALLING: {
            code: "CALL_IN_PROGRESS",
            name: "CALL_ALREADY_INITIATED",
            message: "There is already call in progress",
            details: {},
          },
          CANNOT_ACCEPT_CALL: {
            code: "CALL_IN_PROGRESS",
            name: "CALL_IN_PROGRESS",
            message: "There is already a call in progress",
            details: {},
          },
          NOT_INITIALIZED: {
            code: "NOT_INITIALIZED",
            name: "NOT_INITIALIZED",
            message:
              "Please call the CometChat.init() method before calling any other methods related to CometChat.",
            details: {},
          },
          NOT_LOGGED_IN: {
            code: "NOT_LOGGED_IN",
            name: "NOT_LOGGED_IN",
            message: "Please login before starting a call.",
            details: {},
          },
          SESSION_ID_REQUIRED: {
            code: "SESSION_ID_REQUIRED",
            name: "SESSION_ID_REQUIRED",
            message: "Please make sure you are passing correct session id.",
            details: {},
          },
          CALL_SETTINGS_REQUIRED: {
            code: "CALL_SETTINGS_REQUIRED",
            name: "CALL_SETTINGS_REQUIRED",
            message:
              "Please make sure you are passing the call settings object.",
            details: {},
          },
          JWT_NOT_FOUND: {
            code: "JWT_NOT_FOUND",
            name: "JWT_NOT_FOUND",
            message: "There was some issue while fetching JWT from API.",
            details: {},
          },
        }),
        (t.PARAMETER_ERROR = {
          PARAMETER_REQUIRED: {
            code: "%s_NOT_PROVIDED",
            name: "%s_NOT_PROVIDED",
            message: "please provide the %s.",
            details: {},
          },
        }),
        (t.GENERAL_ERROR = {
          MUST_BE_A_STRING: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a string.",
            details: {},
          },
          MUST_BE_A_NUMBER: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a number.",
            details: {},
          },
          MUST_BE_A_OBJECT: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a object.",
            details: {},
          },
          MUST_BE_AN_ARRAY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be an array.",
            details: {},
          },
          MUST_BE_A_BOOLEAN: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a boolean.",
            details: {},
          },
          MUST_BE_A_BLOB: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a blob.",
            details: {},
          },
          INVALID: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "Invalid %s. Please provide a valid %s.",
            details: {},
          },
          METHOD_COMPULSORY: {
            code: "%s_IS_COMPULSORY",
            name: "%s_IS_COMPULSORY",
            message: "%s is required.",
            details: {},
          },
          LIMIT_EXCEEDED: {
            code: "ERROR_%s_EXCEEDED",
            name: "ERROR_%s_EXCEEDED",
            message: "Limit exceeded max limit of %s.",
            details: {},
          },
          MUST_BE_A_POSITIVE_NUMBER: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s should be a postive integer greater than 0.",
            details: {},
          },
          INVALID_MEDIA_FILE: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "The message type does not match the file's mime type.",
            details: {},
          },
          EMPTY_STRING: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s cannot be empty.",
            details: {},
          },
          MISSING_KEY: {
            code: "MISSING_KEY",
            name: "MISSING_KEY",
            message: "The key %s is missing from the %s object.",
            details: {},
          },
          EMPTY_ARRAY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message:
              "The parameter %s should be an array and it cannot be empty.",
            details: {},
          },
          INVALID_SEARCH_KEYWORD: {
            code: "INVALID_SEARCH_KEYWORD",
            name: "INVALID_SEARCH_KEYWORD",
            message:
              "Invalid search keyword. Please provide a valid search keyword.",
            details: {},
          },
          INVALID_GROUP_PROPERTY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message:
              "Invalid %s provided for the group. Please provide a valid %s.",
            details: {},
          },
          INVALID_USER_PROPERTY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message:
              "Invalid %s provided for a user. Please provide a valid %s.",
            details: {},
          },
          PARAMETER_MUST_BE_A_NUMBER: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s method accepts parameter as a number.",
            details: {},
          },
          PARAMETER_MUST_BE_AN_ARRAY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s method accepts parameter as an array.",
            details: {},
          },
          PARAMETER_MUST_BE_A_BOOLEAN: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s method accepts parameter as a boolean.",
            details: {},
          },
          PARAMETER_MUST_BE_A_POSITIVE_NUMBER: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message:
              "%s method accepts parameter to be a positive number greater than 0.",
            details: {},
          },
          PARAMETER_MUST_BE_A_STRING: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s method accepts parameter as a string.",
            details: {},
          },
          PARAMETER_COMPULSORY: {
            code: "%s_IS_COMPULSORY",
            name: "%s_IS_COMPULSORY",
            message: "%s cannot be blank. Please provide a valid %s.",
            details: {},
          },
          PASSWORD_COMPULSORY: {
            code: "%s_IS_COMPULSORY",
            name: "%s_IS_COMPULSORY",
            message: "Password is mandatory for a password group.",
            details: {},
          },
          INVALID_ARRAY: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s",
            details: {},
          },
        }),
        (t.ReceiptErrors = {
          MISSING_PARAMETERS: {
            code: "MISSING_PARAMETERS",
            name: "MISSING_PARAMETERS",
            message: "Expected 4 parameters received 3",
            details: {},
          },
          INVALID_PARAMETER: {
            code: "INVALID_%s",
            name: "INVALID_%s",
            message: "%s",
            details: {},
          },
          NO_WEBSOCKET_CONNECTION: {
            code: "NO_WEBSOCKET_CONNECTION",
            name: "NO_WEBSOCKET_CONNECTION",
            message:
              "Connection to our Websockets server is broken. Please retry after some time.",
            details: {},
          },
          RECEIPTS_TEMPORARILY_BLOCKED: {
            code: "RECEIPTS_TEMPORARILY_BLOCKED",
            name: "RECEIPTS_TEMPORARILY_BLOCKED",
            message:
              "Due to high load. Receipts have been blocked for your app.",
            details: {},
          },
          UNKNOWN_ERROR_OCCURRED: {
            code: "UNKNOWN_ERROR_OCCURRED",
            name: "UNKNOWN_ERROR_OCCURRED",
            message: "Unknown error occurred while marking a message as read.",
            details: {},
          },
        }),
        (t.UserErrors = {
          INVALID_STATUS: new i.CometChatException({
            code: "INVALID_STATUS_VALUE",
            name: "INVALID_STATUS_VALUE",
            message:
              "The `status` parameter accepts only `online` or `offline`.",
            details: "",
          }),
          INVALID_DIRECTION: new i.CometChatException({
            code: "INVALID_DIRECTION_VALUE",
            name: "INVALID_DIRECTION_VALUE",
            message:
              "The `direction` parameter accepts only `both`, `blockeyByMe` or `hasBlockedMe`.",
            details: "",
          }),
          USER_NOT_LOGGED_IN: new i.CometChatException({
            code: "USER_NOT_LOGGED_IN",
            name: "USER_NOT_LOGGED_IN",
            message: "Please log in to CometChat before calling this method.",
            details: "",
          }),
        }),
        (t.GroupErrors = {
          NOT_A_GROUP: new i.CometChatException({
            code: "NOT_A_GROUP",
            message: "Please use group class to construct a new group.",
          }),
          INVALID_SCOPE: new i.CometChatException({
            code: "INVALID_SCOPE_VALUE",
            name: "INVALID_SCOPE_VALUE",
            message: "Scope can be `admin`, `moderator` or `participant`.",
            details: "",
          }),
          INVALID_GROUP_TYPE: new i.CometChatException({
            code: "INVALID_GROUP_TYPE",
            name: "INVALID_GROUP_TYPE",
            message:
              "Group type can be `public`, `private`, `protected` or `password`.",
            details: "",
          }),
        }),
        (t.ConversationErrors = {
          INVALID_CONVERSATION_TYPE: {
            code: "INVALID_CONVERSATION_TYPE",
            name: "INVALID_CONVERSATION_TYPE",
            message: "Conversation type can be `user` or `group`.",
            details: "Please check the value of conversationType.",
          },
          CONVERSATION_NOT_FOUND: {
            code: "CONVERSATION_NOT_FOUND",
            name: "CONVERSATION_NOT_FOUND",
            message: "Conversation for %s %s not found.",
            details:
              "Please check the value of conversationWith and conversationType.",
          },
        }),
        (t.ExtensionErrors = {
          INVALID_EXTENSION: {
            code: "ERROR_INVALID_EXTENSION",
            name: "ERROR_INVALID_EXTENSION",
            message:
              "The provided extension cannot be null or empty. Please provide a valid extension.",
            details: {},
          },
          EXTENSION_NOT_FOUND: {
            code: "ERROR_EXTENSION_NOT_FOUND",
            name: "ERROR_EXTENSION_NOT_FOUND",
            message: "The provided extension could not be found.",
            details: {},
          },
        }),
        (t.MessageErrors = {
          INVALID_RECEIVER_TYPE: {
            code: "INVALID_RECEIVER_TYPE",
            name: "INVALID_RECEIVER_TYPE",
            message: "Receiver type can be `user` or `group`.",
            details: "Please check the value of receiverType.",
          },
        }),
        (t.FeatureRestrictionErrors = {
          INVALID_FEATURE: {
            code: "ERROR_INVALID_FEATURE",
            name: "ERROR_INVALID_FEATURE",
            message:
              "The provided feature cannot be null or empty. Please provide a valid feature.",
            details: {},
          },
          FEATURE_NOT_FOUND: {
            code: "ERROR_FEATURE_NOT_FOUND",
            name: "ERROR_FEATURE_NOT_FOUND",
            message: "The provided feature could not be found.",
            details: {},
          },
        }),
        (t.PresenceConstatnts = {
          STATUS: {
            ONLINE: "online",
            AVAILABLE: "available",
            OFFLINE: "offline",
            JOINED: "JOINED",
            LEFT: "LEFT",
          },
        }),
        (t.APP_SETTINGS = {
          APP_SETTINGS: "app_settings",
          KEYS: {
            CHAT_HOST: "CHAT_HOST",
            CHAT_USE_SSL: "CHAT_USE_SSL",
            GROUP_SERVICE: "GROUP_SERVICE",
            CALL_SERVICE: "CALL_SERVICE",
            CHAT_WS_PORT: "CHAT_WS_PORT",
            CHAT_WSS_PORT: "CHAT_WSS_PORT",
            CHAT_HTTP_BIND_PORT: "CHAT_HTTP_BIND_PORT",
            CHAT_HTTPS_BIND_PORT: "CHAT_HTTPS_BIND_PORT",
            ADMIN_API_HOST: "ADMIN_API_HOST",
            CLIENT_API_HOST: "CLIENT_API_HOST",
            WEBRTC_HOST: "WEBRTC_HOST",
            WEBRTC_USE_SSL: "WEBRTC_USE_SSL",
            WEBRTC_WS_PORT: "WEBRTC_WS_PORT",
            WEBRTC_WSS_PORT: "WEBRTC_WSS_PORT",
            WEBRTC_HTTP_BIND_PORT: "WEBRTC_HTTP_BIND_PORT",
            WEBRTC_HTTPS_BIND_PORT: "WEBRTC_HTTPS_BIND_PORT",
            EXTENSION_LIST: "extensions",
            EXTENSION_KEYS: { ID: "id", NAME: "name" },
            JID_HOST_OVERRIDE: "JID_HOST_OVERRIDE",
            CHAT_HOST_OVERRIDE: "CHAT_HOST_OVERRIDE",
            CHAT_HOST_APP_SPECIFIC: "CHAT_HOST_APP_SPECIFIC",
            MODE: "MODE",
            CONNECTION_TYPE: "connection_type",
            DEFAULT_MODE: "DEFAULT",
            LIMITED_TRANSIENT: "LIMITED_TRANSIENT",
            NO_TRANSIENT: "NO_TRANSIENT",
            POLLING_ENABLED: "POLLING_ENABLED",
            POLLING_INTERVAL: "POLLING_INTERVAL",
            ANALYTICS_PING_DISABLED: "ANALYTICS_PING_DISABLED",
            ANALYTICS_HOST: "ANALYTICS_HOST",
            ANALYTICS_VERSION: "ANALYTICS_VERSION",
            ANALYTICS_USE_SSL: "ANALYTICS_USE_SSL",
            SETTINGS_HASH: "settingsHash",
            SETTINGS_HASH_RECEIVED_AT: "settingsHashReceivedAt",
            DENY_FALLBACK_TO_POLLING: "DENY_FALLBACK_TO_POLLING",
            APP_VERSION: "APP_VERSION",
          },
        }),
        (t.COMMON_UTILITY_CONSTANTS = {
          TYPE_CONSTANTS: {
            BOOLEAN: "boolean",
            STRING: "string",
            OBJECT: "object",
            NUMBER: "number",
          },
        }),
        (t.CONNECTION_STATUS = {
          CONNECTED: "connected",
          CONNECTING: "connecting",
          DISCONNECTED: "disconnected",
          FEATURE_THROTTLED: "featureThrottled",
        }),
        (t.SESSION_STORE = { SESSION_ID: "sessionId" }),
        (t.API_ERROR_CODES = {
          AUTH_ERR_AUTH_TOKEN_NOT_FOUND: "AUTH_ERR_AUTH_TOKEN_NOT_FOUND",
        }),
        (t.PROSODY_API = {
          DOMAIN_PREFIX: "xmpp",
          PATH: { ROOM: "room", ROOM_SIZE: "room-size", SESSIONS: "sessions" },
          RESPONSE: { PARTICIPANTS: "participants" },
          QUERY_PARAMETERS: { DOMAIN: "domain", ROOM: "room" },
        }),
        (t.ProsodyApiErrors = {
          INVALID_SESSIONID: {
            code: "ERROR_INVALID_SESSIONID",
            name: "ERROR_INVALID_SESSIONID",
            message:
              "The provided sessionId cannot be null or empty. Please provide a valid sessionId.",
            details: "",
          },
          INVALID_TYPE: {
            code: "ERROR_INVALID_TYPE",
            name: "ERROR_INVALID_TYPE",
            message:
              "The provided type cannot be null or empty. Please provide a valid type.",
            details: "",
          },
        }),
        (t.JWT_API = {
          KEYS: { PASSTHROUGH: "passthrough", EXPAND: "expand" },
        }),
        (t.ONLINE_MEMBER_COUNT_API = {
          ENDPOINTS: { GET_ONLINE_MEMBER_COUNT: "api/v1/online-members" },
          RESPONSE: {
            ONLINE_USERS_COUNT: "onlineUsersCount",
            GROUPS: "groups",
          },
          ERRORS: {
            INVALID_GROUPLIST: {
              code: "ERROR_INVALID_GROUPLIST",
              name: "ERROR_INVALID_GROUPLIST",
              message: "Grouplist cannot be null or empty.",
              details: "",
            },
          },
        });
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = function (e) {
        null !== e.code &&
          void 0 !== e.code &&
          "" !== e.code &&
          (this.code = e.code),
          null !== e.name &&
            void 0 !== e.name &&
            "" !== e.name &&
            (this.name = e.name),
          null !== e.message &&
            void 0 !== e.message &&
            "" !== e.message &&
            (this.message = e.message),
          null !== e.details &&
            void 0 !== e.details &&
            "" !== e.details &&
            (this.details = e.details);
      };
      t.CometChatException = o;
    },
    function (e, t, n) {
      "use strict";
      var p =
          (this && this.__assign) ||
          function () {
            return (p =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          },
        E =
          (this && this.__awaiter) ||
          function (r, i, a, E) {
            return new (a || (a = Promise))(function (e, t) {
              function n(e) {
                try {
                  s(E.next(e));
                } catch (e) {
                  t(e);
                }
              }
              function o(e) {
                try {
                  s(E.throw(e));
                } catch (e) {
                  t(e);
                }
              }
              function s(t) {
                t.done
                  ? e(t.value)
                  : new a(function (e) {
                      e(t.value);
                    }).then(n, o);
              }
              s((E = E.apply(r, i || [])).next());
            });
          },
        c =
          (this && this.__generator) ||
          function (n, o) {
            var s,
              r,
              i,
              e,
              a = {
                label: 0,
                sent: function () {
                  if (1 & i[0]) throw i[1];
                  return i[1];
                },
                trys: [],
                ops: [],
              };
            return (
              (e = { next: t(0), throw: t(1), return: t(2) }),
              "function" == typeof Symbol &&
                (e[Symbol.iterator] = function () {
                  return this;
                }),
              e
            );
            function t(t) {
              return function (e) {
                return (function (t) {
                  if (s) throw new TypeError("Generator is already executing.");
                  for (; a; )
                    try {
                      if (
                        ((s = 1),
                        r &&
                          (i =
                            2 & t[0]
                              ? r.return
                              : t[0]
                              ? r.throw || ((i = r.return) && i.call(r), 0)
                              : r.next) &&
                          !(i = i.call(r, t[1])).done)
                      )
                        return i;
                      switch (((r = 0), i && (t = [2 & t[0], i.value]), t[0])) {
                        case 0:
                        case 1:
                          i = t;
                          break;
                        case 4:
                          return a.label++, { value: t[1], done: !1 };
                        case 5:
                          a.label++, (r = t[1]), (t = [0]);
                          continue;
                        case 7:
                          (t = a.ops.pop()), a.trys.pop();
                          continue;
                        default:
                          if (
                            !(i = 0 < (i = a.trys).length && i[i.length - 1]) &&
                            (6 === t[0] || 2 === t[0])
                          ) {
                            a = 0;
                            continue;
                          }
                          if (
                            3 === t[0] &&
                            (!i || (t[1] > i[0] && t[1] < i[3]))
                          ) {
                            a.label = t[1];
                            break;
                          }
                          if (6 === t[0] && a.label < i[1]) {
                            (a.label = i[1]), (i = t);
                            break;
                          }
                          if (i && a.label < i[2]) {
                            (a.label = i[2]), a.ops.push(t);
                            break;
                          }
                          i[2] && a.ops.pop(), a.trys.pop();
                          continue;
                      }
                      t = o.call(n, a);
                    } catch (e) {
                      (t = [6, e]), (r = 0);
                    } finally {
                      s = i = 0;
                    }
                  if (5 & t[0]) throw t[1];
                  return { value: t[0] ? t[1] : void 0, done: !0 };
                })([t, e]);
              };
            }
          };
      t.__esModule = !0;
      var u = n(27),
        D = n(0),
        U = n(2),
        C = n(6),
        T = n(4),
        g = n(12),
        S = n(17),
        l = n(7),
        d = n(16),
        _ = n(10),
        h = n(11),
        Y = n(1),
        a = n(8),
        A = n(15),
        r = n(38),
        o = n(23),
        s = n(21),
        G = n(29),
        I = n(14),
        i = n(20),
        f = n(39),
        R = n(40),
        O = n(42),
        N = n(44),
        y = n(45),
        m = n(33),
        L = n(25),
        M = n(34),
        P = n(19),
        w = n(31),
        K = n(47),
        b = n(48),
        F = n(49),
        B = n(32),
        x = n(24),
        V = n(18),
        J = n(50),
        H = n(51),
        k = n(52),
        v = n(35),
        W = n(5),
        j = n(61),
        X = n(28),
        q = n(30),
        Q = n(26),
        z = n(62),
        Z = k.WSConnectionHelper.getInstance(),
        $ = v.ListenerHandlers.getInstance(),
        ee = (function () {
          function v(e) {
            try {
              (v.appId = e),
                (v.localStorage = g.LocalStorage.getInstance()),
                (v.keyStore = r.KeyStore.getInstance());
            } catch (e) {
              D.Logger.error("CometChat: constructor", e);
            }
          }
          return (
            (v.setAuthToken = function (e) {
              try {
                v.authToken = e;
              } catch (e) {
                D.Logger.error("CometChat: setAuthToken", e);
              }
            }),
            (v.prototype.getAuthToken = function () {
              try {
                return v.authToken;
              } catch (e) {
                D.Logger.error("CometChat: getAuthToken", e);
              }
            }),
            (v.getAppId = function () {
              try {
                return v.appId;
              } catch (e) {
                D.Logger.error("CometChat: getAppId", e);
              }
            }),
            (v.prototype.getApiKey = function () {
              try {
                return v.apiKey;
              } catch (e) {
                D.Logger.error("CometChat: getApiKey", e);
              }
            }),
            (v.getMode = function () {
              try {
                return v.mode;
              } catch (e) {
                D.Logger.error("CometChat: getMode", e);
              }
            }),
            (v.setMode = function (e) {
              try {
                v.mode = e;
              } catch (e) {
                D.Logger.error("CometChat: getMode", e);
              }
            }),
            (v.getSessionId = function () {
              try {
                return v.sessionId;
              } catch (e) {
                D.Logger.error("CometChat: getAppId", e);
              }
            }),
            (v.getLastMessageId = function () {
              try {
                return v.lastMessageId;
              } catch (e) {
                D.Logger.error("CometChat: getLastMessageId", e);
              }
            }),
            (v.setLastMessageId = function (e) {
              try {
                v.lastMessageId = e;
              } catch (e) {
                D.Logger.error("CometChat: getLastMessageId", e);
              }
            }),
            (v.setSuccessfulTimeStamp = function (e) {
              try {
                v.successfulPingTimeStamp = e;
              } catch (e) {
                D.Logger.error("CometChat: setSuccessfulTimeStamp", e);
              }
            }),
            (v.getSuccessfultTimeStamp = function () {
              try {
                return v.successfulPingTimeStamp;
              } catch (e) {
                D.Logger.error("CometChat: getSuccessfultTimeStamp", e);
              }
            }),
            (v.onStorageEvent = function (e) {
              var o = this;
              if (document && !document.hasFocus()) {
                var t = v.appId + ":common_store/user";
                e.key === t &&
                  null === e.newValue &&
                  ((v.authToken = void 0),
                  v.didAnalyticsPingStart() && v.clearAnalyticsPingTimer(),
                  v.didMessagesPollingStart() && v.clearMessagesTimer(),
                  Z.WSLogout(),
                  v.pushToLoginListener("", "Logout_Success")),
                  e.key === t &&
                    null === e.oldValue &&
                    g.LocalStorage.getInstance()
                      .get("user")
                      .then(function (e) {
                        e &&
                          ((v.user = new T.Me(e)),
                          v.setAuthToken(v.user.getAuthToken()),
                          e.jwt && (v.jwt = e.jwt),
                          g.LocalStorage.getInstance()
                            .get("app_settings")
                            .then(function (e) {
                              if (e) {
                                if (
                                  e.hasOwnProperty(
                                    Y.APP_SETTINGS.KEYS.APP_VERSION
                                  )
                                ) {
                                  var t = parseInt(
                                    Y.APPINFO.sdkVersion.charAt(1)
                                  );
                                  e[Y.APP_SETTINGS.KEYS.APP_VERSION] < t &&
                                    v
                                      .getInstance()
                                      .restart(v.user.getAuthToken());
                                } else
                                  v.getInstance().restart(
                                    v.user.getAuthToken()
                                  );
                                if (
                                  (e[Y.APP_SETTINGS.KEYS.MODE] &&
                                    (v.mode = e[Y.APP_SETTINGS.KEYS.MODE]),
                                  e[Y.APP_SETTINGS.KEYS.SETTINGS_HASH] &&
                                    (v.settingsHash =
                                      e[Y.APP_SETTINGS.KEYS.SETTINGS_HASH]),
                                  e[
                                    Y.APP_SETTINGS.KEYS
                                      .SETTINGS_HASH_RECEIVED_AT
                                  ] &&
                                    (v.settingsHashReceivedAt =
                                      e[
                                        Y.APP_SETTINGS.KEYS.SETTINGS_HASH_RECEIVED_AT
                                      ]),
                                  e[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST] &&
                                    (v.analyticsHost =
                                      e[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST]),
                                  e[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION] &&
                                    (v.analyticsVersion =
                                      e[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION]),
                                  (v.isAnalyticsDisabled =
                                    !!e[
                                      Y.APP_SETTINGS.KEYS
                                        .ANALYTICS_PING_DISABLED
                                    ]),
                                  e.hasOwnProperty(
                                    Y.APP_SETTINGS.KEYS.DENY_FALLBACK_TO_POLLING
                                  ) && (v.shouldFallBackToPolling = !1),
                                  v.didAnalyticsPingStart() ||
                                    v.isAnalyticsDisabled ||
                                    (v.pingAnalytics(),
                                    v.startAnalyticsPingTimer()),
                                  e[Y.APP_SETTINGS.KEYS.POLLING_ENABLED])
                                )
                                  (o.currentConnectionStatus =
                                    Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                    (v.pollingEnabled = !0),
                                    (v.pollingInterval =
                                      e[Y.APP_SETTINGS.KEYS.POLLING_INTERVAL]),
                                    v.didMessagesPollingStart() ||
                                      ($.connectionHandlers.map(function (e) {
                                        try {
                                          e._eventListener &&
                                            (D.isFalsy(
                                              e._eventListener
                                                .onFeatureThrottled
                                            ) ||
                                              e._eventListener.onFeatureThrottled());
                                        } catch (e) {
                                          D.Logger.error(
                                            "ConnectionHandlers: Feature Throttled Status",
                                            e
                                          );
                                        }
                                      }),
                                      v.fetchMessages(),
                                      v.startMessagesTimer());
                                else if (
                                  v.getConnectionStatus() !==
                                  Y.CONNECTION_STATUS.CONNECTED
                                ) {
                                  var n = new T.User(v.user);
                                  v.pushToLoginListener(n, "Login_Success"),
                                    v.WSLogin(v.user);
                                }
                              }
                            }));
                      });
              }
            }),
            (v.beforeUnload = function (e) {
              var t = v.getDataFromSessionStorage(Y.SESSION_STORE.SESSION_ID);
              v.removeDataFromSessionStorage(Y.SESSION_STORE.SESSION_ID),
                g.LocalStorage.getInstance().set(Y.SESSION_STORE.SESSION_ID, t);
            }),
            (v.isPollingEnabled = function () {
              try {
                return v.pollingEnabled;
              } catch (e) {
                D.Logger.error("CometChat: isPollingEnabled", e);
              }
            }),
            (v.didMessagesPollingStart = function () {
              try {
                return v.isMessagesPollingStarted;
              } catch (e) {
                D.Logger.error("CometChat: didMessagesPollingStart", e);
              }
            }),
            (v.didAnalyticsPingStart = function () {
              try {
                return v.isAnalyticsPingStarted;
              } catch (e) {
                D.Logger.error("CometChat: didAnalyticsPingStart", e);
              }
            }),
            (v.getDataFromSessionStorage = function (e) {
              if (window.sessionStorage)
                return window.sessionStorage.getItem(e);
            }),
            (v.addDataToSessionStorage = function (e, t) {
              window.sessionStorage && window.sessionStorage.setItem(e, t);
            }),
            (v.removeDataFromSessionStorage = function (e) {
              window.sessionStorage && window.sessionStorage.removeItem(e);
            }),
            (v.init = function (s, e) {
              var o = this;
              return (
                void 0 === s && (s = ""),
                void 0 === e && (e = {}),
                new Promise(function (n, t) {
                  try {
                    (v.pollingMessagesId = 0),
                      "object" == typeof s &&
                        (s.hasOwnProperty("appId") && (s = s.appId),
                        s.hasOwnProperty("appSettings") && (e = s.appSettings)),
                      D.isFalsy(e)
                        ? (e = new b.AppSettingsBuilder()
                            .setRegion(b.AppSettings.REGION_EU)
                            .build())
                        : e.getRegion() == b.AppSettings.REGION_PRIVATE &&
                          (e.region = s),
                      (o.appSettings = e),
                      D.isFalsy(s)
                        ? t(new U.CometChatException(I.INIT_ERROR.NO_APP_ID))
                        : (v.setSuccessfulTimeStamp(new Date().getTime()),
                          window.addEventListener &&
                            (window.addEventListener(
                              "storage",
                              v.onStorageEvent,
                              !1
                            ),
                            window.addEventListener(
                              "beforeunload",
                              v.beforeUnload,
                              !1
                            )),
                          (o.initialzed = !0),
                          (v.appId = s),
                          v.getInstance(s),
                          v.setLastMessageId(0),
                          (v.shouldFallBackToPolling = !0),
                          g.LocalStorage.getInstance()
                            .get(Y.SESSION_STORE.SESSION_ID)
                            .then(function (e) {
                              null == e || null == e
                                ? ((v.sessionId =
                                    Y.APPINFO.platform +
                                    "-" +
                                    Y.APPINFO.sdkVersionWithUnderScore +
                                    "-" +
                                    z() +
                                    "-" +
                                    new Date().getTime()),
                                  v.addDataToSessionStorage(
                                    Y.SESSION_STORE.SESSION_ID,
                                    v.getSessionId()
                                  ),
                                  g.LocalStorage.getInstance().remove(
                                    Y.SESSION_STORE.SESSION_ID
                                  ))
                                : ((v.sessionId = e.toLocaleString()),
                                  g.LocalStorage.getInstance().remove(
                                    Y.SESSION_STORE.SESSION_ID
                                  ),
                                  v.addDataToSessionStorage(
                                    Y.SESSION_STORE.SESSION_ID,
                                    v.getSessionId()
                                  )),
                                g.LocalStorage.getInstance()
                                  .get(Y.LOCAL_STORE.KEY_APP_ID)
                                  .then(function (e) {
                                    if (null == e || null == e)
                                      (v.appId = s),
                                        v.getInstance(s),
                                        g.LocalStorage.getInstance().set(
                                          Y.LOCAL_STORE.KEY_APP_ID,
                                          s
                                        ),
                                        n(!0);
                                    else {
                                      var t = e.toLocaleString();
                                      t === s
                                        ? ((v.appId = t),
                                          v.getInstance(t),
                                          g.LocalStorage.getInstance()
                                            .get(Y.LOCAL_STORE.KEY_USER)
                                            .then(function (e) {
                                              e
                                                ? ((v.isLoggedOut = !1),
                                                  (v.user = new T.Me(e)),
                                                  v.setAuthToken(
                                                    v.user.getAuthToken()
                                                  ),
                                                  e.jwt && (v.jwt = e.jwt),
                                                  n(!0),
                                                  g.LocalStorage.getInstance()
                                                    .get(
                                                      Y.LOCAL_STORE
                                                        .KEY_APP_SETTINGS
                                                    )
                                                    .then(function (e) {
                                                      if (e) {
                                                        if (
                                                          e.hasOwnProperty(
                                                            Y.APP_SETTINGS.KEYS
                                                              .APP_VERSION
                                                          )
                                                        ) {
                                                          var t = parseInt(
                                                            Y.APPINFO.sdkVersion.charAt(
                                                              1
                                                            )
                                                          );
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .APP_VERSION
                                                          ] < t &&
                                                            v
                                                              .getInstance()
                                                              .restart(
                                                                v.user.getAuthToken()
                                                              );
                                                        } else
                                                          v.getInstance().restart(
                                                            v.user.getAuthToken()
                                                          );
                                                        e[
                                                          Y.APP_SETTINGS.KEYS
                                                            .MODE
                                                        ] &&
                                                          (v.mode =
                                                            e[
                                                              Y.APP_SETTINGS.KEYS.MODE
                                                            ]),
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .SETTINGS_HASH
                                                          ] &&
                                                            (v.settingsHash =
                                                              e[
                                                                Y.APP_SETTINGS.KEYS.SETTINGS_HASH
                                                              ]),
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .SETTINGS_HASH_RECEIVED_AT
                                                          ] &&
                                                            (v.settingsHashReceivedAt =
                                                              e[
                                                                Y.APP_SETTINGS.KEYS.SETTINGS_HASH_RECEIVED_AT
                                                              ]),
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .ANALYTICS_HOST
                                                          ] &&
                                                            (v.analyticsHost =
                                                              e[
                                                                Y.APP_SETTINGS.KEYS.ANALYTICS_HOST
                                                              ]),
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .ANALYTICS_VERSION
                                                          ] &&
                                                            (v.analyticsVersion =
                                                              e[
                                                                Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION
                                                              ]),
                                                          (v.isAnalyticsDisabled =
                                                            !!e[
                                                              Y.APP_SETTINGS
                                                                .KEYS
                                                                .ANALYTICS_PING_DISABLED
                                                            ]),
                                                          e.hasOwnProperty(
                                                            Y.APP_SETTINGS.KEYS
                                                              .DENY_FALLBACK_TO_POLLING
                                                          ) &&
                                                            (v.shouldFallBackToPolling =
                                                              !1),
                                                          v.didAnalyticsPingStart() ||
                                                            v.isAnalyticsDisabled ||
                                                            (v.pingAnalytics(),
                                                            v.startAnalyticsPingTimer()),
                                                          e[
                                                            Y.APP_SETTINGS.KEYS
                                                              .POLLING_ENABLED
                                                          ]
                                                            ? ((v.pollingEnabled =
                                                                !0),
                                                              e[
                                                                Y.APP_SETTINGS
                                                                  .KEYS
                                                                  .POLLING_INTERVAL
                                                              ] &&
                                                                (v.pollingInterval =
                                                                  e[
                                                                    Y.APP_SETTINGS.KEYS.POLLING_INTERVAL
                                                                  ]),
                                                              (o.currentConnectionStatus =
                                                                Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                                              v.didMessagesPollingStart() ||
                                                                ($.connectionHandlers.map(
                                                                  function (e) {
                                                                    try {
                                                                      e._eventListener &&
                                                                        (D.isFalsy(
                                                                          e
                                                                            ._eventListener
                                                                            .onFeatureThrottled
                                                                        ) ||
                                                                          e._eventListener.onFeatureThrottled());
                                                                    } catch (e) {
                                                                      D.Logger.error(
                                                                        "ConnectionHandlers: Feature Throttled Status",
                                                                        e
                                                                      );
                                                                    }
                                                                  }
                                                                ),
                                                                (v.pollingMessagesTimestamp =
                                                                  Math.floor(
                                                                    new Date().getTime() /
                                                                      1e3
                                                                  )),
                                                                v.fetchMessages(),
                                                                v.startMessagesTimer()))
                                                            : Z &&
                                                              !Z.connection &&
                                                              ((v.pollingEnabled =
                                                                !1),
                                                              (v.isConnectingFromInit =
                                                                !0),
                                                              v.WSLogin(
                                                                v.user
                                                              ));
                                                      }
                                                    }))
                                                : n(!0);
                                            }))
                                        : o.clearCache().then(function () {
                                            (v.apiKey = void 0),
                                              (v.user = void 0),
                                              (v.authToken = void 0),
                                              (v.cometChat = void 0),
                                              (v.mode = void 0),
                                              Z.WSLogout(),
                                              (v.appId = s),
                                              g.LocalStorage.getInstance().set(
                                                Y.LOCAL_STORE.KEY_APP_ID,
                                                s
                                              ),
                                              v.getInstance(s),
                                              n(!0);
                                          });
                                    }
                                    r.KeyStore.getInstance()
                                      .get(Y.LOCAL_STORE.KEY_DEVICE_ID)
                                      .then(function (e) {
                                        if (null == e) {
                                          var t = z(),
                                            n = new Date().getTime(),
                                            o = s + "_" + t + "_" + n;
                                          r.KeyStore.getInstance().set(
                                            Y.LOCAL_STORE.KEY_DEVICE_ID,
                                            o
                                          );
                                        }
                                      });
                                  });
                            }));
                  } catch (e) {
                    t(new U.CometChatException(e));
                  }
                })
              );
            }),
            (v.isInitialized = function () {
              try {
                return this.initialzed;
              } catch (e) {
                D.Logger.error("CometChat: isInitialized", e);
              }
            }),
            (v.getInstance = function (e) {
              try {
                return (
                  this.cometChat || (this.cometChat = new v(e)), this.cometChat
                );
              } catch (e) {
                D.Logger.error("CometChat: getInstance", e);
              }
            }),
            (v.registerTokenForPushNotification = function (S, l) {
              var p = this;
              return new Promise(function (c, u) {
                try {
                  v.keyStore
                    .get(Y.LOCAL_STORE.KEY_DEVICE_ID)
                    .then(function (e) {
                      var t = "",
                        n = e,
                        o = Y.APPINFO.platform,
                        s = Y.APPINFO.sdkVersion,
                        r = Y.APPINFO.apiVersion;
                      if ((navigator && (t = navigator.userAgent), null == n)) {
                        var i = z(),
                          a = new Date().getTime();
                        (n = p.appId + "_" + i + "_" + a),
                          v.keyStore.set(Y.LOCAL_STORE.KEY_DEVICE_ID, n);
                      }
                      var E = {
                        platform: o,
                        deviceId: n,
                        appInfo: {
                          version: s,
                          apiVersion: r,
                          userAgent: t,
                          pushNotification: { fcmDeviceToken: S, settings: l },
                        },
                      };
                      C.makeApiCall("updateMyDetails", {}, E, !1)
                        .then(
                          function (e) {
                            c("Token Registration successful");
                          },
                          function (e) {
                            u(new U.CometChatException(e.error));
                          }
                        )
                        .catch(function (e) {
                          u(new U.CometChatException(e));
                        });
                    });
                } catch (e) {
                  u(new U.CometChatException(e));
                }
              });
            }),
            (v.pushToLoginListener = function (t, n) {
              v.shouldPushLoginListener &&
                $.loginHandlers.map(function (e) {
                  try {
                    if (e._eventListener)
                      switch (n) {
                        case "Login_Success":
                          D.isFalsy(e._eventListener.loginSuccess) ||
                            e._eventListener.loginSuccess(t);
                          break;
                        case "Login_Failure":
                          D.isFalsy(e._eventListener.loginFailure) ||
                            e._eventListener.loginFailure(t);
                          break;
                        case "Logout_Success":
                          D.isFalsy(e._eventListener.logoutSuccess) ||
                            e._eventListener.logoutSuccess();
                      }
                  } catch (e) {
                    D.Logger.error("ConnectionHandlers: onConnected Status", e);
                  }
                });
            }),
            (v.login = function () {
              for (var E = this, c = [], e = 0; e < arguments.length; e++)
                c[e] = arguments[e];
              return new Promise(function (i, a) {
                try {
                  if (((v.pollingMessagesId = 0), v.loginInProgress)) {
                    v.isLoggedOut = !0;
                    var t = new U.CometChatException(
                      I.LOGIN_ERROR.REQUEST_IN_PROGRESS
                    );
                    return a(t);
                  }
                  (v.loginInProgress = !0),
                    (v.isConnectingFromInit = !1),
                    v.localStorage
                      .get(Y.LOCAL_STORE.KEY_APP_ID)
                      .then(function (e) {
                        if (null == e || null == e) {
                          var t = new U.CometChatException(
                            I.LOGIN_ERROR.NOT_INITIALIZED
                          );
                          return (
                            v.internalRestart ||
                              v.pushToLoginListener(t, "Login_Failure"),
                            (v.loginInProgress = !1),
                            (v.isLoggedOut = !0),
                            a(t)
                          );
                        }
                        var n = e.toLocaleString();
                        if (
                          (E.getInstance(n),
                          (v.appId = n),
                          g.LocalStorage.getInstance().set(
                            Y.LOCAL_STORE.KEY_APP_ID,
                            n
                          ),
                          "object" == typeof c[0])
                        ) {
                          var o = c[0];
                          c[0].hasOwnProperty("authToken")
                            ? (c[0] = o.authToken)
                            : c[0].hasOwnProperty("username") &&
                              c[0].hasOwnProperty("apiKey") &&
                              ((c[0] = o.username), (c[1] = o.apiKey));
                        }
                        if (2 == c.length) {
                          if (D.isFalsy(c[0]) || D.isFalsy(c[1])) {
                            t = new U.CometChatException(
                              I.ERRORS.PARAMETER_MISSING
                            );
                            return (
                              v.internalRestart ||
                                v.pushToLoginListener(t, "Login_Failure"),
                              (v.loginInProgress = !1),
                              (v.isLoggedOut = !0),
                              a(t)
                            );
                          }
                        } else {
                          if (1 != c.length) {
                            t = new U.CometChatException(
                              I.ERRORS.PARAMETER_MISSING
                            );
                            return (
                              v.internalRestart ||
                                v.pushToLoginListener(t, "Login_Failure"),
                              (v.loginInProgress = !1),
                              (v.isLoggedOut = !0),
                              a(t)
                            );
                          }
                          if (D.isFalsy(c[0])) {
                            var t = new U.CometChatException(
                              I.ERRORS.PARAMETER_MISSING
                            );
                            return (
                              v.internalRestart ||
                                v.pushToLoginListener(t, "Login_Failure"),
                              (v.loginInProgress = !1),
                              (v.isLoggedOut = !0),
                              a(t)
                            );
                          }
                        }
                        if (D.isFalsy(E.getAppId())) {
                          t = new U.CometChatException(
                            I.LOGIN_ERROR.NOT_INITIALIZED
                          );
                          return (
                            v.internalRestart ||
                              v.pushToLoginListener(t, "Login_Failure"),
                            (v.loginInProgress = !1),
                            (v.isLoggedOut = !0),
                            a(t)
                          );
                        }
                        return v.localStorage
                          .get(Y.LOCAL_STORE.KEY_USER)
                          .then(function (r) {
                            if (2 == c.length) {
                              v.apiKey = c[1];
                              var e = c[0];
                              if (null != r && r.uid !== c[0])
                                v.localStorage.clearStore().then(function () {
                                  Z.WSLogout(),
                                    v.generateAuthToken(e).then(
                                      function (e) {
                                        (v.user = new T.Me(e)),
                                          v.setAuthToken(v.user.getAuthToken()),
                                          v.getLoggedInUser().then(
                                            function (e) {
                                              (v.user = new T.Me(
                                                e
                                              )).setAuthToken(v.authToken),
                                                v.setAuthToken(
                                                  v.user.getAuthToken()
                                                );
                                              var t = v.user;
                                              t.setStatus(
                                                Y.PresenceConstatnts.STATUS
                                                  .ONLINE
                                              ),
                                                v.localStorage.set("user", t);
                                              var n = new T.User(t);
                                              i(n),
                                                v.isConnectingFromInit ||
                                                  v.internalRestart ||
                                                  v.pushToLoginListener(
                                                    n,
                                                    "Login_Success"
                                                  ),
                                                (v.loginInProgress = !1),
                                                v.didAnalyticsPingStart() ||
                                                  v.isAnalyticsDisabled ||
                                                  (v.pingAnalytics(),
                                                  v.startAnalyticsPingTimer()),
                                                (v.isLoggedOut = !1),
                                                v.isPollingEnabled()
                                                  ? ((v.currentConnectionStatus =
                                                      Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                                    v.didMessagesPollingStart() ||
                                                      ($.connectionHandlers.map(
                                                        function (e) {
                                                          try {
                                                            e._eventListener &&
                                                              (D.isFalsy(
                                                                e._eventListener
                                                                  .onFeatureThrottled
                                                              ) ||
                                                                e._eventListener.onFeatureThrottled());
                                                          } catch (e) {
                                                            D.Logger.error(
                                                              "ConnectionHandlers: Feature Throttled Status",
                                                              e
                                                            );
                                                          }
                                                        }
                                                      ),
                                                      (v.pollingMessagesTimestamp =
                                                        Math.floor(
                                                          new Date().getTime() /
                                                            1e3
                                                        )),
                                                      v.fetchMessages(),
                                                      v.startMessagesTimer()))
                                                  : v.WSLogin(v.user);
                                            },
                                            function (e) {
                                              return (
                                                v.internalRestart ||
                                                  v.pushToLoginListener(
                                                    e,
                                                    "Login_Failure"
                                                  ),
                                                (v.loginInProgress = !1),
                                                (v.isLoggedOut = !0),
                                                a(e)
                                              );
                                            }
                                          );
                                      },
                                      function (e) {
                                        return (
                                          v.internalRestart ||
                                            v.pushToLoginListener(
                                              e,
                                              "Login_Failure"
                                            ),
                                          (v.loginInProgress = !1),
                                          (v.isLoggedOut = !0),
                                          a(e)
                                        );
                                      }
                                    );
                                });
                              else {
                                if (!D.isFalsy(v.authToken)) {
                                  var t = new T.User(E.user);
                                  return (
                                    v.internalRestart ||
                                      v.pushToLoginListener(t, "Login_Success"),
                                    (v.loginInProgress = !1),
                                    (v.isLoggedOut = !1),
                                    i(t)
                                  );
                                }
                                if (!D.isFalsy(e)) {
                                  if (D.isFalsy(v.apiKey)) {
                                    var n = new U.CometChatException(
                                      I.LOGIN_ERROR.UNAUTHORISED
                                    );
                                    return (
                                      v.internalRestart ||
                                        v.pushToLoginListener(
                                          n,
                                          "Login_Failure"
                                        ),
                                      (v.loginInProgress = !1),
                                      (v.isLoggedOut = !0),
                                      a(n)
                                    );
                                  }
                                  E.generateAuthToken(e).then(
                                    function (e) {
                                      (E.user = new T.Me(e)),
                                        E.setAuthToken(v.user.getAuthToken()),
                                        E.getLoggedInUser().then(
                                          function (e) {
                                            (E.user = new T.Me(e)),
                                              E.user.setAuthToken(v.authToken),
                                              E.setAuthToken(
                                                E.user.getAuthToken()
                                              ),
                                              Z.WSLogout();
                                            var t = E.user;
                                            t.setStatus(
                                              Y.PresenceConstatnts.STATUS.ONLINE
                                            ),
                                              E.localStorage.set("user", t);
                                            var n = new T.User(t);
                                            i(n),
                                              v.isConnectingFromInit ||
                                                v.internalRestart ||
                                                v.pushToLoginListener(
                                                  n,
                                                  "Login_Success"
                                                ),
                                              (v.loginInProgress = !1),
                                              v.didAnalyticsPingStart() ||
                                                v.isAnalyticsDisabled ||
                                                (v.pingAnalytics(),
                                                v.startAnalyticsPingTimer()),
                                              (v.isLoggedOut = !1),
                                              v.isPollingEnabled()
                                                ? ((E.currentConnectionStatus =
                                                    Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                                  v.didMessagesPollingStart() ||
                                                    ($.connectionHandlers.map(
                                                      function (e) {
                                                        try {
                                                          e._eventListener &&
                                                            (D.isFalsy(
                                                              e._eventListener
                                                                .onFeatureThrottled
                                                            ) ||
                                                              e._eventListener.onFeatureThrottled());
                                                        } catch (e) {
                                                          D.Logger.error(
                                                            "ConnectionHandlers: Feature Throttled Status",
                                                            e
                                                          );
                                                        }
                                                      }
                                                    ),
                                                    (v.pollingMessagesTimestamp =
                                                      Math.floor(
                                                        new Date().getTime() /
                                                          1e3
                                                      )),
                                                    v.fetchMessages(),
                                                    v.startMessagesTimer()))
                                                : E.WSLogin(E.user);
                                          },
                                          function (e) {
                                            return (
                                              v.internalRestart ||
                                                v.pushToLoginListener(
                                                  e,
                                                  "Login_Failure"
                                                ),
                                              (v.loginInProgress = !1),
                                              (v.isLoggedOut = !0),
                                              a(e)
                                            );
                                          }
                                        );
                                    },
                                    function (e) {
                                      return (
                                        v.internalRestart ||
                                          v.pushToLoginListener(
                                            e,
                                            "Login_Failure"
                                          ),
                                        (v.loginInProgress = !1),
                                        (v.isLoggedOut = !0),
                                        a(e)
                                      );
                                    }
                                  );
                                }
                              }
                            } else
                              (v.authToken = c[0]),
                                E.getLoggedInUser().then(
                                  function (e) {
                                    if (e.authToken != c[0]) {
                                      var o = E;
                                      o.localStorage
                                        .clearStore()
                                        .then(function () {
                                          Z.WSLogout(),
                                            o.getLoggedInUser().then(
                                              function (e) {
                                                (v.user = new T.Me(
                                                  e
                                                )).setAuthToken(v.authToken),
                                                  v.setAuthToken(
                                                    v.user.getAuthToken()
                                                  );
                                                var t = v.user;
                                                t.setStatus(
                                                  Y.PresenceConstatnts.STATUS
                                                    .ONLINE
                                                ),
                                                  o.localStorage.set("user", t);
                                                var n = new T.User(t);
                                                i(n),
                                                  v.isConnectingFromInit ||
                                                    v.internalRestart ||
                                                    v.pushToLoginListener(
                                                      n,
                                                      "Login_Success"
                                                    ),
                                                  (v.loginInProgress = !1),
                                                  v.didAnalyticsPingStart() ||
                                                    v.isAnalyticsDisabled ||
                                                    (v.pingAnalytics(),
                                                    v.startAnalyticsPingTimer()),
                                                  (v.isLoggedOut = !1),
                                                  v.isPollingEnabled()
                                                    ? ((o.currentConnectionStatus =
                                                        Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                                      v.didMessagesPollingStart() ||
                                                        ($.connectionHandlers.map(
                                                          function (e) {
                                                            try {
                                                              e._eventListener &&
                                                                (D.isFalsy(
                                                                  e
                                                                    ._eventListener
                                                                    .onFeatureThrottled
                                                                ) ||
                                                                  e._eventListener.onFeatureThrottled());
                                                            } catch (e) {
                                                              D.Logger.error(
                                                                "ConnectionHandlers: Feature Throttled Status",
                                                                e
                                                              );
                                                            }
                                                          }
                                                        ),
                                                        (v.pollingMessagesTimestamp =
                                                          Math.floor(
                                                            new Date().getTime() /
                                                              1e3
                                                          )),
                                                        v.fetchMessages(),
                                                        v.startMessagesTimer()))
                                                    : v.WSLogin(new T.Me(e));
                                              },
                                              function (e) {
                                                return (
                                                  v.internalRestart ||
                                                    v.pushToLoginListener(
                                                      e,
                                                      "Login_Failure"
                                                    ),
                                                  (v.loginInProgress = !1),
                                                  (v.isLoggedOut = !0),
                                                  a(e)
                                                );
                                              }
                                            );
                                        });
                                    } else {
                                      if (r) {
                                        var t = new T.User(e);
                                        return (
                                          v.internalRestart ||
                                            v.pushToLoginListener(
                                              t,
                                              "Login_Success"
                                            ),
                                          (v.loginInProgress = !1),
                                          (v.isLoggedOut = !1),
                                          i(t)
                                        );
                                      }
                                      (v.user = new T.Me(e)).setAuthToken(
                                        v.authToken
                                      ),
                                        v.setAuthToken(v.user.getAuthToken());
                                      var n = v.user;
                                      n.setStatus(
                                        Y.PresenceConstatnts.STATUS.ONLINE
                                      ),
                                        v.localStorage.set("user", n);
                                      var s = new T.User(n);
                                      i(s),
                                        v.isConnectingFromInit ||
                                          v.internalRestart ||
                                          v.pushToLoginListener(
                                            s,
                                            "Login_Success"
                                          ),
                                        (v.loginInProgress = !1),
                                        v.didAnalyticsPingStart() ||
                                          v.isAnalyticsDisabled ||
                                          (v.pingAnalytics(),
                                          v.startAnalyticsPingTimer()),
                                        (v.isLoggedOut = !1),
                                        v.isPollingEnabled()
                                          ? ((v.currentConnectionStatus =
                                              Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                                            v.didMessagesPollingStart() ||
                                              ($.connectionHandlers.map(
                                                function (e) {
                                                  try {
                                                    e._eventListener &&
                                                      (D.isFalsy(
                                                        e._eventListener
                                                          .onFeatureThrottled
                                                      ) ||
                                                        e._eventListener.onFeatureThrottled());
                                                  } catch (e) {
                                                    D.Logger.error(
                                                      "ConnectionHandlers: Feature Throttled Status",
                                                      e
                                                    );
                                                  }
                                                }
                                              ),
                                              (v.pollingMessagesTimestamp =
                                                Math.floor(
                                                  new Date().getTime() / 1e3
                                                )),
                                              v.fetchMessages(),
                                              v.startMessagesTimer()))
                                          : v.WSLogin(new T.Me(e));
                                    }
                                  },
                                  function (e) {
                                    return (
                                      v.internalRestart ||
                                        v.pushToLoginListener(
                                          e,
                                          "Login_Failure"
                                        ),
                                      (v.loginInProgress = !1),
                                      (v.isLoggedOut = !0),
                                      a(e)
                                    );
                                  }
                                );
                          });
                      });
                } catch (e) {
                  t = new U.CometChatException(e);
                  return (
                    v.internalRestart ||
                      v.pushToLoginListener(t, "Login_Failure"),
                    (v.loginInProgress = !1),
                    (v.isLoggedOut = !0),
                    a(t)
                  );
                }
              });
            }),
            (v.sendMessage = function (a) {
              var e = this;
              return new Promise(function (r, i) {
                return E(e, void 0, void 0, function () {
                  var t,
                    n,
                    o,
                    s = this;
                  return c(this, function (e) {
                    try {
                      return (
                        a instanceof d.TextMessage ||
                          a instanceof S.MediaMessage ||
                          a instanceof P.CustomMessage ||
                          (a = a[Y.MessageConstatnts.KEYS.ATTATCHMENT]
                            ? ((t = Y.MessageConstatnts.TYPE.FILE),
                              D.isImage(a[Y.MessageConstatnts.KEYS.ATTATCHMENT])
                                ? (t = Y.MessageConstatnts.TYPE.IMAGE)
                                : D.isAudio(
                                    a[Y.MessageConstatnts.KEYS.ATTATCHMENT]
                                  )
                                ? (t = Y.MessageConstatnts.TYPE.AUDIO)
                                : D.isVideo(
                                    a[Y.MessageConstatnts.KEYS.ATTATCHMENT]
                                  ) && (t = Y.MessageConstatnts.TYPE.VIDEO),
                              new S.MediaMessage(
                                a[Y.MessageConstatnts.KEYS.RECEIVER_ID],
                                a[Y.MessageConstatnts.KEYS.ATTATCHMENT],
                                t,
                                a[Y.MessageConstatnts.KEYS.RECEIVER_TYPE]
                              ))
                            : new d.TextMessage(
                                a[Y.MessageConstatnts.KEYS.RECEIVER_ID],
                                a[Y.MessageConstatnts.KEYS.TEXT],
                                a[Y.MessageConstatnts.KEYS.RECEIVER_TYPE]
                              )),
                        (n = D.validateMessage(a)) instanceof
                        U.CometChatException
                          ? (i(n), [2])
                          : ((a.receiver = a.receiverId),
                            delete a.receiverId,
                            (o = a.parentMessageId),
                            [
                              2,
                              C.makeApiCall(
                                o ? "sendMessageInThread" : "sendMessage",
                                o ? { parentId: o } : {},
                                a,
                                a instanceof S.MediaMessage
                              ).then(
                                function (n) {
                                  return E(s, void 0, void 0, function () {
                                    var t;
                                    return c(this, function (e) {
                                      return (
                                        (t =
                                          _.MessageController.trasformJSONMessge(
                                            n.data
                                          )),
                                        v.setLastMessageId(t.id),
                                        r(t),
                                        [2]
                                      );
                                    });
                                  });
                                },
                                function (e) {
                                  i(new U.CometChatException(e.error));
                                }
                              ),
                            ])
                      );
                    } catch (e) {
                      i(new U.CometChatException(e));
                    }
                    return [2];
                  });
                });
              });
            }),
            (v.sendDirectMessage = function (e) {
              try {
                return (
                  Object.assign(e, {
                    receiverType: Y.MessageConstatnts.RECEIVER_TYPE.USER,
                  }),
                  this.sendMessage(e)
                );
              } catch (e) {
                D.Logger.error("CometChat: sendDirectMessage", e);
              }
            }),
            (v.sendGroupMessage = function (e) {
              try {
                return (
                  Object.assign(e, {
                    receiverType: Y.MessageConstatnts.RECEIVER_TYPE.GROUP,
                  }),
                  this.sendMessage(e)
                );
              } catch (e) {
                D.Logger.error("CometChat: sendGroupMessage", e);
              }
            }),
            (v.sendMediaMessage = function (e) {
              try {
                return this.sendMessage(e);
              } catch (e) {
                D.Logger.error("CometChat: sendMediaMessage", e);
              }
            }),
            (v.sendCustomMessage = function (e) {
              try {
                return this.sendMessage(e);
              } catch (e) {
                D.Logger.error("CometChat: sendCustomMessage", e);
              }
            }),
            (v.getLastDeliveredMessageId = function () {
              return E(this, void 0, void 0, function () {
                var t;
                return c(this, function (e) {
                  switch (e.label) {
                    case 0:
                      return (
                        e.trys.push([0, 2, , 3]),
                        [
                          4,
                          m.MessageListnerMaping.getInstance().get(
                            Y.LOCAL_STORE.KEY_MESSAGE_LISTENER_LIST
                          ),
                        ]
                      );
                    case 1:
                      return [2, e.sent()];
                    case 2:
                      return (
                        (t = e.sent()),
                        D.Logger.error(
                          "CometChat: getLastDeliveredMessageId",
                          t
                        ),
                        [3, 3]
                      );
                    case 3:
                      return [2];
                  }
                });
              });
            }),
            (v.startTyping = function (e) {
              try {
                if (D.isFalsy(e)) return;
                var t = void 0,
                  n = this.RECEIVER_TYPE.USER,
                  o = {};
                if (e instanceof L.TypingIndicator)
                  (t = e.getReceiverId()),
                    (n = e.getReceiverType()),
                    (o = e.getMetadata());
                else {
                  if (!e.hasOwnProperty(Y.TYPING_NOTIFICATION.RECEIVER_ID))
                    return;
                  (t = e[Y.TYPING_NOTIFICATION.RECEIVER_ID]),
                    e.hasOwnProperty(Y.TYPING_NOTIFICATION.RECEIVER_TYPE) &&
                      (n = e[Y.TYPING_NOTIFICATION.RECEIVER_TYPE]),
                    e.hasOwnProperty(Y.TYPING_NOTIFICATION.META) &&
                      (o = e[Y.TYPING_NOTIFICATION.META]);
                }
                if (D.isFalsy(t)) return;
                if (
                  null == M.TypingNotificationController.getTypingStartedMap(t)
                ) {
                  var s = v.getMode();
                  return D.isFalsy(s) ||
                    (s &&
                      s !== Y.APP_SETTINGS.KEYS.NO_TRANSIENT &&
                      s !== Y.APP_SETTINGS.KEYS.LIMITED_TRANSIENT)
                    ? (Z.startTypingIndicator(t, n, o),
                      M.TypingNotificationController.addTypingStarted(t),
                      void M.TypingNotificationController.removeTypingEnded(t))
                    : void 0;
                }
              } catch (e) {
                D.Logger.error("CometChat: startTyping", e);
              }
            }),
            (v.endTyping = function (e) {
              try {
                if (D.isFalsy(e)) return;
                var t = void 0,
                  n = this.RECEIVER_TYPE.USER,
                  o = {};
                if (e instanceof L.TypingIndicator)
                  (t = e.getReceiverId()),
                    (n = e.getReceiverType()),
                    (o = e.getMetadata());
                else {
                  if (!e.hasOwnProperty(Y.TYPING_NOTIFICATION.RECEIVER_ID))
                    return;
                  (t = e[Y.TYPING_NOTIFICATION.RECEIVER_ID]),
                    e.hasOwnProperty(Y.TYPING_NOTIFICATION.RECEIVER_TYPE) &&
                      (n = e[Y.TYPING_NOTIFICATION.RECEIVER_TYPE]),
                    (n =
                      n == this.RECEIVER_TYPE.USER
                        ? Y.WS.CONVERSATION.TYPE.CHAT
                        : Y.WS.CONVERSATION.TYPE.GROUP_CHAT),
                    e.hasOwnProperty(Y.TYPING_NOTIFICATION.META) &&
                      (o = e[Y.TYPING_NOTIFICATION.META]);
                }
                if (D.isFalsy(t)) return;
                if (
                  null == M.TypingNotificationController.getTypingEndedMap(t)
                ) {
                  var s = v.getMode();
                  return D.isFalsy(s) ||
                    (s &&
                      s !== Y.APP_SETTINGS.KEYS.NO_TRANSIENT &&
                      s !== Y.APP_SETTINGS.KEYS.LIMITED_TRANSIENT)
                    ? (Z.pauseTypingIndicator(t, n, o),
                      M.TypingNotificationController.addTypingEnded(t),
                      void M.TypingNotificationController.removeTypingStarted(
                        t
                      ))
                    : void 0;
                }
              } catch (e) {
                D.Logger.error("CometChat: endTyping", e);
              }
            }),
            (v.markAsRead = function () {
              for (var E = [], e = 0; e < arguments.length; e++)
                E[e] = arguments[e];
              return new Promise(function (e, t) {
                try {
                  var n = void 0,
                    o = void 0,
                    s = void 0,
                    r = void 0;
                  if (3 === E.length)
                    return t(
                      new U.CometChatException(
                        Y.ReceiptErrors.MISSING_PARAMETERS
                      )
                    );
                  if (4 === E.length) {
                    if (D.isFalsy(E[0]) || "string" != typeof E[0])
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "MESSAGE_ID",
                              "MESSAGE_ID",
                              "Message ID should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((n = E[0]), D.isFalsy(E[1]) || "string" != typeof E[1])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "RECEIVER_ID",
                              "RECEIVER_ID",
                              "Receiver ID should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((o = E[1]), D.isFalsy(E[2]) || "string" != typeof E[2])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "RECEIVER_TYPE",
                              "RECEIVER_TYPE",
                              "Receiver type should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((s = E[2]), D.isFalsy(E[3]) || "string" != typeof E[3])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "SENDER_ID",
                              "SENDER_ID",
                              "Sender ID should be a string."
                            )
                          )
                        )
                      );
                    r = E[3];
                  } else {
                    if (1 !== E.length)
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "ARGUMENTS",
                              "ARGUMENTS",
                              "markAsRead() expects either 1 or 4 arguments."
                            )
                          )
                        )
                      );
                    if (D.isFalsy(E[0]) || !(E[0] instanceof l.BaseMessage))
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "MESSAGE",
                              "MESSAGE",
                              "Invalid message object received."
                            )
                          )
                        )
                      );
                    var i = E[0];
                    (n = i.getId().toString()),
                      (o =
                        (s = i.getReceiverType()) ===
                        Y.MessageConstatnts.RECEIVER_TYPE.USER
                          ? i.getSender().getUid() === v.user.getUid()
                            ? i.getReceiverId()
                            : i.getSender().getUid()
                          : i.getReceiverId()),
                      (r = i.getSender().getUid());
                  }
                  var a = v.getMode();
                  return D.isFalsy(a) ||
                    (a &&
                      a !== Y.APP_SETTINGS.KEYS.NO_TRANSIENT &&
                      a !== Y.APP_SETTINGS.KEYS.LIMITED_TRANSIENT)
                    ? v.getConnectionStatus() === Y.CONNECTION_STATUS.CONNECTED
                      ? (Z.markAsRead(o, s, n, r), e())
                      : t(
                          new U.CometChatException(
                            Y.ReceiptErrors.NO_WEBSOCKET_CONNECTION
                          )
                        )
                    : t(
                        new U.CometChatException(
                          Y.ReceiptErrors.RECEIPTS_TEMPORARILY_BLOCKED
                        )
                      );
                } catch (e) {
                  return (
                    D.Logger.error("CometChat: markAsRead", e),
                    t(
                      new U.CometChatException(
                        Y.ReceiptErrors.UNKNOWN_ERROR_OCCURRED
                      )
                    )
                  );
                }
              });
            }),
            (v.markAsDelivered = function () {
              for (var E = [], e = 0; e < arguments.length; e++)
                E[e] = arguments[e];
              return new Promise(function (e, t) {
                try {
                  var n = void 0,
                    o = void 0,
                    s = void 0,
                    r = void 0;
                  if (3 === E.length)
                    return t(
                      new U.CometChatException(
                        Y.ReceiptErrors.MISSING_PARAMETERS
                      )
                    );
                  if (4 === E.length) {
                    if (D.isFalsy(E[0]) || "string" != typeof E[0])
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "MESSAGE_ID",
                              "MESSAGE_ID",
                              "Message ID should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((n = E[0]), D.isFalsy(E[1]) || "string" != typeof E[1])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "RECEIVER_ID",
                              "RECEIVER_ID",
                              "Receiver ID should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((o = E[1]), D.isFalsy(E[2]) || "string" != typeof E[2])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "RECEIVER_TYPE",
                              "RECEIVER_TYPE",
                              "Receiver type should be a string."
                            )
                          )
                        )
                      );
                    if (
                      ((s = E[2]), D.isFalsy(E[3]) || "string" != typeof E[3])
                    )
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "SENDER_ID",
                              "SENDER_ID",
                              "Sender ID should be a string."
                            )
                          )
                        )
                      );
                    r = E[3];
                  } else {
                    if (1 !== E.length)
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "ARGUMENTS",
                              "ARGUMENTS",
                              "markAsDelivered() expects either 1 or 4 arguments."
                            )
                          )
                        )
                      );
                    if (D.isFalsy(E[0]) || !(E[0] instanceof l.BaseMessage))
                      return t(
                        new U.CometChatException(
                          JSON.parse(
                            D.format(
                              JSON.stringify(Y.ReceiptErrors.INVALID_PARAMETER),
                              "MESSAGE",
                              "MESSAGE",
                              "Invalid message object received."
                            )
                          )
                        )
                      );
                    var i = E[0];
                    (n = i.getId().toString()),
                      (o =
                        (s = i.getReceiverType()) ===
                        Y.MessageConstatnts.RECEIVER_TYPE.USER
                          ? i.getSender().getUid() === v.user.getUid()
                            ? i.getReceiverId()
                            : i.getSender().getUid()
                          : i.getReceiverId()),
                      (r = i.getSender().getUid());
                  }
                  var a = v.getMode();
                  return D.isFalsy(a) ||
                    (a &&
                      a !== Y.APP_SETTINGS.KEYS.NO_TRANSIENT &&
                      a !== Y.APP_SETTINGS.KEYS.LIMITED_TRANSIENT)
                    ? v.getConnectionStatus() === Y.CONNECTION_STATUS.CONNECTED
                      ? (Z.markAsDelivered(o, s, n, r), e())
                      : t(
                          new U.CometChatException(
                            Y.ReceiptErrors.NO_WEBSOCKET_CONNECTION
                          )
                        )
                    : t(
                        new U.CometChatException(
                          Y.ReceiptErrors.RECEIPTS_TEMPORARILY_BLOCKED
                        )
                      );
                } catch (e) {
                  return (
                    D.Logger.error("CometChat: markAsDelivered", e),
                    t(
                      new U.CometChatException(
                        Y.ReceiptErrors.UNKNOWN_ERROR_OCCURRED
                      )
                    )
                  );
                }
              });
            }),
            (v.sendTransientMessage = function (e) {
              try {
                if (D.isFalsy(e)) return;
                var t = void 0,
                  n = void 0,
                  o = {};
                if (!(e instanceof Q.TransientMessage)) return;
                if (
                  ((t = e.getReceiverId()),
                  (n = e.getReceiverType()),
                  (o = e.getData()),
                  D.isFalsy(t) || D.isFalsy(n))
                )
                  return;
                var s = v.getMode();
                return D.isFalsy(s) ||
                  (s && s !== Y.APP_SETTINGS.KEYS.NO_TRANSIENT)
                  ? void Z.sendTransientMessage(t, n, o)
                  : void 0;
              } catch (e) {
                D.Logger.error("CometChat: sendTransientMessage", e);
              }
            }),
            (v.sendTestMessage = function (o) {
              return E(this, void 0, void 0, function () {
                var t, n;
                return c(this, function (e) {
                  switch (e.label) {
                    case 0:
                      return (
                        e.trys.push([0, 3, , 4]),
                        (t = o) instanceof l.BaseMessage
                          ? [3, 2]
                          : [4, F.CometChatHelper.processMessage(o)]
                      );
                    case 1:
                      (t = e.sent()), (e.label = 2);
                    case 2:
                      return (
                        t instanceof d.TextMessage &&
                          k.WSConnectionHelper.getInstance().publishMessage(t),
                        [3, 4]
                      );
                    case 3:
                      return (
                        (n = e.sent()),
                        D.Logger.error("CometChat: sendTestMessage", n),
                        [3, 4]
                      );
                    case 4:
                      return [2];
                  }
                });
              });
            }),
            (v.getMessageDetails = function (e) {
              return new Promise(function (t, n) {
                try {
                  D.isFalsy(e)
                    ? n(new U.CometChatException(I.ERRORS.PARAMETER_MISSING))
                    : C.makeApiCall("getMessageDetails", { messageId: e }).then(
                        function (e) {
                          t(_.MessageController.trasformJSONMessge(e.data));
                        },
                        function (e) {
                          D.Logger.error("CometChat:GetMessageDetails:", e),
                            n(new U.CometChatException(e.error));
                        }
                      );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.getMessageReceipts = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateMsgId(o);
                  if (e instanceof U.CometChatException) return void n(e);
                  D.isFalsy(o)
                    ? n(new U.CometChatException(I.ERRORS.PARAMETER_MISSING))
                    : C.makeApiCall("getMessageDetails", { messageId: o }).then(
                        function (e) {
                          _.MessageController.getReceiptsFromJSON(e.data).then(
                            function (e) {
                              t(e);
                            },
                            function (e) {
                              n(new U.CometChatException(e));
                            }
                          );
                        },
                        function (e) {
                          D.Logger.error("CometChat:GetMessageDetails:", e),
                            n(new U.CometChatException(e.error));
                        }
                      );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.getUnreadMessageCount = function (n) {
              void 0 === n && (n = !1);
              var s = 0;
              return new Promise(function (o, t) {
                try {
                  var e = D.validateHideMessagesFromBlockedUsers(n);
                  if (e instanceof U.CometChatException) return void t(e);
                  n && (s = 1),
                    C.makeApiCall(
                      "getMessages",
                      {},
                      { unread: 1, count: 1, hideMessagesFromBlockedUsers: s }
                    ).then(
                      function (e) {
                        var t = {},
                          n = {};
                        e.data.map(function (e) {
                          e[
                            Y.ResponseConstants.RESPONSE_KEYS
                              .UNREAD_UNDELIVERED_KEYS.ENTITY_TYPE
                          ] == Y.MessageConstatnts.RECEIVER_TYPE.GROUP
                            ? (n[
                                e[
                                  Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                                ]
                              ] =
                                e[
                                  Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                                ])
                            : (t[
                                e[
                                  Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                                ]
                              ] =
                                e[
                                  Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                                ]);
                        }),
                          o({ users: t, groups: n });
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getUnreadMessageCountForAllUsers = function (o) {
              void 0 === o && (o = !1);
              var s = 0;
              return new Promise(function (n, t) {
                try {
                  var e = D.validateHideMessagesFromBlockedUsers(o);
                  if (e instanceof U.CometChatException) return void t(e);
                  o && (s = 1),
                    C.makeApiCall(
                      "getMessages",
                      {},
                      {
                        hideMessagesFromBlockedUsers: s,
                        receiverType: Y.MessageConstatnts.RECEIVER_TYPE.USER,
                        unread: 1,
                        count: 1,
                      }
                    ).then(
                      function (e) {
                        var t = {};
                        e.data.map(function (e) {
                          t[
                            e[
                              Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                            ]
                          ] =
                            e[
                              Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                            ];
                        }),
                          n(p({}, t));
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getUnreadMessageCountForAllGroups = function (o) {
              void 0 === o && (o = !1);
              var s = 0;
              return new Promise(function (n, t) {
                try {
                  var e = D.validateHideMessagesFromBlockedUsers(o);
                  if (e instanceof U.CometChatException) return void t(e);
                  o && (s = 1),
                    C.makeApiCall(
                      "getMessages",
                      {},
                      {
                        hideMessagesFromBlockedUsers: s,
                        receiverType: Y.MessageConstatnts.RECEIVER_TYPE.GROUP,
                        unread: 1,
                        count: 1,
                      }
                    ).then(
                      function (e) {
                        var t = {};
                        e.data.map(function (e) {
                          e[
                            Y.ResponseConstants.RESPONSE_KEYS
                              .UNREAD_UNDELIVERED_KEYS.ENTITY_TYPE
                          ] == Y.MessageConstatnts.RECEIVER_TYPE.GROUP &&
                            (t[
                              e[
                                Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                              ]
                            ] =
                              e[
                                Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                              ]);
                        }),
                          n(p({}, t));
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getUnreadMessageCountForUser = function (s, r) {
              void 0 === r && (r = !1);
              var i = 0;
              return new Promise(function (n, t) {
                try {
                  var e = D.validateId(s, "user");
                  if (e instanceof U.CometChatException) return void t(e);
                  var o = D.validateHideMessagesFromBlockedUsers(r);
                  if (o instanceof U.CometChatException) return void t(o);
                  r && (i = 1),
                    C.makeApiCall(
                      "getUserMessages",
                      { listId: s },
                      {
                        hideMessagesFromBlockedUsers: i,
                        unread: 1,
                        count: 1,
                        uid: s,
                      }
                    ).then(
                      function (e) {
                        var t = {};
                        e.data.map(function (e) {
                          t[
                            e[
                              Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                            ]
                          ] =
                            e[
                              Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                            ];
                        }),
                          n(p({}, t));
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getUnreadMessageCountForGroup = function (s, r) {
              void 0 === r && (r = !1);
              var i = 0;
              return new Promise(function (n, t) {
                try {
                  var e = D.validateId(s, "group");
                  if (e instanceof U.CometChatException) return void t(e);
                  var o = D.validateHideMessagesFromBlockedUsers(r);
                  if (o instanceof U.CometChatException) return void t(o);
                  r && (i = 1),
                    C.makeApiCall(
                      "getGroupMessages",
                      { listId: s },
                      {
                        hideMessagesFromBlockedUsers: i,
                        unread: 1,
                        count: 1,
                        guid: s,
                      }
                    ).then(
                      function (e) {
                        var t = {};
                        e.data.map(function (e) {
                          e[
                            Y.ResponseConstants.RESPONSE_KEYS
                              .UNREAD_UNDELIVERED_KEYS.ENTITY_TYPE
                          ] == Y.MessageConstatnts.RECEIVER_TYPE.GROUP &&
                            (t[
                              e[
                                Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.ENTITY_Id
                              ]
                            ] =
                              e[
                                Y.ResponseConstants.RESPONSE_KEYS.UNREAD_UNDELIVERED_KEYS.COUNT
                              ]);
                        }),
                          n(p({}, t));
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.editMessage = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateMsgId(o.getId());
                  if (e instanceof U.CometChatException) return void n(e);
                  C.makeApiCall(
                    "updateMessage",
                    { messageId: o.getId() },
                    o
                  ).then(
                    function (e) {
                      t(
                        _.MessageController.trasformJSONMessge(
                          e.data
                        ).getActionOn()
                      );
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.deleteMessage = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateMsgId(o);
                  if (e instanceof U.CometChatException) return void n(e);
                  C.makeApiCall(
                    "deleteMessage",
                    { messageId: o },
                    { id: o }
                  ).then(
                    function (e) {
                      t(
                        _.MessageController.trasformJSONMessge(
                          e.data
                        ).getActionOn()
                      );
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.getOnlineUserCount = function () {
              return new Promise(function (o, s) {
                try {
                  D.getAppSettings().then(
                    function (e) {
                      var t = D.format(
                          new u.EndpointFactory().wsApi,
                          D.getChatHost(e),
                          Y.ONLINE_MEMBER_COUNT_API.ENDPOINTS
                            .GET_ONLINE_MEMBER_COUNT
                        ),
                        n = {
                          appId: v.appId,
                          Authorization: v.jwt,
                          Accept: "application/json",
                          "Content-Type": "application/json",
                        };
                      C.postData(t, "POST", {}, n, !1)
                        .then(function (e) {
                          return e.json();
                        })
                        .then(function (e) {
                          return e.hasOwnProperty(
                            Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA
                          )
                            ? o(
                                e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA][
                                  Y.ONLINE_MEMBER_COUNT_API.RESPONSE
                                    .ONLINE_USERS_COUNT
                                ]
                              )
                            : s(new U.CometChatException(e.error));
                        })
                        .catch(function () {
                          var e = { error: I.FETCH_ERROR.ERROR_IN_API_CALL };
                          return s(e);
                        });
                    },
                    function (e) {
                      return s(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return s(new U.CometChatException(e));
                }
              });
            }),
            (v.getOnlineGroupMemberCount = function (i) {
              return new Promise(function (s, r) {
                try {
                  if (!i || 0 == i.length)
                    return r(
                      new U.CometChatException(
                        Y.ONLINE_MEMBER_COUNT_API.ERRORS.INVALID_GROUPLIST
                      )
                    );
                  D.getAppSettings().then(
                    function (e) {
                      var t = D.format(
                          new u.EndpointFactory().wsApi,
                          D.getChatHost(e),
                          Y.ONLINE_MEMBER_COUNT_API.ENDPOINTS
                            .GET_ONLINE_MEMBER_COUNT
                        ),
                        n = { groups: i },
                        o = {
                          appId: v.appId,
                          Authorization: v.jwt,
                          Accept: "application/json",
                          "Content-Type": "application/json",
                        };
                      C.postData(t, "POST", n, o, !1)
                        .then(function (e) {
                          return e.json();
                        })
                        .then(function (e) {
                          return e.hasOwnProperty(
                            Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA
                          )
                            ? s(
                                e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA][
                                  Y.ONLINE_MEMBER_COUNT_API.RESPONSE.GROUPS
                                ]
                              )
                            : r(new U.CometChatException(e.error));
                        })
                        .catch(function () {
                          var e = { error: I.FETCH_ERROR.ERROR_IN_API_CALL };
                          return r(e);
                        });
                    },
                    function (e) {
                      return r(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return r(new U.CometChatException(e));
                }
              });
            }),
            (v.createUser = function (s, r) {
              return new Promise(function (n, t) {
                try {
                  if (D.isFalsy(r))
                    return void t(
                      new U.CometChatException(
                        JSON.parse(
                          D.format(
                            JSON.stringify(Y.GENERAL_ERROR.INVALID),
                            "AUTH_KEY",
                            "AUTH_KEY",
                            "AUTH_KEY",
                            "AUTH_KEY"
                          )
                        )
                      )
                    );
                  v.apiKey = r;
                  var e = D.validateCreateUser(s);
                  if (e instanceof U.CometChatException) return void t(e);
                  if (!(s instanceof T.User)) {
                    var o = void 0;
                    if (!s.hasOwnProperty(Y.UserConstants.UID))
                      return void t(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    if (!s.hasOwnProperty(Y.UserConstants.NAME))
                      return void t(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    (o = new T.User(
                      s[Y.UserConstants.UID],
                      s[Y.UserConstants.NAME]
                    )),
                      s.hasOwnProperty(Y.UserConstants.AVATAR) &&
                        o.setAvatar(s[Y.UserConstants.AVATAR]),
                      s.hasOwnProperty(Y.UserConstants.ROLE) &&
                        o.setRole(s[Y.UserConstants.ROLE]),
                      s.hasOwnProperty(Y.UserConstants.META_DATA) &&
                        o.setMetadata(s[Y.UserConstants.META_DATA]),
                      s.hasOwnProperty(Y.UserConstants.LINK) &&
                        o.setLink(s[Y.UserConstants.LINK]),
                      s.hasOwnProperty(Y.UserConstants.STATUS_MESSAGE) &&
                        o.setStatusMessage(s[Y.UserConstants.STATUS_MESSAGE]),
                      s.hasOwnProperty(Y.UserConstants.TAGS) &&
                        o.setTags(s[Y.UserConstants.TAGS]),
                      (s = o);
                  }
                  C.makeApiCall("createUser", {}, s).then(
                    function (e) {
                      var t = h.UsersController.trasformJSONUser(e.data);
                      n(t);
                    },
                    function (e) {
                      t(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.updateUser = function (o, i) {
              var a = this;
              return new Promise(function (s, t) {
                try {
                  if (D.isFalsy(i))
                    return void t(
                      new U.CometChatException(
                        JSON.parse(
                          D.format(
                            JSON.stringify(Y.GENERAL_ERROR.INVALID),
                            "AUTH_KEY",
                            "AUTH_KEY",
                            "AUTH_KEY",
                            "AUTH_KEY"
                          )
                        )
                      )
                    );
                  v.apiKey = i;
                  var e = D.validateUpdateUser(o);
                  if (e instanceof U.CometChatException) return void t(e);
                  if (!(o instanceof T.User)) {
                    var n = void 0;
                    if (!o.hasOwnProperty(Y.UserConstants.UID))
                      return void t(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    (n = new T.User(o[Y.UserConstants.UID])),
                      o.hasOwnProperty(Y.UserConstants.NAME) &&
                        n.setName(o[Y.UserConstants.NAME]),
                      o.hasOwnProperty(Y.UserConstants.AVATAR) &&
                        n.setAvatar(o[Y.UserConstants.AVATAR]),
                      o.hasOwnProperty(Y.UserConstants.ROLE) &&
                        n.setRole(o[Y.UserConstants.ROLE]),
                      o.hasOwnProperty(Y.UserConstants.META_DATA) &&
                        n.setMetadata(o[Y.UserConstants.META_DATA]),
                      o.hasOwnProperty(Y.UserConstants.LINK) &&
                        n.setLink(o[Y.UserConstants.LINK]),
                      o.hasOwnProperty(Y.UserConstants.STATUS_MESSAGE) &&
                        n.setStatusMessage(o[Y.UserConstants.STATUS_MESSAGE]),
                      o.hasOwnProperty(Y.UserConstants.TAGS) &&
                        n.setTags(o[Y.UserConstants.TAGS]),
                      (o = n);
                  }
                  var r = o.uid;
                  C.makeApiCall("updateUser", { uid: r }, o).then(
                    function (o) {
                      if (
                        a.user &&
                        r.toLocaleLowerCase() ===
                          a.user.getUid().toLocaleLowerCase()
                      )
                        g.LocalStorage.getInstance()
                          .get("user")
                          .then(function (e) {
                            if (e) {
                              var t = h.UsersController.trasformJSONUser(
                                  o.data
                                ),
                                n = o.data;
                              (n.wsChannel = e.wsChannel),
                                (n.authToken = v.authToken),
                                (n.status = Y.PresenceConstatnts.STATUS.ONLINE),
                                e.jwt && (n.jwt = e.jwt),
                                (v.user = new T.Me(n)),
                                a.localStorage.set("user", v.user),
                                s(t);
                            }
                          });
                      else {
                        var e = h.UsersController.trasformJSONUser(o.data);
                        s(e);
                      }
                    },
                    function (e) {
                      t(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.updateCurrentUserDetails = function (s) {
              var r = this;
              return new Promise(function (o, t) {
                try {
                  s.uid = r.user.uid;
                  var e = D.validateUpdateUser(s);
                  if (e instanceof U.CometChatException) return void t(e);
                  if (!(s instanceof T.User)) {
                    var n = void 0;
                    s.hasOwnProperty(Y.UserConstants.UID) &&
                      (n = new T.User(s[Y.UserConstants.UID])),
                      s.hasOwnProperty(Y.UserConstants.NAME) &&
                        n.setName(s[Y.UserConstants.NAME]),
                      s.hasOwnProperty(Y.UserConstants.AVATAR) &&
                        n.setAvatar(s[Y.UserConstants.AVATAR]),
                      s.hasOwnProperty(Y.UserConstants.ROLE) &&
                        n.setRole(s[Y.UserConstants.ROLE]),
                      s.hasOwnProperty(Y.UserConstants.META_DATA) &&
                        n.setMetadata(s[Y.UserConstants.META_DATA]),
                      s.hasOwnProperty(Y.UserConstants.LINK) &&
                        n.setLink(s[Y.UserConstants.LINK]),
                      s.hasOwnProperty(Y.UserConstants.STATUS_MESSAGE) &&
                        n.setStatusMessage(s[Y.UserConstants.STATUS_MESSAGE]),
                      s.hasOwnProperty(Y.UserConstants.TAGS) &&
                        n.setTags(s[Y.UserConstants.TAGS]),
                      (s = n);
                  }
                  C.makeApiCall("updateMyDetails", {}, s).then(
                    function (e) {
                      var t = h.UsersController.trasformJSONUser(e.data),
                        n = (v.user = new T.Me(e.data));
                      n.setStatus(Y.PresenceConstatnts.STATUS.ONLINE),
                        v.localStorage.set("user", n),
                        o(t);
                    },
                    function (e) {
                      t(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getUser = function (o) {
              return new Promise(function (n, t) {
                try {
                  "object" == typeof o &&
                    o.hasOwnProperty("uid") &&
                    (o = o.uid);
                  var e = D.validateId(o, "user");
                  if (e instanceof U.CometChatException) return void t(e);
                  C.makeApiCall("user", { uid: o })
                    .then(function (e) {
                      var t = h.UsersController.trasformJSONUser(e.data);
                      n(t);
                    })
                    .catch(function (e) {
                      t(new U.CometChatException(e.error));
                    });
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.getLoggedInUser = function () {
              var l = this;
              return new Promise(function (u, S) {
                try {
                  v.localStorage.get(Y.LOCAL_STORE.KEY_USER).then(function (e) {
                    if (e) u((v.user = new T.Me(e)));
                    else {
                      var r = "",
                        i = "",
                        a = Y.APPINFO.platform,
                        E = Y.APPINFO.sdkVersion,
                        c = Y.APPINFO.apiVersion;
                      v.keyStore
                        .get(Y.LOCAL_STORE.KEY_DEVICE_ID)
                        .then(function (e) {
                          if (
                            ((i = e),
                            navigator && (r = navigator.userAgent),
                            null == i)
                          ) {
                            var t = z(),
                              n = new Date().getTime();
                            (i = l.appId + "_" + t + "_" + n),
                              v.keyStore.set(Y.LOCAL_STORE.KEY_DEVICE_ID, i);
                          }
                          var o = { version: E, apiVersion: c, userAgent: r };
                          D.isFalsy(l.platform) || (o.platform = l.platform),
                            D.isFalsy(l.language) || (o.language = l.language),
                            D.isFalsy(l.resource) || (o.resource = l.resource);
                          var s = { platform: a, deviceId: i, appInfo: o };
                          C.makeApiCall("updateMyDetails", {}, s, !1)
                            .then(
                              function (e) {
                                e.data.jwt && (v.jwt = e.data.jwt);
                                var t = e.data.settings;
                                t &&
                                  (g.LocalStorage.getInstance().set(
                                    "app_settings",
                                    t
                                  ),
                                  t[Y.APP_SETTINGS.KEYS.MODE] &&
                                    v.setMode(t[Y.APP_SETTINGS.KEYS.MODE]),
                                  t[Y.APP_SETTINGS.KEYS.SETTINGS_HASH] &&
                                    (v.settingsHash =
                                      t[Y.APP_SETTINGS.KEYS.SETTINGS_HASH]),
                                  t[
                                    Y.APP_SETTINGS.KEYS
                                      .SETTINGS_HASH_RECEIVED_AT
                                  ] &&
                                    (v.settingsHashReceivedAt =
                                      t[
                                        Y.APP_SETTINGS.KEYS.SETTINGS_HASH_RECEIVED_AT
                                      ]),
                                  t[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST] &&
                                    (v.analyticsHost =
                                      t[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST]),
                                  t[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION] &&
                                    (v.analyticsVersion =
                                      t[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION]),
                                  (v.isAnalyticsDisabled =
                                    !!t[
                                      Y.APP_SETTINGS.KEYS
                                        .ANALYTICS_PING_DISABLED
                                    ]),
                                  t.hasOwnProperty(
                                    Y.APP_SETTINGS.KEYS.DENY_FALLBACK_TO_POLLING
                                  ) && (v.shouldFallBackToPolling = !1),
                                  (v.pollingEnabled =
                                    t[Y.APP_SETTINGS.KEYS.POLLING_ENABLED]),
                                  v.isPollingEnabled() &&
                                    t[Y.APP_SETTINGS.KEYS.POLLING_INTERVAL] &&
                                    (v.pollingInterval =
                                      t[Y.APP_SETTINGS.KEYS.POLLING_INTERVAL])),
                                  u(new T.Me(e.data));
                              },
                              function (e) {
                                S(new U.CometChatException(e.error));
                              }
                            )
                            .catch(function (e) {
                              S(new U.CometChatException(e));
                            });
                        });
                    }
                  });
                } catch (e) {
                  S(new U.CometChatException(e));
                }
              });
            }),
            (v.getLoggedinUser = function () {
              return new Promise(function (t, n) {
                try {
                  v.localStorage.get(Y.LOCAL_STORE.KEY_USER).then(
                    function (e) {
                      t(e ? new T.User(e) : null);
                    },
                    function (e) {
                      t(null);
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.blockUsers = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateArray(o, "blockUsers");
                  if (e instanceof U.CometChatException) return void n(e);
                  D.isFalsy(o)
                    ? n(
                        new U.CometChatException(
                          I.USERS_REQUEST_ERRORS.EMPTY_USERS_LIST
                        )
                      )
                    : C.makeApiCall("blockUsers", {}, { blockedUids: o }).then(
                        function (e) {
                          t(e.data);
                        },
                        function (e) {
                          n(new U.CometChatException(e.error));
                        }
                      );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.unblockUsers = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateArray(o, "unblockUsers");
                  if (e instanceof U.CometChatException) return void n(e);
                  D.isFalsy(o)
                    ? n(
                        new U.CometChatException(
                          I.USERS_REQUEST_ERRORS.EMPTY_USERS_LIST
                        )
                      )
                    : C.makeApiCall(
                        "unblockUsers",
                        {},
                        { blockedUids: o }
                      ).then(
                        function (e) {
                          t(e.data);
                        },
                        function (e) {
                          n(new U.CometChatException(e.error));
                        }
                      );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.getConversation = function (i, a) {
              return new Promise(function (n, o) {
                try {
                  var e = D.validateConversationType(a);
                  if (e instanceof U.CometChatException) return void o(e);
                  var t = D.validateId(i, a);
                  if (t instanceof U.CometChatException) return void o(t);
                  (a = a.toLowerCase()), (i = i.toLowerCase());
                  var s = {},
                    r = "";
                  a === Y.MessageConstatnts.RECEIVER_TYPE.GROUP
                    ? ((r = "getGroupConversation"), (s.guid = i))
                    : ((r = "getUserConversation"), (s.uid = i)),
                    C.makeApiCall(r, s).then(
                      function (e) {
                        if (e.data) {
                          var t = e.data;
                          n(
                            x.ConversationController.trasformJSONConversation(
                              t.conversationId,
                              t.conversationType,
                              t.lastMessage,
                              t.conversationWith,
                              t.unreadMessageCount
                            )
                          );
                        } else
                          o(
                            new U.CometChatException(
                              JSON.parse(
                                D.format(
                                  JSON.stringify(
                                    Y.ConversationErrors.CONVERSATION_NOT_FOUND
                                  ),
                                  a,
                                  i
                                )
                              )
                            )
                          );
                      },
                      function (e) {
                        o(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  o(new U.CometChatException(e));
                }
              });
            }),
            (v.deleteConversation = function (i, a) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateConversationType(a);
                  if (e instanceof U.CometChatException) return void n(e);
                  var o = D.validateId(i, a);
                  if (o instanceof U.CometChatException) return void n(o);
                  var s = {},
                    r = "";
                  (a = a.toLowerCase()),
                    (i = i.toLowerCase()),
                    a === Y.MessageConstatnts.RECEIVER_TYPE.GROUP
                      ? ((r = "deleteGroupConversation"), (s.guid = i))
                      : ((r = "deleteUserConversation"), (s.uid = i)),
                    C.makeApiCall(r, s).then(
                      function (e) {
                        t("Conversation deleted successfully.");
                      },
                      function (e) {
                        n(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.createGroup = function (s) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateCreateGroup(s);
                  if (e instanceof U.CometChatException) return void n(e);
                  if (!(s instanceof a.Group)) {
                    var o = void 0;
                    if (!s.hasOwnProperty(Y.GroupConstants.KEYS.GUID))
                      return void n(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    if (!s.hasOwnProperty(Y.GroupConstants.KEYS.NAME))
                      return void n(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    if (
                      ((o = new a.Group(
                        s[Y.GroupConstants.KEYS.GUID],
                        s[Y.GroupConstants.KEYS.NAME],
                        ""
                      )),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.TYPE))
                    )
                      if (
                        s[Y.GroupConstants.KEYS.TYPE].toLocaleLowerCase() ==
                        Y.GroupType.Password
                      ) {
                        if (!s.hasOwnProperty(Y.GroupConstants.KEYS.PASSWORD))
                          return void n(
                            new U.CometChatException(
                              I.GROUP_CREATION_ERRORS.EMPTY_PASSWORD
                            )
                          );
                        o.setType(Y.GROUP_TYPE.PASSWORD),
                          o.setPassword(s[Y.GroupConstants.KEYS.PASSWORD]);
                      } else o.setType(s[Y.GroupConstants.KEYS.TYPE]);
                    else o.setType(Y.GROUP_TYPE.PUBLIC);
                    s.hasOwnProperty(Y.GroupConstants.KEYS.ICON) &&
                      o.setIcon(s[Y.GroupConstants.KEYS.ICON]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.DESCRIPTION) &&
                        o.setDescription(s[Y.GroupConstants.KEYS.DESCRIPTION]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.TAGS) &&
                        o.setTags(s[Y.GroupConstants.KEYS.TAGS]),
                      (s = o);
                  }
                  C.makeApiCall("createGroup", {}, s).then(
                    function (e) {
                      A.GroupsController.trasformJSONGroup(e.data).setHasJoined(
                        !0
                      ),
                        t(A.GroupsController.trasformJSONGroup(e.data));
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.getGroup = function (o) {
              return new Promise(function (t, n) {
                try {
                  "object" == typeof o &&
                    o.hasOwnProperty("guid") &&
                    (o = o.guid);
                  var e = D.validateId(o, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  C.makeApiCall("getGroup", { guid: o }).then(
                    function (e) {
                      t(A.GroupsController.trasformJSONGroup(e.data));
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.joinGroup = function (s, r, i) {
              return (
                void 0 === r && (r = Y.GroupType.Public),
                void 0 === i && (i = ""),
                new Promise(function (n, t) {
                  try {
                    var e = D.validateJoinGroup(s, r, i);
                    if (e instanceof U.CometChatException) return void t(e);
                    var o = void 0;
                    "object" == typeof s &&
                      (s.hasOwnProperty(Y.GroupConstants.KEYS.GUID)
                        ? (s.hasOwnProperty(Y.GroupConstants.KEYS.TYPE) &&
                            ((r = s[Y.GroupConstants.KEYS.TYPE]),
                            s[
                              Y.GroupConstants.KEYS.TYPE
                            ].toLocaleLowerCase() === Y.GroupType.Password &&
                              s.hasOwnProperty(
                                Y.GroupConstants.KEYS.PASSWORD
                              ) &&
                              (i = s[Y.GroupConstants.KEYS.PASSWORD])),
                          (s = s[Y.GroupConstants.KEYS.GUID]))
                        : t(
                            new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                          )),
                      (o = D.isFalsy(i)
                        ? new a.Group(s, "name", r)
                        : new a.Group(s, "name", r, i)),
                      C.makeApiCall("joinGroup", o, o).then(
                        function (e) {
                          var t = A.GroupsController.trasformJSONGroup(
                            e.data[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA][
                              Y.ActionConstatnts.ACTION_KEYS.ENTITIES
                            ][Y.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                              Y.ActionConstatnts.ACTION_KEYS.ENTITY
                            ]
                          );
                          t.setHasJoined(!0), n(t);
                        },
                        function (e) {
                          t(new U.CometChatException(e.error));
                        }
                      );
                  } catch (e) {
                    t(new U.CometChatException(e));
                  }
                })
              );
            }),
            (v.updateGroup = function (s) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateUpdateGroup(s);
                  if (e instanceof U.CometChatException) return void n(e);
                  if (!(s instanceof a.Group)) {
                    var o = void 0;
                    if (!s.hasOwnProperty(Y.GroupConstants.KEYS.GUID))
                      return void n(
                        new U.CometChatException(I.ERRORS.PARAMETER_MISSING)
                      );
                    (o = new a.Group(Y.GroupConstants.KEYS.GUID, "", "")),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.TYPE)
                        ? o.setType(s[Y.GroupConstants.KEYS.TYPE])
                        : ((s[Y.GroupConstants.KEYS.TYPE] =
                            Y.GROUP_TYPE.PUBLIC),
                          o.setType[Y.GROUP_TYPE.PUBLIC]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.NAME) &&
                        o.setName(s[Y.GroupConstants.KEYS.NAME]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.ICON) &&
                        o.setIcon(s[Y.GroupConstants.KEYS.ICON]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.DESCRIPTION) &&
                        o.setDescription(s[Y.GroupConstants.KEYS.DESCRIPTION]),
                      s.hasOwnProperty(Y.GroupConstants.KEYS.TAGS) &&
                        o.setTags(s[Y.GroupConstants.KEYS.TAGS]),
                      (s = o);
                  }
                  C.makeApiCall("updateGroup", s, s).then(
                    function (e) {
                      t(A.GroupsController.trasformJSONGroup(e.data));
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.deleteGroup = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(o, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  C.makeApiCall("deleteGroup", { guid: o }).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.leaveGroup = function (o) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(o, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  C.makeApiCall("leaveGroup", { guid: o }).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.kickGroupMember = function (s, r) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(s, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  var o = D.validateId(r, "user");
                  if (o instanceof U.CometChatException) return void n(o);
                  C.makeApiCall("kickGroupMembers", { guid: s, uid: r }).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.updateGroupMemberScope = function (r, i, a) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(r, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  var o = D.validateId(i, "user");
                  if (o instanceof U.CometChatException) return void n(o);
                  var s = D.validateScope(a);
                  if (s instanceof U.CometChatException) return void n(s);
                  C.makeApiCall(
                    "changeScopeOfMember",
                    { guid: r, uid: i },
                    { scope: a }
                  ).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.banGroupMember = function (s, r) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(s, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  var o = D.validateId(r, "user");
                  if (o instanceof U.CometChatException) return void n(o);
                  C.makeApiCall("banGroupMember", { guid: s, uid: r }).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.unbanGroupMember = function (s, r) {
              return new Promise(function (t, n) {
                try {
                  var e = D.validateId(s, "group");
                  if (e instanceof U.CometChatException) return void n(e);
                  var o = D.validateId(r, "user");
                  if (o instanceof U.CometChatException) return void n(o);
                  C.makeApiCall("unbanGroupMember", { guid: s, uid: r }).then(
                    function (e) {
                      t(!0);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.addMembersToGroup = function (i, a, E) {
              var c = [],
                u = [],
                S = [],
                l = [];
              return new Promise(function (e, t) {
                try {
                  var n = D.validateId(i, "group");
                  if (n instanceof U.CometChatException) return void t(n);
                  var o = D.validateArray(a, "groupMembers");
                  if (o instanceof U.CometChatException) return void t(o);
                  if (E) {
                    var s = D.validateArray(E, "bannedMembers");
                    if (s instanceof U.CometChatException) return void t(s);
                  }
                  D.isFalsy(a) && D.isFalsy(E)
                    ? t(new U.CometChatException({}))
                    : D.isFalsy(a) || D.isFalsy(E)
                    ? D.isFalsy(a)
                      ? E.map(function (e) {
                          l.push(e);
                        })
                      : (a
                          .filter(function (e) {
                            if (e.getScope() == Y.GROUP_MEMBER_SCOPE.ADMIN)
                              return !0;
                          })
                          .map(function (e) {
                            c.push(e.getUid());
                          }),
                        a
                          .filter(function (e) {
                            if (e.getScope() == Y.GROUP_MEMBER_SCOPE.MODERATOR)
                              return !0;
                          })
                          .map(function (e) {
                            u.push(e.getUid());
                          }),
                        a
                          .filter(function (e) {
                            if (
                              e.getScope() == Y.GROUP_MEMBER_SCOPE.PARTICIPANT
                            )
                              return !0;
                          })
                          .map(function (e) {
                            S.push(e.getUid());
                          }))
                    : (a
                        .filter(function (e) {
                          if (e.getScope() == Y.GROUP_MEMBER_SCOPE.ADMIN)
                            return !0;
                        })
                        .map(function (e) {
                          c.push(e.getUid());
                        }),
                      a
                        .filter(function (e) {
                          if (e.getScope() == Y.GROUP_MEMBER_SCOPE.MODERATOR)
                            return !0;
                        })
                        .map(function (e) {
                          u.push(e.getUid());
                        }),
                      a
                        .filter(function (e) {
                          if (e.getScope() == Y.GROUP_MEMBER_SCOPE.PARTICIPANT)
                            return !0;
                        })
                        .map(function (e) {
                          S.push(e.getUid());
                        }),
                      E.map(function (e) {
                        l.push(e);
                      }));
                  var r = {};
                  D.isFalsy(c) || (r = p({}, r, { admins: c })),
                    D.isFalsy(S) || (r = p({}, r, { participants: S })),
                    D.isFalsy(u) || (r = p({}, r, { moderators: u })),
                    D.isFalsy(l) || (r = p({}, r, { usersToBan: l })),
                    C.makeApiCall("addMemebersToGroup", { guid: i }, r).then(
                      function (t) {
                        var n = {};
                        Object.keys(t.data.admins).map(function (e) {
                          t.data.admins[e][
                            Y.ResponseConstants.RESPONSE_KEYS
                              .GROUP_MEMBERS_RESPONSE.SUCCESS
                          ]
                            ? (n[e] =
                                Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.SUCCESS)
                            : (n[e] =
                                t.data.admins[e][
                                  Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.ERROR
                                ][
                                  Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.MESSAGE
                                ]);
                        }),
                          Object.keys(t.data.participants).map(function (e) {
                            t.data.participants[e][
                              Y.ResponseConstants.RESPONSE_KEYS
                                .GROUP_MEMBERS_RESPONSE.SUCCESS
                            ]
                              ? (n[e] =
                                  Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.SUCCESS)
                              : (n[e] =
                                  t.data.participants[e][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.ERROR
                                  ][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.MESSAGE
                                  ]);
                          }),
                          Object.keys(t.data.moderators).map(function (e) {
                            t.data.moderators[e][
                              Y.ResponseConstants.RESPONSE_KEYS
                                .GROUP_MEMBERS_RESPONSE.SUCCESS
                            ]
                              ? (n[e] =
                                  Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.SUCCESS)
                              : (n[e] =
                                  t.data.moderators[e][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.ERROR
                                  ][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.MESSAGE
                                  ]);
                          }),
                          Object.keys(t.data.usersToBan).map(function (e) {
                            t.data.usersToBan[e][
                              Y.ResponseConstants.RESPONSE_KEYS
                                .GROUP_MEMBERS_RESPONSE.SUCCESS
                            ]
                              ? (n[e] =
                                  Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.SUCCESS)
                              : (n[e] =
                                  t.data.usersToBan[e][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.ERROR
                                  ][
                                    Y.ResponseConstants.RESPONSE_KEYS.GROUP_MEMBERS_RESPONSE.MESSAGE
                                  ]);
                          }),
                          e(n);
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.transferGroupOwnership = function (s, r) {
              return new Promise(function (n, t) {
                try {
                  var e = D.validateId(s, "group");
                  if (e instanceof U.CometChatException) return void t(e);
                  var o = D.validateId(r, "user");
                  if (o instanceof U.CometChatException) return void t(o);
                  C.makeApiCall(
                    "transferOwnership",
                    { guid: s },
                    { owner: r }
                  ).then(
                    function (e) {
                      var t;
                      (t =
                        e && e.data && e.data.message
                          ? e.data.message
                          : "Ownership transferred to user " +
                            r +
                            " for the group with guid " +
                            s +
                            "."),
                        n(t);
                    },
                    function (e) {
                      t(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.initiateCall = function (t) {
              var s = this;
              return new Promise(function (n, o) {
                var e = s.getActiveCall();
                if (null === e)
                  try {
                    D.isFalsy(JSON.parse(JSON.stringify(t)).sender)
                      ? D.isFalsy(e)
                        ? (t.setStatus(Y.CallConstants.CALL_STATUS.INITIATED),
                          (t.receiver = t.receiverId.toString()),
                          delete t.receiverId,
                          C.makeApiCall("createCallSession", {}, t).then(
                            function (e) {
                              var t = _.MessageController.trasformJSONMessge(
                                e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                              );
                              G.CallController.getInstance()
                                .initiateCall(t)
                                .then(function (e) {
                                  n(t);
                                })
                                .catch(function (e) {
                                  o(new U.CometChatException(e));
                                });
                            },
                            function (e) {
                              o(new U.CometChatException(e.error));
                            }
                          ))
                        : o(
                            new U.CometChatException(
                              Y.CALL_ERROR.ERROR_IN_CALLING
                            )
                          )
                      : G.CallController.getInstance()
                          .initiateCall(t)
                          .then(function (e) {
                            n(Object.assign(t));
                          })
                          .catch(function (e) {
                            o(new U.CometChatException(e));
                          });
                  } catch (e) {
                    o(new U.CometChatException(e));
                  }
                else
                  o(
                    new U.CometChatException(
                      Y.CALL_ERROR.CALL_ALREADY_INITIATED
                    )
                  );
              });
            }),
            (v.acceptCall = function (o) {
              var s = this;
              return new Promise(function (n, t) {
                if (null === s.getActiveCall())
                  try {
                    var e = {};
                    (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                      Y.CallConstants.CALL_STATUS.ONGOING),
                      C.makeApiCall(
                        "updateCallSession",
                        { sessionid: o },
                        e
                      ).then(
                        function (e) {
                          var t = _.MessageController.trasformJSONMessge(
                            e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                          );
                          G.CallController.getInstance().onCallStarted(t), n(t);
                        },
                        function (e) {
                          t(new U.CometChatException(e.error));
                        }
                      );
                  } catch (e) {
                    t(new U.CometChatException(e));
                  }
                else
                  t(new U.CometChatException(Y.CALL_ERROR.CANNOT_ACCEPT_CALL));
              });
            }),
            (v.rejectCall = function (e, t) {
              try {
                switch (t) {
                  case Y.CallConstants.CALL_STATUS.REJECTED:
                    return this.rejectIncomingCall(e);
                  case Y.CallConstants.CALL_STATUS.CANCELLED:
                    return this.cancelCall(e);
                  case Y.CallConstants.CALL_STATUS.BUSY:
                    return this.sendBusyResponse(e);
                  default:
                    return this.endCall(e, !0);
                }
              } catch (e) {
                D.Logger.error("CometChat: rejectCall", e);
              }
            }),
            (v.endCall = function (s, r) {
              var i = this;
              return new Promise(function (n, t) {
                D.isFalsy(r) && (r = !1);
                var o = i.getActiveCall();
                if (null !== o) {
                  if (o.getSessionId() === s)
                    try {
                      var e = {};
                      (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                        Y.CallConstants.CALL_STATUS.ENDED),
                        o.getJoinedAt() &&
                          (e[Y.CallConstants.CALL_KEYS.CALL_JOINED_AT] =
                            o.getJoinedAt()),
                        C.makeApiCall(
                          "updateCallSession",
                          { sessionid: s },
                          e
                        ).then(
                          function (e) {
                            r || G.CallController.getInstance().endSession();
                            var t = _.MessageController.trasformJSONMessge(
                              e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                            );
                            n(t),
                              G.CallController.getInstance().getCallListner() &&
                                G.CallController.getInstance()
                                  .getCallListner()
                                  ._eventListener.onCallEnded(t),
                              G.CallController.getInstance().endCall();
                          },
                          function (e) {
                            D.Logger.log("calling Log", { error: e }),
                              r || G.CallController.getInstance().endSession(),
                              o.setStatus(Y.CallConstants.CALL_STATUS.ENDED),
                              n(o),
                              G.CallController.getInstance().getCallListner() &&
                                G.CallController.getInstance()
                                  .getCallListner()
                                  ._eventListener.onCallEnded(o),
                              G.CallController.getInstance().endCall();
                          }
                        );
                    } catch (e) {
                      t(new U.CometChatException(e));
                    }
                } else r || G.CallController.getInstance().endSession(), n(null), G.CallController.getInstance().getCallListner() && G.CallController.getInstance().getCallListner()._eventListener.onCallEnded(null), G.CallController.getInstance().endCall();
              });
            }),
            (v.getActiveCall = function () {
              try {
                return G.CallController.getInstance().getActiveCall();
              } catch (e) {
                D.Logger.error("CometChat: getActiveCall", e);
              }
            }),
            (v.startCall = function (o, s, r, e) {
              var i = this;
              try {
                var a,
                  E,
                  c,
                  u,
                  S,
                  l,
                  p,
                  C,
                  T = this.getActiveCall(),
                  g = !1,
                  d = !0,
                  _ = !0,
                  h = !0,
                  A = !0,
                  I = !0,
                  f = !0,
                  R = !1,
                  O = !1,
                  N = Y.CallConstants.CALL_MODE.DEFAULT,
                  y = !1,
                  m = !1,
                  L = !1,
                  M = {},
                  P = this.user;
                D.getAppSettings().then(
                  function (e) {
                    if (
                      ((l = e[Y.APP_SETTINGS.KEYS.WEBRTC_HOST]),
                      e.hasOwnProperty(Y.APP_SETTINGS.KEYS.ANALYTICS_HOST)
                        ? (M[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST] =
                            e[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST])
                        : (M[Y.APP_SETTINGS.KEYS.ANALYTICS_HOST] = D.format(
                            Y.ANALYTICS.analyticsHost,
                            v.appSettings.getRegion()
                          )),
                      e.hasOwnProperty(Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION)
                        ? (M[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION] =
                            e[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION])
                        : (M[Y.APP_SETTINGS.KEYS.ANALYTICS_VERSION] =
                            Y.ANALYTICS.analyticsVersion),
                      e.hasOwnProperty(
                        Y.APP_SETTINGS.KEYS.ANALYTICS_PING_DISABLED
                      )
                        ? (M[Y.APP_SETTINGS.KEYS.ANALYTICS_PING_DISABLED] =
                            e[Y.APP_SETTINGS.KEYS.ANALYTICS_PING_DISABLED])
                        : (M[Y.APP_SETTINGS.KEYS.ANALYTICS_PING_DISABLED] = !1),
                      e.hasOwnProperty(Y.APP_SETTINGS.KEYS.ANALYTICS_USE_SSL)
                        ? (M[Y.APP_SETTINGS.KEYS.ANALYTICS_USE_SSL] =
                            e[Y.APP_SETTINGS.KEYS.ANALYTICS_USE_SSL])
                        : (M[Y.APP_SETTINGS.KEYS.ANALYTICS_USE_SSL] = !0),
                      D.isFalsy(r) ||
                        G.CallController.getInstance().setCallListner(r),
                      D.isFalsy(i.appSettings))
                    )
                      G.CallController.getInstance().getCallListner() &&
                        G.CallController.getInstance()
                          .getCallListner()
                          ._eventListener.onError(
                            new U.CometChatException(
                              Y.CALL_ERROR.NOT_INITIALIZED
                            )
                          );
                    else if ((u = i.appSettings.getRegion()))
                      if (P)
                        if (
                          ((C = new H.RTCUser(P.getUid())).setName(P.getName()),
                          C.setAvatar(P.getAvatar()),
                          C.setResource(v.getSessionId()),
                          v.appId)
                        )
                          if (((S = v.appId), "string" == typeof o)) {
                            if (T) {
                              var t = T.getType();
                              (g = t === Y.CallConstants.CALL_TYPE.AUDIO),
                                (E = T.getSessionId());
                            } else {
                              if (D.isFalsy(o))
                                return void (
                                  G.CallController.getInstance().getCallListner() &&
                                  G.CallController.getInstance()
                                    .getCallListner()
                                    ._eventListener.onError(
                                      new U.CometChatException(
                                        Y.CALL_ERROR.SESSION_ID_REQUIRED
                                      )
                                    )
                                );
                              E = (
                                "v1." +
                                u +
                                "." +
                                v.getAppId() +
                                "." +
                                o
                              ).toLowerCase();
                            }
                            if (!E)
                              return void (
                                G.CallController.getInstance().getCallListner() &&
                                G.CallController.getInstance()
                                  .getCallListner()
                                  ._eventListener.onError(
                                    new U.CometChatException(
                                      Y.CALL_ERROR.SESSION_ID_REQUIRED
                                    )
                                  )
                              );
                            var n = { uid: P.getUid(), sessionId: E };
                            v.getJWT(n).then(
                              function (e) {
                                e.hasOwnProperty("token")
                                  ? (C.setJWT(e.token),
                                    (a = new J.CallSettingsBuilder()
                                      .setSessionID(E)
                                      .enableDefaultLayout(d)
                                      .setIsAudioOnlyCall(g)
                                      .setUser(C)
                                      .setRegion(u)
                                      .setAppId(S)
                                      .setDomain(l)
                                      .showEndCallButton(A)
                                      .showMuteAudioButton(h)
                                      .showPauseVideoButton(_)
                                      .showScreenShareButton(I)
                                      .showModeButton(f)
                                      .setMode(N)
                                      .setAnalyticsSettings(M)
                                      .startWithAudioMuted(R)
                                      .startWithVideoMuted(O)
                                      .showRecordingButton(y)
                                      .startRecordingOnCallStart(m)
                                      .forceLegacyUI(L)
                                      .build()),
                                    G.CallController.getInstance().startCall(
                                      a,
                                      s
                                    ))
                                  : G.CallController.getInstance().getCallListner() &&
                                    G.CallController.getInstance()
                                      .getCallListner()
                                      ._eventListener.onError(
                                        new U.CometChatException(
                                          Y.CALL_ERROR.JWT_NOT_FOUND
                                        )
                                      );
                              },
                              function (e) {
                                G.CallController.getInstance().getCallListner() &&
                                  G.CallController.getInstance()
                                    .getCallListner()
                                    ._eventListener.onError(
                                      new U.CometChatException(e)
                                    );
                              }
                            );
                          } else {
                            if (D.isFalsy(o))
                              return void (
                                G.CallController.getInstance().getCallListner() &&
                                G.CallController.getInstance()
                                  .getCallListner()
                                  ._eventListener.onError(
                                    new U.CometChatException(
                                      Y.CALL_ERROR.CALL_SETTINGS_REQUIRED
                                    )
                                  )
                              );
                            if (T) {
                              t = T.getType();
                              (g = t === Y.CallConstants.CALL_TYPE.AUDIO),
                                (E = T.getSessionId());
                            } else {
                              if (
                                ((g = o.isAudioOnlyCall()),
                                D.isFalsy(o.getSessionId()))
                              )
                                return void (
                                  G.CallController.getInstance().getCallListner() &&
                                  G.CallController.getInstance()
                                    .getCallListner()
                                    ._eventListener.onError(
                                      new U.CometChatException(
                                        Y.CALL_ERROR.SESSION_ID_REQUIRED
                                      )
                                    )
                                );
                              E = (
                                "v1." +
                                u +
                                "." +
                                v.getAppId() +
                                "." +
                                o.getSessionId()
                              ).toLowerCase();
                            }
                            if (!E)
                              return void (
                                G.CallController.getInstance().getCallListner() &&
                                G.CallController.getInstance()
                                  .getCallListner()
                                  ._eventListener.onError(
                                    new U.CometChatException(
                                      Y.CALL_ERROR.SESSION_ID_REQUIRED
                                    )
                                  )
                              );
                            (d = o.isDefaultLayoutEnabled()),
                              (_ = o.isPauseVideoButtonEnabled()),
                              (h = o.isMuteAudioButtonEnabled()),
                              (A = o.isEndCallButtonEnabled()),
                              (I = o.isScreenShareButtonEnabled()),
                              (N = o.getMode()),
                              (p = o.getLocalizedStringObject()),
                              (c = o.getCustomCSS()),
                              (f = o.isModeButtonEnabled()),
                              (R = o.getStartWithAudioMuted()),
                              (O = o.getStartWithVideoMuted()),
                              (y = o.isRecordingButtonEnabled()),
                              (m = o.shouldStartRecordingOnCallStart()),
                              (L = o.shouldUseLegacyUI());
                            n = { uid: P.getUid(), sessionId: E };
                            v.getJWT(n).then(
                              function (e) {
                                e.hasOwnProperty("token")
                                  ? (C.setJWT(e.token),
                                    (a = new J.CallSettingsBuilder()
                                      .setSessionID(E)
                                      .enableDefaultLayout(d)
                                      .setIsAudioOnlyCall(g)
                                      .setUser(C)
                                      .setRegion(u)
                                      .setAppId(S)
                                      .setDomain(l)
                                      .showEndCallButton(A)
                                      .showMuteAudioButton(h)
                                      .showPauseVideoButton(_)
                                      .showScreenShareButton(I)
                                      .showModeButton(f)
                                      .setMode(N)
                                      .setLocalizedStringObject(p)
                                      .setCustomCSS(c)
                                      .setAnalyticsSettings(M)
                                      .startWithAudioMuted(R)
                                      .startWithVideoMuted(O)
                                      .showRecordingButton(y)
                                      .startRecordingOnCallStart(m)
                                      .forceLegacyUI(L)
                                      .build()),
                                    G.CallController.getInstance().startCall(
                                      a,
                                      s
                                    ))
                                  : G.CallController.getInstance().getCallListner() &&
                                    G.CallController.getInstance()
                                      .getCallListner()
                                      ._eventListener.onError(
                                        new U.CometChatException(
                                          Y.CALL_ERROR.JWT_NOT_FOUND
                                        )
                                      );
                              },
                              function (e) {
                                G.CallController.getInstance().getCallListner() &&
                                  G.CallController.getInstance()
                                    .getCallListner()
                                    ._eventListener.onError(
                                      new U.CometChatException(e)
                                    );
                              }
                            );
                          }
                        else
                          G.CallController.getInstance().getCallListner() &&
                            G.CallController.getInstance()
                              .getCallListner()
                              ._eventListener.onError(
                                new U.CometChatException(
                                  Y.CALL_ERROR.NOT_INITIALIZED
                                )
                              );
                      else
                        G.CallController.getInstance().getCallListner() &&
                          G.CallController.getInstance()
                            .getCallListner()
                            ._eventListener.onError(
                              new U.CometChatException(
                                Y.CALL_ERROR.NOT_LOGGED_IN
                              )
                            );
                    else
                      G.CallController.getInstance().getCallListner() &&
                        G.CallController.getInstance()
                          .getCallListner()
                          ._eventListener.onError(
                            new U.CometChatException(
                              Y.CALL_ERROR.NOT_INITIALIZED
                            )
                          );
                  },
                  function (e) {
                    D.Logger.error("CometChat: startCall", e);
                  }
                );
              } catch (e) {
                D.Logger.error("CometChat: startCall", e);
              }
            }),
            (v.getCallParticipantCount = function (a, E) {
              var c = this;
              return new Promise(function (r, i) {
                try {
                  D.getAppSettings().then(
                    function (e) {
                      if (D.isFalsy(a))
                        return i(
                          new U.CometChatException(
                            Y.ProsodyApiErrors.INVALID_SESSIONID
                          )
                        );
                      if (D.isFalsy(E))
                        return i(
                          new U.CometChatException(
                            Y.ProsodyApiErrors.INVALID_TYPE
                          )
                        );
                      var t = c.appSettings.getRegion(),
                        n = {},
                        o = e[Y.APP_SETTINGS.KEYS.WEBRTC_HOST],
                        s = D.format(
                          new u.EndpointFactory().prosodyApi,
                          Y.PROSODY_API.DOMAIN_PREFIX,
                          o,
                          Y.PROSODY_API.PATH.ROOM_SIZE
                        );
                      "direct" === E.toLowerCase() &&
                        (a = (
                          "v1." +
                          t +
                          "." +
                          v.getAppId() +
                          "." +
                          a
                        ).toLowerCase()),
                        (n[Y.PROSODY_API.QUERY_PARAMETERS.DOMAIN] = o),
                        (n[Y.PROSODY_API.QUERY_PARAMETERS.ROOM] = a),
                        C.postData(s, "GET", n, {}, !1)
                          .then(function (e) {
                            return e.text();
                          })
                          .then(function (e) {
                            var t = e ? JSON.parse(e) : {};
                            return t.hasOwnProperty(
                              Y.PROSODY_API.RESPONSE.PARTICIPANTS
                            )
                              ? r(t[Y.PROSODY_API.RESPONSE.PARTICIPANTS])
                              : r(0);
                          })
                          .catch(function () {
                            var e = { error: I.FETCH_ERROR.ERROR_IN_API_CALL };
                            return i(e);
                          });
                    },
                    function (e) {
                      return i(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return i(new U.CometChatException(e));
                }
              });
            }),
            (v.rejectIncomingCall = function (o) {
              return new Promise(function (n, t) {
                try {
                  var e = {};
                  (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                    Y.CallConstants.CALL_STATUS.REJECTED),
                    C.makeApiCall(
                      "updateCallSession",
                      { sessionid: o },
                      e
                    ).then(
                      function (e) {
                        var t = _.MessageController.trasformJSONMessge(
                          e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                        );
                        n(t);
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.cancelCall = function (o) {
              var s = this;
              return new Promise(function (n, t) {
                try {
                  var e = {};
                  (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                    Y.CallConstants.CALL_STATUS.CANCELLED),
                    C.makeApiCall(
                      "updateCallSession",
                      { sessionid: o },
                      e
                    ).then(
                      function (e) {
                        var t = _.MessageController.trasformJSONMessge(
                          e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                        );
                        s.getActiveCall().getSessionId() === o &&
                          G.CallController.getInstance().endCallSession(),
                          n(t);
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.sendBusyResponse = function (o) {
              return new Promise(function (n, t) {
                try {
                  var e = {};
                  (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                    Y.CallConstants.CALL_STATUS.BUSY),
                    C.makeApiCall(
                      "updateCallSession",
                      { sessionid: o },
                      e
                    ).then(
                      function (e) {
                        var t = _.MessageController.trasformJSONMessge(
                          e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                        );
                        n(t);
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.sendUnansweredResponse = function (n) {
              return new Promise(function (o, t) {
                try {
                  var e = {};
                  (e[Y.CallConstants.CALL_KEYS.CALL_STATUS] =
                    Y.CallConstants.CALL_STATUS.UNANSWERED),
                    C.makeApiCall(
                      "updateCallSession",
                      { sessionid: n },
                      e
                    ).then(
                      function (e) {
                        var t = _.MessageController.trasformJSONMessge(
                            e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                          ),
                          n = Z.getCometChatEventFromMessage(
                            Object.assign(
                              t,
                              e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                            )
                          );
                        Z.publishMessages(n), o(t);
                      },
                      function (e) {
                        t(new U.CometChatException(e.error));
                      }
                    );
                } catch (e) {
                  t(new U.CometChatException(e));
                }
              });
            }),
            (v.addConnectionListener = function (e, t) {
              try {
                $.addConnectionEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addConnectionListener", e);
              }
            }),
            (v.removeConnectionListener = function (e) {
              try {
                $.removeConnectionEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeConnectionListener", e);
              }
            }),
            (v.addMessageListener = function (e, t) {
              try {
                $.addMessageEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addMessageListener", e);
              }
            }),
            (v.removeMessageListener = function (e) {
              try {
                $.removeMessageEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeMessageListener", e);
              }
            }),
            (v.addCallListener = function (e, t) {
              try {
                $.addCallEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addCallListener", e);
              }
            }),
            (v.removeCallListener = function (e) {
              try {
                $.removeCallEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeCallListener", e);
              }
            }),
            (v.addUserListener = function (e, t) {
              try {
                $.addUserEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addUserListener", e);
              }
            }),
            (v.removeUserListener = function (e) {
              try {
                $.removeUserEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeUserListener", e);
              }
            }),
            (v.addGroupListener = function (e, t) {
              try {
                $.addGroupEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addGroupListener", e);
              }
            }),
            (v.removeGroupListener = function (e) {
              try {
                $.removeGroupEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeGroupListener", e);
              }
            }),
            (v.addLoginListener = function (e, t) {
              try {
                $.addLoginEventListener(e, t);
              } catch (e) {
                D.Logger.error("CometChat: addLoginListener", e);
              }
            }),
            (v.removeLoginListener = function (e) {
              try {
                $.removeLoginEventListener(e);
              } catch (e) {
                D.Logger.error("CometChat: removeLoginListener", e);
              }
            }),
            (v.generateAuthToken = function (S) {
              var l = this;
              return new Promise(function (o, s) {
                try {
                  var r = {},
                    i = "",
                    a = "",
                    E = Y.APPINFO.platform,
                    c = Y.APPINFO.sdkVersion,
                    u = Y.APPINFO.apiVersion;
                  navigator && (a = navigator.userAgent),
                    v.keyStore.get(Y.LOCAL_STORE.KEY_DEVICE_ID).then(
                      function (e) {
                        if (null == (i = e)) {
                          var t = z(),
                            n = new Date().getTime();
                          (i = l.appId + "_" + t + "_" + n),
                            v.keyStore.set(Y.LOCAL_STORE.KEY_DEVICE_ID, i);
                        }
                        (r = {
                          platform: E,
                          deviceId: i,
                          appInfo: { version: c, apiVersion: u, userAgent: a },
                        }),
                          C.makeApiCall("authToken", { uid: S }, r)
                            .then(function (e) {
                              o(e.data);
                            })
                            .catch(function (e) {
                              s(new U.CometChatException(e.error));
                            });
                      },
                      function (e) {
                        D.Logger.error(
                          "Got error while fetching data from key store",
                          e
                        );
                      }
                    );
                } catch (e) {
                  s(new U.CometChatException(e));
                }
              });
            }),
            (v.setAccidentalDisconnect = function (e) {
              v.accidentalDisconnect = e;
            }),
            (v.generateNewSessionId = function () {
              (v.sessionId =
                Y.APPINFO.platform +
                "-" +
                Y.APPINFO.sdkVersionWithUnderScore +
                "-" +
                z() +
                "-" +
                new Date().getTime()),
                v.addDataToSessionStorage(
                  Y.SESSION_STORE.SESSION_ID,
                  v.getSessionId()
                );
            }),
            (v.tryReconnectingToWSOrStartPolling = function () {
              0 === this.WSReconnectionStartTime &&
                (this.WSReconnectionStartTime = new Date().getTime()),
                v.WSReconnectionInProgress || v.startWSReconnectionTimer(),
                v.isWSCheckTimerStarted || v.startWSCheckTimer();
            }),
            (v.startPolling = function (e) {
              if (
                ((v.currentConnectionStatus =
                  Y.CONNECTION_STATUS.FEATURE_THROTTLED),
                !v.didMessagesPollingStart())
              ) {
                $.connectionHandlers.map(function (e) {
                  try {
                    e._eventListener &&
                      (D.isFalsy(e._eventListener.onFeatureThrottled) ||
                        e._eventListener.onFeatureThrottled());
                  } catch (e) {
                    D.Logger.error(
                      "ConnectionHandlers: Feature Throttled Status",
                      e
                    );
                  }
                });
                var t = void 0;
                if (e) {
                  var n = v.getLastMessageId();
                  0 === (v.pollingMessagesId = n || 0) &&
                    (t = v.getSuccessfultTimeStamp());
                } else
                  t =
                    0 < v.WSReconnectionStartTime
                      ? v.WSReconnectionStartTime
                      : new Date().getTime();
                (v.pollingEnabledInternally = !0),
                  (v.pollingEnabled = !0),
                  (v.pollingMessagesTimestamp = Math.floor(t / 1e3)),
                  v.fetchMessages(),
                  v.startMessagesTimer();
              }
            }),
            (v.prototype.makeWSConnection = function () {
              v.setAccidentalDisconnect(!1), v.WSLogin(v.user);
            }),
            (v.prototype.accidentallyDisconnected = function () {
              (v.currentConnectionStatus = Y.CONNECTION_STATUS.CONNECTING),
                $.connectionHandlers.map(function (e) {
                  try {
                    e._eventListener &&
                      (D.isFalsy(e._eventListener.inConnecting) ||
                        e._eventListener.inConnecting());
                  } catch (e) {
                    D.Logger.error(
                      "ConnectionHandlers: inConnecting Status",
                      e
                    );
                  }
                }),
                v.setAccidentalDisconnect(!0),
                v.tryReconnectingToWSOrStartPolling();
            }),
            (v.WSLogin = function (e) {
              var n = this;
              Z.WSLogin(e.getJWT(), function (e) {
                switch (e) {
                  case W.READY_STATE.CONNECTING:
                    var t = v.getConnectionStatus();
                    (n.currentConnectionStatus =
                      Y.CONNECTION_STATUS.CONNECTING),
                      t == Y.CONNECTION_STATUS.DISCONNECTED &&
                        $.connectionHandlers.map(function (e) {
                          try {
                            e._eventListener &&
                              (D.isFalsy(e._eventListener.inConnecting) ||
                                e._eventListener.inConnecting());
                          } catch (e) {
                            D.Logger.error(
                              "connectionHandlers: Connecting Status",
                              e
                            );
                          }
                        });
                    break;
                  case W.READY_STATE.OPEN:
                    t = v.getConnectionStatus();
                    (n.currentConnectionStatus = Y.CONNECTION_STATUS.CONNECTED),
                      t == Y.CONNECTION_STATUS.CONNECTING &&
                        $.connectionHandlers.map(function (e) {
                          try {
                            e._eventListener &&
                              (D.isFalsy(e._eventListener.onConnected) ||
                                e._eventListener.onConnected());
                          } catch (e) {
                            D.Logger.error(
                              "connectionHandlers: Connected Status",
                              e
                            );
                          }
                        }),
                      (v.WSReconnectionStartTime = 0),
                      (v.pollingMessagesId = 0),
                      (v.pollingEnabled = !1),
                      (v.pollingEnabledInternally = !1),
                      (v.WSCurrentReconnectionCount = 0),
                      v.setAccidentalDisconnect(!1),
                      v.didMessagesPollingStart() && v.clearMessagesTimer(),
                      v.WSReconnectionInProgress &&
                        v.clearWSReconnectionTimer(),
                      v.isWSCheckTimerStarted && v.clearWSCheckTimer();
                    break;
                  case W.READY_STATE.CLOSING:
                    break;
                  case W.READY_STATE.CLOSED:
                    t = v.getConnectionStatus();
                    (n.currentConnectionStatus =
                      Y.CONNECTION_STATUS.DISCONNECTED),
                      t !== Y.CONNECTION_STATUS.DISCONNECTED &&
                        ($.connectionHandlers.map(function (e) {
                          try {
                            e._eventListener &&
                              (D.isFalsy(e._eventListener.onDisconnected) ||
                                e._eventListener.onDisconnected());
                          } catch (e) {
                            D.Logger.error(
                              "connectionHandlers: Disconnected Status",
                              e
                            );
                          }
                        }),
                        v.isLoggedOut || n.tryReconnectingToWSOrStartPolling());
                }
              });
            }),
            (v.fetchMessages = function () {
              var e = this;
              try {
                var t = {
                  per_page: v.pollingLimit,
                  affix: v.pollingAffix,
                  polling: 1,
                };
                v.pollingMessagesId
                  ? (t.id = v.pollingMessagesId)
                  : (t.sentAt = v.pollingMessagesTimestamp),
                  C.makeApiCall("getMessages", {}, t)
                    .then(
                      function (i) {
                        return E(e, void 0, void 0, function () {
                          var t, n, o, s, r;
                          return c(this, function (e) {
                            switch (e.label) {
                              case 0:
                                if (((t = i.data), !(0 < (n = i.data.length))))
                                  return [3, 4];
                                (v.pollingMessagesId = t[n - 1].id),
                                  (o = Math.floor(v.pollingInterval / n)),
                                  (s = 0),
                                  (e.label = 1);
                              case 1:
                                return s < n
                                  ? v.isLoggedOut
                                    ? [3, 3]
                                    : (t[s] &&
                                        t[s][Y.MessageConstatnts.KEYS.DATA] &&
                                        t[s][Y.MessageConstatnts.KEYS.DATA][
                                          Y.MessageConstatnts.KEYS.RESOURCE
                                        ] &&
                                        t[s][Y.MessageConstatnts.KEYS.DATA][
                                          Y.MessageConstatnts.KEYS.RESOURCE
                                        ] !== v.getSessionId() &&
                                        v.getConnectionStatus() !==
                                          Y.CONNECTION_STATUS.CONNECTED &&
                                        (v.getLastMessageId() <= t[s].id &&
                                          v.setLastMessageId(t[s].id),
                                        m.MessageListnerMaping.getInstance().set(
                                          "all",
                                          parseInt(t[s].id)
                                        ),
                                        (r = Z.getCometChatEventFromMessage(
                                          t[s]
                                        )),
                                        Z.publishMessages(r)),
                                      s < n - 1 ? [4, v.timer(o)] : [3, 3])
                                  : [3, 4];
                              case 2:
                                e.sent(), (e.label = 3);
                              case 3:
                                return s++, [3, 1];
                              case 4:
                                return [2];
                            }
                          });
                        });
                      },
                      function (e) {
                        D.Logger.error(
                          "CometChat: fetchMessages: API Error",
                          e
                        );
                      }
                    )
                    .catch(function (e) {
                      D.Logger.error("CometChat: fetchMessages: API Error", e);
                    });
              } catch (e) {
                D.Logger.error("CometChat: fetchMessages", e);
              }
            }),
            (v.fetchMissedMessages = function () {
              var e = this;
              try {
                var t = {
                  per_page: 1e3,
                  affix: Y.MessageConstatnts.PAGINATION.AFFIX.APPEND,
                  missedMessages: 1,
                };
                0 < v.getLastMessageId()
                  ? (t.id = v.getLastMessageId())
                  : 0 < v.getSuccessfultTimeStamp()
                  ? (t.sentAt = Math.floor(v.getSuccessfultTimeStamp() / 1e3))
                  : (t.sentAt = Math.floor(new Date().getTime() / 1e3)),
                  C.makeApiCall("getMessages", {}, t)
                    .then(
                      function (r) {
                        return E(e, void 0, void 0, function () {
                          var t, n, o, s;
                          return c(this, function (e) {
                            if (((t = r.data), 0 < (n = r.data.length)))
                              for (
                                v.pollingMessagesId = t[n - 1].id, o = 0;
                                o < n;
                                o++
                              )
                                v.isLoggedOut ||
                                  (t[o] &&
                                    t[o][Y.MessageConstatnts.KEYS.DATA] &&
                                    t[o][Y.MessageConstatnts.KEYS.DATA][
                                      Y.MessageConstatnts.KEYS.RESOURCE
                                    ] &&
                                    t[o][Y.MessageConstatnts.KEYS.DATA][
                                      Y.MessageConstatnts.KEYS.RESOURCE
                                    ] !== v.getSessionId() &&
                                    (v.getLastMessageId() <= t[o].id &&
                                      v.setLastMessageId(t[o].id),
                                    m.MessageListnerMaping.getInstance().set(
                                      "all",
                                      parseInt(t[o].id)
                                    ),
                                    t[o][Y.MessageConstatnts.KEYS.CATEGORY] !==
                                      Y.MessageConstatnts.CATEGORY.CALL ||
                                    t[o][Y.MessageConstatnts.KEYS.DATA][
                                      Y.MessageConstatnts.KEYS.ACTION
                                    ] !== Y.CallConstants.CALL_STATUS.INITIATED
                                      ? ((s = Z.getCometChatEventFromMessage(
                                          t[o]
                                        )),
                                        Z.publishMessages(s))
                                      : D.Logger.info(
                                          "CometChat: fetchMissedMessages",
                                          "Call Initiated Message"
                                        )));
                            return [2];
                          });
                        });
                      },
                      function (e) {
                        D.Logger.error(
                          "CometChat: fetchMissedMessages: API Error",
                          e
                        );
                      }
                    )
                    .catch(function (e) {
                      D.Logger.error(
                        "CometChat: fetchMissedMessages: API Error",
                        e
                      );
                    });
              } catch (e) {
                D.Logger.error("CometChat: fetchMissedMessages", e);
              }
            }),
            (v.pingAnalytics = function () {
              var E = this;
              try {
                v.keyStore.get("deviceId").then(function (e) {
                  var t = null;
                  window &&
                    window.location &&
                    window.location.origin &&
                    (t = window.location.origin);
                  var n = "",
                    o = e,
                    s = {
                      version: Y.SDKHeader.sdkVersion,
                      apiVersion: Y.APPINFO.apiVersion,
                      origin: t,
                      uts: new Date().getTime(),
                    };
                  D.isFalsy(E.resource) || (s.resource = E.resource),
                    D.isFalsy(E.platform) || (s.platform = E.platform),
                    D.isFalsy(E.language) || (s.language = E.language),
                    navigator && (n = navigator.userAgent);
                  var r = {
                    appInfo: s,
                    uid: v.user.getUid(),
                    userAgent: n,
                    deviceId: o,
                    platform: Y.SDKHeader.platform,
                  };
                  D.isFalsy(v.getSessionId()) || (r.wsId = v.getSessionId()),
                    v.analyticsHost ||
                      (v.analyticsHost = D.format(
                        Y.ANALYTICS.analyticsHost,
                        v.appSettings.getRegion()
                      )),
                    v.analyticsVersion ||
                      (v.analyticsVersion = Y.ANALYTICS.analyticsVersion);
                  var i =
                      "https://" +
                      v.analyticsHost +
                      "/" +
                      v.analyticsVersion +
                      "/ping",
                    a = {
                      appId: v.appId,
                      sdk: D.format(
                        Y.SDKHeader.sdk,
                        Y.SDKHeader.platform,
                        Y.SDKHeader.sdkVersion
                      ),
                      "Content-Type": "application/json",
                    };
                  v.settingsHash && (a.settingsHash = v.settingsHash),
                    v.settingsHashReceivedAt &&
                      (a.settingsHashReceivedAt = v.settingsHashReceivedAt),
                    v.jwt && (a.Authorization = "Bearer " + v.jwt),
                    v.authToken && (a.authToken = v.authToken),
                    C.postData(i, "POST", r, a, !1)
                      .then(function (e) {
                        return e.json();
                      })
                      .then(function (e) {
                        if (
                          e.hasOwnProperty(
                            Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA
                          )
                        )
                          D.Logger.log(
                            "Analytics Ping Request Data",
                            e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA]
                          );
                        else if (
                          e.hasOwnProperty(
                            Y.ResponseConstants.RESPONSE_KEYS.KEY_ERROR
                          )
                        ) {
                          var t =
                            e[Y.ResponseConstants.RESPONSE_KEYS.KEY_ERROR];
                          D.Logger.log(
                            "Analytics Ping Request Error",
                            new U.CometChatException(t)
                          );
                          var n = t.code;
                          if (n === Y.Errors.ERR_SETTINGS_HASH_OUTDATED) {
                            var o = v.authToken;
                            v.getInstance()
                              .internalLogout(!1)
                              .then(function () {
                                (v.internalRestart = !0),
                                  v.login(o).then(function (e) {
                                    v.internalRestart = !1;
                                  });
                              });
                          }
                          n === Y.Errors.ERR_NO_AUTH && E.updateJWT();
                        }
                      })
                      .catch(function (e) {
                        D.Logger.error(
                          "CometChat: pingAnalytics Fetch Error",
                          e
                        );
                      });
                });
              } catch (e) {
                D.Logger.error("CometChat: pingAnalytics", e);
              }
            }),
            (v.updateJWT = function () {
              C.makeApiCall("getMyDetails", {}, {}, !1)
                .then(
                  function (e) {
                    var t = e.data,
                      n = t.settings,
                      o = new T.Me(t);
                    if (
                      (t.hasOwnProperty("jwt") && t.jwt && (v.jwt = t.jwt),
                      g.LocalStorage.getInstance().set("user", o),
                      n &&
                        n[Y.APP_SETTINGS.KEYS.SETTINGS_HASH] &&
                        v.settingsHash !== n[Y.APP_SETTINGS.KEYS.SETTINGS_HASH])
                    ) {
                      var s = v.getInstance().getAuthToken();
                      v.getInstance()
                        .internalLogout(!1)
                        .then(function () {
                          (v.internalRestart = !0),
                            v.login(s).then(function (e) {
                              v.internalRestart = !1;
                            });
                        });
                    }
                    D.Logger.log("CometChat: updateJWT response", e);
                  },
                  function (e) {
                    D.Logger.error("CometChat: updateJWT Fetch Error", e);
                  }
                )
                .catch(function (e) {
                  D.Logger.error("CometChat: updateJWT", e);
                });
            }),
            (v.startMessagesTimer = function () {
              var e = this;
              (v.isMessagesPollingStarted = !0),
                (v.messagesTimer = setInterval(function () {
                  try {
                    e.fetchMessages();
                  } catch (e) {
                    D.Logger.error("CometChat: startMessagesTimer", e);
                  }
                }, v.pollingInterval));
            }),
            (v.timer = function (t) {
              return new Promise(function (e) {
                return setTimeout(e, t);
              });
            }),
            (v.startAnalyticsPingTimer = function () {
              var e = this;
              (v.isAnalyticsPingStarted = !0),
                (v.analyticsPingTimer = setInterval(function () {
                  try {
                    e.pingAnalytics();
                  } catch (e) {
                    D.Logger.error("CometChat: startAnalyticsPingTimer", e);
                  }
                }, v.settingsInterval));
            }),
            (v.clearMessagesTimer = function () {
              try {
                (v.isMessagesPollingStarted = !1),
                  clearInterval(v.messagesTimer);
              } catch (e) {
                D.Logger.error("CometChat: clearMessagesTimer", e);
              }
            }),
            (v.clearAnalyticsPingTimer = function () {
              try {
                (v.isAnalyticsPingStarted = !1),
                  clearInterval(v.analyticsPingTimer);
              } catch (e) {
                D.Logger.error("CometChat: clearAnalyticsPingTimer", e);
              }
            }),
            (v.startWSReconnectionTimer = function () {
              (v.WSReconnectionInProgress = !0),
                (v.WSReconnectionTimer = setInterval(function () {
                  try {
                    v.WSLogin(v.user);
                  } catch (e) {
                    D.Logger.error("CometChat: startWSReconnectionTimer", e);
                  }
                }, v.WSReconnectionTimerInterval));
            }),
            (v.clearWSReconnectionTimer = function () {
              (v.WSReconnectionInProgress = !1),
                clearInterval(v.WSReconnectionTimer);
            }),
            (v.getJWT = function (r) {
              return new Promise(function (o, s) {
                try {
                  X.getEndPoint("getJWT").then(
                    function (e) {
                      var t = {
                          appId: v.appId,
                          Accept: "application/json",
                          authToken: v.authToken,
                          resource: v.getSessionId(),
                          sdk: D.format(
                            Y.SDKHeader.sdk,
                            Y.SDKHeader.platform,
                            Y.SDKHeader.sdkVersion
                          ),
                          "Content-Type": "application/json",
                        },
                        n = {};
                      (n[Y.JWT_API.KEYS.PASSTHROUGH] = r),
                        C.postData(e.endpoint, e.method, n, t, !1)
                          .then(function (e) {
                            return e.json();
                          })
                          .then(function (e) {
                            e.hasOwnProperty(
                              Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA
                            )
                              ? o(e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA])
                              : s(new U.CometChatException(e.error));
                          })
                          .catch(function (e) {
                            var t = { error: I.FETCH_ERROR.ERROR_IN_API_CALL };
                            s(new U.CometChatException(t));
                          });
                    },
                    function (e) {
                      s(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  s(new U.CometChatException(e));
                }
              });
            }),
            (v.startWSCheckTimer = function () {
              (v.isWSCheckTimerStarted = !0),
                (v.wsCheckTimer = setTimeout(function () {
                  try {
                    v.shouldFallBackToPolling &&
                      v.startPolling(!!v.accidentalDisconnect);
                  } catch (e) {
                    D.Logger.error("CometChat: startMessagesTimer", e);
                  }
                }, v.wsCheckTimerDuration));
            }),
            (v.clearWSCheckTimer = function () {
              (v.isWSCheckTimerStarted = !1), clearTimeout(v.wsCheckTimer);
            }),
            (v.getConnectionStatus = function () {
              return this.currentConnectionStatus;
            }),
            (v.prototype.setConnectionStatus = function (e) {
              v.currentConnectionStatus = e;
            }),
            (v.isExtensionEnabled = function (r) {
              return new Promise(function (o, s) {
                try {
                  if (D.isFalsy(r))
                    return s(
                      new U.CometChatException(
                        Y.ExtensionErrors.INVALID_EXTENSION
                      )
                    );
                  D.getAppSettings().then(
                    function (e) {
                      if (e.extensions) {
                        var t = e.extensions;
                        if (0 < t.length) {
                          var n = t.some(function (e) {
                            return e.id === r;
                          });
                          return o(n);
                        }
                        return o(!1);
                      }
                      return s(
                        new U.CometChatException(
                          Y.ExtensionErrors.EXTENSION_NOT_FOUND
                        )
                      );
                    },
                    function (e) {
                      return s(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return s(new U.CometChatException(e));
                }
              });
            }),
            (v.getExtensionDetails = function (i) {
              return new Promise(function (s, r) {
                try {
                  if (D.isFalsy(i))
                    return r(
                      new U.CometChatException(
                        Y.ExtensionErrors.INVALID_EXTENSION
                      )
                    );
                  D.getAppSettings().then(
                    function (e) {
                      if (e.extensions) {
                        var t = e.extensions;
                        if (0 < t.length) {
                          var n = t.filter(function (e) {
                            return e.id === i;
                          });
                          if (0 < n.length) {
                            var o = new j.CCExtension(n[0]);
                            return s(o);
                          }
                          return r(
                            new U.CometChatException(
                              Y.ExtensionErrors.EXTENSION_NOT_FOUND
                            )
                          );
                        }
                        return r(
                          new U.CometChatException(
                            Y.ExtensionErrors.EXTENSION_NOT_FOUND
                          )
                        );
                      }
                      return r(
                        new U.CometChatException(
                          Y.ExtensionErrors.EXTENSION_NOT_FOUND
                        )
                      );
                    },
                    function (e) {
                      return r(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return r(new U.CometChatException(e));
                }
              });
            }),
            (v.getAppSettings = function () {
              return new Promise(function (t, n) {
                try {
                  C.makeApiCall("appSettings").then(
                    function (e) {
                      g.LocalStorage.getInstance().set(
                        Y.LOCAL_STORE.KEY_APP_SETTINGS,
                        e.data
                      ),
                        t(e.data);
                    },
                    function (e) {
                      n(new U.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.isFeatureEnabled = function (s) {
              return new Promise(function (n, o) {
                try {
                  if (D.isFalsy(s))
                    return o(
                      new U.CometChatException(
                        Y.FeatureRestrictionErrors.INVALID_FEATURE
                      )
                    );
                  D.getAppSettings().then(
                    function (e) {
                      if (e.parameters) {
                        var t = e.parameters;
                        return t.hasOwnProperty(s) ? n(t[s]) : n(!1);
                      }
                      return o(
                        new U.CometChatException(
                          Y.FeatureRestrictionErrors.FEATURE_NOT_FOUND
                        )
                      );
                    },
                    function (e) {
                      return o(new U.CometChatException(e));
                    }
                  );
                } catch (e) {
                  return o(new U.CometChatException(e));
                }
              });
            }),
            (v.logout = function () {
              var o = this;
              return new Promise(function (t, n) {
                try {
                  v.didAnalyticsPingStart() && v.clearAnalyticsPingTimer(),
                    v.didMessagesPollingStart() && v.clearMessagesTimer(),
                    v.WSReconnectionInProgress && v.clearWSReconnectionTimer(),
                    v.isWSCheckTimerStarted && v.clearWSCheckTimer(),
                    (v.isLoggedOut = !0),
                    (v.pollingMessagesId = 0),
                    (v.pollingEnabled = !1),
                    (v.pollingInterval = 5e3),
                    (v.pollingEnabledInternally = !1),
                    (v.WSReconnectionStartTime = 0),
                    (v.WSReconnectionInProgress = !1),
                    v.setLastMessageId(0),
                    v.setAccidentalDisconnect(!1),
                    (v.isAnalyticsDisabled = !1),
                    (v.shouldFallBackToPolling = !0),
                    C.makeApiCall("userLogout").then(
                      function (e) {
                        o.clearCache().then(function () {
                          (v.apiKey = void 0),
                            (v.user = void 0),
                            (v.authToken = void 0),
                            (v.cometChat = void 0),
                            (v.mode = void 0),
                            Z.WSLogout(),
                            v.pushToLoginListener("", "Logout_Success"),
                            t(e.data);
                        });
                      },
                      function (e) {
                        o.clearCache().then(function () {
                          (v.apiKey = void 0),
                            (v.user = void 0),
                            (v.authToken = void 0),
                            (v.cometChat = void 0),
                            (v.mode = void 0),
                            Z.WSLogout(),
                            v.pushToLoginListener("", "Logout_Success"),
                            new U.CometChatException(e.error).code ==
                            I.SERVER_ERRORS.AUTH_ERR.code
                              ? t({})
                              : n(new U.CometChatException(e.error));
                        });
                      }
                    );
                } catch (e) {
                  n(new U.CometChatException(e));
                }
              });
            }),
            (v.callExtension = function (s, r, i, a) {
              return (
                void 0 === a && (a = {}),
                new Promise(function (t, n) {
                  var e = D.format(
                      new u.EndpointFactory().extensionApi,
                      s,
                      v.appSettings.getRegion(),
                      i
                    ),
                    o = {
                      appId: v.appId,
                      Accept: "application/json",
                      authToken: v.authToken,
                      resource: v.getSessionId(),
                      sdk: D.format(
                        Y.SDKHeader.sdk,
                        Y.SDKHeader.platform,
                        Y.SDKHeader.sdkVersion
                      ),
                      chatApiVersion: Y.APPINFO.apiVersion,
                      "Content-Type": "application/json",
                    };
                  C.postData(e, r, a, o, !1)
                    .then(function (e) {
                      return e.json();
                    })
                    .then(function (e) {
                      e.hasOwnProperty(
                        Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA
                      )
                        ? t(e[Y.ResponseConstants.RESPONSE_KEYS.KEY_DATA])
                        : n(new U.CometChatException(e.error));
                    })
                    .catch(function (e) {
                      var t = { error: I.FETCH_ERROR.ERROR_IN_API_CALL };
                      n(t);
                    });
                })
              );
            }),
            (v.setSource = function (e, t, n) {
              D.isFalsy(e) || (this.resource = e),
                D.isFalsy(t) || (this.platform = t),
                D.isFalsy(n) || (this.language = n);
            }),
            (v.clearCache = function () {
              return new Promise(function (e, t) {
                try {
                  g.LocalStorage.getInstance()
                    .clearStore()
                    .then(function () {
                      v.removeDataFromSessionStorage(
                        Y.SESSION_STORE.SESSION_ID
                      ),
                        D.Logger.info(
                          "CometChat: clearCache => All store cleared successfully",
                          "true"
                        ),
                        e(!0);
                    });
                } catch (e) {
                  D.Logger.error("CometChat: clearCache", e), t(e);
                }
              });
            }),
            (v.prototype.restart = function (e) {
              (v.shouldPushLoginListener = !1),
                v.logout().then(function () {
                  v.login(e).then(
                    function () {
                      v.shouldPushLoginListener = !0;
                    },
                    function () {
                      v.shouldPushLoginListener = !0;
                    }
                  );
                });
            }),
            (v.prototype.internalLogout = function (n) {
              return (
                void 0 === n && (n = !0),
                new Promise(function (e, t) {
                  try {
                    v.didAnalyticsPingStart() && v.clearAnalyticsPingTimer(),
                      v.didMessagesPollingStart() && v.clearMessagesTimer(),
                      v.WSReconnectionInProgress &&
                        v.clearWSReconnectionTimer(),
                      (v.isLoggedOut = !0),
                      (v.pollingMessagesId = 0),
                      (v.pollingEnabled = !1),
                      (v.pollingInterval = 5e3),
                      (v.pollingEnabledInternally = !1),
                      (v.WSReconnectionStartTime = 0),
                      (v.WSReconnectionInProgress = !1),
                      v.setLastMessageId(0),
                      v.setAccidentalDisconnect(!1),
                      (v.isAnalyticsDisabled = !1),
                      (v.shouldFallBackToPolling = !0),
                      v.clearCache().then(function () {
                        (v.apiKey = void 0),
                          (v.user = void 0),
                          (v.authToken = void 0),
                          (v.cometChat = void 0),
                          (v.mode = void 0),
                          Z.WSLogout(),
                          n && v.pushToLoginListener("", "Logout_Success"),
                          e(!0);
                      });
                  } catch (e) {
                    D.Logger.error("CometChat: internalLogout", e), t(e);
                  }
                })
              );
            }),
            (v.initialzed = !1),
            (v.CometChatException = U.CometChatException),
            (v.TextMessage = d.TextMessage),
            (v.MediaMessage = S.MediaMessage),
            (v.CustomMessage = P.CustomMessage),
            (v.Action = i.Action),
            (v.Call = s.Call),
            (v.TypingIndicator = L.TypingIndicator),
            (v.TransientMessage = Q.TransientMessage),
            (v.Group = a.Group),
            (v.AppUser = T.User),
            (v.User = T.User),
            (v.GroupMember = w.GroupMember),
            (v.Conversation = B.Conversation),
            (v.USER_STATUS = {
              ONLINE: Y.PresenceConstatnts.STATUS.ONLINE,
              OFFLINE: Y.PresenceConstatnts.STATUS.OFFLINE,
            }),
            (v.MessagesRequest = y.DefaultMessagesRequest),
            (v.MessagesRequestBuilder = y.DefaultMessagesRequestBuilder),
            (v.UsersRequest = O.UsersRequest),
            (v.UsersRequestBuilder = O.UsersRequestBuilder),
            (v.ConversationsRequest = N.ConversationsRequest),
            (v.ConversationsRequestBuilder = N.ConversationsRequestBuilder),
            (v.BlockedUsersRequest = K.BlockedUsersRequest),
            (v.BlockedUsersRequestBuilder = K.BlockedUsersRequestBuilder),
            (v.GroupsRequest = f.GroupsRequest),
            (v.GroupsRequestBuilder = f.GroupsRequestBuilder),
            (v.GroupMembersRequest = R.GroupMembersRequest),
            (v.GroupMembersRequestBuilder = R.GroupMembersRequestBuilder),
            (v.BannedMembersRequest = R.GroupMembersRequest),
            (v.BannedMembersRequestBuilder =
              R.GroupOutCastMembersRequestBuilder),
            (v.CallSettings = J.CallSettings),
            (v.CallSettingsBuilder = J.CallSettingsBuilder),
            (v.AppSettings = b.AppSettings),
            (v.AppSettingsBuilder = b.AppSettingsBuilder),
            (v.MessageListener = o.MessageEventListener),
            (v.UserListener = o.UserEventListener),
            (v.GroupListener = o.GroupEventListener),
            (v.OngoingCallListener = o.UserCallEventListener),
            (v.CallListener = o.CallEventListener),
            (v.ConnectionListener = o.ConnectionEventListener),
            (v.LoginListener = o.LoginEventListener),
            (v.CallController = G.CallController),
            (v.CometChatHelper = F.CometChatHelper),
            (v.Attachment = V.Attachment),
            (v.MediaDevice = q.MediaDevice),
            (v.MESSAGE_TYPE = Y.MessageConstatnts.TYPE),
            (v.CATEGORY_MESSAGE = Y.MessageConstatnts.CATEGORY.MESSAGE),
            (v.CATEGORY_ACTION = Y.MessageConstatnts.CATEGORY.ACTION),
            (v.CATEGORY_CALL = Y.MessageConstatnts.CATEGORY.CALL),
            (v.CATEGORY_CUSTOM = Y.MessageConstatnts.CATEGORY.CUSTOM),
            (v.ACTION_TYPE = Y.ActionConstatnts.ACTIONS),
            (v.CALL_TYPE = Y.CallConstants.CALL_TYPE),
            (v.RECEIVER_TYPE = Y.MessageConstatnts.RECEIVER_TYPE),
            (v.CALL_STATUS = Y.CallConstants.CALL_STATUS),
            (v.GROUP_MEMBER_SCOPE = Y.GROUP_MEMBER_SCOPE),
            (v.GROUP_TYPE = Y.GROUP_TYPE),
            (v.MESSAGE_REQUEST = Y.MessageConstatnts.PAGINATION.CURSOR_FILEDS),
            (v.CONNECTION_STATUS = Y.CONNECTION_STATUS),
            (v.CALL_MODE = Y.CallConstants.CALL_MODE),
            (v.WSReconnectionStartTime = 0),
            (v.WSMaxReconnectionLimit = 10),
            (v.WSCurrentReconnectionCount = 0),
            (v.WSReconnectionInProgress = !1),
            (v.WSReconnectionPeriod = 3e4),
            (v.WSReconnectionTimerInterval = 5e3),
            (v.pollingEnabledInternally = !1),
            (v.accidentalDisconnect = !1),
            (v.successfulPingTimeStamp = 0),
            (v.currentConnectionStatus = Y.CONNECTION_STATUS.DISCONNECTED),
            (v.isWSCheckTimerStarted = !1),
            (v.wsCheckTimerDuration = 3e4),
            (v.isConnectingFromInit = !1),
            (v.loginInProgress = !1),
            (v.lastMessageId = 0),
            (v.internalRestart = !1),
            (v.pollingEnabled = !1),
            (v.pollingInterval = 5e3),
            (v.pollingLimit = 100),
            (v.pollingAffix = "append"),
            (v.pollingMessagesId = 0),
            (v.settingsInterval = 6e4),
            (v.isMessagesPollingStarted = !1),
            (v.isAnalyticsPingStarted = !1),
            (v.isLoggedOut = !0),
            (v.isAnalyticsDisabled = !1),
            (v.shouldFallBackToPolling = !0),
            (v.shouldPushLoginListener = !0),
            v
          );
        })();
      t.CometChat = ee;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var r = n(2),
        i = n(14),
        a = n(1),
        E = (function () {
          function e() {
            for (var e = [], t = 0; t < arguments.length; t++)
              e[t] = arguments[t];
            if (
              ((this.hasBlockedMe = !1),
              (this.blockedByMe = !1),
              1 === e.length)
            )
              typeof e[0] === a.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                ? (this.uid = e[0])
                : ((this.uid = e[0].uid),
                  (this.name = e[0].name),
                  e[0].authToken && (this.authToken = e[0].authToken),
                  e[0].avatar && (this.avatar = e[0].avatar),
                  e[0].lastActiveAt && (this.lastActiveAt = e[0].lastActiveAt),
                  e[0].link && (this.link = e[0].link),
                  e[0].metadata && (this.metadata = e[0].metadata),
                  e[0].role && (this.role = e[0].role),
                  e[0].statusMessage &&
                    (this.statusMessage = e[0].statusMessage),
                  e[0].status && "offline" !== e[0].status
                    ? (this.status = "online")
                    : (this.status = "offline"),
                  e[0].tags && (this.tags = e[0].tags));
            else {
              if (2 !== e.length)
                throw new r.CometChatException(i.ERRORS.PARAMETER_MISSING);
              (this.uid = e[0]), (this.name = e[1]);
            }
          }
          return (
            (e.prototype.getUid = function () {
              return this.uid.toString();
            }),
            (e.prototype.setUid = function (e) {
              this.uid = e;
            }),
            (e.prototype.getName = function () {
              return this.name.toString();
            }),
            (e.prototype.setName = function (e) {
              e && (this.name = e);
            }),
            (e.prototype.getAuthToken = function () {
              return this.authToken;
            }),
            (e.prototype.setAuthToken = function (e) {
              this.authToken = e;
            }),
            (e.prototype.getAvatar = function () {
              return this.avatar;
            }),
            (e.prototype.setAvatar = function (e) {
              this.avatar = e;
            }),
            (e.prototype.getLastActiveAt = function () {
              return this.lastActiveAt;
            }),
            (e.prototype.setLastActiveAt = function (e) {
              this.lastActiveAt = e;
            }),
            (e.prototype.getLink = function () {
              return this.link;
            }),
            (e.prototype.setLink = function (e) {
              return (this.link = e);
            }),
            (e.prototype.getMetadata = function () {
              return this.metadata;
            }),
            (e.prototype.setMetadata = function (e) {
              this.metadata = e;
            }),
            (e.prototype.getRole = function () {
              return this.role;
            }),
            (e.prototype.setRole = function (e) {
              this.role = e;
            }),
            (e.prototype.getStatus = function () {
              return this.status;
            }),
            (e.prototype.setStatus = function (e) {
              this.status = e;
            }),
            (e.prototype.getStatusMessage = function () {
              return this.statusMessage;
            }),
            (e.prototype.setStatusMessage = function (e) {
              this.statusMessage = e;
            }),
            (e.prototype.setBlockedByMe = function (e) {
              this.blockedByMe = e;
            }),
            (e.prototype.getBlockedByMe = function () {
              return this.blockedByMe;
            }),
            (e.prototype.setHasBlockedMe = function (e) {
              this.hasBlockedMe = e;
            }),
            (e.prototype.getHasBlockedMe = function () {
              return this.hasBlockedMe;
            }),
            (e.prototype.setTags = function (e) {
              this.tags = e;
            }),
            (e.prototype.getTags = function () {
              return this.tags;
            }),
            e
          );
        })(),
        c = (function (n) {
          function e(e) {
            var t = n.call(this, e) || this;
            return (t.wsChannel = e.wsChannel), e.jwt && (t.jwt = e.jwt), t;
          }
          return (
            s(e, n),
            (e.prototype.getWsChannel = function () {
              return this.wsChannel;
            }),
            (e.prototype.getJWT = function () {
              return this.jwt;
            }),
            e
          );
        })((t.User = E));
      t.Me = c;
    },
    function (e, t, n) {
      "use strict";
      (t.__esModule = !0),
        (t.WS = { protocol: "wss://" }),
        (t.READY_STATE = {
          CONNECTING: 0,
          OPEN: 1,
          CLOSING: 2,
          CLOSED: 3,
          INVALID_JWT: 4,
        }),
        (t.LOGOUT_CODE = 1e3),
        (t.LOGOUT_REASON = "User logged out"),
        (t.TYPING_INDICATOR = {
          TYPE: "typing_indicator",
          ACTION: { STARTED: "started", ENDED: "ended" },
        }),
        (t.TRANSIENT_MESSAGE = { TYPE: "transient_message" }),
        (t.RECEIPTS = {
          TYPE: "receipts",
          ACTION: { READ: "read", DELIVERED: "delivered" },
          RECEIPT_TYPE: { READ_RECEIPT: "read", DELIVERY_RECEIPT: "delivery" },
        }),
        (t.PRESENCE = {
          TYPE: "presence",
          ACTION: {
            ONLINE: "online",
            AVAILABLE: "available",
            OFFLINE: "offline",
          },
        }),
        (t.MESSAGE = { TYPE: "message" }),
        (t.AUTH = { TYPE: "auth" }),
        (t.KEYS = {
          TYPE: "type",
          ACTION: "action",
          APP_ID: "appId",
          RECEIVER: "receiver",
          RECEIVER_TYPE: "receiverType",
          DEVICE_ID: "deviceId",
          BODY: "body",
          USER: "user",
          METADATA: "metadata",
          MESSAGE_ID: "messageId",
          TIMESTAMP: "timestamp",
          STATUS: "status",
          CODE: "code",
          SENDER: "sender",
          MESSAGE_SENDER: "messageSender",
          PRESENCE_SUBSCRIPTION: "presenceSubscription",
          AUTH: "auth",
          PING: "ping",
          DATA: "data",
        });
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var E = n(28),
        c = n(3),
        u = n(0),
        S = n(2),
        l = n(14),
        p = n(1);
      function C(n, e, o, t, s) {
        var r;
        return (
          void 0 === n && (n = ""),
          void 0 === e && (e = "GET"),
          void 0 === o && (o = {}),
          void 0 === t && (t = {}),
          (o = u.isFalsy(o)
            ? void 0
            : ("GET" == e &&
                ((n += "?"),
                Object.keys(o).map(function (e, t) {
                  n =
                    t === Object.keys(o).length - 1
                      ? n + e + "=" + o[e]
                      : n + e + "=" + o[e] + "&";
                }),
                (o = void 0)),
              s &&
                ((r = new FormData()),
                Object.keys(o).map(function (e) {
                  "data" != e
                    ? "metadata" != e && r.append(e, o[e])
                    : r.append(e, JSON.stringify(o[e]));
                })),
              JSON.stringify(o))),
          fetch(n, {
            method: e,
            mode: "cors",
            cache: "no-cache",
            headers: t,
            redirect: "follow",
            referrer: "no-referrer",
            body: s ? r : o,
          })
        );
      }
      (t.makeApiCall = function (s, e, r, i) {
        void 0 === s && (s = ""),
          void 0 === e && (e = {}),
          void 0 === r && (r = {});
        var a = c.CometChat.getInstance(c.CometChat.getAppId());
        return new Promise(function (n, o) {
          try {
            E.getEndPoint(s, e)
              .then(function (e) {
                var t = {
                  resource: c.CometChat.getSessionId(),
                  appId: c.CometChat.getAppId(),
                  Accept: "application/json",
                  sdk: u.format(
                    p.SDKHeader.sdk,
                    p.SDKHeader.platform,
                    p.SDKHeader.sdkVersion
                  ),
                };
                i || (t["Content-Type"] = "application/json"),
                  e.hasOwnProperty("isAdminApi") && e.isAdminApi
                    ? a.getApiKey()
                      ? (t.apiKey = a.getApiKey())
                      : o({
                          error: {
                            code: "API_KEY_NOT_SET",
                            message:
                              "An apiKey is needed to use the " + s + " api.",
                            name: "API_KEY_NOT_SET",
                          },
                        })
                    : a.getAuthToken()
                    ? (t.authToken = a.getAuthToken())
                    : o({
                        error: {
                          code: "USER_NOT_LOGED_IN",
                          message:
                            "An authToken is need to use the " +
                            s +
                            " end-point. PS- We are aware of the spelling mistake, but in order to maintain backward compatibility we cannot change it :(",
                          name: "User not logged-in",
                        },
                      }),
                  C(e.endpoint, e.method, r, t, i)
                    .then(function (e) {
                      return e.json();
                    })
                    .then(function (e) {
                      if (e.hasOwnProperty("data"))
                        e.data.hasOwnProperty("authToken") &&
                          c.CometChat.setAuthToken(e.data.authToken),
                          n(e);
                      else {
                        if (e.hasOwnProperty("error")) {
                          var t = e.error;
                          t.hasOwnProperty("code") &&
                            t.code ===
                              p.API_ERROR_CODES.AUTH_ERR_AUTH_TOKEN_NOT_FOUND &&
                            c.CometChat.getInstance()
                              .internalLogout()
                              .then(function () {
                                u.Logger.log(
                                  "CometChat: makeApiCall",
                                  "User logged out"
                                );
                              });
                        }
                        o(e);
                      }
                    })
                    .catch(function (e) {
                      var t = { error: l.FETCH_ERROR.ERROR_IN_API_CALL };
                      o(t);
                    });
              })
              .catch(function (e) {
                return o;
              });
          } catch (e) {
            o(new S.CometChatException(e));
          }
        });
      }),
        (t.makeAdminApiCall = function (s, e, r, i) {
          void 0 === s && (s = ""),
            void 0 === e && (e = {}),
            void 0 === r && (r = {});
          var a = c.CometChat.getInstance(c.CometChat.getAppId());
          return new Promise(function (n, o) {
            E.getEndPoint(s, e)
              .then(function (e) {
                var t = {
                  appId: c.CometChat.getAppId(),
                  Accept: "application/json",
                  sdk: u.format(
                    p.SDKHeader.sdk,
                    p.SDKHeader.platform,
                    p.SDKHeader.sdkVersion
                  ),
                };
                i || (t["Content-Type"] = "application/json"),
                  e.hasOwnProperty("isAdminApi") && e.isAdminApi
                    ? a.getApiKey()
                      ? (t.apiKey = a.getApiKey())
                      : o({
                          error: "An apiKey is need to use the " + s + " api.",
                        })
                    : a.getAuthToken()
                    ? (t.authToken = a.getAuthToken())
                    : o({
                        error: "An authToken is need to use the " + s + " api.",
                      }),
                  C(e.endpoint, e.method, r, t, i)
                    .then(function (e) {
                      return e.json();
                    })
                    .then(function (e) {
                      e.hasOwnProperty("data")
                        ? (e.data.hasOwnProperty("authToken") &&
                            c.CometChat.setAuthToken(e.data.authToken),
                          n(e))
                        : o(e);
                    })
                    .catch(function (e) {
                      var t = { error: l.FETCH_ERROR.ERROR_IN_API_CALL };
                      o(t);
                    });
              })
              .catch(function (e) {
                return o;
              });
          });
        }),
        (t.postData = C);
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = (function () {
        function e(e, t, n, o) {
          (this.receiverId = e),
            (this.type = t),
            (this.receiverType = n),
            (this.category = o);
        }
        return (
          (e.prototype.getId = function () {
            return this.id;
          }),
          (e.prototype.setId = function (e) {
            this.id = e;
          }),
          (e.prototype.getConversationId = function () {
            return this.conversationId;
          }),
          (e.prototype.setConversationId = function (e) {
            this.conversationId = e;
          }),
          (e.prototype.getParentMessageId = function () {
            return this.parentMessageId;
          }),
          (e.prototype.setParentMessageId = function (e) {
            this.parentMessageId = e;
          }),
          (e.prototype.getMuid = function () {
            return this.muid;
          }),
          (e.prototype.setMuid = function (e) {
            this.muid = e;
          }),
          (e.prototype.getSender = function () {
            return this.sender;
          }),
          (e.prototype.setSender = function (e) {
            this.sender = e;
          }),
          (e.prototype.getReceiver = function () {
            return this.receiver;
          }),
          (e.prototype.setReceiver = function (e) {
            this.receiver = e;
          }),
          (e.prototype.getReceiverId = function () {
            return this.receiverId;
          }),
          (e.prototype.setReceiverId = function (e) {
            this.receiverId = e;
          }),
          (e.prototype.getType = function () {
            return this.type;
          }),
          (e.prototype.setType = function (e) {
            this.type = e;
          }),
          (e.prototype.get = function () {
            return this.id;
          }),
          (e.prototype.setReceiverType = function (e) {
            this.receiverType = e;
          }),
          (e.prototype.getReceiverType = function () {
            return this.receiverType;
          }),
          (e.prototype.setSentAt = function (e) {
            this.sentAt = e;
          }),
          (e.prototype.getSentAt = function () {
            return this.sentAt;
          }),
          (e.prototype.getStatus = function () {
            return this.status;
          }),
          (e.prototype.setStatus = function (e) {
            this.status = e;
          }),
          (e.prototype.getDeliveredAt = function () {
            return this.deliveredAt;
          }),
          (e.prototype.setDeliveredAt = function (e) {
            this.deliveredAt = e;
          }),
          (e.prototype.getDeliveredToMeAt = function () {
            return this.deliveredToMeAt;
          }),
          (e.prototype.setDeliveredToMeAt = function (e) {
            this.deliveredToMeAt = e;
          }),
          (e.prototype.getReadAt = function () {
            return this.readAt;
          }),
          (e.prototype.setReadAt = function (e) {
            this.readAt = e;
          }),
          (e.prototype.getReadByMeAt = function () {
            return this.readByMeAt;
          }),
          (e.prototype.setReadByMeAt = function (e) {
            this.readByMeAt = e;
          }),
          (e.prototype.getCategory = function () {
            return this.category;
          }),
          (e.prototype.setCategory = function (e) {
            this.category = e;
          }),
          (e.prototype.setEditedAt = function (e) {
            this.editedAt = e;
          }),
          (e.prototype.getEditedAt = function () {
            return this.editedAt;
          }),
          (e.prototype.setEditedBy = function (e) {
            this.editedBy = e;
          }),
          (e.prototype.getEditedBy = function () {
            return this.editedBy;
          }),
          (e.prototype.setDeletedAt = function (e) {
            this.deletedAt = e;
          }),
          (e.prototype.getDeletedAt = function () {
            return this.deletedAt;
          }),
          (e.prototype.setDeletedBy = function (e) {
            this.deletedBy = e;
          }),
          (e.prototype.getDeletedBy = function () {
            return this.deletedBy;
          }),
          (e.prototype.getReplyCount = function () {
            return this.replyCount;
          }),
          (e.prototype.setReplyCount = function (e) {
            this.replyCount = e;
          }),
          (e.prototype.getRawMessage = function () {
            return this.rawMessage;
          }),
          (e.prototype.setRawMessage = function (e) {
            this.rawMessage = e;
          }),
          e
        );
      })();
      t.BaseMessage = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = (function () {
          function e(e, t, n, o, s, r, i) {
            (this.hasJoined = !1),
              (this.membersCount = 0),
              (this.guid = e),
              t && (this.name = t),
              n && (this.type = n),
              (!o && "" !== o) || "password" != n || (this.password = o),
              (s || "" === s) && (this.icon = s),
              (r || "" === r) && (this.description = r),
              i && (this.hasJoined = i);
          }
          return (
            (e.prototype.getGuid = function () {
              return this.guid;
            }),
            (e.prototype.setGuid = function (e) {
              this.guid = e;
            }),
            (e.prototype.getName = function () {
              return this.name;
            }),
            (e.prototype.setName = function (e) {
              e && (this.name = e);
            }),
            (e.prototype.getType = function () {
              return this.type;
            }),
            (e.prototype.setType = function (e) {
              this.type = e;
            }),
            (e.prototype.setPassword = function (e) {
              this.password = e;
            }),
            (e.prototype.getIcon = function () {
              return this.icon;
            }),
            (e.prototype.setIcon = function (e) {
              this.icon = e;
            }),
            (e.prototype.getDescription = function () {
              return this.description;
            }),
            (e.prototype.setDescription = function (e) {
              this.description = e;
            }),
            (e.prototype.getOwner = function () {
              return this.owner;
            }),
            (e.prototype.setOwner = function (e) {
              this.owner = e;
            }),
            (e.prototype.getMetadata = function () {
              return this.metadata;
            }),
            (e.prototype.setMetadata = function (e) {
              this.metadata = e;
            }),
            (e.prototype.getCreatedAt = function () {
              return this.createdAt;
            }),
            (e.prototype.setCreatedAt = function (e) {
              this.createdAt = e;
            }),
            (e.prototype.getUpdatedAt = function () {
              return this.updatedAt;
            }),
            (e.prototype.setUpdatedAt = function (e) {
              this.updatedAt = e;
            }),
            (e.prototype.getHasJoined = function () {
              return this.hasJoined;
            }),
            (e.prototype.setHasJoined = function (e) {
              this.hasJoined = e;
            }),
            (e.prototype.getWsChannel = function () {
              return this.wsChannel;
            }),
            (e.prototype.setWsChannel = function (e) {
              this.wsChannel = e;
            }),
            (e.prototype.setScope = function (e) {
              this.scope = e;
            }),
            (e.prototype.getScope = function () {
              return this.scope;
            }),
            (e.prototype.getJoinedAt = function () {
              return this.joinedAt;
            }),
            (e.prototype.setJoinedAt = function (e) {
              this.joinedAt = e;
            }),
            (e.prototype.getMembersCount = function () {
              return this.membersCount;
            }),
            (e.prototype.setMembersCount = function (e) {
              this.membersCount = e;
            }),
            (e.prototype.setTags = function (e) {
              this.tags = e;
            }),
            (e.prototype.getTags = function () {
              return this.tags;
            }),
            (e.TYPE = o.GroupType),
            (e.Type = e.TYPE),
            e
          );
        })();
      t.Group = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var i = n(0),
        o = n(5),
        s = (function () {
          function e(e, t, n, o, s, r) {
            i.isFalsy(e) || (this.appId = e),
              i.isFalsy(t) || (this.receiver = t),
              i.isFalsy(n) || (this.receiverType = n),
              i.isFalsy(s) || (this.deviceId = s),
              i.isFalsy(o) || (this.sender = o),
              i.isFalsy(r) || (this.messageSender = r);
          }
          return (
            (e.prototype.getAppId = function () {
              return this.appId;
            }),
            (e.prototype.setAppId = function (e) {
              this.appId = e;
            }),
            (e.prototype.getReceiver = function () {
              return this.receiver;
            }),
            (e.prototype.setReceiver = function (e) {
              this.receiver = e;
            }),
            (e.prototype.getSender = function () {
              return this.sender;
            }),
            (e.prototype.setSender = function (e) {
              this.sender = e;
            }),
            (e.prototype.getReceiverType = function () {
              return this.receiverType;
            }),
            (e.prototype.setReceiverType = function (e) {
              this.receiverType = e;
            }),
            (e.prototype.getType = function () {
              return this.type;
            }),
            (e.prototype.setType = function (e) {
              this.type = e;
            }),
            (e.prototype.getDeviceId = function () {
              return this.deviceId;
            }),
            (e.prototype.setDeviceId = function (e) {
              this.deviceId = e;
            }),
            (e.prototype.getMessageSender = function () {
              return this.messageSender;
            }),
            (e.prototype.setMessageSender = function (e) {
              this.messageSender = e;
            }),
            (e.prototype.getCometChatEventJSON = function () {
              var e = {};
              return (
                (e[o.KEYS.APP_ID] = this.getAppId()),
                (e[o.KEYS.RECEIVER] = this.getReceiver()),
                (e[o.KEYS.RECEIVER_TYPE] = this.getReceiverType()),
                (e[o.KEYS.DEVICE_ID] = this.getDeviceId()),
                (e[o.KEYS.TYPE] = this.getType()),
                (e[o.KEYS.SENDER] = this.getSender()),
                i.isFalsy(this.getMessageSender()) ||
                  (e[o.KEYS.MESSAGE_SENDER] = this.getMessageSender()),
                e
              );
            }),
            e
          );
        })();
      t.CometChatEvent = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var i = n(7),
        a = n(16),
        E = n(17),
        c = n(0),
        u = n(1),
        S = n(20),
        l = n(21),
        p = n(22),
        r = n(2),
        C = n(18),
        T = n(19),
        g = n(3),
        d = n(11),
        o = (function () {
          function e() {}
          return (
            (e.trasformJSONMessge = function (n) {
              try {
                var e = null;
                n.hasOwnProperty("rawMessage") ||
                  (e = JSON.parse(JSON.stringify(n)));
                var t = void 0;
                switch (n[u.MessageConstatnts.KEYS.CATEGORY]) {
                  case u.MessageConstatnts.CATEGORY.ACTION:
                    t = S.Action.actionFromJSON(n);
                    break;
                  case u.MessageConstatnts.CATEGORY.CALL:
                    t = l.Call.callFromJSON(n);
                    break;
                  case u.MessageConstatnts.CATEGORY.MESSAGE:
                    switch (n[u.MessageConstatnts.KEYS.TYPE]) {
                      case u.MessageConstatnts.TYPE.TEXT:
                        t = new a.TextMessage(
                          n[u.MessageConstatnts.KEYS.RECEIVER],
                          n[u.MessageConstatnts.KEYS.DATA][
                            u.MessageConstatnts.KEYS.TEXT
                          ],
                          n[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                        );
                        break;
                      case u.MessageConstatnts.TYPE.CUSTOM:
                        t = new T.CustomMessage(
                          n[u.MessageConstatnts.KEYS.RECEIVER],
                          n[u.MessageConstatnts.KEYS.DATA][
                            u.MessageConstatnts.KEYS.CUSTOM_DATA
                          ],
                          n[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                        );
                        break;
                      default:
                        if (
                          ((t = new E.MediaMessage(
                            n[u.MessageConstatnts.KEYS.RECEIVER],
                            n[u.MessageConstatnts.KEYS.DATA][
                              u.MessageConstatnts.KEYS.URL
                            ],
                            n[u.MessageConstatnts.KEYS.TYPE],
                            n[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                          )),
                          n.hasOwnProperty(u.MessageConstatnts.KEYS.DATA))
                        ) {
                          var o = n[u.MessageConstatnts.KEYS.DATA];
                          if (
                            o.hasOwnProperty(
                              u.MessageConstatnts.KEYS.ATTATCHMENTS
                            )
                          ) {
                            var s,
                              r = o[u.MessageConstatnts.KEYS.ATTATCHMENTS];
                            new Array();
                            r.map(function (e) {
                              s = new C.Attachment(e);
                            }),
                              t.setAttachment(s);
                          }
                          o.hasOwnProperty(u.MessageConstatnts.KEYS.TEXT) &&
                            t.setCaption(o[u.MessageConstatnts.KEYS.TEXT]);
                        }
                        t.hasOwnProperty("file") && delete t.file;
                    }
                    break;
                  case u.MessageConstatnts.CATEGORY.CUSTOM:
                    t = new T.CustomMessage(
                      n[u.MessageConstatnts.KEYS.RECEIVER],
                      n[u.MessageConstatnts.KEYS.DATA][
                        u.MessageConstatnts.KEYS.CUSTOM_DATA
                      ],
                      n[u.MessageConstatnts.KEYS.RECEIVER_TYPE],
                      n.type
                    );
                }
                n[u.MessageConstatnts.KEYS.MY_RECEIPTS] &&
                  ((n[u.MessageConstatnts.KEYS.MY_RECEIPTS] =
                    n[u.MessageConstatnts.KEYS.MY_RECEIPTS]),
                  Object.keys(n[u.MessageConstatnts.KEYS.MY_RECEIPTS]).map(
                    function (e) {
                      var t = new p.MessageReceipt();
                      e == u.DELIVERY_RECEIPTS.DELIVERED_AT &&
                        (t.setReceiptType(t.RECEIPT_TYPE.DELIVERY_RECEIPT),
                        t.setDeliveredAt(
                          n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                            u.DELIVERY_RECEIPTS.DELIVERED_AT
                          ]
                        ),
                        c.isFalsy(
                          n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                            u.DELIVERY_RECEIPTS.RECIPIENT
                          ]
                        )
                          ? (n[u.DELIVERY_RECEIPTS.DELIVERED_TO_ME_AT] =
                              n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                                u.DELIVERY_RECEIPTS.DELIVERED_AT
                              ])
                          : t.setSender(
                              n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                                u.DELIVERY_RECEIPTS.RECIPIENT
                              ]
                            ),
                        t.setReceiverType(
                          n[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                        ),
                        t.setReceiver(n[u.MessageConstatnts.KEYS.RECEIVER])),
                        n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                          u.READ_RECEIPTS.READ_AT
                        ] &&
                          (t.setReceiptType(t.RECEIPT_TYPE.READ_RECEIPT),
                          t.setReadAt(
                            n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                              u.READ_RECEIPTS.READ_AT
                            ]
                          ),
                          c.isFalsy(
                            n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                              u.READ_RECEIPTS.RECIPIENT
                            ]
                          )
                            ? (n[u.READ_RECEIPTS.READ_BY_ME_AT] =
                                n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                                  u.READ_RECEIPTS.READ_AT
                                ])
                            : t.setSender(
                                n[u.MessageConstatnts.KEYS.MY_RECEIPTS][
                                  u.READ_RECEIPTS.RECIPIENT
                                ]
                              ),
                          t.setReceiverType(
                            n[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                          ),
                          t.setReceiver(n[u.MessageConstatnts.KEYS.RECEIVER]));
                    }
                  ));
                try {
                  if (
                    (Object.assign(t, n),
                    (n = t).parentId &&
                      ((n.parentMessageId = n.parentId), delete n.parentId),
                    n instanceof i.BaseMessage &&
                      (c.isFalsy(e) || n.setRawMessage(e)),
                    n instanceof a.TextMessage)
                  )
                    n.setSender(n.getSender()),
                      n.setReceiver(n.getReceiver()),
                      n.getData()[u.MessageConstatnts.KEYS.METADATA] &&
                        n.setMetadata(
                          n.getData()[u.MessageConstatnts.KEYS.METADATA]
                        );
                  else if (n instanceof E.MediaMessage)
                    n.getData()[u.MessageConstatnts.KEYS.METADATA] &&
                      n.setMetadata(
                        n.getData()[u.MessageConstatnts.KEYS.METADATA]
                      ),
                      n.setSender(n.getSender()),
                      n.setReceiver(n.getReceiver());
                  else if (n instanceof S.Action)
                    n.setSender(n.getSender()),
                      n.setReceiver(n.getActionFor()),
                      n.setActionBy(n.getActionBy()),
                      n.setActionOn(n.getActionOn()),
                      n.setActionFor(n.getActionFor()),
                      n.setMessage(n.getMessage());
                  else if (n instanceof l.Call) {
                    try {
                      n.setSender(n.getSender());
                    } catch (e) {
                      c.Logger.error(
                        "MessageController: trasformJSONMessge: setSender",
                        e
                      );
                    }
                    try {
                      n.setCallInitiator(n.getCallInitiator());
                    } catch (e) {
                      c.Logger.error(
                        "MessageController: trasformJSONMessge: setCallInitiator",
                        e
                      );
                    }
                    try {
                      n.setReceiver(n.getCallReceiver()),
                        n.setCallReceiver(n.getCallReceiver());
                    } catch (e) {
                      c.Logger.error(
                        "MessageController: trasformJSONMessge: setCallreceiver",
                        e
                      );
                    }
                  } else
                    n instanceof T.CustomMessage &&
                      (n.getData()[u.MessageConstatnts.KEYS.METADATA] &&
                        n.setMetadata(
                          n.getData()[u.MessageConstatnts.KEYS.METADATA]
                        ),
                      n.setSubType(
                        n.getData()[u.MessageConstatnts.KEYS.CUSTOM_SUB_TYPE]
                      ),
                      n.setSender(n.getSender()),
                      n.setReceiver(n.getReceiver()));
                } catch (e) {
                  c.Logger.error(
                    "MessageController: trasformJSONMessge: Main",
                    e
                  ),
                    (n = null);
                }
                return n;
              } catch (e) {
                c.Logger.error("MessageController: trasformJSONMessge", e);
              }
            }),
            (e.getReceiptsFromJSON = function (s) {
              return new Promise(function (e, t) {
                try {
                  var o = [];
                  g.CometChat.getLoggedInUser().then(function (n) {
                    c.isFalsy(s.receipts)
                      ? e([])
                      : (s.receipts.data.map(function (e) {
                          var t = new p.MessageReceipt();
                          e[u.DELIVERY_RECEIPTS.DELIVERED_AT] &&
                            (t.setReceiptType(t.RECEIPT_TYPE.DELIVERY_RECEIPT),
                            t.setDeliveredAt(
                              e[u.DELIVERY_RECEIPTS.DELIVERED_AT]
                            ),
                            t.setTimestamp(e[u.DELIVERY_RECEIPTS.DELIVERED_AT]),
                            c.isFalsy(e[u.DELIVERY_RECEIPTS.RECIPIENT])
                              ? t.setSender(n)
                              : t.setSender(
                                  d.UsersController.trasformJSONUser(
                                    e[u.DELIVERY_RECEIPTS.RECIPIENT]
                                  )
                                ),
                            t.setReceiverType(
                              s[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                            ),
                            t.setReceiver(
                              s[u.MessageConstatnts.KEYS.RECEIVER]
                            )),
                            e[u.READ_RECEIPTS.READ_AT] &&
                              (t.setReceiptType(t.RECEIPT_TYPE.READ_RECEIPT),
                              t.setReadAt(e[u.READ_RECEIPTS.READ_AT]),
                              t.setTimestamp(e[e[u.READ_RECEIPTS.READ_AT]]),
                              c.isFalsy(e[u.READ_RECEIPTS.RECIPIENT])
                                ? t.setSender(n)
                                : t.setSender(
                                    d.UsersController.trasformJSONUser(
                                      e[u.READ_RECEIPTS.RECIPIENT]
                                    )
                                  ),
                              t.setReceiverType(
                                s[u.MessageConstatnts.KEYS.RECEIVER_TYPE]
                              ),
                              t.setReceiver(
                                s[u.MessageConstatnts.KEYS.RECEIVER]
                              )),
                            o.push(t);
                        }),
                        e(o));
                  });
                } catch (e) {
                  t(new r.CometChatException(e));
                }
              });
            }),
            e
          );
        })();
      t.MessageController = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(4),
        s = n(0),
        r = (function () {
          function e() {}
          return (
            (e.trasformJSONUser = function (e) {
              var t;
              try {
                (e.uid = e.uid.toString()),
                  (e.name = e.name.toString()),
                  e.status && "offline" !== e.status
                    ? (e.status = "online")
                    : (e.status = "offline"),
                  (t = new o.User(e)),
                  Object.assign(t, e),
                  (e = t);
              } catch (e) {
                s.Logger.error("UsersController:transformJSONUser", e);
              }
              return e;
            }),
            e
          );
        })();
      t.UsersController = r;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = n(1),
        r = n(13),
        i = n(3),
        a = n(2),
        E = (function () {
          function e(e) {
            (this.store = s.constants.DEFAULT_STORE),
              o.isFalsy(e) || (this.store = e),
              (this.localStore = r.createInstance({
                name: o.format(
                  s.LOCAL_STORE.STORE_STRING,
                  i.CometChat.getAppId(),
                  s.LOCAL_STORE.COMMON_STORE
                ),
              })),
              this.localStore.setDriver([
                r.LOCALSTORAGE,
                r.INDEXEDDB,
                r.WEBSQL,
              ]);
          }
          return (
            (e.getInstance = function () {
              return (
                null == e.localStorage && (e.localStorage = new e()),
                e.localStorage
              );
            }),
            (e.prototype.set = function (e, t) {
              return this.localStore.setItem(e, JSON.stringify(t));
            }),
            (e.prototype.remove = function (e) {
              this.localStore.removeItem(e);
            }),
            (e.prototype.update = function (e, t) {
              this.remove(e), this.set(e, t);
            }),
            (e.prototype.get = function (e) {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  o.localStore.getItem(e).then(
                    function (t) {
                      try {
                        n(JSON.parse(t));
                      } catch (e) {
                        n(t);
                      }
                    },
                    function (e) {
                      t(e);
                    }
                  );
                } catch (e) {
                  t(new a.CometChatException(e));
                }
              });
            }),
            (e.prototype.clearStore = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  o.localStore.keys().then(function (e) {
                    if (0 < e.length)
                      for (var t = 0; t < e.length; t++)
                        "appId" !== e[t] && o.localStore.removeItem(e[t]),
                          t === e.length - 1 && n(!0);
                  });
                } catch (e) {
                  t(e);
                }
              });
            }),
            (e.prototype.clear = function (e) {}),
            (e.prototype.selectStore = function (e) {
              this.store = e;
            }),
            (e.localStorage = null),
            e
          );
        })();
      t.LocalStorage = E;
    },
    function (e, t, n) {
      (function (o) {
        e.exports = (function s(r, i, a) {
          function E(n, e) {
            if (!i[n]) {
              if (!r[n]) {
                if (c) return c(n, !0);
                var t = new Error("Cannot find module '" + n + "'");
                throw ((t.code = "MODULE_NOT_FOUND"), t);
              }
              var o = (i[n] = { exports: {} });
              r[n][0].call(
                o.exports,
                function (e) {
                  var t = r[n][1][e];
                  return E(t || e);
                },
                o,
                o.exports,
                s,
                r,
                i,
                a
              );
            }
            return i[n].exports;
          }
          for (var c = !1, e = 0; e < a.length; e++) E(a[e]);
          return E;
        })(
          {
            1: [
              function (e, u, t) {
                (function (t) {
                  "use strict";
                  var n,
                    o,
                    e = t.MutationObserver || t.WebKitMutationObserver;
                  if (e) {
                    var s = 0,
                      r = new e(c),
                      i = t.document.createTextNode("");
                    r.observe(i, { characterData: !0 }),
                      (n = function () {
                        i.data = s = ++s % 2;
                      });
                  } else if (t.setImmediate || void 0 === t.MessageChannel)
                    n =
                      "document" in t &&
                      "onreadystatechange" in t.document.createElement("script")
                        ? function () {
                            var e = t.document.createElement("script");
                            (e.onreadystatechange = function () {
                              c(),
                                (e.onreadystatechange = null),
                                e.parentNode.removeChild(e),
                                (e = null);
                            }),
                              t.document.documentElement.appendChild(e);
                          }
                        : function () {
                            setTimeout(c, 0);
                          };
                  else {
                    var a = new t.MessageChannel();
                    (a.port1.onmessage = c),
                      (n = function () {
                        a.port2.postMessage(0);
                      });
                  }
                  var E = [];
                  function c() {
                    var e, t;
                    o = !0;
                    for (var n = E.length; n; ) {
                      for (t = E, E = [], e = -1; ++e < n; ) t[e]();
                      n = E.length;
                    }
                    o = !1;
                  }
                  u.exports = function (e) {
                    1 !== E.push(e) || o || n();
                  };
                }.call(
                  this,
                  void 0 !== o
                    ? o
                    : "undefined" != typeof self
                    ? self
                    : "undefined" != typeof window
                    ? window
                    : {}
                ));
              },
              {},
            ],
            2: [
              function (e, t, n) {
                "use strict";
                var s = e(1);
                function c() {}
                var u = {},
                  r = ["REJECTED"],
                  i = ["FULFILLED"],
                  a = ["PENDING"];
                function o(e) {
                  if ("function" != typeof e)
                    throw new TypeError("resolver must be a function");
                  (this.state = a),
                    (this.queue = []),
                    (this.outcome = void 0),
                    e !== c && p(this, e);
                }
                function E(e, t, n) {
                  (this.promise = e),
                    "function" == typeof t &&
                      ((this.onFulfilled = t),
                      (this.callFulfilled = this.otherCallFulfilled)),
                    "function" == typeof n &&
                      ((this.onRejected = n),
                      (this.callRejected = this.otherCallRejected));
                }
                function S(t, n, o) {
                  s(function () {
                    var e;
                    try {
                      e = n(o);
                    } catch (e) {
                      return u.reject(t, e);
                    }
                    e === t
                      ? u.reject(
                          t,
                          new TypeError("Cannot resolve promise with itself")
                        )
                      : u.resolve(t, e);
                  });
                }
                function l(e) {
                  var t = e && e.then;
                  if (
                    e &&
                    ("object" == typeof e || "function" == typeof e) &&
                    "function" == typeof t
                  )
                    return function () {
                      t.apply(e, arguments);
                    };
                }
                function p(t, e) {
                  var n = !1;
                  function o(e) {
                    n || ((n = !0), u.reject(t, e));
                  }
                  function s(e) {
                    n || ((n = !0), u.resolve(t, e));
                  }
                  var r = C(function () {
                    e(s, o);
                  });
                  "error" === r.status && o(r.value);
                }
                function C(e, t) {
                  var n = {};
                  try {
                    (n.value = e(t)), (n.status = "success");
                  } catch (e) {
                    (n.status = "error"), (n.value = e);
                  }
                  return n;
                }
                ((t.exports = o).prototype.catch = function (e) {
                  return this.then(null, e);
                }),
                  (o.prototype.then = function (e, t) {
                    if (
                      ("function" != typeof e && this.state === i) ||
                      ("function" != typeof t && this.state === r)
                    )
                      return this;
                    var n = new this.constructor(c);
                    if (this.state !== a) {
                      var o = this.state === i ? e : t;
                      S(n, o, this.outcome);
                    } else this.queue.push(new E(n, e, t));
                    return n;
                  }),
                  (E.prototype.callFulfilled = function (e) {
                    u.resolve(this.promise, e);
                  }),
                  (E.prototype.otherCallFulfilled = function (e) {
                    S(this.promise, this.onFulfilled, e);
                  }),
                  (E.prototype.callRejected = function (e) {
                    u.reject(this.promise, e);
                  }),
                  (E.prototype.otherCallRejected = function (e) {
                    S(this.promise, this.onRejected, e);
                  }),
                  (u.resolve = function (e, t) {
                    var n = C(l, t);
                    if ("error" === n.status) return u.reject(e, n.value);
                    var o = n.value;
                    if (o) p(e, o);
                    else {
                      (e.state = i), (e.outcome = t);
                      for (var s = -1, r = e.queue.length; ++s < r; )
                        e.queue[s].callFulfilled(t);
                    }
                    return e;
                  }),
                  (u.reject = function (e, t) {
                    (e.state = r), (e.outcome = t);
                    for (var n = -1, o = e.queue.length; ++n < o; )
                      e.queue[n].callRejected(t);
                    return e;
                  }),
                  (o.resolve = function (e) {
                    return e instanceof this ? e : u.resolve(new this(c), e);
                  }),
                  (o.reject = function (e) {
                    var t = new this(c);
                    return u.reject(t, e);
                  }),
                  (o.all = function (e) {
                    var n = this;
                    if ("[object Array]" !== Object.prototype.toString.call(e))
                      return this.reject(new TypeError("must be an array"));
                    var o = e.length,
                      s = !1;
                    if (!o) return this.resolve([]);
                    for (
                      var r = new Array(o), i = 0, t = -1, a = new this(c);
                      ++t < o;

                    )
                      E(e[t], t);
                    return a;
                    function E(e, t) {
                      n.resolve(e).then(
                        function (e) {
                          (r[t] = e),
                            ++i !== o || s || ((s = !0), u.resolve(a, r));
                        },
                        function (e) {
                          s || ((s = !0), u.reject(a, e));
                        }
                      );
                    }
                  }),
                  (o.race = function (e) {
                    var t = this;
                    if ("[object Array]" !== Object.prototype.toString.call(e))
                      return this.reject(new TypeError("must be an array"));
                    var n = e.length,
                      o = !1;
                    if (!n) return this.resolve([]);
                    for (var s, r = -1, i = new this(c); ++r < n; )
                      (s = e[r]),
                        t.resolve(s).then(
                          function (e) {
                            o || ((o = !0), u.resolve(i, e));
                          },
                          function (e) {
                            o || ((o = !0), u.reject(i, e));
                          }
                        );
                    return i;
                  });
              },
              { 1: 1 },
            ],
            3: [
              function (t, e, n) {
                (function (e) {
                  "use strict";
                  "function" != typeof e.Promise && (e.Promise = t(2));
                }.call(
                  this,
                  void 0 !== o
                    ? o
                    : "undefined" != typeof self
                    ? self
                    : "undefined" != typeof window
                    ? window
                    : {}
                ));
              },
              { 2: 2 },
            ],
            4: [
              function (e, t, n) {
                "use strict";
                var o =
                    "function" == typeof Symbol &&
                    "symbol" == typeof Symbol.iterator
                      ? function (e) {
                          return typeof e;
                        }
                      : function (e) {
                          return e &&
                            "function" == typeof Symbol &&
                            e.constructor === Symbol &&
                            e !== Symbol.prototype
                            ? "symbol"
                            : typeof e;
                        },
                  E = (function () {
                    try {
                      if ("undefined" != typeof indexedDB) return indexedDB;
                      if ("undefined" != typeof webkitIndexedDB)
                        return webkitIndexedDB;
                      if ("undefined" != typeof mozIndexedDB)
                        return mozIndexedDB;
                      if ("undefined" != typeof OIndexedDB) return OIndexedDB;
                      if ("undefined" != typeof msIndexedDB) return msIndexedDB;
                    } catch (e) {
                      return;
                    }
                  })();
                function i(t, n) {
                  (t = t || []), (n = n || {});
                  try {
                    return new Blob(t, n);
                  } catch (e) {
                    if ("TypeError" !== e.name) throw e;
                    for (
                      var o =
                          "undefined" != typeof BlobBuilder
                            ? BlobBuilder
                            : "undefined" != typeof MSBlobBuilder
                            ? MSBlobBuilder
                            : "undefined" != typeof MozBlobBuilder
                            ? MozBlobBuilder
                            : WebKitBlobBuilder,
                        s = new o(),
                        r = 0;
                      r < t.length;
                      r += 1
                    )
                      s.append(t[r]);
                    return s.getBlob(n.type);
                  }
                }
                "undefined" == typeof Promise && e(3);
                var S = Promise;
                function l(e, t) {
                  t &&
                    e.then(
                      function (e) {
                        t(null, e);
                      },
                      function (e) {
                        t(e);
                      }
                    );
                }
                function c(e, t, n) {
                  "function" == typeof t && e.then(t),
                    "function" == typeof n && e.catch(n);
                }
                function u(e) {
                  return "string" != typeof e && (e = String(e)), e;
                }
                function r() {
                  if (
                    arguments.length &&
                    "function" == typeof arguments[arguments.length - 1]
                  )
                    return arguments[arguments.length - 1];
                }
                var a = "local-forage-detect-blob-support",
                  s = void 0,
                  p = {},
                  C = Object.prototype.toString,
                  T = "readonly",
                  g = "readwrite";
                function d(e) {
                  return "boolean" == typeof s
                    ? S.resolve(s)
                    : ((o = e),
                      new S(function (n) {
                        var e = o.transaction(a, g),
                          t = i([""]);
                        e.objectStore(a).put(t, "key"),
                          (e.onabort = function (e) {
                            e.preventDefault(), e.stopPropagation(), n(!1);
                          }),
                          (e.oncomplete = function () {
                            var e = navigator.userAgent.match(/Chrome\/(\d+)/),
                              t = navigator.userAgent.match(/Edge\//);
                            n(t || !e || 43 <= parseInt(e[1], 10));
                          });
                      }).catch(function () {
                        return !1;
                      })).then(function (e) {
                        return (s = e);
                      });
                  var o;
                }
                function _(e) {
                  var t = p[e.name],
                    n = {};
                  (n.promise = new S(function (e, t) {
                    (n.resolve = e), (n.reject = t);
                  })),
                    t.deferredOperations.push(n),
                    t.dbReady
                      ? (t.dbReady = t.dbReady.then(function () {
                          return n.promise;
                        }))
                      : (t.dbReady = n.promise);
                }
                function h(e) {
                  var t = p[e.name],
                    n = t.deferredOperations.pop();
                  if (n) return n.resolve(), n.promise;
                }
                function A(e, t) {
                  var n = p[e.name],
                    o = n.deferredOperations.pop();
                  if (o) return o.reject(t), o.promise;
                }
                function I(s, r) {
                  return new S(function (e, t) {
                    if (
                      ((p[s.name] = p[s.name] || {
                        forages: [],
                        db: null,
                        dbReady: null,
                        deferredOperations: [],
                      }),
                      s.db)
                    ) {
                      if (!r) return e(s.db);
                      _(s), s.db.close();
                    }
                    var n = [s.name];
                    r && n.push(s.version);
                    var o = E.open.apply(E, n);
                    r &&
                      (o.onupgradeneeded = function (e) {
                        var t = o.result;
                        try {
                          t.createObjectStore(s.storeName),
                            e.oldVersion <= 1 && t.createObjectStore(a);
                        } catch (e) {
                          if ("ConstraintError" !== e.name) throw e;
                        }
                      }),
                      (o.onerror = function (e) {
                        e.preventDefault(), t(o.error);
                      }),
                      (o.onsuccess = function () {
                        e(o.result), h(s);
                      });
                  });
                }
                function f(e) {
                  return I(e, !1);
                }
                function R(e) {
                  return I(e, !0);
                }
                function O(e, t) {
                  if (!e.db) return !0;
                  var n = !e.db.objectStoreNames.contains(e.storeName),
                    o = e.version < e.db.version,
                    s = e.version > e.db.version;
                  if ((o && (e.version, (e.version = e.db.version)), s || n)) {
                    if (n) {
                      var r = e.db.version + 1;
                      r > e.version && (e.version = r);
                    }
                    return !0;
                  }
                  return !1;
                }
                function N(e) {
                  var t = (function (e) {
                    for (
                      var t = e.length,
                        n = new ArrayBuffer(t),
                        o = new Uint8Array(n),
                        s = 0;
                      s < t;
                      s++
                    )
                      o[s] = e.charCodeAt(s);
                    return n;
                  })(atob(e.data));
                  return i([t], { type: e.type });
                }
                function y(e) {
                  return e && e.__local_forage_encoded_blob;
                }
                function m(e) {
                  var t = this,
                    n = t._initReady().then(function () {
                      var e = p[t._dbInfo.name];
                      if (e && e.dbReady) return e.dbReady;
                    });
                  return c(n, e, e), n;
                }
                function L(t, n, o, s) {
                  void 0 === s && (s = 1);
                  try {
                    var e = t.db.transaction(t.storeName, n);
                    o(null, e);
                  } catch (e) {
                    if (
                      0 < s &&
                      (!t.db ||
                        "InvalidStateError" === e.name ||
                        "NotFoundError" === e.name)
                    )
                      return S.resolve()
                        .then(function () {
                          if (
                            !t.db ||
                            ("NotFoundError" === e.name &&
                              !t.db.objectStoreNames.contains(t.storeName) &&
                              t.version <= t.db.version)
                          )
                            return t.db && (t.version = t.db.version + 1), R(t);
                        })
                        .then(function () {
                          return (function (n) {
                            _(n);
                            for (
                              var o = p[n.name], s = o.forages, e = 0;
                              e < s.length;
                              e++
                            ) {
                              var t = s[e];
                              t._dbInfo.db &&
                                (t._dbInfo.db.close(), (t._dbInfo.db = null));
                            }
                            return (
                              (n.db = null),
                              f(n)
                                .then(function (e) {
                                  return (n.db = e), O(n) ? R(n) : e;
                                })
                                .then(function (e) {
                                  n.db = o.db = e;
                                  for (var t = 0; t < s.length; t++)
                                    s[t]._dbInfo.db = e;
                                })
                                .catch(function (e) {
                                  throw (A(n, e), e);
                                })
                            );
                          })(t).then(function () {
                            L(t, n, o, s - 1);
                          });
                        })
                        .catch(o);
                    o(e);
                  }
                }
                var M = {
                    _driver: "asyncStorage",
                    _initStorage: function (e) {
                      var o = this,
                        s = { db: null };
                      if (e) for (var t in e) s[t] = e[t];
                      var r = p[s.name];
                      r ||
                        ((r = {
                          forages: [],
                          db: null,
                          dbReady: null,
                          deferredOperations: [],
                        }),
                        (p[s.name] = r)),
                        r.forages.push(o),
                        o._initReady ||
                          ((o._initReady = o.ready), (o.ready = m));
                      var n = [];
                      function i() {
                        return S.resolve();
                      }
                      for (var a = 0; a < r.forages.length; a++) {
                        var E = r.forages[a];
                        E !== o && n.push(E._initReady().catch(i));
                      }
                      var c = r.forages.slice(0);
                      return S.all(n)
                        .then(function () {
                          return (s.db = r.db), f(s);
                        })
                        .then(function (e) {
                          return (
                            (s.db = e),
                            O(s, o._defaultConfig.version) ? R(s) : e
                          );
                        })
                        .then(function (e) {
                          (s.db = r.db = e), (o._dbInfo = s);
                          for (var t = 0; t < c.length; t++) {
                            var n = c[t];
                            n !== o &&
                              ((n._dbInfo.db = s.db),
                              (n._dbInfo.version = s.version));
                          }
                        });
                    },
                    _support: (function () {
                      try {
                        if (!E) return !1;
                        var e =
                            "undefined" != typeof openDatabase &&
                            /(Safari|iPhone|iPad|iPod)/.test(
                              navigator.userAgent
                            ) &&
                            !/Chrome/.test(navigator.userAgent) &&
                            !/BlackBerry/.test(navigator.platform),
                          t =
                            "function" == typeof fetch &&
                            -1 !== fetch.toString().indexOf("[native code");
                        return (
                          (!e || t) &&
                          "undefined" != typeof indexedDB &&
                          "undefined" != typeof IDBKeyRange
                        );
                      } catch (e) {
                        return !1;
                      }
                    })(),
                    iterate: function (a, e) {
                      var E = this,
                        t = new S(function (r, i) {
                          E.ready()
                            .then(function () {
                              L(E._dbInfo, T, function (e, t) {
                                if (e) return i(e);
                                try {
                                  var n = t.objectStore(E._dbInfo.storeName),
                                    o = n.openCursor(),
                                    s = 1;
                                  (o.onsuccess = function () {
                                    var e = o.result;
                                    if (e) {
                                      var t = e.value;
                                      y(t) && (t = N(t));
                                      var n = a(t, e.key, s++);
                                      void 0 !== n ? r(n) : e.continue();
                                    } else r();
                                  }),
                                    (o.onerror = function () {
                                      i(o.error);
                                    });
                                } catch (e) {
                                  i(e);
                                }
                              });
                            })
                            .catch(i);
                        });
                      return l(t, e), t;
                    },
                    getItem: function (i, e) {
                      var a = this;
                      i = u(i);
                      var t = new S(function (s, r) {
                        a.ready()
                          .then(function () {
                            L(a._dbInfo, T, function (e, t) {
                              if (e) return r(e);
                              try {
                                var n = t.objectStore(a._dbInfo.storeName),
                                  o = n.get(i);
                                (o.onsuccess = function () {
                                  var e = o.result;
                                  void 0 === e && (e = null),
                                    y(e) && (e = N(e)),
                                    s(e);
                                }),
                                  (o.onerror = function () {
                                    r(o.error);
                                  });
                              } catch (e) {
                                r(e);
                              }
                            });
                          })
                          .catch(r);
                      });
                      return l(t, e), t;
                    },
                    setItem: function (a, t, e) {
                      var E = this;
                      a = u(a);
                      var n = new S(function (r, i) {
                        var e;
                        E.ready()
                          .then(function () {
                            return (
                              (e = E._dbInfo),
                              "[object Blob]" === C.call(t)
                                ? d(e.db).then(function (e) {
                                    return e
                                      ? t
                                      : ((o = t),
                                        new S(function (n, e) {
                                          var t = new FileReader();
                                          (t.onerror = e),
                                            (t.onloadend = function (e) {
                                              var t = btoa(
                                                e.target.result || ""
                                              );
                                              n({
                                                __local_forage_encoded_blob: !0,
                                                data: t,
                                                type: o.type,
                                              });
                                            }),
                                            t.readAsBinaryString(o);
                                        }));
                                    var o;
                                  })
                                : t
                            );
                          })
                          .then(function (s) {
                            L(E._dbInfo, g, function (e, t) {
                              if (e) return i(e);
                              try {
                                var n = t.objectStore(E._dbInfo.storeName);
                                null === s && (s = void 0);
                                var o = n.put(s, a);
                                (t.oncomplete = function () {
                                  void 0 === s && (s = null), r(s);
                                }),
                                  (t.onabort = t.onerror =
                                    function () {
                                      var e = o.error
                                        ? o.error
                                        : o.transaction.error;
                                      i(e);
                                    });
                              } catch (e) {
                                i(e);
                              }
                            });
                          })
                          .catch(i);
                      });
                      return l(n, e), n;
                    },
                    removeItem: function (i, e) {
                      var a = this;
                      i = u(i);
                      var t = new S(function (s, r) {
                        a.ready()
                          .then(function () {
                            L(a._dbInfo, g, function (e, t) {
                              if (e) return r(e);
                              try {
                                var n = t.objectStore(a._dbInfo.storeName),
                                  o = n.delete(i);
                                (t.oncomplete = function () {
                                  s();
                                }),
                                  (t.onerror = function () {
                                    r(o.error);
                                  }),
                                  (t.onabort = function () {
                                    var e = o.error
                                      ? o.error
                                      : o.transaction.error;
                                    r(e);
                                  });
                              } catch (e) {
                                r(e);
                              }
                            });
                          })
                          .catch(r);
                      });
                      return l(t, e), t;
                    },
                    clear: function (e) {
                      var i = this,
                        t = new S(function (s, r) {
                          i.ready()
                            .then(function () {
                              L(i._dbInfo, g, function (e, t) {
                                if (e) return r(e);
                                try {
                                  var n = t.objectStore(i._dbInfo.storeName),
                                    o = n.clear();
                                  (t.oncomplete = function () {
                                    s();
                                  }),
                                    (t.onabort = t.onerror =
                                      function () {
                                        var e = o.error
                                          ? o.error
                                          : o.transaction.error;
                                        r(e);
                                      });
                                } catch (e) {
                                  r(e);
                                }
                              });
                            })
                            .catch(r);
                        });
                      return l(t, e), t;
                    },
                    length: function (e) {
                      var i = this,
                        t = new S(function (s, r) {
                          i.ready()
                            .then(function () {
                              L(i._dbInfo, T, function (e, t) {
                                if (e) return r(e);
                                try {
                                  var n = t.objectStore(i._dbInfo.storeName),
                                    o = n.count();
                                  (o.onsuccess = function () {
                                    s(o.result);
                                  }),
                                    (o.onerror = function () {
                                      r(o.error);
                                    });
                                } catch (e) {
                                  r(e);
                                }
                              });
                            })
                            .catch(r);
                        });
                      return l(t, e), t;
                    },
                    key: function (a, e) {
                      var E = this,
                        t = new S(function (r, i) {
                          a < 0
                            ? r(null)
                            : E.ready()
                                .then(function () {
                                  L(E._dbInfo, T, function (e, t) {
                                    if (e) return i(e);
                                    try {
                                      var n = t.objectStore(
                                          E._dbInfo.storeName
                                        ),
                                        o = !1,
                                        s = n.openCursor();
                                      (s.onsuccess = function () {
                                        var e = s.result;
                                        e
                                          ? 0 === a
                                            ? r(e.key)
                                            : o
                                            ? r(e.key)
                                            : ((o = !0), e.advance(a))
                                          : r(null);
                                      }),
                                        (s.onerror = function () {
                                          i(s.error);
                                        });
                                    } catch (e) {
                                      i(e);
                                    }
                                  });
                                })
                                .catch(i);
                        });
                      return l(t, e), t;
                    },
                    keys: function (e) {
                      var a = this,
                        t = new S(function (r, i) {
                          a.ready()
                            .then(function () {
                              L(a._dbInfo, T, function (e, t) {
                                if (e) return i(e);
                                try {
                                  var n = t.objectStore(a._dbInfo.storeName),
                                    o = n.openCursor(),
                                    s = [];
                                  (o.onsuccess = function () {
                                    var e = o.result;
                                    e ? (s.push(e.key), e.continue()) : r(s);
                                  }),
                                    (o.onerror = function () {
                                      i(o.error);
                                    });
                                } catch (e) {
                                  i(e);
                                }
                              });
                            })
                            .catch(i);
                        });
                      return l(t, e), t;
                    },
                    dropInstance: function (a, e) {
                      e = r.apply(this, arguments);
                      var t,
                        n = this.config();
                      if (
                        ((a = ("function" != typeof a && a) || {}).name ||
                          ((a.name = a.name || n.name),
                          (a.storeName = a.storeName || n.storeName)),
                        a.name)
                      ) {
                        var o = a.name === n.name && this._dbInfo.db,
                          s = o
                            ? S.resolve(this._dbInfo.db)
                            : f(a).then(function (e) {
                                var t = p[a.name],
                                  n = t.forages;
                                t.db = e;
                                for (var o = 0; o < n.length; o++)
                                  n[o]._dbInfo.db = e;
                                return e;
                              });
                        t = a.storeName
                          ? s.then(function (e) {
                              if (e.objectStoreNames.contains(a.storeName)) {
                                var s = e.version + 1;
                                _(a);
                                var o = p[a.name],
                                  r = o.forages;
                                e.close();
                                for (var t = 0; t < r.length; t++) {
                                  var n = r[t];
                                  (n._dbInfo.db = null),
                                    (n._dbInfo.version = s);
                                }
                                var i = new S(function (t, n) {
                                  var o = E.open(a.name, s);
                                  (o.onerror = function (e) {
                                    var t = o.result;
                                    t.close(), n(e);
                                  }),
                                    (o.onupgradeneeded = function () {
                                      var e = o.result;
                                      e.deleteObjectStore(a.storeName);
                                    }),
                                    (o.onsuccess = function () {
                                      var e = o.result;
                                      e.close(), t(e);
                                    });
                                });
                                return i
                                  .then(function (e) {
                                    o.db = e;
                                    for (var t = 0; t < r.length; t++) {
                                      var n = r[t];
                                      (n._dbInfo.db = e), h(n._dbInfo);
                                    }
                                  })
                                  .catch(function (e) {
                                    throw (
                                      ((A(a, e) || S.resolve()).catch(
                                        function () {}
                                      ),
                                      e)
                                    );
                                  });
                              }
                            })
                          : s.then(function (e) {
                              _(a);
                              var o = p[a.name],
                                s = o.forages;
                              e.close();
                              for (var t = 0; t < s.length; t++) {
                                var n = s[t];
                                n._dbInfo.db = null;
                              }
                              var r = new S(function (t, n) {
                                var o = E.deleteDatabase(a.name);
                                (o.onerror = o.onblocked =
                                  function (e) {
                                    var t = o.result;
                                    t && t.close(), n(e);
                                  }),
                                  (o.onsuccess = function () {
                                    var e = o.result;
                                    e && e.close(), t(e);
                                  });
                              });
                              return r
                                .then(function (e) {
                                  o.db = e;
                                  for (var t = 0; t < s.length; t++) {
                                    var n = s[t];
                                    h(n._dbInfo);
                                  }
                                })
                                .catch(function (e) {
                                  throw (
                                    ((A(a, e) || S.resolve()).catch(
                                      function () {}
                                    ),
                                    e)
                                  );
                                });
                            });
                      } else t = S.reject("Invalid arguments");
                      return l(t, e), t;
                    },
                  },
                  P =
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
                  v = /^~~local_forage_type~([^~]+)~/,
                  D = "__lfsc__:",
                  U = D.length,
                  Y = "arbf",
                  G = "blob",
                  w = U + Y.length,
                  K = Object.prototype.toString;
                function b(e) {
                  var t,
                    n,
                    o,
                    s,
                    r,
                    i = 0.75 * e.length,
                    a = e.length,
                    E = 0;
                  "=" === e[e.length - 1] &&
                    (i--, "=" === e[e.length - 2] && i--);
                  var c = new ArrayBuffer(i),
                    u = new Uint8Array(c);
                  for (t = 0; t < a; t += 4)
                    (n = P.indexOf(e[t])),
                      (o = P.indexOf(e[t + 1])),
                      (s = P.indexOf(e[t + 2])),
                      (r = P.indexOf(e[t + 3])),
                      (u[E++] = (n << 2) | (o >> 4)),
                      (u[E++] = ((15 & o) << 4) | (s >> 2)),
                      (u[E++] = ((3 & s) << 6) | (63 & r));
                  return c;
                }
                function F(e) {
                  var t,
                    n = new Uint8Array(e),
                    o = "";
                  for (t = 0; t < n.length; t += 3)
                    (o += P[n[t] >> 2]),
                      (o += P[((3 & n[t]) << 4) | (n[t + 1] >> 4)]),
                      (o += P[((15 & n[t + 1]) << 2) | (n[t + 2] >> 6)]),
                      (o += P[63 & n[t + 2]]);
                  return (
                    n.length % 3 == 2
                      ? (o = o.substring(0, o.length - 1) + "=")
                      : n.length % 3 == 1 &&
                        (o = o.substring(0, o.length - 2) + "=="),
                    o
                  );
                }
                var B = {
                  serialize: function (t, n) {
                    var e = "";
                    if (
                      (t && (e = K.call(t)),
                      t &&
                        ("[object ArrayBuffer]" === e ||
                          (t.buffer &&
                            "[object ArrayBuffer]" === K.call(t.buffer))))
                    ) {
                      var o,
                        s = D;
                      t instanceof ArrayBuffer
                        ? ((o = t), (s += Y))
                        : ((o = t.buffer),
                          "[object Int8Array]" === e
                            ? (s += "si08")
                            : "[object Uint8Array]" === e
                            ? (s += "ui08")
                            : "[object Uint8ClampedArray]" === e
                            ? (s += "uic8")
                            : "[object Int16Array]" === e
                            ? (s += "si16")
                            : "[object Uint16Array]" === e
                            ? (s += "ur16")
                            : "[object Int32Array]" === e
                            ? (s += "si32")
                            : "[object Uint32Array]" === e
                            ? (s += "ui32")
                            : "[object Float32Array]" === e
                            ? (s += "fl32")
                            : "[object Float64Array]" === e
                            ? (s += "fl64")
                            : n(
                                new Error("Failed to get type for BinaryArray")
                              )),
                        n(s + F(o));
                    } else if ("[object Blob]" === e) {
                      var r = new FileReader();
                      (r.onload = function () {
                        var e =
                          "~~local_forage_type~" +
                          t.type +
                          "~" +
                          F(this.result);
                        n(D + G + e);
                      }),
                        r.readAsArrayBuffer(t);
                    } else
                      try {
                        n(JSON.stringify(t));
                      } catch (e) {
                        n(null, e);
                      }
                  },
                  deserialize: function (e) {
                    if (e.substring(0, U) !== D) return JSON.parse(e);
                    var t,
                      n = e.substring(w),
                      o = e.substring(U, w);
                    if (o === G && v.test(n)) {
                      var s = n.match(v);
                      (t = s[1]), (n = n.substring(s[0].length));
                    }
                    var r = b(n);
                    switch (o) {
                      case Y:
                        return r;
                      case G:
                        return i([r], { type: t });
                      case "si08":
                        return new Int8Array(r);
                      case "ui08":
                        return new Uint8Array(r);
                      case "uic8":
                        return new Uint8ClampedArray(r);
                      case "si16":
                        return new Int16Array(r);
                      case "ur16":
                        return new Uint16Array(r);
                      case "si32":
                        return new Int32Array(r);
                      case "ui32":
                        return new Uint32Array(r);
                      case "fl32":
                        return new Float32Array(r);
                      case "fl64":
                        return new Float64Array(r);
                      default:
                        throw new Error("Unkown type: " + o);
                    }
                  },
                  stringToBuffer: b,
                  bufferToString: F,
                };
                function x(e, t, n, o) {
                  e.executeSql(
                    "CREATE TABLE IF NOT EXISTS " +
                      t.storeName +
                      " (id INTEGER PRIMARY KEY, key unique, value)",
                    [],
                    n,
                    o
                  );
                }
                function V(e, o, s, r, i, a) {
                  e.executeSql(
                    s,
                    r,
                    i,
                    function (e, n) {
                      n.code === n.SYNTAX_ERR
                        ? e.executeSql(
                            "SELECT name FROM sqlite_master WHERE type='table' AND name = ?",
                            [o.storeName],
                            function (e, t) {
                              t.rows.length
                                ? a(e, n)
                                : x(
                                    e,
                                    o,
                                    function () {
                                      e.executeSql(s, r, i, a);
                                    },
                                    a
                                  );
                            },
                            a
                          )
                        : a(e, n);
                    },
                    a
                  );
                }
                function J(i, e, a, E) {
                  var c = this;
                  i = u(i);
                  var t = new S(function (s, r) {
                    c.ready()
                      .then(function () {
                        void 0 === e && (e = null);
                        var n = e,
                          o = c._dbInfo;
                        o.serializer.serialize(e, function (t, e) {
                          e
                            ? r(e)
                            : o.db.transaction(
                                function (e) {
                                  V(
                                    e,
                                    o,
                                    "INSERT OR REPLACE INTO " +
                                      o.storeName +
                                      " (key, value) VALUES (?, ?)",
                                    [i, t],
                                    function () {
                                      s(n);
                                    },
                                    function (e, t) {
                                      r(t);
                                    }
                                  );
                                },
                                function (e) {
                                  if (e.code === e.QUOTA_ERR) {
                                    if (0 < E)
                                      return void s(
                                        J.apply(c, [i, n, a, E - 1])
                                      );
                                    r(e);
                                  }
                                }
                              );
                        });
                      })
                      .catch(r);
                  });
                  return l(t, a), t;
                }
                var H = {
                  _driver: "webSQLStorage",
                  _initStorage: function (e) {
                    var o = this,
                      s = { db: null };
                    if (e)
                      for (var t in e)
                        s[t] = "string" != typeof e[t] ? e[t].toString() : e[t];
                    var n = new S(function (t, n) {
                      try {
                        s.db = openDatabase(
                          s.name,
                          String(s.version),
                          s.description,
                          s.size
                        );
                      } catch (e) {
                        return n(e);
                      }
                      s.db.transaction(function (e) {
                        x(
                          e,
                          s,
                          function () {
                            (o._dbInfo = s), t();
                          },
                          function (e, t) {
                            n(t);
                          }
                        );
                      }, n);
                    });
                    return (s.serializer = B), n;
                  },
                  _support: "function" == typeof openDatabase,
                  iterate: function (c, e) {
                    var t = this,
                      n = new S(function (E, n) {
                        t.ready()
                          .then(function () {
                            var a = t._dbInfo;
                            a.db.transaction(function (e) {
                              V(
                                e,
                                a,
                                "SELECT * FROM " + a.storeName,
                                [],
                                function (e, t) {
                                  for (
                                    var n = t.rows, o = n.length, s = 0;
                                    s < o;
                                    s++
                                  ) {
                                    var r = n.item(s),
                                      i = r.value;
                                    if (
                                      (i && (i = a.serializer.deserialize(i)),
                                      void 0 !== (i = c(i, r.key, s + 1)))
                                    )
                                      return void E(i);
                                  }
                                  E();
                                },
                                function (e, t) {
                                  n(t);
                                }
                              );
                            });
                          })
                          .catch(n);
                      });
                    return l(n, e), n;
                  },
                  getItem: function (t, e) {
                    var r = this;
                    t = u(t);
                    var n = new S(function (s, n) {
                      r.ready()
                        .then(function () {
                          var o = r._dbInfo;
                          o.db.transaction(function (e) {
                            V(
                              e,
                              o,
                              "SELECT * FROM " +
                                o.storeName +
                                " WHERE key = ? LIMIT 1",
                              [t],
                              function (e, t) {
                                var n = t.rows.length
                                  ? t.rows.item(0).value
                                  : null;
                                n && (n = o.serializer.deserialize(n)), s(n);
                              },
                              function (e, t) {
                                n(t);
                              }
                            );
                          });
                        })
                        .catch(n);
                    });
                    return l(n, e), n;
                  },
                  setItem: function (e, t, n) {
                    return J.apply(this, [e, t, n, 1]);
                  },
                  removeItem: function (s, e) {
                    var r = this;
                    s = u(s);
                    var t = new S(function (n, o) {
                      r.ready()
                        .then(function () {
                          var t = r._dbInfo;
                          t.db.transaction(function (e) {
                            V(
                              e,
                              t,
                              "DELETE FROM " + t.storeName + " WHERE key = ?",
                              [s],
                              function () {
                                n();
                              },
                              function (e, t) {
                                o(t);
                              }
                            );
                          });
                        })
                        .catch(o);
                    });
                    return l(t, e), t;
                  },
                  clear: function (e) {
                    var s = this,
                      t = new S(function (n, o) {
                        s.ready()
                          .then(function () {
                            var t = s._dbInfo;
                            t.db.transaction(function (e) {
                              V(
                                e,
                                t,
                                "DELETE FROM " + t.storeName,
                                [],
                                function () {
                                  n();
                                },
                                function (e, t) {
                                  o(t);
                                }
                              );
                            });
                          })
                          .catch(o);
                      });
                    return l(t, e), t;
                  },
                  length: function (e) {
                    var s = this,
                      t = new S(function (o, n) {
                        s.ready()
                          .then(function () {
                            var t = s._dbInfo;
                            t.db.transaction(function (e) {
                              V(
                                e,
                                t,
                                "SELECT COUNT(key) as c FROM " + t.storeName,
                                [],
                                function (e, t) {
                                  var n = t.rows.item(0).c;
                                  o(n);
                                },
                                function (e, t) {
                                  n(t);
                                }
                              );
                            });
                          })
                          .catch(n);
                      });
                    return l(t, e), t;
                  },
                  key: function (s, e) {
                    var r = this,
                      t = new S(function (o, n) {
                        r.ready()
                          .then(function () {
                            var t = r._dbInfo;
                            t.db.transaction(function (e) {
                              V(
                                e,
                                t,
                                "SELECT key FROM " +
                                  t.storeName +
                                  " WHERE id = ? LIMIT 1",
                                [s + 1],
                                function (e, t) {
                                  var n = t.rows.length
                                    ? t.rows.item(0).key
                                    : null;
                                  o(n);
                                },
                                function (e, t) {
                                  n(t);
                                }
                              );
                            });
                          })
                          .catch(n);
                      });
                    return l(t, e), t;
                  },
                  keys: function (e) {
                    var o = this,
                      t = new S(function (s, n) {
                        o.ready()
                          .then(function () {
                            var t = o._dbInfo;
                            t.db.transaction(function (e) {
                              V(
                                e,
                                t,
                                "SELECT key FROM " + t.storeName,
                                [],
                                function (e, t) {
                                  for (
                                    var n = [], o = 0;
                                    o < t.rows.length;
                                    o++
                                  )
                                    n.push(t.rows.item(o).key);
                                  s(n);
                                },
                                function (e, t) {
                                  n(t);
                                }
                              );
                            });
                          })
                          .catch(n);
                      });
                    return l(t, e), t;
                  },
                  dropInstance: function (n, e) {
                    e = r.apply(this, arguments);
                    var o = this.config();
                    (n = ("function" != typeof n && n) || {}).name ||
                      ((n.name = n.name || o.name),
                      (n.storeName = n.storeName || o.storeName));
                    var t,
                      s = this;
                    return (
                      l(
                        (t = n.name
                          ? new S(function (e) {
                              var t, r;
                              (t =
                                n.name === o.name
                                  ? s._dbInfo.db
                                  : openDatabase(n.name, "", "", 0)),
                                n.storeName
                                  ? e({ db: t, storeNames: [n.storeName] })
                                  : e(
                                      ((r = t),
                                      new S(function (s, n) {
                                        r.transaction(
                                          function (e) {
                                            e.executeSql(
                                              "SELECT name FROM sqlite_master WHERE type='table' AND name <> '__WebKitDatabaseInfoTable__'",
                                              [],
                                              function (e, t) {
                                                for (
                                                  var n = [], o = 0;
                                                  o < t.rows.length;
                                                  o++
                                                )
                                                  n.push(t.rows.item(o).name);
                                                s({ db: r, storeNames: n });
                                              },
                                              function (e, t) {
                                                n(t);
                                              }
                                            );
                                          },
                                          function (e) {
                                            n(e);
                                          }
                                        );
                                      }))
                                    );
                            }).then(function (a) {
                              return new S(function (r, i) {
                                a.db.transaction(
                                  function (o) {
                                    function e(t) {
                                      return new S(function (e, n) {
                                        o.executeSql(
                                          "DROP TABLE IF EXISTS " + t,
                                          [],
                                          function () {
                                            e();
                                          },
                                          function (e, t) {
                                            n(t);
                                          }
                                        );
                                      });
                                    }
                                    for (
                                      var t = [],
                                        n = 0,
                                        s = a.storeNames.length;
                                      n < s;
                                      n++
                                    )
                                      t.push(e(a.storeNames[n]));
                                    S.all(t)
                                      .then(function () {
                                        r();
                                      })
                                      .catch(function (e) {
                                        i(e);
                                      });
                                  },
                                  function (e) {
                                    i(e);
                                  }
                                );
                              });
                            })
                          : S.reject("Invalid arguments")),
                        e
                      ),
                      t
                    );
                  },
                };
                function k(e, t) {
                  var n = e.name + "/";
                  return (
                    e.storeName !== t.storeName && (n += e.storeName + "/"), n
                  );
                }
                function W() {
                  return (
                    !(function () {
                      var e = "_localforage_support_test";
                      try {
                        return (
                          localStorage.setItem(e, !0),
                          localStorage.removeItem(e),
                          !1
                        );
                      } catch (e) {
                        return !0;
                      }
                    })() || 0 < localStorage.length
                  );
                }
                var j = {
                    _driver: "localStorageWrapper",
                    _initStorage: function (e) {
                      var t = {};
                      if (e) for (var n in e) t[n] = e[n];
                      return (
                        (t.keyPrefix = k(e, this._defaultConfig)),
                        W()
                          ? (((this._dbInfo = t).serializer = B), S.resolve())
                          : S.reject()
                      );
                    },
                    _support: (function () {
                      try {
                        return (
                          "undefined" != typeof localStorage &&
                          "setItem" in localStorage &&
                          !!localStorage.setItem
                        );
                      } catch (e) {
                        return !1;
                      }
                    })(),
                    iterate: function (E, e) {
                      var c = this,
                        t = c.ready().then(function () {
                          for (
                            var e = c._dbInfo,
                              t = e.keyPrefix,
                              n = t.length,
                              o = localStorage.length,
                              s = 1,
                              r = 0;
                            r < o;
                            r++
                          ) {
                            var i = localStorage.key(r);
                            if (0 === i.indexOf(t)) {
                              var a = localStorage.getItem(i);
                              if (
                                (a && (a = e.serializer.deserialize(a)),
                                void 0 !== (a = E(a, i.substring(n), s++)))
                              )
                                return a;
                            }
                          }
                        });
                      return l(t, e), t;
                    },
                    getItem: function (n, e) {
                      var o = this;
                      n = u(n);
                      var t = o.ready().then(function () {
                        var e = o._dbInfo,
                          t = localStorage.getItem(e.keyPrefix + n);
                        return t && (t = e.serializer.deserialize(t)), t;
                      });
                      return l(t, e), t;
                    },
                    setItem: function (i, e, t) {
                      var a = this;
                      i = u(i);
                      var n = a.ready().then(function () {
                        void 0 === e && (e = null);
                        var r = e;
                        return new S(function (n, o) {
                          var s = a._dbInfo;
                          s.serializer.serialize(e, function (e, t) {
                            if (t) o(t);
                            else
                              try {
                                localStorage.setItem(s.keyPrefix + i, e), n(r);
                              } catch (e) {
                                ("QuotaExceededError" !== e.name &&
                                  "NS_ERROR_DOM_QUOTA_REACHED" !== e.name) ||
                                  o(e),
                                  o(e);
                              }
                          });
                        });
                      });
                      return l(n, t), n;
                    },
                    removeItem: function (t, e) {
                      var n = this;
                      t = u(t);
                      var o = n.ready().then(function () {
                        var e = n._dbInfo;
                        localStorage.removeItem(e.keyPrefix + t);
                      });
                      return l(o, e), o;
                    },
                    clear: function (e) {
                      var o = this,
                        t = o.ready().then(function () {
                          for (
                            var e = o._dbInfo.keyPrefix,
                              t = localStorage.length - 1;
                            0 <= t;
                            t--
                          ) {
                            var n = localStorage.key(t);
                            0 === n.indexOf(e) && localStorage.removeItem(n);
                          }
                        });
                      return l(t, e), t;
                    },
                    length: function (e) {
                      var t = this.keys().then(function (e) {
                        return e.length;
                      });
                      return l(t, e), t;
                    },
                    key: function (n, e) {
                      var o = this,
                        t = o.ready().then(function () {
                          var t,
                            e = o._dbInfo;
                          try {
                            t = localStorage.key(n);
                          } catch (e) {
                            t = null;
                          }
                          return t && (t = t.substring(e.keyPrefix.length)), t;
                        });
                      return l(t, e), t;
                    },
                    keys: function (e) {
                      var r = this,
                        t = r.ready().then(function () {
                          for (
                            var e = r._dbInfo,
                              t = localStorage.length,
                              n = [],
                              o = 0;
                            o < t;
                            o++
                          ) {
                            var s = localStorage.key(o);
                            0 === s.indexOf(e.keyPrefix) &&
                              n.push(s.substring(e.keyPrefix.length));
                          }
                          return n;
                        });
                      return l(t, e), t;
                    },
                    dropInstance: function (t, e) {
                      if (
                        ((e = r.apply(this, arguments)),
                        !(t = ("function" != typeof t && t) || {}).name)
                      ) {
                        var n = this.config();
                        (t.name = t.name || n.name),
                          (t.storeName = t.storeName || n.storeName);
                      }
                      var o,
                        s = this;
                      return (
                        l(
                          (o = t.name
                            ? new S(function (e) {
                                t.storeName
                                  ? e(k(t, s._defaultConfig))
                                  : e(t.name + "/");
                              }).then(function (e) {
                                for (
                                  var t = localStorage.length - 1;
                                  0 <= t;
                                  t--
                                ) {
                                  var n = localStorage.key(t);
                                  0 === n.indexOf(e) &&
                                    localStorage.removeItem(n);
                                }
                              })
                            : S.reject("Invalid arguments")),
                          e
                        ),
                        o
                      );
                    },
                  },
                  X = function (e, t) {
                    for (var n = e.length, o = 0; o < n; ) {
                      if (
                        (s = e[o]) === (r = t) ||
                        ("number" == typeof s &&
                          "number" == typeof r &&
                          isNaN(s) &&
                          isNaN(r))
                      )
                        return !0;
                      o++;
                    }
                    var s, r;
                    return !1;
                  },
                  q =
                    Array.isArray ||
                    function (e) {
                      return (
                        "[object Array]" === Object.prototype.toString.call(e)
                      );
                    },
                  Q = {},
                  z = {},
                  Z = { INDEXEDDB: M, WEBSQL: H, LOCALSTORAGE: j },
                  $ = [
                    Z.INDEXEDDB._driver,
                    Z.WEBSQL._driver,
                    Z.LOCALSTORAGE._driver,
                  ],
                  ee = ["dropInstance"],
                  te = [
                    "clear",
                    "getItem",
                    "iterate",
                    "key",
                    "keys",
                    "length",
                    "removeItem",
                    "setItem",
                  ].concat(ee),
                  ne = {
                    description: "",
                    driver: $.slice(),
                    name: "localforage",
                    size: 4980736,
                    storeName: "keyvaluepairs",
                    version: 1,
                  };
                function oe(t, n) {
                  t[n] = function () {
                    var e = arguments;
                    return t.ready().then(function () {
                      return t[n].apply(t, e);
                    });
                  };
                }
                function se() {
                  for (var e = 1; e < arguments.length; e++) {
                    var t = arguments[e];
                    if (t)
                      for (var n in t)
                        t.hasOwnProperty(n) &&
                          (q(t[n])
                            ? (arguments[0][n] = t[n].slice())
                            : (arguments[0][n] = t[n]));
                  }
                  return arguments[0];
                }
                var re = (function () {
                    function s(e) {
                      for (var t in ((function (e, t) {
                        if (!(e instanceof t))
                          throw new TypeError(
                            "Cannot call a class as a function"
                          );
                      })(this, s),
                      Z))
                        if (Z.hasOwnProperty(t)) {
                          var n = Z[t],
                            o = n._driver;
                          (this[t] = o), Q[o] || this.defineDriver(n);
                        }
                      (this._defaultConfig = se({}, ne)),
                        (this._config = se({}, this._defaultConfig, e)),
                        (this._driverSet = null),
                        (this._initDriver = null),
                        (this._ready = !1),
                        (this._dbInfo = null),
                        this._wrapLibraryMethodsWithReady(),
                        this.setDriver(this._config.driver).catch(
                          function () {}
                        );
                    }
                    return (
                      (s.prototype.config = function (e) {
                        if ("object" !== (void 0 === e ? "undefined" : o(e)))
                          return "string" == typeof e
                            ? this._config[e]
                            : this._config;
                        if (this._ready)
                          return new Error(
                            "Can't call config() after localforage has been used."
                          );
                        for (var t in e) {
                          if (
                            ("storeName" === t &&
                              (e[t] = e[t].replace(/\W/g, "_")),
                            "version" === t && "number" != typeof e[t])
                          )
                            return new Error(
                              "Database version must be a number."
                            );
                          this._config[t] = e[t];
                        }
                        return (
                          !("driver" in e && e.driver) ||
                          this.setDriver(this._config.driver)
                        );
                      }),
                      (s.prototype.defineDriver = function (u, e, t) {
                        var n = new S(function (t, n) {
                          try {
                            var o = u._driver,
                              e = new Error(
                                "Custom driver not compliant; see https://mozilla.github.io/localForage/#definedriver"
                              );
                            if (!u._driver) return void n(e);
                            for (
                              var s = te.concat("_initStorage"),
                                r = 0,
                                i = s.length;
                              r < i;
                              r++
                            ) {
                              var a = s[r],
                                E = !X(ee, a);
                              if ((E || u[a]) && "function" != typeof u[a])
                                return void n(e);
                            }
                            !(function () {
                              for (
                                var e = function (n) {
                                    return function () {
                                      var e = new Error(
                                          "Method " +
                                            n +
                                            " is not implemented by the current driver"
                                        ),
                                        t = S.reject(e);
                                      return (
                                        l(t, arguments[arguments.length - 1]), t
                                      );
                                    };
                                  },
                                  t = 0,
                                  n = ee.length;
                                t < n;
                                t++
                              ) {
                                var o = ee[t];
                                u[o] || (u[o] = e(o));
                              }
                            })();
                            var c = function (e) {
                              Q[o], (Q[o] = u), (z[o] = e), t();
                            };
                            "_support" in u
                              ? u._support && "function" == typeof u._support
                                ? u._support().then(c, n)
                                : c(!!u._support)
                              : c(!0);
                          } catch (e) {
                            n(e);
                          }
                        });
                        return c(n, e, t), n;
                      }),
                      (s.prototype.driver = function () {
                        return this._driver || null;
                      }),
                      (s.prototype.getDriver = function (e, t, n) {
                        var o = Q[e]
                          ? S.resolve(Q[e])
                          : S.reject(new Error("Driver not found."));
                        return c(o, t, n), o;
                      }),
                      (s.prototype.getSerializer = function (e) {
                        var t = S.resolve(B);
                        return c(t, e), t;
                      }),
                      (s.prototype.ready = function (e) {
                        var t = this,
                          n = t._driverSet.then(function () {
                            return (
                              null === t._ready && (t._ready = t._initDriver()),
                              t._ready
                            );
                          });
                        return c(n, e, e), n;
                      }),
                      (s.prototype.setDriver = function (e, t, n) {
                        var r = this;
                        q(e) || (e = [e]);
                        var o = this._getSupportedDrivers(e);
                        function i() {
                          r._config.driver = r.driver();
                        }
                        function a(e) {
                          return (
                            r._extend(e),
                            i(),
                            (r._ready = r._initStorage(r._config)),
                            r._ready
                          );
                        }
                        var s =
                          null !== this._driverSet
                            ? this._driverSet.catch(function () {
                                return S.resolve();
                              })
                            : S.resolve();
                        return (
                          (this._driverSet = s
                            .then(function () {
                              var e = o[0];
                              return (
                                (r._dbInfo = null),
                                (r._ready = null),
                                r.getDriver(e).then(function (e) {
                                  var s;
                                  (r._driver = e._driver),
                                    i(),
                                    r._wrapLibraryMethodsWithReady(),
                                    (r._initDriver =
                                      ((s = o),
                                      function () {
                                        var n = 0;
                                        function o() {
                                          for (; n < s.length; ) {
                                            var e = s[n];
                                            return (
                                              n++,
                                              (r._dbInfo = null),
                                              (r._ready = null),
                                              r.getDriver(e).then(a).catch(o)
                                            );
                                          }
                                          i();
                                          var t = new Error(
                                            "No available storage method found."
                                          );
                                          return (
                                            (r._driverSet = S.reject(t)),
                                            r._driverSet
                                          );
                                        }
                                        return o();
                                      }));
                                })
                              );
                            })
                            .catch(function () {
                              i();
                              var e = new Error(
                                "No available storage method found."
                              );
                              return (r._driverSet = S.reject(e)), r._driverSet;
                            })),
                          c(this._driverSet, t, n),
                          this._driverSet
                        );
                      }),
                      (s.prototype.supports = function (e) {
                        return !!z[e];
                      }),
                      (s.prototype._extend = function (e) {
                        se(this, e);
                      }),
                      (s.prototype._getSupportedDrivers = function (e) {
                        for (var t = [], n = 0, o = e.length; n < o; n++) {
                          var s = e[n];
                          this.supports(s) && t.push(s);
                        }
                        return t;
                      }),
                      (s.prototype._wrapLibraryMethodsWithReady = function () {
                        for (var e = 0, t = te.length; e < t; e++)
                          oe(this, te[e]);
                      }),
                      (s.prototype.createInstance = function (e) {
                        return new s(e);
                      }),
                      s
                    );
                  })(),
                  ie = new re();
                t.exports = ie;
              },
              { 3: 3 },
            ],
          },
          {},
          [4]
        )(4);
      }.call(this, n(37)));
    },
    function (e, t, n) {
      "use strict";
      (t.__esModule = !0),
        (t.SERVER_ERRORS = {
          AUTH_ERR: {
            code: "AUTH_ERR_AUTH_TOKEN_NOT_FOUND",
            message:
              "The auth token %s% does not exist. Please make sure you are logged in and have a valid auth token or try login again.",
          },
        }),
        (t.ERRORS = {
          PARAMETER_MISSING: {
            code: "MISSING_PARAMETERS",
            name: "Invalid or no parameter passed to the method.",
          },
        }),
        (t.INIT_ERROR = {
          NO_APP_ID: {
            code: t.ERRORS.PARAMETER_MISSING.code,
            name: t.ERRORS.PARAMETER_MISSING.name,
            message: "AppID cannot be empty. Please specify a valid appID.",
            details: {},
          },
        }),
        (t.GROUP_CREATION_ERRORS = {
          EMPTY_PASSWORD: {
            code: "ERR_EMPTY_GROUP_PASS",
            details: void 0,
            message: "Password is mandatory to join a group.",
            name: void 0,
          },
        }),
        (t.USERS_REQUEST_ERRORS = {
          EMPTY_USERS_LIST: {
            code: "EMPTY_USERS_LIST",
            name: "EMPTY_USERS_LIST",
            message: "The users list needs to have atleast one UID.",
            details: {},
          },
        }),
        (t.MESSAGES_REQUEST_ERRORS = {
          REQUEST_IN_PROGRESS_ERROR: {
            code: "REQUEST_IN_PROGRESS",
            name: "REQUEST_IN_PROGRESS",
            message: "Request in progress.",
            details: {},
          },
          NOT_ENOUGH_PARAMS: {
            code: "NOT_ENOUGH_PARAMETERS",
            name: "NOT_ENOUGH_PARAMETERS",
            message:
              "`Timestamp`, `MessageId` or `updatedAfter` is required to use the 'fetchNext()' method.",
            details: {},
          },
        }),
        (t.MESSAGE_ERRORS = {
          INVALID_CUSTOM_DATA: {
            code: "-1",
            name: "%s_CUSTOM_DATA",
            message: "",
            details: {},
          },
        }),
        (t.LOGIN_ERROR = {
          NOT_INITIALIZED: {
            code: "-1",
            name: "COMETCHAT_INITIALIZATION_NOT_DONE",
            message:
              "please initialize the cometchat before using login method.",
            details: {},
          },
          UNAUTHORISED: {
            code: 401,
            name: "USER_NOT_AUTHORISED",
            message:
              "The `authToken` of the user is not authorised. Please verify again.",
            details: {},
          },
          WS_CONNECTION_FAIL: {
            code: -1,
            name: "WS_CONNECTION_FAIL",
            message: "WS Connection failed. %s",
            details: {},
          },
          WS_CONNECTION_FAIL_PORT_ERROR: {
            code: -1,
            name: "WS_CONNECTION_FAIL",
            message: "WS Connection failed. Trying to connect with port: %s",
            details: {},
          },
          WS_CONNECTION_FALLBACK_FAIL_PORT_ERROR: {
            code: -1,
            name: "WS_CONNECTION_FALLBACK_FAIL",
            message:
              "WS Connection fallback failed. Trying to connect with port: %s",
            details: {},
          },
          WS_AUTH_FAIL: {
            code: -1,
            name: "WS_AUTH_FAIL",
            message: "WS username/password not correct.",
            details: {},
          },
          NO_INTERNET: {
            code: -1,
            name: "NO_INTERNET_CONNECTION",
            message: "You do not have internet connection.",
            details: {},
          },
          REQUEST_IN_PROGRESS: {
            code: -1,
            name: "LOGIN_IN_PROGRESS",
            message: "Please wait until the previous login request ends.",
            details: {},
          },
        }),
        (t.TYPINGNOTIFICATION_CONSTANTS = {
          TOO_MANY_REQUEST: {
            code: "TOO_MANY_REQUEST",
            name: "TOO MANY REQUEST",
            message:
              "too many request, wait for `%s` seconds before sending next request.",
            details: {},
          },
        }),
        (t.FETCH_ERROR = {
          ERROR_IN_API_CALL: {
            code: "FAILED_TO_FETCH",
            name: "FAILED_TO_FETCH",
            message:
              "There is an unknown issue with the API request. Please check your internet connection and verify the api call.",
            details: {},
          },
        });
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(8),
        s = n(1),
        r = n(0),
        i = (function () {
          function e() {}
          return (
            (e.trasformJSONGroup = function (e) {
              var t;
              try {
                (t = new o.Group(
                  e[s.GroupConstants.KEYS.GUID],
                  e[s.GroupConstants.KEYS.NAME],
                  e[s.GroupConstants.KEYS.TYPE]
                )),
                  e.wsChannel && delete e.wsChannel,
                  Object.assign(t, e),
                  (e = t);
              } catch (e) {
                r.Logger.error("GroupsController:transformJSONGroup", e);
              }
              return e;
            }),
            e
          );
        })();
      t.GroupsController = i;
    },
    function (e, t, n) {
      "use strict";
      var o,
        r =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          }),
        i =
          (this && this.__assign) ||
          function () {
            return (i =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          };
      t.__esModule = !0;
      var s = n(7),
        a = n(1),
        E = n(4),
        c = n(8),
        u = n(0),
        S = (function (s) {
          function e(e, t, n) {
            var o =
              s.call(this, e, "text", n, a.MessageCategory.MESSAGE) || this;
            return (o.type = "text"), (o.data = { text: t }), (o.text = t), o;
          }
          return (
            r(e, s),
            (e.prototype.getSender = function () {
              try {
                return this.getSenderFromData();
              } catch (e) {
                return this.sender;
              }
            }),
            (e.prototype.getReceiver = function () {
              try {
                return this.getReceiverFromData();
              } catch (e) {
                return this.receiver;
              }
            }),
            (e.prototype.getMetadata = function () {
              return (
                this.data.metadata && (this.metadata = this.data.metadata),
                this.metadata
              );
            }),
            (e.prototype.setMetadata = function (e) {
              (this.metadata = e),
                (this.data = i({}, this.data, { metadata: e }));
            }),
            (e.prototype.getData = function () {
              return this.data;
            }),
            (e.prototype.setData = function (e) {
              this.data = e;
            }),
            (e.prototype.getText = function () {
              return this.text;
            }),
            (e.prototype.setText = function (e) {
              (this.text = e), (this.data.text = e);
            }),
            (e.prototype.setProcessedText = function (e) {
              this.processedText = e;
            }),
            (e.prototype.getProcessedText = function () {
              return this.processedText;
            }),
            (e.prototype.getSenderFromData = function () {
              try {
                var e = this.getData();
                if (
                  e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    a.MessageConstatnts.KEYS.SENDER
                  ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                )
                  return new E.User(
                    e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      a.MessageConstatnts.KEYS.SENDER
                    ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                  );
              } catch (e) {
                u.Logger.error("TextMessageModel: getSenderFromData", e);
              }
            }),
            (e.prototype.getReceiverFromData = function () {
              try {
                var e = this.getData(),
                  t =
                    e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      a.MessageConstatnts.KEYS.RECEIVER
                    ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY];
                if (
                  e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    a.MessageConstatnts.KEYS.RECEIVER
                  ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITYTYPE] ==
                  [a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITY_TYPE.USER]
                )
                  return new E.User(t);
                var n = new c.Group(
                  t[a.GroupConstants.KEYS.GUID],
                  t[a.GroupConstants.KEYS.NAME],
                  t[a.MessageConstatnts.KEYS.TYPE]
                );
                return Object.assign(n, t);
              } catch (e) {
                u.Logger.error("TextMessageModel: getReceiverFromData", e);
              }
            }),
            (e.TYPE = a.MessageConstatnts.TYPE.TEXT),
            (e.RECEIVER_TYPE = a.MessageConstatnts.RECEIVER_TYPE),
            (e.CATEGORY = a.MessageConstatnts.CATEGORY.MESSAGE),
            e
          );
        })(s.BaseMessage);
      t.TextMessage = S;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          }),
        i =
          (this && this.__assign) ||
          function () {
            return (i =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          };
      t.__esModule = !0;
      var r = n(7),
        a = n(1),
        E = n(4),
        c = n(18),
        u = n(8),
        S = n(0),
        l = (function (r) {
          function e(e, t, n, o) {
            var s = r.call(this, e, n, o, a.MessageCategory.MESSAGE) || this;
            return (
              (s.data = {}),
              "object" != typeof t ? (s.data = { url: t }) : (s.file = t),
              s
            );
          }
          return (
            s(e, r),
            (e.prototype.setCaption = function (e) {
              (this.caption = e),
                (this.data[a.MessageConstatnts.KEYS.TEXT] = e);
            }),
            (e.prototype.getCaption = function () {
              return this.data[a.MessageConstatnts.KEYS.TEXT];
            }),
            (e.prototype.getSender = function () {
              try {
                return this.getSenderFromData();
              } catch (e) {
                return this.sender;
              }
            }),
            (e.prototype.getReceiver = function () {
              try {
                return this.getReceiverFromData();
              } catch (e) {
                return this.receiver;
              }
            }),
            (e.prototype.getMetadata = function () {
              return (
                this.data.metadata && (this.metadata = this.data.metadata),
                this.metadata
              );
            }),
            (e.prototype.setMetadata = function (e) {
              (this.metadata = e),
                (this.data = i({}, this.data, { metadata: e }));
            }),
            (e.prototype.getData = function () {
              return this.data;
            }),
            (e.prototype.setData = function (e) {
              this.data = e;
            }),
            (e.prototype.getAttachment = function () {
              return new c.Attachment(this.data.attachments[0]);
            }),
            (e.prototype.setAttachment = function (e) {
              this.data.attachments = [e.toJSON(e)];
            }),
            (e.prototype.getURL = function () {
              try {
                var e = this.getData();
                if (e[a.MessageConstatnts.KEYS.URL])
                  return e[a.MessageConstatnts.KEYS.URL];
              } catch (e) {
                S.Logger.error("MediaMessageModel: getURL", e);
              }
            }),
            (e.prototype.getSenderFromData = function () {
              try {
                var e = this.getData();
                if (
                  e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    a.MessageConstatnts.KEYS.SENDER
                  ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                )
                  return new E.User(
                    e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      a.MessageConstatnts.KEYS.SENDER
                    ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                  );
              } catch (e) {
                S.Logger.error("MediaMessageModel: getSenderFromData", e);
              }
            }),
            (e.prototype.getReceiverFromData = function () {
              try {
                var e = this.getData(),
                  t =
                    e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      a.MessageConstatnts.KEYS.RECEIVER
                    ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY];
                if (
                  e[a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    a.MessageConstatnts.KEYS.RECEIVER
                  ][a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITYTYPE] ==
                  [a.ResponseConstants.RESPONSE_KEYS.KEY_ENTITY_TYPE.USER]
                )
                  return new E.User(t);
                var n = new u.Group(
                  t[a.GroupConstants.KEYS.GUID],
                  t[a.GroupConstants.KEYS.NAME],
                  t[a.MessageConstatnts.KEYS.TYPE]
                );
                return Object.assign(n, t);
              } catch (e) {
                S.Logger.error("MediaMessageModel: getReceiverFromData", e);
              }
            }),
            (e.TYPE = a.MessageConstatnts.TYPE),
            (e.RECEIVER_TYPE = a.MessageConstatnts.RECEIVER_TYPE),
            (e.CATEGORY = a.MessageConstatnts.CATEGORY),
            e
          );
        })(r.BaseMessage);
      t.MediaMessage = l;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = (function () {
          function t(e) {
            e.hasOwnProperty(o.ATTACHMENTS_CONSTANTS.KEYS.EXTENSION) &&
              (this.extension = e[o.ATTACHMENTS_CONSTANTS.KEYS.EXTENSION]),
              e.hasOwnProperty(o.ATTACHMENTS_CONSTANTS.KEYS.MIME_TYPE) &&
                (this.mimeType = e[o.ATTACHMENTS_CONSTANTS.KEYS.MIME_TYPE]),
              e.hasOwnProperty(o.ATTACHMENTS_CONSTANTS.KEYS.NAME) &&
                (this.name = e[o.ATTACHMENTS_CONSTANTS.KEYS.NAME]),
              e.hasOwnProperty(o.ATTACHMENTS_CONSTANTS.KEYS.SIZE) &&
                (this.size = e[o.ATTACHMENTS_CONSTANTS.KEYS.SIZE]),
              e.hasOwnProperty(o.ATTACHMENTS_CONSTANTS.KEYS.URL) &&
                (this.url = e[o.ATTACHMENTS_CONSTANTS.KEYS.URL]);
          }
          return (
            (t.prototype.createFileFromJSON = function (e) {
              return new t(e);
            }),
            (t.prototype.toJSON = function (e) {
              return {
                extension: e.getExtension(),
                mimeType: e.getMimeType(),
                name: e.getName(),
                url: e.getUrl(),
                size: e.getSize(),
              };
            }),
            (t.prototype.getExtension = function () {
              return this.extension;
            }),
            (t.prototype.setExtension = function (e) {
              this.extension = e;
            }),
            (t.prototype.getMimeType = function () {
              return this.mimeType;
            }),
            (t.prototype.setMimeType = function (e) {
              this.mimeType = e;
            }),
            (t.prototype.getName = function () {
              return this.name;
            }),
            (t.prototype.setName = function (e) {
              this.name = e;
            }),
            (t.prototype.getSize = function () {
              return this.size;
            }),
            (t.prototype.setSize = function (e) {
              this.size = e;
            }),
            (t.prototype.getUrl = function () {
              return this.url;
            }),
            (t.prototype.setUrl = function (e) {
              this.url = e;
            }),
            t
          );
        })();
      t.Attachment = s;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          }),
        r =
          (this && this.__assign) ||
          function () {
            return (r =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          };
      t.__esModule = !0;
      var i = n(7),
        E = n(1),
        c = n(4),
        u = n(0),
        S = n(8),
        a = (function (a) {
          function e() {
            for (var e = [], t = 0; t < arguments.length; t++)
              e[t] = arguments[t];
            var n = this;
            if (3 == e.length) {
              var o = e[0],
                s = e[1],
                r = e[2];
              (n =
                a.call(
                  this,
                  o,
                  E.MessageConstatnts.TYPE.CUSTOM,
                  s,
                  E.MessageCategory.MESSAGE
                ) || this),
                ("object" != typeof r || Array.isArray(r)) && (r = {}),
                (n.customData = r),
                u.isFalsy(n.data) && (n.data = {}),
                (n.data[E.MessageConstatnts.KEYS.CUSTOM_DATA] = r);
            }
            if (4 == e.length) {
              (o = e[0]), (s = e[1]);
              var i = e[2];
              r = e[3];
              (n = a.call(this, o, i, s, E.MessageCategory.CUSTOM) || this),
                ("object" != typeof r || Array.isArray(r)) && (r = {}),
                (n.customData = r),
                u.isFalsy(n.data) && (n.data = {}),
                (n.data[E.MessageConstatnts.KEYS.CUSTOM_DATA] = r);
            }
            return n;
          }
          return (
            s(e, a),
            (e.prototype.getCustomData = function () {
              return this.customData;
            }),
            (e.prototype.setCustomData = function (e) {
              (this.customData = e),
                (this.data[E.MessageConstatnts.KEYS.CUSTOM_DATA] = e);
            }),
            (e.prototype.getSender = function () {
              try {
                return this.getSenderFromData();
              } catch (e) {
                return this.sender;
              }
            }),
            (e.prototype.getReceiver = function () {
              try {
                return this.getReceiverFromData();
              } catch (e) {
                return this.receiver;
              }
            }),
            (e.prototype.getSubType = function () {
              return this.subType;
            }),
            (e.prototype.setSubType = function (e) {
              (this.subType = e),
                (this.data = r({}, this.data, { subType: e }));
            }),
            (e.prototype.getMetadata = function () {
              return this.metadata;
            }),
            (e.prototype.setMetadata = function (e) {
              (this.metadata = e),
                (this.data = r({}, this.data, { metadata: e }));
            }),
            (e.prototype.getData = function () {
              return this.data;
            }),
            (e.prototype.getSenderFromData = function () {
              try {
                var e = this.getData();
                if (
                  e[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    E.MessageConstatnts.KEYS.SENDER
                  ][E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                )
                  return new c.User(
                    e[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      E.MessageConstatnts.KEYS.SENDER
                    ][E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY]
                  );
              } catch (e) {
                u.Logger.error("CustomMessageModel: getSenderFromData", e);
              }
            }),
            (e.prototype.getReceiverFromData = function () {
              try {
                var e = this.getData(),
                  t =
                    e[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                      E.MessageConstatnts.KEYS.RECEIVER
                    ][E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITITY];
                if (
                  e[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES][
                    E.MessageConstatnts.KEYS.RECEIVER
                  ][E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITYTYPE] ==
                  [E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITY_TYPE.USER]
                )
                  return new c.User(t);
                var n = new S.Group(
                  t[E.GroupConstants.KEYS.GUID],
                  t[E.GroupConstants.KEYS.NAME],
                  t[E.MessageConstatnts.KEYS.TYPE]
                );
                return Object.assign(n, t);
              } catch (e) {
                u.Logger.error("CustomMessageModel: getReceiverFromData", e);
              }
            }),
            e
          );
        })(i.BaseMessage);
      t.CustomMessage = a;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          }),
        r =
          (this && this.__assign) ||
          function () {
            return (r =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          };
      t.__esModule = !0;
      var i = n(7),
        c = n(8),
        u = n(4),
        S = n(1),
        a = n(0),
        l = n(10),
        E = (function (e) {
          function t() {
            return (null !== e && e.apply(this, arguments)) || this;
          }
          return (
            s(t, e),
            (t.actionFromJSON = function (e) {
              var t = new this(
                  e[S.MessageConstatnts.KEYS.RECEIVER],
                  e[S.MessageConstatnts.KEYS.TYPE],
                  e[S.MessageConstatnts.KEYS.RECEIVER_TYPE],
                  S.MessageCategory.ACTION
                ),
                n = e[S.MessageConstatnts.KEYS.DATA];
              if (
                ((t.data = n),
                t.setAction(n[S.MessageConstatnts.KEYS.ACTION]),
                n[S.ActionConstatnts.ACTION_KEYS.EXTRAS])
              ) {
                var o = n[S.ActionConstatnts.ACTION_KEYS.EXTRAS];
                if (o[S.ActionConstatnts.ACTION_KEYS.SCOPE]) {
                  var s = o[S.ActionConstatnts.ACTION_KEYS.SCOPE];
                  s[S.ActionConstatnts.ACTION_KEYS.NEW] &&
                    s[S.ActionConstatnts.ACTION_KEYS.OLD] &&
                    (t.setOldScope(s[S.ActionConstatnts.ACTION_KEYS.OLD]),
                    t.setNewScope(s[S.ActionConstatnts.ACTION_KEYS.NEW]));
                }
              }
              n[S.MessageConstatnts.KEYS.METADATA] &&
                t.setMetadata(n[S.MessageConstatnts.KEYS.METADATA]);
              var r = n[S.ActionConstatnts.ACTION_KEYS.ENTITIES];
              if (r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY])
                if (
                  r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                  ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.USER
                ) {
                  var i = new u.User(
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  );
                  Object.assign(
                    i,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionBy = i),
                    t.setSender(i);
                } else {
                  var a = new c.Group("", "", "");
                  Object.assign(
                    a,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionBy = a);
                }
              if (r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR])
                if (
                  r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                  ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.USER
                ) {
                  i = new u.User(
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  );
                  Object.assign(
                    i,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionFor = i);
                } else {
                  a = new c.Group("", "", "");
                  Object.assign(
                    a,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionFor = a);
                }
              if (r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON])
                if (
                  r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                  ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.USER
                ) {
                  i = new u.User(
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  );
                  Object.assign(
                    i,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionOn = i);
                } else if (
                  r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                  ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.GROUP
                ) {
                  a = new c.Group("", "", "");
                  Object.assign(
                    a,
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                    (t.actionOn = a);
                } else if (
                  r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                  ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.MESSAGE
                ) {
                  var E = l.MessageController.trasformJSONMessge(
                    r[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  );
                  t.actionOn = E;
                }
              return t.setMessage(t.getActionMessage(t)), t;
            }),
            (t.prototype.getOldScope = function () {
              return this.oldScope;
            }),
            (t.prototype.getNewScope = function () {
              return this.newScope;
            }),
            (t.prototype.setNewScope = function (e) {
              this.newScope = e;
            }),
            (t.prototype.setOldScope = function (e) {
              this.oldScope = e;
            }),
            (t.prototype.setRawData = function (e) {
              this.rawData = e;
            }),
            (t.prototype.getRawData = function () {
              return this.rawData;
            }),
            (t.prototype.setMessage = function (e) {
              this.message = e;
            }),
            (t.prototype.getMessage = function () {
              return this.message;
            }),
            (t.prototype.setAction = function (e) {
              this.action = e;
            }),
            (t.prototype.getAction = function () {
              return this.action;
            }),
            (t.prototype.getSender = function () {
              try {
                return this.getSenderFromData();
              } catch (e) {
                return this.sender;
              }
            }),
            (t.prototype.setActionBy = function (e) {
              this.actionBy = e;
            }),
            (t.prototype.getActionBy = function () {
              return this.actionBy;
            }),
            (t.prototype.setActionOn = function (e) {
              this.actionOn = e;
            }),
            (t.prototype.getActionOn = function () {
              return this.actionOn;
            }),
            (t.prototype.setActionFor = function (e) {
              this.actionFor = e;
            }),
            (t.prototype.getActionFor = function () {
              return this.actionFor;
            }),
            (t.prototype.getMetadata = function () {
              return (
                this.data.metadata && (this.metadata = this.data.metadata),
                this.metadata
              );
            }),
            (t.prototype.setMetadata = function (e) {
              (this.metadata = e),
                (this.data = r({}, this.data, { metadata: e }));
            }),
            (t.prototype.getActionMessage = function (e) {
              var t = "";
              switch (e.getType()) {
                case S.ActionConstatnts.ACTION_KEYS.ACTION_TYPE_USER:
                case S.ActionConstatnts.ACTION_KEYS.ACTION_TYPE_GROUP:
                  switch (e.getAction()) {
                    case S.ActionConstatnts.ACTION_KEYS.ACTION_CREATED:
                    case S.ActionConstatnts.ACTION_KEYS.ACTION_UPDATED:
                    case S.ActionConstatnts.ACTION_KEYS.ACTION_DELETED:
                  }
                  break;
                case S.ActionConstatnts.ACTION_KEYS.ACTION_TYPE_GROUP_MEMBER:
                  switch (e.getAction()) {
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_JOINED:
                      var n = e.getActionBy();
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_GROUP_JOINED_MESSAGE,
                        n.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_LEFT:
                      n = e.getActionBy();
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_GROUP_LEFT_MESSAGE,
                        n.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_KICKED:
                      n = e.getActionBy();
                      var o = e.getActionOn();
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MEMBER_KICKED_MESSAGE,
                        n.getName(),
                        o.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_BANNED:
                      (n = e.getActionBy()), (o = e.getActionOn());
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MEMBER_BANNED_MESSAGE,
                        n.getName(),
                        o.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_UNBANNED:
                      (n = e.getActionBy()), (o = e.getActionOn());
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MEMBER_UNBANNED_MESSAGE,
                        n.getName(),
                        o.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.ACTION_SCOPE_CHANGED:
                      (n = e.getActionBy()), (o = e.getActionOn());
                      var s = e.getNewScope();
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MEMBER_SCOPE_CHANGED,
                        n.getName(),
                        o.getName(),
                        s
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MEMBER_ADDED:
                      (n = e.getActionBy()), (o = e.getActionOn());
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MEMBER_ADDED_TO_GROUP,
                        n.getName(),
                        o.getName()
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MESSAGE_EDITED:
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MESSAGE_EDITED_MESSAGE,
                        ""
                      );
                      break;
                    case S.ActionConstatnts.ACTION_KEYS.TYPE_MESSAGE_DELETED:
                      t = a.format(
                        S.ActionConstatnts.ActionMessages
                          .ACTION_MESSAGE_DELETED_MESSAGE,
                        ""
                      );
                  }
              }
              return t;
            }),
            (t.prototype.getSenderFromData = function () {
              var e = this.data[S.ActionConstatnts.ACTION_KEYS.ENTITIES];
              if (
                e[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY] &&
                e[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                  S.ActionConstatnts.ACTION_KEYS.ENTITY_TYPE
                ] == S.ActionConstatnts.ACTION_ENTITY_TYPE.USER
              ) {
                var t = new u.User(
                  e[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                    S.ActionConstatnts.ACTION_KEYS.ENTITY
                  ]
                );
                return (
                  Object.assign(
                    t,
                    e[S.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                      S.ActionConstatnts.ACTION_KEYS.ENTITY
                    ]
                  ),
                  t
                );
              }
            }),
            (t.TYPE = S.MessageConstatnts.TYPE),
            (t.RECEIVER_TYPE = S.MessageConstatnts.RECEIVER_TYPE),
            (t.CATEGORY = S.MessageConstatnts.CATEGORY),
            (t.ACTION_TYPE = S.ActionConstatnts.ACTION_TYPE),
            t
          );
        })(i.BaseMessage);
      t.Action = E;
    },
    function (e, t, n) {
      "use strict";
      var o,
        r =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          }),
        i =
          (this && this.__assign) ||
          function () {
            return (i =
              Object.assign ||
              function (e) {
                for (var t, n = 1, o = arguments.length; n < o; n++)
                  for (var s in (t = arguments[n]))
                    Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
                return e;
              }).apply(this, arguments);
          };
      t.__esModule = !0;
      var s = n(7),
        a = n(8),
        E = n(1),
        c = n(0),
        u = n(4),
        S = (function (s) {
          function e(e, t, n, o) {
            return c.isFalsy(o)
              ? s.call(this, e, t, n, E.MessageCategory.CALL) || this
              : s.call(this, e, t, n, o) || this;
          }
          return (
            r(e, s),
            (e.prototype.getCallInitiator = function () {
              return this.getCallInitiatedByFromData();
            }),
            (e.prototype.getCallReceiver = function () {
              return this.getCallReceivedByFromData();
            }),
            (e.prototype.setCallInitiator = function (e) {
              this.callInitiator = e;
            }),
            (e.prototype.setCallReceiver = function (e) {
              this.callReceiver = e;
            }),
            (e.prototype.getData = function () {
              return this.data;
            }),
            (e.prototype.setData = function (e) {
              this.data = e;
            }),
            (e.prototype.getSessionId = function () {
              return this.sessionId;
            }),
            (e.prototype.setSessionId = function (e) {
              this.sessionId = e;
            }),
            (e.prototype.getMetadata = function () {
              return (
                this.data.metadata && (this.metadata = this.data.metadata),
                this.metadata
              );
            }),
            (e.prototype.getSender = function () {
              try {
                return this.getSenderFromData();
              } catch (e) {
                return this.sender;
              }
            }),
            (e.prototype.setMetadata = function (e) {
              (this.metadata = e),
                (this.data = i({}, this.data, { metadata: e }));
            }),
            (e.prototype.getAction = function () {
              return this.action;
            }),
            (e.prototype.setAction = function (e) {
              this.action = e;
            }),
            (e.prototype.getInitiatedAt = function () {
              return this.initiatedAt;
            }),
            (e.prototype.setInitiatedAt = function (e) {
              this.initiatedAt = e;
            }),
            (e.prototype.getJoinedAt = function () {
              return this.joinedAt;
            }),
            (e.prototype.setJoinedAt = function (e) {
              this.joinedAt = e;
            }),
            (e.prototype.setRawData = function (e) {
              this.rawData = e;
            }),
            (e.prototype.getRawData = function () {
              return this.rawData;
            }),
            (e.prototype.setSender = function (e) {
              this.sender = e;
            }),
            (e.prototype.setReceiver = function (e) {
              this.receiver = e;
            }),
            (e.callFromJSON = function (e) {
              try {
                var t = new this(
                    e[E.MessageConstatnts.KEYS.RECEIVER],
                    e[E.MessageConstatnts.KEYS.TYPE],
                    e[E.MessageConstatnts.KEYS.RECEIVER_TYPE],
                    E.MessageCategory.CALL
                  ),
                  n = e[E.MessageConstatnts.KEYS.DATA];
                t.setAction(n[E.MessageConstatnts.KEYS.ACTION]),
                  n[E.MessageConstatnts.KEYS.METADATA] &&
                    t.setMetadata(n[E.MessageConstatnts.KEYS.METADATA]);
                var o = n[E.ActionConstatnts.ACTION_KEYS.ENTITIES];
                if (o[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY])
                  o[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY];
                if (o[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON]) {
                  var s = o[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON];
                  if (s[E.CallConstants.CALL_KEYS.CALL_ENTITY]) {
                    var r = s[E.CallConstants.CALL_KEYS.CALL_ENTITY];
                    if (
                      (r[E.CallConstants.CALL_KEYS.CALL_SESSION_ID] &&
                        (t.sessionId =
                          r[E.CallConstants.CALL_KEYS.CALL_SESSION_ID]),
                      r[E.CallConstants.CALL_KEYS.CALL_STATUS] &&
                        (t.status = r[E.CallConstants.CALL_KEYS.CALL_STATUS]),
                      r[E.CallConstants.CALL_KEYS.CALL_DATA])
                    ) {
                      var i = r[E.CallConstants.CALL_KEYS.CALL_DATA];
                      if (
                        (i[E.CallConstants.CALL_KEYS.CALL_METADATA] &&
                          (t.metadata =
                            i[E.CallConstants.CALL_KEYS.CALL_METADATA]),
                        i[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES])
                      )
                        i[E.ResponseConstants.RESPONSE_KEYS.KEY_ENTITIES];
                    }
                    r[E.CallConstants.CALL_KEYS.CALL_INITIATED_AT] &&
                      (t.initiatedAt =
                        r[E.CallConstants.CALL_KEYS.CALL_INITIATED_AT]),
                      r[E.CallConstants.CALL_KEYS.CALL_JOINED_AT] &&
                        (t.joinedAt =
                          r[E.CallConstants.CALL_KEYS.CALL_JOINED_AT]),
                      r[E.CallConstants.CALL_KEYS.CALL_LEFT_AT] &&
                        (t.joinedAt =
                          r[E.CallConstants.CALL_KEYS.CALL_LEFT_AT]);
                  }
                }
                return t;
              } catch (e) {
                c.Logger.error("CallModel: callFromJSON", e);
              }
            }),
            (e.prototype.getSenderFromData = function () {
              try {
                var e = this.getData();
                if (
                  (e = e[E.ActionConstatnts.ACTION_KEYS.ENTITIES])[
                    E.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY
                  ][E.CallConstants.CALL_KEYS.CALL_ENTITY]
                )
                  return new u.User(
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_BY][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ]
                  );
              } catch (e) {
                c.Logger.error("CallModel:getSenderFromData", e);
              }
            }),
            (e.prototype.getCallInitiatedByFromData = function () {
              try {
                var e = this.getData();
                if (
                  (e = e[E.ActionConstatnts.ACTION_KEYS.ENTITIES])[
                    E.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON
                  ][E.CallConstants.CALL_KEYS.CALL_ENTITY][
                    E.CallConstants.CALL_KEYS.CALL_DATA
                  ][E.ActionConstatnts.ACTION_KEYS.ENTITIES][
                    E.CallConstants.CALL_KEYS.CALL_SENDER
                  ]
                )
                  return new u.User(
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_ON][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ][E.CallConstants.CALL_KEYS.CALL_DATA][
                      E.ActionConstatnts.ACTION_KEYS.ENTITIES
                    ][E.CallConstants.CALL_KEYS.CALL_SENDER][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ]
                  );
              } catch (e) {
                c.Logger.error("CallModel:getCallInitiatedByFromData", e);
              }
            }),
            (e.prototype.getCallReceivedByFromData = function () {
              try {
                var e = this.getData();
                if (
                  (e = e[E.ActionConstatnts.ACTION_KEYS.ENTITIES])[
                    E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR
                  ][E.CallConstants.CALL_KEYS.CALL_ENTITY]
                ) {
                  if (
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY_TYPE
                    ] == E.CallConstants.CALL_KEYS.CALL_ENTITY_USER
                  )
                    return new u.User(
                      e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                        E.CallConstants.CALL_KEYS.CALL_ENTITY
                      ]
                    );
                  var t = new a.Group(
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ][E.GroupConstants.KEYS.GUID],
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ][E.GroupConstants.KEYS.NAME],
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ][E.GroupConstants.KEYS.TYPE]
                  );
                  return Object.assign(
                    t,
                    e[E.ActionConstatnts.ACTION_SUBJECTS.ACTION_FOR][
                      E.CallConstants.CALL_KEYS.CALL_ENTITY
                    ]
                  );
                }
              } catch (e) {
                c.Logger.error("CallModel:getCallReceivedByFromData", e);
              }
            }),
            (e.TYPE = E.MessageConstatnts.TYPE),
            (e.RECEIVER_TYPE = E.MessageConstatnts.RECEIVER_TYPE),
            (e.CATEGORY = E.MessageConstatnts.CATEGORY),
            (e.ACTION_TYPE = E.ActionConstatnts.ACTION_TYPE),
            e
          );
        })(s.BaseMessage);
      t.Call = S;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = (function () {
        function e() {
          (this.RECEIPT_TYPE = {
            READ_RECEIPT: "read",
            DELIVERY_RECEIPT: "delivery",
          }),
            (this.receiptType = this.RECEIPT_TYPE.DELIVERY_RECEIPT);
        }
        return (
          (e.prototype.getReceiverType = function () {
            return this.receiverType;
          }),
          (e.prototype.setReceiverType = function (e) {
            this.receiverType = e;
          }),
          (e.prototype.getSender = function () {
            return this.sender;
          }),
          (e.prototype.setSender = function (e) {
            this.sender = e;
          }),
          (e.prototype.getReceiver = function () {
            return this.receiver;
          }),
          (e.prototype.setReceiver = function (e) {
            this.receiver = e;
          }),
          (e.prototype.getTimestamp = function () {
            return this.timestamp;
          }),
          (e.prototype.setTimestamp = function (e) {
            this.timestamp = e;
          }),
          (e.prototype.setReadAt = function (e) {
            this.readAt = e;
          }),
          (e.prototype.getReadAt = function () {
            return this.readAt;
          }),
          (e.prototype.setDeliveredAt = function (e) {
            this.deliveredAt = e;
          }),
          (e.prototype.getDeliveredAt = function () {
            return this.deliveredAt;
          }),
          (e.prototype.getMessageId = function () {
            return this.messageId;
          }),
          (e.prototype.setMessageId = function (e) {
            this.messageId = e;
          }),
          (e.prototype.getReceiptType = function () {
            return this.receiptType;
          }),
          (e.prototype.setReceiptType = function (e) {
            void 0 === e && (e = this.RECEIPT_TYPE.DELIVERY_RECEIPT),
              (this.receiptType = e);
          }),
          e
        );
      })();
      t.MessageReceipt = o;
    },
    function (e, t, n) {
      "use strict";
      var o,
        i =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var s = n(0),
        r = function () {
          for (var e = [], t = 0; t < arguments.length; t++)
            e[t] = arguments[t];
          (this.onAction = void 0),
            (this.onTextMessageReceived = void 0),
            (this.onMediaMessageReceived = void 0),
            (this.onCustomMessageReceived = void 0),
            (this.onCall = void 0),
            (this.onTypingStarted = void 0),
            (this.onTypingEnded = void 0),
            (this.onMessagesDelivered = void 0),
            (this.onMessagesRead = void 0),
            (this.onMessageEdited = void 0),
            (this.onMessageDeleted = void 0),
            (this.onTransientMessageReceived = void 0),
            s.isFalsy(e[0].onAction) || (this.onAction = e[0].onAction),
            s.isFalsy(e[0].onTextMessageReceived) ||
              (this.onTextMessageReceived = e[0].onTextMessageReceived),
            s.isFalsy(e[0].onMediaMessageReceived) ||
              (this.onMediaMessageReceived = e[0].onMediaMessageReceived),
            s.isFalsy(e[0].onCall) || (this.onCall = e[0].onCall),
            s.isFalsy(e[0].onTypingStarted) ||
              (this.onTypingStarted = e[0].onTypingStarted),
            s.isFalsy(e[0].onTypingEnded) ||
              (this.onTypingEnded = e[0].onTypingEnded),
            s.isFalsy(e[0].onMessagesDelivered) ||
              (this.onMessagesDelivered = e[0].onMessagesDelivered),
            s.isFalsy(e[0].onMessagesRead) ||
              (this.onMessagesRead = e[0].onMessagesRead),
            s.isFalsy(e[0].onCustomMessageReceived) ||
              (this.onCustomMessageReceived = e[0].onCustomMessageReceived),
            s.isFalsy(e[0].onMessageEdited) ||
              (this.onMessageEdited = e[0].onMessageEdited),
            s.isFalsy(e[0].onMessageDeleted) ||
              (this.onMessageDeleted = e[0].onMessageDeleted),
            s.isFalsy(e[0].onTransientMessageReceived) ||
              (this.onTransientMessageReceived =
                e[0].onTransientMessageReceived);
        };
      t.MessageEventListener = r;
      var a = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        (this.onIncomingCallReceived = void 0),
          (this.onOutgoingCallAccepted = void 0),
          (this.onOutgoingCallRejected = void 0),
          (this.onIncomingCallCancelled = void 0),
          s.isFalsy(e[0].onIncomingCallReceived) ||
            (this.onIncomingCallReceived = e[0].onIncomingCallReceived),
          s.isFalsy(e[0].onOutgoingCallAccepted) ||
            (this.onOutgoingCallAccepted = e[0].onOutgoingCallAccepted),
          s.isFalsy(e[0].onOutgoingCallRejected) ||
            (this.onOutgoingCallRejected = e[0].onOutgoingCallRejected),
          s.isFalsy(e[0].onIncomingCallCancelled) ||
            (this.onIncomingCallCancelled = e[0].onIncomingCallCancelled);
      };
      t.CallEventListener = a;
      var E = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        s.isFalsy(e[0].onUserOnline) || (this.onUserOnline = e[0].onUserOnline),
          s.isFalsy(e[0].onUserOffline) ||
            (this.onUserOffline = e[0].onUserOffline);
      };
      t.UserEventListener = E;
      var c = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        s.isFalsy(e[0].onGroupMemberJoined) ||
          (this.onUserJoined = e[0].onGroupMemberJoined),
          s.isFalsy(e[0].onGroupMemberLeft) ||
            (this.onUserLeft = e[0].onGroupMemberLeft),
          s.isFalsy(e[0].onGroupMemberKicked) ||
            (this.onUserKicked = e[0].onGroupMemberKicked),
          s.isFalsy(e[0].onGroupMemberBanned) ||
            (this.onUserBanned = e[0].onGroupMemberBanned),
          s.isFalsy(e[0].onGroupMemberUnbanned) ||
            (this.onUserUnbanned = e[0].onGroupMemberUnbanned),
          s.isFalsy(e[0].onGroupMemberScopeChanged) ||
            (this.onMemberScopeChanged = e[0].onGroupMemberScopeChanged),
          s.isFalsy(e[0].onMemberAddedToGroup) ||
            (this.onMemberAddedToGroup = e[0].onMemberAddedToGroup);
      };
      t.GroupEventListener = c;
      var u = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        s.isFalsy(e[0].onYouLeft) || (this.onYouLeft = e[0].onYouLeft),
          s.isFalsy(e[0].onYouJoined) || (this.onYouJoined = e[0].onYouJoined),
          s.isFalsy(e[0].onUserJoined) ||
            (this.onUserJoined = e[0].onUserJoined),
          s.isFalsy(e[0].onUserLeft) || (this.onUserLeft = e[0].onUserLeft),
          s.isFalsy(e[0].onUserListUpdated) ||
            (this.onUserListUpdated = e[0].onUserListUpdated),
          s.isFalsy(e[0].onMediaDeviceListUpdated) ||
            (this.onMediaDeviceListUpdated = e[0].onMediaDeviceListUpdated),
          s.isFalsy(e[0].onRecordingStarted) ||
            (this.onRecordingStarted = e[0].onRecordingStarted),
          s.isFalsy(e[0].onRecordingStopped) ||
            (this.onRecordingStopped = e[0].onRecordingStopped),
          s.isFalsy(e[0].onScreenShareStarted) ||
            (this.onScreenShareStarted = e[0].onScreenShareStarted),
          s.isFalsy(e[0].onScreenShareStopped) ||
            (this.onScreenShareStopped = e[0].onScreenShareStopped),
          s.isFalsy(e[0].onUserMuted) || (this.onUserMuted = e[0].onUserMuted),
          s.isFalsy(e[0].onCallEnded) || (this.onCallEnded = e[0].onCallEnded),
          s.isFalsy(e[0].onError) || (this.onError = e[0].onError);
      };
      t.UserCallEventListener = u;
      var S = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        s.isFalsy(e[0].loginSuccess) || (this.loginSuccess = e[0].loginSuccess),
          s.isFalsy(e[0].loginFailure) ||
            (this.loginFailure = e[0].loginFailure),
          s.isFalsy(e[0].logoutSuccess) ||
            (this.logoutSuccess = e[0].logoutSuccess),
          s.isFalsy(e[0].logoutFailure) ||
            (this.logoutFailure = e[0].logoutFailure);
      };
      t.LoginEventListener = S;
      var l = function () {
        for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
        (this.onConnected = void 0),
          (this.inConnecting = void 0),
          (this.onDisconnected = void 0),
          (this.onFeatureThrottled = void 0),
          s.isFalsy(e[0].onConnected) || (this.onConnected = e[0].onConnected),
          s.isFalsy(e[0].inConnecting) ||
            (this.inConnecting = e[0].inConnecting),
          s.isFalsy(e[0].onDisconnected) ||
            (this.onDisconnected = e[0].onDisconnected),
          s.isFalsy(e[0].onFeatureThrottled) ||
            (this.onFeatureThrottled = e[0].onFeatureThrottled);
      };
      t.ConnectionEventListener = l;
      var p = function (e, t) {
          (this._name = e), (this._callback = t);
        },
        C = (function (r) {
          function e(e, t, n, o) {
            var s = r.call(this, e, o) || this;
            return (s._eventListener = t), n && (s._cursor = n), s;
          }
          return i(e, r), e;
        })((t.Listener = p));
      t.MessageListener = C;
      var T = (function (r) {
        function e(e, t, n, o) {
          var s = r.call(this, e, o) || this;
          return (s._eventListener = t), n && (s._cursor = n), s;
        }
        return i(e, r), e;
      })(p);
      t.UserListener = T;
      var g = (function (r) {
        function e(e, t, n, o) {
          var s = r.call(this, e, o) || this;
          return (s._eventListener = t), n && (s._cursor = n), s;
        }
        return i(e, r), e;
      })(p);
      t.GroupListener = g;
      var d = (function (s) {
        function e(e, t, n) {
          var o = s.call(this, "callListner", n) || this;
          return (o._eventListener = e), o;
        }
        return i(e, s), e;
      })(p);
      t.UserCallListener = d;
      var _ = (function (r) {
        function e(e, t, n, o) {
          var s = r.call(this, e, o) || this;
          return (s._eventListener = t), s;
        }
        return i(e, r), e;
      })(p);
      t.CallListener = _;
      var h = (function (r) {
        function e(e, t, n, o) {
          var s = r.call(this, e, o) || this;
          return (s._eventListener = t), n && (s._cursor = n), s;
        }
        return i(e, r), e;
      })(p);
      t.LoginListener = h;
      var A = (function (r) {
        function e(e, t, n, o) {
          var s = r.call(this, e, o) || this;
          return (s._eventListener = t), n && (s._cursor = n), s;
        }
        return i(e, r), e;
      })(p);
      t.ConnectionListener = A;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var i = n(32),
        a = n(0),
        o = (function () {
          function e() {}
          return (
            (e.trasformJSONConversation = function (e, t, n, o, s) {
              var r;
              try {
                r = new i.Conversation(e, t, n, o, s);
              } catch (e) {
                a.Logger.error(
                  "ConversationController:transformJSONConversation",
                  e
                );
              }
              return r;
            }),
            e
          );
        })();
      t.ConversationController = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = (function () {
        function e(e, t, n) {
          (this.receiverId = e), (this.receiverType = t), (this.metadata = n);
        }
        return (
          (e.prototype.getReceiverType = function () {
            return this.receiverType;
          }),
          (e.prototype.setReceiverType = function (e) {
            this.receiverType = e;
          }),
          (e.prototype.getReceiverId = function () {
            return this.receiverId;
          }),
          (e.prototype.setReceiverId = function (e) {
            this.receiverId = e;
          }),
          (e.prototype.getMetadata = function () {
            return this.metadata;
          }),
          (e.prototype.setMetadata = function (e) {
            this.metadata = e;
          }),
          (e.prototype.getSender = function () {
            return this.sender;
          }),
          (e.prototype.setSender = function (e) {
            this.sender = e;
          }),
          e
        );
      })();
      t.TypingIndicator = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = (function () {
        function e(e, t, n) {
          (this.receiverId = e), (this.receiverType = t), (this.data = n || {});
        }
        return (
          (e.prototype.getReceiverId = function () {
            return this.receiverId;
          }),
          (e.prototype.setReceiverId = function (e) {
            this.receiverId = e;
          }),
          (e.prototype.getReceiverType = function () {
            return this.receiverType;
          }),
          (e.prototype.setReceiverType = function (e) {
            this.receiverType = e;
          }),
          (e.prototype.getData = function () {
            return this.data;
          }),
          (e.prototype.setData = function (e) {
            this.data = e;
          }),
          (e.prototype.getSender = function () {
            return this.sender;
          }),
          (e.prototype.setSender = function (e) {
            this.sender = e;
          }),
          e
        );
      })();
      t.TransientMessage = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var i = n(0),
        a = n(1),
        E = n(12),
        c = n(3),
        u = n(2),
        o = (function () {
          function r() {
            (this.baseUrl = "https://%s.apiclient-%s.cometchat.io/"),
              (this.apiVersion = a.APPINFO.apiVersion),
              (this.adminApiUrl = "https://%s.api-%s.cometchat.io/"),
              (this.adminApiVersion = a.APPINFO.apiVersion),
              (this.extensionApi = "https://%s-%s.cometchat.io/%s"),
              (this.prosodyApi = "https://%s.%s/%s"),
              (this.wsApi = "https://%s/%s"),
              (this.uriEndpoints = {
                authToken: {
                  endpoint: "users/{{uid}}/auth_tokens",
                  method: "POST",
                  data: { uid: "string|max:100" },
                  isAdminApi: !0,
                },
                appSettings: { endpoint: "settings", method: "GET" },
                login: {
                  endpoint: "admin/users/auth",
                  method: "POST",
                  data: { uid: "string|max:100" },
                },
                logout: {
                  endpoint: "admin/users/auth/{{authToken}}",
                  method: "DELETE",
                },
                getMyDetails: { endpoint: "me", method: "GET" },
                updateMyDetails: { endpoint: "me", method: "PUT" },
                getJWT: { endpoint: "me/jwt", method: "POST" },
                users: { endpoint: "users", method: "GET" },
                user: { endpoint: "users/{{uid}}", method: "GET" },
                blockUsers: { endpoint: "blockedusers", method: "POST" },
                blockedUsersList: { endpoint: "blockedusers", method: "GET" },
                unblockUsers: { endpoint: "blockedusers", method: "DELETE" },
                userLogout: { endpoint: "me", method: "DELETE" },
                createUser: {
                  endpoint: "users",
                  method: "POST",
                  isAdminApi: !0,
                },
                updateUser: {
                  endpoint: "users/{{uid}}",
                  method: "PUT",
                  isAdminApi: !0,
                },
                sendFriendRequests: {
                  endpoint: "user/friends",
                  method: "POST",
                  data: { uids: "array<string|max:100>" },
                },
                getFriends: { endpoint: "user/friends", method: "GET" },
                unfriend: {
                  endpoint: "user/friends/{{uid}}/{{gid}}",
                  method: "DELETE",
                  data: { uids: "array<string|max:100>" },
                },
                acceptFriendRequest: {
                  endpoint: "user/friends/{{uid}}/accept",
                  method: "PUT",
                  data: { uids: "array<string|max:100>" },
                },
                rejectFriendRequest: {
                  endpoint: "user/friends/{{uid}}/reject",
                  method: "DELETE",
                  data: { uids: "array<string|max:100>" },
                },
                createGroup: {
                  endpoint: "groups",
                  method: "POST",
                  data: {
                    guid: "required|string|max:100",
                    name: "required|string|max:100",
                    type: "enum|public,protected,password",
                    password: "filled|string|max:100",
                  },
                },
                getGroups: { endpoint: "groups", method: "GET" },
                getGroup: { endpoint: "groups/{{guid}}", method: "GET" },
                updateGroup: { endpoint: "groups/{{guid}}", method: "PUT" },
                deleteGroup: { endpoint: "groups/{{guid}}", method: "DELETE" },
                addGroupMembers: {
                  endpoint: "groups/{{guid}}/members",
                  method: "POST",
                  data: { uids: "array<string|max:100>" },
                },
                getGroupMembers: {
                  endpoint: "groups/{{guid}}/members",
                  method: "GET",
                },
                joinGroup: {
                  endpoint: "groups/{{guid}}/members",
                  method: "POST",
                },
                leaveGroup: {
                  endpoint: "groups/{{guid}}/members",
                  method: "DELETE",
                },
                kickGroupMembers: {
                  endpoint: "groups/{{guid}}/members/{{uid}}",
                  method: "DELETE",
                  data: { uids: "array<string|max:100>" },
                },
                changeScopeOfMember: {
                  endpoint: "groups/{{guid}}/members/{{uid}}",
                  method: "PUT",
                  data: { uids: "array<string|max:100>" },
                },
                banGroupMember: {
                  endpoint: "groups/{{guid}}/bannedusers/{{uid}}",
                  method: "POST",
                  data: { uids: "array<string|max:100>" },
                },
                unbanGroupMember: {
                  endpoint: "groups/{{guid}}/bannedusers/{{uid}}",
                  method: "DELETE",
                  data: { uids: "array<string|max:100>" },
                },
                addMemebersToGroup: {
                  endpoint: "groups/{{guid}}/members",
                  method: "PUT",
                },
                getBannedGroupMembers: {
                  endpoint: "groups/{{guid}}/bannedusers",
                  method: "GET",
                },
                promotemoteGroupMember: {
                  endpoint: "groups/{{guid}}/promote",
                  method: "PUT",
                  data: { uids: "array<string|max:100>" },
                },
                demoteGroupMember: {
                  endpoint: "groups/{{guid}}/demote",
                  method: "DELETE",
                  data: { uids: "array<string|max:100>" },
                },
                transferOwnership: {
                  endpoint: "groups/{{guid}}/owner",
                  method: "PATCH",
                },
                sendMessage: {
                  endpoint: "messages",
                  method: "POST",
                  data: {
                    sender: "array:string:max:100>",
                    isGroupMember: "filled|boolean|bail",
                    data: "required|json",
                  },
                },
                sendMessageInThread: {
                  endpoint: "messages/{{parentId}}/thread",
                  method: "POST",
                  data: {
                    sender: "array:string:max:100>",
                    isGroupMember: "filled|boolean|bail",
                    data: "required|json",
                  },
                },
                getMessages: { endpoint: "messages", method: "GET" },
                getMessageDetails: {
                  endpoint: "messages/{{messageId}}",
                  method: "GET",
                },
                getUserMessages: {
                  endpoint: "users/{{listId}}/messages",
                  method: "GET",
                },
                getGroupMessages: {
                  endpoint: "groups/{{listId}}/messages",
                  method: "GET",
                },
                getThreadMessages: {
                  endpoint: "messages/{{listId}}/thread",
                  method: "GET",
                },
                getMessage: {
                  endpoint: "user/messages/{{muid}}",
                  method: "GET",
                },
                updateMessage: {
                  endpoint: "messages/{{messageId}}",
                  method: "PUT",
                },
                deleteMessage: {
                  endpoint: "messages/{{messageId}}",
                  method: "DELETE",
                },
                createCallSession: {
                  endpoint: "calls",
                  method: "POST",
                  data: {},
                },
                updateCallSession: {
                  endpoint: "calls/{{sessionid}}",
                  method: "put",
                  data: {},
                },
                getConversations: { endpoint: "conversations", method: "GET" },
                getUserConversation: {
                  endpoint: "users/{{uid}}/conversation",
                  method: "GET",
                },
                getGroupConversation: {
                  endpoint: "groups/{{guid}}/conversation",
                  method: "GET",
                },
                deleteUserConversation: {
                  endpoint: "users/{{uid}}/conversation",
                  method: "DELETE",
                },
                deleteGroupConversation: {
                  endpoint: "groups/{{guid}}/conversation",
                  method: "DELETE",
                },
              });
          }
          return (
            (r.prototype.getEndpointData = function (s) {
              return new Promise(function (o, t) {
                try {
                  E.LocalStorage.getInstance()
                    .get(a.LOCAL_STORE.KEY_APP_SETTINGS)
                    .then(
                      function (e) {
                        if (i.isFalsy(e)) {
                          var t = {};
                          if (new r().uriEndpoints.hasOwnProperty(s))
                            if (
                              (t = new r().uriEndpoints[s]).hasOwnProperty(
                                "isAdminApi"
                              )
                            ) {
                              var n =
                                i.format(
                                  new r().adminApiUrl,
                                  c.CometChat.getAppId(),
                                  c.CometChat.appSettings.getRegion()
                                ) +
                                new r().adminApiVersion +
                                "/" +
                                t.endpoint;
                              t.endpoint = n;
                            } else {
                              n =
                                i.format(
                                  new r().baseUrl,
                                  c.CometChat.getAppId(),
                                  c.CometChat.appSettings.getRegion()
                                ) +
                                new r().apiVersion +
                                "/" +
                                t.endpoint;
                              t.endpoint = n;
                            }
                          o(t);
                        } else {
                          t = {};
                          if (new r().uriEndpoints.hasOwnProperty(s))
                            if (
                              (t = new r().uriEndpoints[s]).hasOwnProperty(
                                "isAdminApi"
                              )
                            )
                              t.endpoint =
                                "https://" +
                                e[a.APP_SETTINGS.KEYS.ADMIN_API_HOST] +
                                "/" +
                                new r().adminApiVersion +
                                "/" +
                                t.endpoint;
                            else {
                              n =
                                "https://" +
                                e[a.APP_SETTINGS.KEYS.CLIENT_API_HOST] +
                                "/" +
                                new r().apiVersion +
                                "/" +
                                t.endpoint;
                              t.endpoint = n;
                            }
                          o(t);
                        }
                      },
                      function (e) {
                        var t;
                        new r().uriEndpoints.hasOwnProperty(s) &&
                          ((t = new r().uriEndpoints[s]).hasOwnProperty([
                            "isAdminApi",
                          ])
                            ? (t.endpoint =
                                i.format(
                                  new r().adminApiUrl,
                                  c.CometChat.getAppId(),
                                  c.CometChat.appSettings.getRegion()
                                ) +
                                new r().adminApiVersion +
                                "/" +
                                t.endpoint)
                            : (t.endpoint =
                                i.format(
                                  new r().baseUrl,
                                  c.CometChat.getAppId(),
                                  c.CometChat.appSettings.getRegion()
                                ) +
                                new r().apiVersion +
                                "/" +
                                t.endpoint)),
                          o(t);
                      }
                    );
                } catch (e) {
                  t(new u.CometChatException(e));
                }
              });
            }),
            r
          );
        })();
      t.EndpointFactory = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(27),
        i = n(2);
      t.getEndPoint = function (e, r) {
        void 0 === e && (e = ""), void 0 === r && (r = {});
        var t = new o.EndpointFactory();
        return new Promise(function (o, s) {
          try {
            t.getEndpointData(e).then(function (e) {
              var t = e;
              if (t) {
                for (var n in r)
                  t.endpoint = t.endpoint.replace("{{" + n + "}}", r[n]);
                o(t);
              } else s({ error: "Unknown endPoint name." });
            });
          } catch (e) {
            s(new i.CometChatException(e));
          }
        });
      };
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var g = n(1),
        d = n(0),
        _ = n(3),
        s = n(2),
        o = n(23),
        r = n(12),
        i = n(30),
        h = n(4),
        a = (function () {
          function T() {
            (this.TAG = "calling Log"),
              (this.CALL_NO_ANSWER_INTERVAL = 45e3),
              (this.view = void 0),
              (this.isDev = !1);
          }
          return (
            (T.prototype.getCallListner = function () {
              return T.callListner;
            }),
            (T.prototype.setCallListner = function (e) {
              T.callListner = new o.UserCallListener(e);
            }),
            (T.getInstance = function () {
              try {
                return (
                  (null != this.callController &&
                    null != this.callController) ||
                    (this.callController = new T()),
                  this.callController
                );
              } catch (e) {
                d.Logger.error("CallController: getInstance", e);
              }
            }),
            (T.prototype.getActiveCall = function () {
              try {
                return d.isFalsy(this.call) ? null : this.call;
              } catch (e) {
                d.Logger.error("CallController: getActiveCall", e);
              }
            }),
            (T.prototype.initiateCall = function (n) {
              var o = this;
              return new Promise(function (e, t) {
                try {
                  d.isFalsy(o.call)
                    ? null != n && null != n
                      ? ((o.call = n), o.startCallTimer(), e(n))
                      : t(
                          new s.CometChatException(
                            g.CALL_ERROR.ERROR_IN_CALLING
                          )
                        )
                    : t(
                        new s.CometChatException(
                          g.CALL_ERROR.CALL_ALREADY_INITIATED
                        )
                      );
                } catch (e) {
                  t(new s.CometChatException(e));
                }
              });
            }),
            (T.prototype.endCall = function (e) {
              var t = this;
              try {
                document.getElementsByName("frame").forEach(function (e) {
                  t.view && (t.view.removeChild(e), (t.view = void 0));
                }),
                  this.endCallSession();
              } catch (e) {
                d.Logger.error("CallController: endCall", e);
              }
            }),
            (T.prototype.onCallStarted = function (n) {
              var o = this;
              return new Promise(function (e, t) {
                try {
                  d.isFalsy(o.call)
                    ? null != n && null != n
                      ? e((o.call = n))
                      : t(
                          new s.CometChatException(
                            g.CALL_ERROR.ERROR_IN_CALLING
                          )
                        )
                    : t(
                        new s.CometChatException(
                          g.CALL_ERROR.CALL_ALREADY_INITIATED
                        )
                      );
                } catch (e) {
                  t(new s.CometChatException(e));
                }
              });
            }),
            (T.prototype.endCallSession = function () {
              try {
                (this.call = void 0),
                  (T.callController = void 0),
                  this.timer && this.stopCallTimer(),
                  (this.view = void 0),
                  this.removeListener(),
                  (T.callScreen = null),
                  (T.callSettings = null);
              } catch (e) {
                d.Logger.error("CallController:EndCallSession", { e: e });
              }
            }),
            (T.prototype.startCallTimer = function () {
              var t = this;
              try {
                this.timer = setTimeout(function () {
                  t.call
                    ? _.CometChat.sendUnansweredResponse(
                        t.call.getSessionId()
                      ).then(
                        function (e) {
                          t.endCallSession();
                        },
                        function (e) {
                          t.endCallSession();
                        }
                      )
                    : t.endCallSession();
                }, this.CALL_NO_ANSWER_INTERVAL);
              } catch (e) {
                d.Logger.error("CallController: startCallTimer", e);
              }
            }),
            (T.prototype.stopCallTimer = function () {
              try {
                clearTimeout(this.timer);
              } catch (e) {
                d.Logger.error("CallController: stopCallTimer", e);
              }
            }),
            (T.prototype.startCall = function (t, n) {
              var o = this;
              try {
                this.timer && this.stopCallTimer();
                var s = document.createElement("iframe");
                this.getCallUrl().then(function (e) {
                  (s.src = e + ""),
                    (s.name = "frame"),
                    s.setAttribute(
                      "allow",
                      "camera; microphone; display-capture;"
                    ),
                    s.setAttribute("width", "100%"),
                    s.setAttribute("height", "100%"),
                    (T.callScreen = s),
                    (T.callSettings = t),
                    n && (o.view = n),
                    n.appendChild(s),
                    o.addListener();
                });
              } catch (e) {
                d.Logger.error("CallController: startCall", e);
              }
            }),
            (T.prototype.addListener = function () {
              window.addEventListener("message", this.handler, !0);
            }),
            (T.prototype.removeListener = function () {
              window.removeEventListener("message", this.handler, !0);
            }),
            (T.prototype.handler = function (e) {
              var t,
                n = this,
                o = T.callSettings,
                s = T.callScreen,
                r = T.getInstance().getActiveCall();
              try {
                if (
                  void 0 !== (t = JSON.parse(e.data)) &&
                  t.type == g.CallConstants.POST_MESSAGES.TYPES.HANGUP
                )
                  (T.deviceList = null),
                    r
                      ? (d.Logger.info(this.TAG, T.callListner),
                        window.setTimeout(function () {
                          _.CometChat.endCall(r.getSessionId(), !0)
                            .then(function (e) {
                              d.Logger.info(n.TAG, { call: e });
                            })
                            .catch(function (e) {
                              d.Logger.info(
                                n.TAG,
                                "The Call Was Already Ended"
                              );
                            });
                        }, 1e3))
                      : _.CometChat.endCall(o.getSessionId(), !0)
                          .then(function (e) {
                            d.Logger.info(n.TAG, { call: e });
                          })
                          .catch(function (e) {
                            d.Logger.info(n.TAG, "The Call Was Already Ended");
                          });
                else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action == g.CallConstants.POST_MESSAGES.ACTIONS.USER_JOINED
                ) {
                  if (t.value) {
                    var i = void 0,
                      a = t.value;
                    d.isFalsy(a.uid) ||
                      d.isFalsy(a.name) ||
                      ((i = new h.User(a)).setStatus("online"),
                      _.CometChat.user.getUid().toLowerCase() !=
                        i.getUid().toLowerCase() &&
                        T.callListner &&
                        (d.isFalsy(T.callListner._eventListener.onUserJoined) ||
                          T.callListner._eventListener.onUserJoined(i)));
                  }
                } else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action == g.CallConstants.POST_MESSAGES.ACTIONS.USER_LEFT
                ) {
                  if (t.value) {
                    (i = void 0), (a = t.value);
                    d.isFalsy(a.uid) ||
                      d.isFalsy(a.name) ||
                      ((i = new h.User(a)).setStatus("online"),
                      _.CometChat.user.getUid().toLowerCase() !=
                        i.getUid().toLowerCase() &&
                        T.callListner &&
                        (d.isFalsy(T.callListner._eventListener.onUserLeft) ||
                          T.callListner._eventListener.onUserLeft(i)));
                  }
                } else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action ==
                    g.CallConstants.POST_MESSAGES.ACTIONS.USER_LIST_CHANGED
                ) {
                  var E = [];
                  if (t.value && 0 < t.value.length)
                    t.value.map(function (e) {
                      if (!d.isFalsy(e.uid) && !d.isFalsy(e.name)) {
                        var t = new h.User(e);
                        t.setStatus("online"), E.push(t);
                      }
                    }),
                      T.callListner &&
                        (d.isFalsy(
                          T.callListner._eventListener.onUserListUpdated
                        ) ||
                          T.callListner._eventListener.onUserListUpdated(E));
                } else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action ==
                    g.CallConstants.POST_MESSAGES.ACTIONS.INITIAL_DEVICE_LIST
                )
                  d.Logger.info("initialDeviceList received in SDK = ", t),
                    t.value && (T.deviceList = t.value);
                else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action ==
                    g.CallConstants.POST_MESSAGES.ACTIONS.DEVICE_CHANGE
                ) {
                  if (
                    (d.Logger.info("onDeviceChange received in SDK = ", t),
                    t.value)
                  ) {
                    T.deviceList = t.value;
                    var c = T.getAvailableDeviceObject(T.deviceList);
                    T.callListner &&
                      (d.isFalsy(
                        T.callListner._eventListener.onMediaDeviceListUpdated
                      ) ||
                        T.callListner._eventListener.onMediaDeviceListUpdated(
                          c
                        ));
                  }
                } else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action ==
                    g.CallConstants.POST_MESSAGES.ACTIONS.RECORDING_TOGGLED
                ) {
                  if (
                    (d.Logger.info("onRecordingToggled received in SDK = ", t),
                    t.value)
                  )
                    if (
                      (l = t.value).hasOwnProperty("user") &&
                      l.hasOwnProperty("recordingStarted") &&
                      !d.isFalsy(l.user.uid) &&
                      !d.isFalsy(l.user.name)
                    ) {
                      var u = new h.User(l.user);
                      u.setStatus("online");
                      var S = l.recordingStarted;
                      T.callListner &&
                        (S
                          ? d.isFalsy(
                              T.callListner._eventListener.onRecordingStarted
                            ) ||
                            T.callListner._eventListener.onRecordingStarted(u)
                          : d.isFalsy(
                              T.callListner._eventListener.onRecordingStopped
                            ) ||
                            T.callListner._eventListener.onRecordingStopped(u));
                    }
                } else if (
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action == g.CallConstants.POST_MESSAGES.ACTIONS.USER_MUTED
                ) {
                  var l;
                  if (
                    (d.Logger.info("onUserMuted received in SDK = ", t),
                    t.value)
                  )
                    if (
                      (l = t.value).hasOwnProperty("muted") &&
                      l.hasOwnProperty("mutedBy") &&
                      !d.isFalsy(l.muted.uid) &&
                      !d.isFalsy(l.muted.name) &&
                      !d.isFalsy(l.mutedBy.uid) &&
                      !d.isFalsy(l.mutedBy.name)
                    ) {
                      var p = new h.User(l.muted);
                      p.setStatus("online");
                      var C = new h.User(l.mutedBy);
                      C.setStatus("online"),
                        T.callListner &&
                          (d.isFalsy(
                            T.callListner._eventListener.onUserMuted
                          ) ||
                            T.callListner._eventListener.onUserMuted(p, C));
                    }
                } else
                  void 0 !== t &&
                  t.type ==
                    g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                  t.action ==
                    g.CallConstants.POST_MESSAGES.ACTIONS.SCREEN_SHARE_STARTED
                    ? T.callListner &&
                      (d.isFalsy(
                        T.callListner._eventListener.onScreenShareStarted
                      ) ||
                        T.callListner._eventListener.onScreenShareStarted(null))
                    : void 0 !== t &&
                      t.type ==
                        g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                      t.action ==
                        g.CallConstants.POST_MESSAGES.ACTIONS
                          .SCREEN_SHARE_STOPPED
                    ? T.callListner &&
                      (d.isFalsy(
                        T.callListner._eventListener.onScreenShareStopped
                      ) ||
                        T.callListner._eventListener.onScreenShareStopped(null))
                    : void 0 !== t &&
                      t.type &&
                      t.action &&
                      t.type ==
                        g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE &&
                      t.action == g.CallConstants.POST_MESSAGES.ACTIONS.LOAD &&
                      null != s.contentWindow &&
                      s.contentWindow.postMessage(
                        {
                          type: g.CallConstants.POST_MESSAGES.TYPES
                            .COMETCHAT_RTC_SETTINGS,
                          callsettings: JSON.stringify(o),
                        },
                        "*"
                      );
              } catch (e) {
                (t = void 0), d.Logger.error("CallController: startCall", e);
              }
            }),
            (T.prototype.getCallUrl = function () {
              var e = this,
                o = void 0;
              return new Promise(function (n, t) {
                try {
                  e.isDev
                    ? ((o =
                        "http://localhost:3000/?v=" +
                        g.CALLING_COMPONENT_VERSION),
                      n(o))
                    : r.LocalStorage.getInstance()
                        .get(g.LOCAL_STORE.KEY_APP_SETTINGS)
                        .then(function (e) {
                          if (d.isFalsy(e))
                            d.getAppSettings().then(function (e) {
                              var t =
                                "rtc-web" +
                                e[g.APP_SETTINGS.KEYS.WEBRTC_HOST].slice(3);
                              (o =
                                "https://" +
                                t +
                                "/?v=" +
                                g.CALLING_COMPONENT_VERSION),
                                n(o);
                            });
                          else {
                            var t =
                              "rtc-web" +
                              e[g.APP_SETTINGS.KEYS.WEBRTC_HOST].slice(3);
                            (o =
                              "https://" +
                              t +
                              "/?v=" +
                              g.CALLING_COMPONENT_VERSION),
                              n(o);
                          }
                        });
                } catch (e) {
                  t(new s.CometChatException(e));
                }
              });
            }),
            (T.getAvailableDeviceArray = function (e) {
              var t = [];
              try {
                return (
                  e &&
                    0 < e.length &&
                    e.map(function (e) {
                      t.push(new i.MediaDevice(e.deviceId, e.label, e.active));
                    }),
                  t
                );
              } catch (e) {
                return (
                  d.Logger.error("CallController: getAvailableDeviceArray", e),
                  t
                );
              }
            }),
            (T.getAvailableDeviceObject = function (e) {
              var t = [],
                n = [],
                o = [];
              try {
                return (
                  d.isFalsy(e) ||
                    d.isFalsy(e[g.CallConstants.AUDIO_INPUT_DEVICES]) ||
                    (t = T.getAvailableDeviceArray(
                      e[g.CallConstants.AUDIO_INPUT_DEVICES]
                    )),
                  d.isFalsy(e) ||
                    d.isFalsy(e[g.CallConstants.AUDIO_OUTPUT_DEVICES]) ||
                    (n = T.getAvailableDeviceArray(
                      e[g.CallConstants.AUDIO_OUTPUT_DEVICES]
                    )),
                  d.isFalsy(e) ||
                    d.isFalsy(e[g.CallConstants.VIDEO_INPUT_DEVICES]) ||
                    (o = T.getAvailableDeviceArray(
                      e[g.CallConstants.VIDEO_INPUT_DEVICES]
                    )),
                  {
                    audioInputDevices: t,
                    audioOutputDevices: n,
                    videoInputDevices: o,
                  }
                );
              } catch (e) {
                return (
                  d.Logger.error("CallController: getAvailableDeviceObject", e),
                  {
                    audioInputDevices: t,
                    audioOutputDevices: n,
                    videoInputDevices: o,
                  }
                );
              }
            }),
            (T.prototype.getAudioInputDevices = function () {
              var t = [];
              try {
                return (
                  d.isFalsy(T.deviceList) ||
                    d.isFalsy(
                      T.deviceList[g.CallConstants.AUDIO_INPUT_DEVICES]
                    ) ||
                    (t = T.getAvailableDeviceArray(
                      T.deviceList[g.CallConstants.AUDIO_INPUT_DEVICES]
                    )),
                  t
                );
              } catch (e) {
                return (
                  d.Logger.error("CallController: getAudioInputDevices", e), t
                );
              }
            }),
            (T.prototype.getAudioOutputDevices = function () {
              var t = [];
              try {
                return (
                  d.isFalsy(T.deviceList) ||
                    d.isFalsy(
                      T.deviceList[g.CallConstants.AUDIO_OUTPUT_DEVICES]
                    ) ||
                    (t = T.getAvailableDeviceArray(
                      T.deviceList[g.CallConstants.AUDIO_OUTPUT_DEVICES]
                    )),
                  t
                );
              } catch (e) {
                return (
                  d.Logger.error("CallController: getAudioOutputDevices", e), t
                );
              }
            }),
            (T.prototype.getVideoInputDevices = function () {
              var t = [];
              try {
                return (
                  d.isFalsy(T.deviceList) ||
                    d.isFalsy(
                      T.deviceList[g.CallConstants.VIDEO_INPUT_DEVICES]
                    ) ||
                    (t = T.getAvailableDeviceArray(
                      T.deviceList[g.CallConstants.VIDEO_INPUT_DEVICES]
                    )),
                  t
                );
              } catch (e) {
                return (
                  d.Logger.error("CallController: getVideoInputDevices", e), t
                );
              }
            }),
            (T.prototype.setAudioInputDevice = function (t) {
              try {
                if (T.callScreen && !d.isFalsy(t)) {
                  var e = T.getAvailableDeviceArray(
                    T.deviceList[g.CallConstants.AUDIO_INPUT_DEVICES]
                  ).filter(function (e) {
                    return (
                      e[g.CallConstants.MEDIA_DEVICE.ID] === t &&
                      !e[g.CallConstants.MEDIA_DEVICE.ACTIVE]
                    );
                  });
                  e &&
                    0 < e.length &&
                    T.callScreen.contentWindow.postMessage(
                      {
                        type: g.CallConstants.POST_MESSAGES.TYPES
                          .ACTION_MESSAGE,
                        action:
                          g.CallConstants.POST_MESSAGES.ACTIONS
                            .CHANGE_AUDIO_INPUT,
                        value: t,
                      },
                      "*"
                    );
                }
              } catch (e) {
                d.Logger.error("CallController: setAudioInputDevice", e);
              }
            }),
            (T.prototype.setAudioOutputDevice = function (t) {
              try {
                if (T.callScreen && !d.isFalsy(t)) {
                  var e = T.getAvailableDeviceArray(
                    T.deviceList[g.CallConstants.AUDIO_OUTPUT_DEVICES]
                  ).filter(function (e) {
                    return (
                      e[g.CallConstants.MEDIA_DEVICE.ID] === t &&
                      !e[g.CallConstants.MEDIA_DEVICE.ACTIVE]
                    );
                  });
                  e &&
                    0 < e.length &&
                    T.callScreen.contentWindow.postMessage(
                      {
                        type: g.CallConstants.POST_MESSAGES.TYPES
                          .ACTION_MESSAGE,
                        action:
                          g.CallConstants.POST_MESSAGES.ACTIONS
                            .CHANGE_AUDIO_OUTPUT,
                        value: t,
                      },
                      "*"
                    );
                }
              } catch (e) {
                d.Logger.error("CallController: setAudioOutputDevice", e);
              }
            }),
            (T.prototype.setVideoInputDevice = function (t) {
              try {
                if (T.callScreen && !d.isFalsy(t)) {
                  var e = T.getAvailableDeviceArray(
                    T.deviceList[g.CallConstants.VIDEO_INPUT_DEVICES]
                  ).filter(function (e) {
                    return (
                      e[g.CallConstants.MEDIA_DEVICE.ID] === t &&
                      !e[g.CallConstants.MEDIA_DEVICE.ACTIVE]
                    );
                  });
                  e &&
                    0 < e.length &&
                    T.callScreen.contentWindow.postMessage(
                      {
                        type: g.CallConstants.POST_MESSAGES.TYPES
                          .ACTION_MESSAGE,
                        action:
                          g.CallConstants.POST_MESSAGES.ACTIONS
                            .CHANGE_VIDEO_INPUT,
                        value: t,
                      },
                      "*"
                    );
                }
              } catch (e) {
                d.Logger.error("CallController: setVideoInputDevice", e);
              }
            }),
            (T.prototype.muteAudio = function (e) {
              try {
                T.callScreen &&
                  (e
                    ? T.callScreen.contentWindow.postMessage(
                        {
                          type: g.CallConstants.POST_MESSAGES.TYPES
                            .ACTION_MESSAGE,
                          action:
                            g.CallConstants.POST_MESSAGES.ACTIONS.MUTE_AUDIO,
                        },
                        "*"
                      )
                    : T.callScreen.contentWindow.postMessage(
                        {
                          type: g.CallConstants.POST_MESSAGES.TYPES
                            .ACTION_MESSAGE,
                          action:
                            g.CallConstants.POST_MESSAGES.ACTIONS.UNMUTE_AUDIO,
                        },
                        "*"
                      ));
              } catch (e) {
                d.Logger.error("CallController: muteAudio", e);
              }
            }),
            (T.prototype.pauseVideo = function (e) {
              try {
                T.callScreen &&
                  (e
                    ? T.callScreen.contentWindow.postMessage(
                        {
                          type: g.CallConstants.POST_MESSAGES.TYPES
                            .ACTION_MESSAGE,
                          action:
                            g.CallConstants.POST_MESSAGES.ACTIONS.PAUSE_VIDEO,
                        },
                        "*"
                      )
                    : T.callScreen.contentWindow.postMessage(
                        {
                          type: g.CallConstants.POST_MESSAGES.TYPES
                            .ACTION_MESSAGE,
                          action:
                            g.CallConstants.POST_MESSAGES.ACTIONS.UNPAUSE_VIDEO,
                        },
                        "*"
                      ));
              } catch (e) {
                d.Logger.error("CallController: pauseVideo", e);
              }
            }),
            (T.prototype.setMode = function (e) {
              try {
                T.callScreen &&
                  !d.isFalsy(e) &&
                  ((e = e.toUpperCase()),
                  -1 < Object.values(g.CallConstants.CALL_MODE).indexOf(e) &&
                    e != g.CallConstants.CALL_MODE.SINGLE &&
                    T.callScreen.contentWindow.postMessage(
                      {
                        type: g.CallConstants.POST_MESSAGES.TYPES
                          .ACTION_MESSAGE,
                        action:
                          g.CallConstants.POST_MESSAGES.ACTIONS.SWITCH_MODE,
                        value: e,
                      },
                      "*"
                    ));
              } catch (e) {
                d.Logger.error("CallController: setMode", e);
              }
            }),
            (T.prototype.startScreenShare = function () {
              try {
                T.callScreen &&
                  T.callScreen.contentWindow.postMessage(
                    {
                      type: g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE,
                      action:
                        g.CallConstants.POST_MESSAGES.ACTIONS.START_SCREENSHARE,
                    },
                    "*"
                  );
              } catch (e) {
                d.Logger.error("CallController: startScreenShare", e);
              }
            }),
            (T.prototype.stopScreenShare = function () {
              try {
                T.callScreen &&
                  T.callScreen.contentWindow.postMessage(
                    {
                      type: g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE,
                      action:
                        g.CallConstants.POST_MESSAGES.ACTIONS.STOP_SCREENSHARE,
                    },
                    "*"
                  );
              } catch (e) {
                d.Logger.error("CallController: stopScreenShare", e);
              }
            }),
            (T.prototype.startRecording = function () {
              try {
                T.callScreen &&
                  T.callScreen.contentWindow.postMessage(
                    {
                      type: g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE,
                      action:
                        g.CallConstants.POST_MESSAGES.ACTIONS.START_RECORDING,
                    },
                    "*"
                  );
              } catch (e) {
                d.Logger.error("CallController: startRecording", e);
              }
            }),
            (T.prototype.stopRecording = function () {
              try {
                T.callScreen &&
                  T.callScreen.contentWindow.postMessage(
                    {
                      type: g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE,
                      action:
                        g.CallConstants.POST_MESSAGES.ACTIONS.STOP_RECORDING,
                    },
                    "*"
                  );
              } catch (e) {
                d.Logger.error("CallController: stopRecording", e);
              }
            }),
            (T.prototype.endSession = function () {
              try {
                T.callScreen &&
                  T.callScreen.contentWindow.postMessage(
                    {
                      type: g.CallConstants.POST_MESSAGES.TYPES.ACTION_MESSAGE,
                      action: g.CallConstants.POST_MESSAGES.ACTIONS.END_CALL,
                    },
                    "*"
                  );
              } catch (e) {
                d.Logger.error("CallController: endSession", e);
              }
            }),
            (T.callController = void 0),
            (T.callListner = void 0),
            T
          );
        })();
      t.CallController = a;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = (function () {
          function e(e, t, n) {
            (this.id = ""),
              (this.name = ""),
              (this.active = !1),
              o.isFalsy(e) || (this.id = e),
              o.isFalsy(t) || (this.name = t),
              (this.active = !!n);
          }
          return (
            (e.prototype.getId = function () {
              return this.id;
            }),
            (e.prototype.setId = function (e) {
              this.id = e || "";
            }),
            (e.prototype.getName = function () {
              return this.name;
            }),
            (e.prototype.setName = function (e) {
              this.name = e || "";
            }),
            (e.prototype.getIsActive = function () {
              return this.active;
            }),
            (e.prototype.setIsActive = function (e) {
              this.active = !!e;
            }),
            e
          );
        })();
      t.MediaDevice = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = (function () {
          function e(e, t) {
            (this.joinedAt = 0),
              (this.isBanned = 0),
              (this.uid = e),
              t && (this.scope = t);
          }
          return (
            (e.prototype.getName = function () {
              return this.name;
            }),
            (e.prototype.setName = function (e) {
              this.name = e;
            }),
            (e.prototype.getAvatar = function () {
              return this.avatar;
            }),
            (e.prototype.setAvatar = function (e) {
              this.avatar = e;
            }),
            (e.prototype.getLastActiveAt = function () {
              return this.lastActiveAt;
            }),
            (e.prototype.setLastActiveAt = function (e) {
              this.lastActiveAt = e;
            }),
            (e.prototype.getLink = function () {
              return this.link;
            }),
            (e.prototype.setLink = function (e) {
              this.link = e;
            }),
            (e.prototype.getMetadata = function () {
              return this.metadata;
            }),
            (e.prototype.setMetadata = function (e) {
              this.metadata = e;
            }),
            (e.prototype.getRole = function () {
              return this.role;
            }),
            (e.prototype.setRole = function (e) {
              this.role = e;
            }),
            (e.prototype.getStatus = function () {
              return this.status;
            }),
            (e.prototype.setStatus = function (e) {
              this.status = e;
            }),
            (e.prototype.getStatusMessage = function () {
              return this.statusMessage;
            }),
            (e.prototype.setStatusMessage = function (e) {
              this.statusMessage = e;
            }),
            (e.prototype.setGuid = function (e) {
              this.guid = e;
            }),
            (e.prototype.setUid = function (e) {
              this.uid = e;
            }),
            (e.prototype.setScope = function (e) {
              this.scope = e;
            }),
            (e.prototype.setJoinedAt = function (e) {
              this.joinedAt = e;
            }),
            (e.prototype.setIsBanned = function (e) {
              this.isBanned = e;
            }),
            (e.prototype.setUser = function (e) {
              this.user = e;
            }),
            (e.prototype.getGuid = function () {
              return this.guid;
            }),
            (e.prototype.getUid = function () {
              return this.uid;
            }),
            (e.prototype.getuser = function () {
              return this.user;
            }),
            (e.prototype.getScope = function () {
              return this.scope;
            }),
            (e.prototype.getJoinedAt = function () {
              return this.joinedAt;
            }),
            (e.prototype.getIsBanned = function () {
              return this.isBanned;
            }),
            (e.GroupMemberScope = o.GroupMemberScope),
            e
          );
        })();
      t.GroupMember = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var r = n(10),
        i = n(1),
        a = n(11),
        E = n(15),
        o = (function () {
          function e(e, t, n, o, s) {
            (this.conversationId = e),
              (this.conversationType = t),
              void 0 !== n &&
                void 0 !== (this.lastMessage = n).id &&
                (this.lastMessage = r.MessageController.trasformJSONMessge(n)),
              this.conversationType == i.MessageConstatnts.RECEIVER_TYPE.USER
                ? (this.conversationWith =
                    a.UsersController.trasformJSONUser(o))
                : (this.conversationWith =
                    E.GroupsController.trasformJSONGroup(o)),
              (this.unreadMessageCount = void 0 !== s ? parseInt(s) : 0);
          }
          return (
            (e.prototype.setConversationId = function (e) {
              this.conversationId = e;
            }),
            (e.prototype.setConversationType = function (e) {
              this.conversationType = e;
            }),
            (e.prototype.setLastMessage = function (e) {
              this.lastMessage = e;
            }),
            (e.prototype.setConversationWith = function (e) {
              this.conversationWith = e;
            }),
            (e.prototype.setUnreadMessageCount = function (e) {
              this.unreadMessageCount = e;
            }),
            (e.prototype.getConversationId = function () {
              return this.conversationId;
            }),
            (e.prototype.getConversationType = function () {
              return this.conversationType;
            }),
            (e.prototype.getLastMessage = function () {
              return this.lastMessage;
            }),
            (e.prototype.getConversationWith = function () {
              return this.conversationWith;
            }),
            (e.prototype.getUnreadMessageCount = function () {
              return this.unreadMessageCount;
            }),
            e
          );
        })();
      t.Conversation = o;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = n(1),
        r = n(13),
        i = n(3),
        a = (function () {
          function e(e) {
            (this.store = s.constants.DEFAULT_STORE),
              o.isFalsy(e) || (this.store = e),
              (this.settingsStore = r.createInstance({
                name: o.format(
                  s.LOCAL_STORE.STORE_STRING,
                  i.CometChat.getAppId(),
                  s.LOCAL_STORE.MESSAGE_LISTENERS_LIST
                ),
              })),
              this.settingsStore.setDriver([
                r.LOCALSTORAGE,
                r.INDEXEDDB,
                r.WEBSQL,
              ]);
          }
          return (
            (e.getInstance = function () {
              return (
                null == e.settingsStore && (e.settingsStore = new e()),
                e.settingsStore
              );
            }),
            (e.prototype.set = function (e, t) {
              return this.settingsStore.setItem(e, t);
            }),
            (e.prototype.remove = function (e) {
              this.settingsStore.removeItem(e);
            }),
            (e.prototype.get = function (e) {
              return this.settingsStore.getItem(e);
            }),
            (e.prototype.clearStore = function () {
              return this.settingsStore.clear();
            }),
            (e.prototype.clear = function (e) {}),
            (e.prototype.selectStore = function (e) {
              this.store = e;
            }),
            (e.settingsStore = null),
            e
          );
        })();
      t.MessageListnerMaping = a;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = (function () {
          function e() {}
          return (
            (e.addTypingStarted = function (e) {
              this.TYPING_STARTED_MAP[e] = o.getCurrentTime();
            }),
            (e.removeTypingStarted = function (e) {
              delete this.TYPING_STARTED_MAP[e];
            }),
            (e.getTypingStartedMap = function (e) {
              if (e) return this.TYPING_STARTED_MAP[e];
            }),
            (e.addTypingEnded = function (e) {
              this.TYPING_ENDED_MAP[e] = o.getCurrentTime();
            }),
            (e.removeTypingEnded = function (e) {
              delete this.TYPING_ENDED_MAP[e];
            }),
            (e.getTypingEndedMap = function (e) {
              if (e) return this.TYPING_ENDED_MAP[e];
            }),
            (e.addIncomingTypingStarted = function (e) {
              this.INCOMING_TYPING_STARTED_MAP[e.getReceiverId()] = {
                typingNotification: e,
                timestamp: o.getCurrentTime(),
              };
            }),
            (e.removeIncomingTypingStarted = function (e) {
              delete this.INCOMING_TYPING_STARTED_MAP[e.getReceiverId()];
            }),
            (e.TYPING_STARTED_MAP = {}),
            (e.TYPING_ENDED_MAP = {}),
            (e.INCOMING_TYPING_STARTED_MAP = {}),
            e
          );
        })();
      t.TypingNotificationController = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = n(23),
        r = (function () {
          function e() {
            (this.connectionHandlers = []),
              (this.loginHandlers = []),
              (this.messageHandlers = []),
              (this.userHandlers = []),
              (this.groupHandlers = []),
              (this.callHandlers = []);
          }
          return (
            (e.getInstance = function () {
              return (
                (null != this.listenerHandlers &&
                  null != this.listenerHandlers) ||
                  (this.listenerHandlers = new e()),
                this.listenerHandlers
              );
            }),
            (e.prototype.addConnectionEventListener = function (t, e) {
              try {
                (this.connectionHandlers = this.connectionHandlers.filter(
                  function (e) {
                    return e._name != t;
                  }
                )),
                  (this.connectionHandlers = this.connectionHandlers.concat([
                    new s.ConnectionListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error(
                  "ListenerHandlers: addWSConnectionEventListener",
                  e
                );
              }
            }),
            (e.prototype.removeConnectionEventListener = function (t) {
              try {
                this.connectionHandlers = this.connectionHandlers.filter(
                  function (e) {
                    return e._name !== t;
                  }
                );
              } catch (e) {
                o.Logger.error(
                  "ListenerHandlers: removeWSConnectionEventListener",
                  e
                );
              }
            }),
            (e.prototype.addLoginEventListener = function (t, e) {
              try {
                (this.loginHandlers = this.loginHandlers.filter(function (e) {
                  return e._name != t;
                })),
                  (this.loginHandlers = this.loginHandlers.concat([
                    new s.LoginListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error("ListenerHandlers: addLoginEventListener", e);
              }
            }),
            (e.prototype.removeLoginEventListener = function (t) {
              try {
                this.loginHandlers = this.loginHandlers.filter(function (e) {
                  return e._name !== t;
                });
              } catch (e) {
                o.Logger.error("ListenerHandlers: removeLoginEventListener", e);
              }
            }),
            (e.prototype.addMessageEventListener = function (t, e) {
              try {
                (this.messageHandlers = this.messageHandlers.filter(function (
                  e
                ) {
                  return e._name != t;
                })),
                  (this.messageHandlers = this.messageHandlers.concat([
                    new s.MessageListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error("ListenerHandlers: addMessageEventListener", e);
              }
            }),
            (e.prototype.removeMessageEventListener = function (t) {
              try {
                this.messageHandlers = this.messageHandlers.filter(function (
                  e
                ) {
                  return e._name !== t;
                });
              } catch (e) {
                o.Logger.error(
                  "ListenerHandlers: removeMessageEventListener",
                  e
                );
              }
            }),
            (e.prototype.addUserEventListener = function (t, e) {
              try {
                (this.userHandlers = this.userHandlers.filter(function (e) {
                  return e._name != t;
                })),
                  (this.userHandlers = this.userHandlers.concat([
                    new s.UserListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error("ListenerHandlers: addUserEventListener", e);
              }
            }),
            (e.prototype.removeUserEventListener = function (t) {
              try {
                this.userHandlers = this.userHandlers.filter(function (e) {
                  return e._name !== t;
                });
              } catch (e) {
                o.Logger.error("ListenerHandlers: removeUserEventListener", e);
              }
            }),
            (e.prototype.addGroupEventListener = function (t, e) {
              try {
                (this.groupHandlers = this.groupHandlers.filter(function (e) {
                  return e._name != t;
                })),
                  (this.groupHandlers = this.groupHandlers.concat([
                    new s.GroupListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error("ListenerHandlers: addGroupEventListener", e);
              }
            }),
            (e.prototype.removeGroupEventListener = function (t) {
              try {
                this.groupHandlers = this.groupHandlers.filter(function (e) {
                  return e._name !== t;
                });
              } catch (e) {
                o.Logger.error("ListenerHandlers: removeGroupEventListener", e);
              }
            }),
            (e.prototype.addCallEventListener = function (t, e) {
              try {
                (this.callHandlers = this.callHandlers.filter(function (e) {
                  return e._name != t;
                })),
                  (this.callHandlers = this.callHandlers.concat([
                    new s.CallListener(t, e),
                  ]));
              } catch (e) {
                o.Logger.error("ListenerHandlers: addCallEventListener", e);
              }
            }),
            (e.prototype.removeCallEventListener = function (t) {
              try {
                this.callHandlers = this.callHandlers.filter(function (e) {
                  return e._name !== t;
                });
              } catch (e) {
                o.Logger.error("ListenerHandlers: removeCallEventListener", e);
              }
            }),
            (e.listenerHandlers = new e()),
            e
          );
        })();
      t.ListenerHandlers = r;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(3);
      (t.init = function (e) {
        return o.CometChat.getInstance(e);
      }),
        (t.CometChat = o.CometChat);
    },
    function (e, t) {
      var n;
      n = (function () {
        return this;
      })();
      try {
        n = n || new Function("return this")();
      } catch (e) {
        "object" == typeof window && (n = window);
      }
      e.exports = n;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(3),
        s = n(0),
        r = n(1),
        i = n(13),
        a = n(2),
        E = (function () {
          function e(e) {
            (this.store = r.constants.DEFAULT_STORE),
              s.isFalsy(e) || (this.store = e),
              (this.keyStore = i.createInstance({
                name: s.format(
                  r.LOCAL_STORE.STORE_STRING,
                  o.CometChat.getAppId(),
                  r.LOCAL_STORE.KEYS_STORE
                ),
              })),
              this.keyStore.setDriver([i.LOCALSTORAGE, i.INDEXEDDB, i.WEBSQL]);
          }
          return (
            (e.getInstance = function () {
              return null == e.KeyStore && (e.KeyStore = new e()), e.KeyStore;
            }),
            (e.prototype.set = function (e, t) {
              return this.keyStore.setItem(e, t);
            }),
            (e.prototype.remove = function (e) {
              this.keyStore.removeItem(e);
            }),
            (e.prototype.get = function (e) {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  o.keyStore.getItem(e).then(
                    function (t) {
                      try {
                        n(JSON.parse(t));
                      } catch (e) {
                        n(t);
                      }
                    },
                    function (e) {
                      t(e);
                    }
                  );
                } catch (e) {
                  t(new a.CometChatException(e));
                }
              });
            }),
            (e.prototype.clearStore = function () {
              return this.keyStore.clear();
            }),
            e
          );
        })();
      t.KeyStore = E;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var s = n(6),
        o = n(0),
        r = n(2),
        i = n(15),
        a = n(1),
        E = (function () {
          function e(e) {
            (this.cursor = -1),
              (this.total = -1),
              (this.next_page = 1),
              (this.last_page = -1),
              (this.current_page = 1),
              (this.total_pages = -1),
              (this.hasJoined = 0),
              (this.withTags = !1),
              (this.pagination = {
                total: 0,
                count: 0,
                per_page: 0,
                current_page: 0,
                total_pages: 0,
                links: [],
              }),
              o.isFalsy(e) ||
                (o.isFalsy(e.limit) || (this.limit = e.limit),
                o.isFalsy(e.searchKeyword) ||
                  (this.searchKeyword = e.searchKeyword),
                o.isFalsy(e.hasJoined) || (this.hasJoined = 1),
                o.isFalsy(e.tags) || (this.tags = e.tags),
                o.isFalsy(e.showTags) || (this.withTags = e.showTags));
          }
          return (
            (e.prototype.validateGroupBuilder = function () {
              if (void 0 === this.limit)
                return new r.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(a.GENERAL_ERROR.METHOD_COMPULSORY),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "Set Limit"
                    )
                  )
                );
              if (isNaN(this.limit))
                return new r.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        a.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (this.limit > a.DEFAULT_VALUES.GROUPS_MAX_LIMIT)
                return new r.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(a.GENERAL_ERROR.LIMIT_EXCEEDED),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      a.DEFAULT_VALUES.GROUPS_MAX_LIMIT
                    )
                  )
                );
              if (this.limit < a.DEFAULT_VALUES.ZERO)
                return new r.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        a.GENERAL_ERROR.PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (void 0 !== this.searchKeyword) {
                if (
                  typeof this.searchKeyword !==
                  a.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "setSearchKeyword()"
                      )
                    )
                  );
                if (o.isFalsy(this.searchKeyword.trim()))
                  return new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(a.GENERAL_ERROR.INVALID),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "search keyword",
                        "search keyword"
                      )
                    )
                  );
                this.searchKeyword = this.searchKeyword.trim();
              }
              if (void 0 !== this.withTags) {
                if (
                  typeof this.withTags !==
                  a.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                )
                  return new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                        ),
                        "WITH_TAGS",
                        "WITH_TAGS",
                        "withTags()"
                      )
                    )
                  );
                1 == this.withTags
                  ? (this.withTags = !0)
                  : (this.withTags = !1);
              }
              return void 0 === this.tags || Array.isArray(this.tags)
                ? void 0
                : new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                        ),
                        "TAGS",
                        "TAGS",
                        "setTags()"
                      )
                    )
                  );
            }),
            (e.prototype.fetchPrevious = function () {
              var o = this;
              return new Promise(function (t, n) {
                try {
                  0 == o.next_page && t([]);
                  var e = o.validateGroupBuilder();
                  return e instanceof r.CometChatException
                    ? void n(e)
                    : s.makeApiCall("groups", {}, o.getPreData()).then(
                        function (e) {
                          if (
                            (e.meta &&
                              (o.total_pages = e.meta.pagination.total_pages),
                            0 < e.data.length)
                          ) {
                            o.pagination = e.meta.pagination;
                            var n = [];
                            e.data.map(function (e, t) {
                              n.push(i.GroupsController.trasformJSONGroup(e));
                            }),
                              t(n);
                          }
                          t([]);
                        },
                        function (e) {
                          n(new r.CometChatException(e.error));
                        }
                      );
                } catch (e) {
                  n(new r.CometChatException(e));
                }
              });
            }),
            (e.prototype.fetchNext = function () {
              var o = this;
              return new Promise(function (t, n) {
                try {
                  var e = o.validateGroupBuilder();
                  if (e instanceof r.CometChatException) return void n(e);
                  s.makeApiCall("getGroups", {}, o.getNextData()).then(
                    function (e) {
                      if (
                        (e.meta &&
                          (o.total_pages = e.meta.pagination.total_pages),
                        0 < e.data.length)
                      ) {
                        o.pagination = e.meta.pagination;
                        var n = [];
                        e.data.map(function (e, t) {
                          n.push(i.GroupsController.trasformJSONGroup(e));
                        }),
                          t(n);
                      }
                      t([]);
                    },
                    function (e) {
                      n(new r.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  n(new r.CometChatException(e));
                }
              });
            }),
            (e.prototype.getNextData = function () {
              var e = {};
              if (
                ((e.per_page = this.limit),
                o.isFalsy(this.searchKeyword) ||
                  (e.searchKey = this.searchKeyword),
                o.isFalsy(this.hasJoined) || (e.hasJoined = 1),
                o.isFalsy(this.tags) || (e.tags = this.tags),
                o.isFalsy(this.withTags) || (e.withTags = 1),
                1 == this.current_page)
              )
                (e.page = this.next_page),
                  this.next_page++,
                  this.current_page++;
              else {
                if (this.next_page > this.total_pages)
                  return (e.page = this.next_page), e;
                e.page = this.next_page++;
              }
              return e;
            }),
            (e.prototype.getPreData = function () {
              var e = {};
              return (
                (e.per_page = this.limit),
                0 <= this.next_page &&
                  0 < this.next_page &&
                  (e.page = --this.next_page),
                e
              );
            }),
            (e.MAX_LIMIT = 100),
            (e.DEFAULT_LIMIT = 30),
            e
          );
        })();
      t.GroupsRequest = E;
      var c = (function () {
        function e() {}
        return (
          (e.prototype.setLimit = function (e) {
            return (this.limit = e), this;
          }),
          (e.prototype.setSearchKeyword = function (e) {
            return (this.searchKeyword = e), this;
          }),
          (e.prototype.joinedOnly = function (e) {
            return (this.hasJoined = e), this;
          }),
          (e.prototype.setTags = function (e) {
            return (this.tags = e), this;
          }),
          (e.prototype.withTags = function (e) {
            return (this.showTags = e), this;
          }),
          (e.prototype.build = function () {
            return new E(this);
          }),
          e
        );
      })();
      t.GroupsRequestBuilder = c;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var r = n(6),
        i = n(0),
        a = n(12),
        E = n(2),
        c = n(41),
        u = n(1),
        S = (function () {
          function e(e) {
            (this.cursor = -1),
              (this.total = -1),
              (this.next_page = 1),
              (this.last_page = -1),
              (this.current_page = 1),
              (this.total_pages = -1),
              (this.isOutCastReq = !1),
              (this.pagination = {
                total: 0,
                count: 0,
                per_page: 0,
                current_page: 0,
                total_pages: 0,
                links: [],
              }),
              (this.store = a.LocalStorage.getInstance()),
              i.isFalsy(e) ||
                ((this.limit = e.limit),
                (this.guid = e.guid),
                i.isFalsy(e.searchKeyword) ||
                  (this.searchKeyword = e.searchKeyword),
                i.isFalsy(e.scopes) || (this.scopes = e.scopes),
                e instanceof p && (this.isOutCastReq = e.isOutCastReq));
          }
          return (
            (e.prototype.validateGroupMembersBuilder = function () {
              if (void 0 === this.limit)
                return new E.CometChatException(
                  JSON.parse(
                    i.format(
                      JSON.stringify(u.GENERAL_ERROR.METHOD_COMPULSORY),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "Set Limit"
                    )
                  )
                );
              if (isNaN(this.limit))
                return new E.CometChatException(
                  JSON.parse(
                    i.format(
                      JSON.stringify(
                        u.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (this.limit > u.DEFAULT_VALUES.USERS_MAX_LIMIT)
                return new E.CometChatException(
                  JSON.parse(
                    i.format(
                      JSON.stringify(u.GENERAL_ERROR.LIMIT_EXCEEDED),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      u.DEFAULT_VALUES.USERS_MAX_LIMIT
                    )
                  )
                );
              if (this.limit < u.DEFAULT_VALUES.ZERO)
                return new E.CometChatException(
                  JSON.parse(
                    i.format(
                      JSON.stringify(
                        u.GENERAL_ERROR.PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (void 0 !== this.searchKeyword) {
                if (
                  typeof this.searchKeyword !==
                  u.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new E.CometChatException(
                    JSON.parse(
                      i.format(
                        JSON.stringify(
                          u.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "setSearchKeyword()"
                      )
                    )
                  );
                if (i.isFalsy(this.searchKeyword.trim()))
                  return new E.CometChatException(
                    JSON.parse(
                      i.format(
                        JSON.stringify(u.GENERAL_ERROR.INVALID),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "search keyword",
                        "search keyword"
                      )
                    )
                  );
                this.searchKeyword = this.searchKeyword.trim();
              }
              if (void 0 !== this.guid) {
                if (
                  typeof this.guid !==
                  u.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new E.CometChatException(
                    JSON.parse(
                      i.format(
                        JSON.stringify(
                          u.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "GUID",
                        "GUID",
                        "GroupMembersRequestBuilder()"
                      )
                    )
                  );
                if (i.isFalsy(this.guid))
                  return new E.CometChatException(
                    JSON.parse(
                      i.format(
                        JSON.stringify(u.GENERAL_ERROR.INVALID),
                        "GUID",
                        "GUID",
                        "GUID",
                        "GUID"
                      )
                    )
                  );
              }
              return void 0 === this.scopes || Array.isArray(this.scopes)
                ? void 0
                : new E.CometChatException(
                    JSON.parse(
                      i.format(
                        JSON.stringify(
                          u.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                        ),
                        "SET_SCOPES",
                        "SET_SCOPES",
                        "setScopes()"
                      )
                    )
                  );
            }),
            (e.prototype.fetchPrevious = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  0 == o.next_page && n([]);
                  var e = o.validateGroupMembersBuilder();
                  return e instanceof E.CometChatException
                    ? void t(e)
                    : r
                        .makeApiCall(
                          o.isOutCastReq
                            ? "getBannedGroupMembers"
                            : "getGroupMembers",
                          { guid: o.guid },
                          o.getPreData()
                        )
                        .then(
                          function (e) {
                            if (
                              (e.meta &&
                                (o.total_pages = e.meta.pagination.total_pages),
                              0 < e.data.length)
                            ) {
                              o.pagination = e.meta.pagination;
                              var t = [];
                              e.data.map(function (e) {
                                (e.guid = o.guid),
                                  t.push(
                                    c.GroupMembersController.trasformJSONGroupMember(
                                      e
                                    )
                                  );
                              }),
                                n(t);
                            }
                            n([]);
                          },
                          function (e) {
                            t(new E.CometChatException(e.error));
                          }
                        );
                } catch (e) {
                  t(new E.CometChatException(e));
                }
              });
            }),
            (e.prototype.fetchNext = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  var e = o.validateGroupMembersBuilder();
                  if (e instanceof E.CometChatException) return void t(e);
                  r.makeApiCall(
                    o.isOutCastReq
                      ? "getBannedGroupMembers"
                      : "getGroupMembers",
                    { guid: o.guid },
                    o.getNextData()
                  ).then(
                    function (e) {
                      if (
                        (e.meta &&
                          (o.total_pages = e.meta.pagination.total_pages),
                        0 < e.data.length)
                      ) {
                        o.pagination = e.meta.pagination;
                        var t = [];
                        e.data.map(function (e) {
                          (e.guid = o.guid),
                            t.push(
                              c.GroupMembersController.trasformJSONGroupMember(
                                e
                              )
                            );
                        }),
                          n(t);
                      } else n([]);
                    },
                    function (e) {
                      t(new E.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new E.CometChatException(e));
                }
              });
            }),
            (e.prototype.getNextData = function () {
              var e = {};
              if (
                ((e.per_page = this.limit),
                i.isFalsy(this.searchKeyword) ||
                  (e.searchKey = this.searchKeyword),
                i.isFalsy(this.scopes) || (e.scopes = this.scopes),
                1 == this.current_page)
              )
                (e.page = this.next_page),
                  this.next_page++,
                  this.current_page++;
              else {
                if (this.next_page > this.total_pages)
                  return (e.page = this.next_page), e;
                e.page = this.next_page++;
              }
              return e;
            }),
            (e.prototype.getPreData = function () {
              var e = {};
              return (
                (e.per_page = this.limit),
                0 <= this.next_page &&
                  0 < this.next_page &&
                  (e.page = --this.next_page),
                e
              );
            }),
            (e.MAX_LIMIT = 2),
            (e.DEFAULT_LIMIT = 1),
            e
          );
        })();
      t.GroupMembersRequest = S;
      var l = (function () {
          function e(e) {
            this.guid = e;
          }
          return (
            (e.prototype.setLimit = function (e) {
              return (this.limit = e), this;
            }),
            (e.prototype.setSearchKeyword = function (e) {
              return (this.searchKeyword = e), this;
            }),
            (e.prototype.setScopes = function (e) {
              return (this.scopes = e), this;
            }),
            (e.prototype.build = function () {
              return new S(this);
            }),
            e
          );
        })(),
        p = (function (n) {
          function e(e) {
            var t = n.call(this, e) || this;
            return (t.isOutCastReq = !0), t;
          }
          return (
            s(e, n),
            (e.prototype.build = function () {
              return new S(this);
            }),
            e
          );
        })((t.GroupMembersRequestBuilder = l));
      t.GroupOutCastMembersRequestBuilder = p;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(31),
        s = n(1),
        r = n(0),
        i = (function () {
          function e() {}
          return (
            (e.trasformJSONGroupMember = function (t) {
              var e;
              try {
                return (
                  t.status && "offline" !== t.status
                    ? (t.status = "online")
                    : (t.status = "offline"),
                  (e = new o.GroupMember(t[s.GroupMemersConstans.KEYS.UID])),
                  Object.assign(e, t),
                  e
                );
              } catch (e) {
                return (
                  r.Logger.error(
                    "GroupMembersController:trasformJSONGroupMember",
                    { e: e, groupMember: t }
                  ),
                  t
                );
              }
            }),
            e
          );
        })();
      t.GroupMembersController = i;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var r = n(6),
        o = n(0),
        i = n(11),
        a = n(2),
        E = n(43),
        c = n(1),
        s = (function () {
          function s(e) {
            (this.next_page = 1),
              (this.current_page = 1),
              (this.total_pages = -1),
              (this.hideBlockedUsers = !1),
              (this.friendsOnly = !1),
              (this.fetchingInProgress = !1),
              (this.withTags = !1),
              (this.pagination = {
                total: 0,
                count: 0,
                per_page: 0,
                current_page: 0,
                total_pages: 0,
                links: [],
              }),
              (s.userStore = E.UserStore.getInstance()),
              o.isFalsy(e) ||
                ((this.limit = e.limit),
                o.isFalsy(e.searchKeyword) ||
                  (this.searchKeyword = e.searchKeyword),
                o.isFalsy(e.status) ||
                  (e.status == s.USER_STATUS.ONLINE
                    ? (this.status = c.PresenceConstatnts.STATUS.AVAILABLE)
                    : (this.status = e.status)),
                o.isFalsy(e.shouldHideBlockedUsers) ||
                  (this.hideBlockedUsers = e.shouldHideBlockedUsers),
                o.isFalsy(e.showFriendsOnly) ||
                  (this.friendsOnly = e.showFriendsOnly),
                o.isFalsy(e.showTags) || (this.withTags = e.showTags),
                o.isFalsy(e.role) || (this.role = e.role),
                o.isFalsy(e.roles) || (this.roles = e.roles),
                o.isFalsy(e.tags) || (this.tags = e.tags),
                o.isFalsy(e.UIDs) || (this.UIDs = e.UIDs));
          }
          return (
            (s.prototype.validateUserBuilder = function () {
              if (void 0 === this.limit)
                return new a.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(c.GENERAL_ERROR.METHOD_COMPULSORY),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "Set Limit"
                    )
                  )
                );
              if (isNaN(this.limit))
                return new a.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        c.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (this.limit > c.DEFAULT_VALUES.USERS_MAX_LIMIT)
                return new a.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(c.GENERAL_ERROR.LIMIT_EXCEEDED),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      c.DEFAULT_VALUES.USERS_MAX_LIMIT
                    )
                  )
                );
              if (this.limit < c.DEFAULT_VALUES.ZERO)
                return new a.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        c.GENERAL_ERROR.PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (void 0 !== this.searchKeyword) {
                if (
                  typeof this.searchKeyword !==
                  c.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          c.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "setSearchKeyword()"
                      )
                    )
                  );
                if (o.isFalsy(this.searchKeyword.trim()))
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(c.GENERAL_ERROR.INVALID),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "search keyword",
                        "search keyword"
                      )
                    )
                  );
                this.searchKeyword = this.searchKeyword.trim();
              }
              if (this.status) {
                if (
                  typeof this.status !==
                  c.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          c.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_STATUS",
                        "SET_STATUS",
                        "setStatus()"
                      )
                    )
                  );
                if (
                  "available" != this.status.toLowerCase() &&
                  "offline" != this.status.toLowerCase()
                )
                  return new a.CometChatException(c.UserErrors.INVALID_STATUS);
              }
              if (void 0 !== this.hideBlockedUsers) {
                if (
                  typeof this.hideBlockedUsers !==
                  c.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                )
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          c.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                        ),
                        "HIDE_BLOCKED_USERS",
                        "HIDE_BLOCKED_USERS",
                        "hideBlockedUsers()"
                      )
                    )
                  );
                1 == this.hideBlockedUsers
                  ? (this.hideBlockedUsers = !0)
                  : (this.hideBlockedUsers = !1);
              }
              if (void 0 !== this.friendsOnly) {
                if (
                  typeof this.friendsOnly !==
                  c.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                )
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          c.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                        ),
                        "FRIENDS_ONLY",
                        "FRIENDS_ONLY",
                        "friendsOnly()"
                      )
                    )
                  );
                1 == this.friendsOnly
                  ? (this.friendsOnly = !0)
                  : (this.friendsOnly = !1);
              }
              if (void 0 !== this.withTags) {
                if (
                  typeof this.withTags !==
                  c.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                )
                  return new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(c.GENERAL_ERROR.MUST_BE_A_BOOLEAN),
                        "WITH_TAGS",
                        "WITH_TAGS",
                        "withTags()"
                      )
                    )
                  );
                1 == this.withTags
                  ? (this.withTags = !0)
                  : (this.withTags = !1);
              }
              return void 0 === this.roles || Array.isArray(this.roles)
                ? void 0 === this.tags || Array.isArray(this.tags)
                  ? void 0 === this.UIDs || Array.isArray(this.UIDs)
                    ? void 0
                    : new a.CometChatException(
                        JSON.parse(
                          o.format(
                            JSON.stringify(
                              c.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                            ),
                            "UIDs",
                            "UIDs",
                            "setUIDs()"
                          )
                        )
                      )
                  : new a.CometChatException(
                      JSON.parse(
                        o.format(
                          JSON.stringify(
                            c.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                          ),
                          "TAGS",
                          "TAGS",
                          "setTags()"
                        )
                      )
                    )
                : new a.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          c.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                        ),
                        "ROLES",
                        "ROLES",
                        "setRoles()"
                      )
                    )
                  );
            }),
            (s.prototype.fetchPrevious = function () {
              var o = this;
              return (
                (this.fetchingInProgress = !0),
                new Promise(function (t, n) {
                  try {
                    0 == o.next_page && (t([]), (o.fetchingInProgress = !1));
                    var e = o.validateUserBuilder();
                    if (e instanceof a.CometChatException) return void n(e);
                    if (!o.fetchingInProgress)
                      return r.makeApiCall("users", {}, o.getPreData()).then(
                        function (e) {
                          if (
                            (e.meta &&
                              (o.total_pages = e.meta.pagination.total_pages),
                            0 < e.data.length)
                          ) {
                            o.pagination = e.meta.pagination;
                            var n = [];
                            e.data.map(function (e, t) {
                              n.push(i.UsersController.trasformJSONUser(e));
                            }),
                              s.userStore.storeUsers(n),
                              t(n),
                              (o.fetchingInProgress = !1);
                          }
                          t([]), (o.fetchingInProgress = !1);
                        },
                        function (e) {
                          n(new a.CometChatException(e.error)),
                            (o.fetchingInProgress = !1);
                        }
                      );
                    t([]);
                  } catch (e) {
                    (o.fetchingInProgress = !1), n(new a.CometChatException(e));
                  }
                })
              );
            }),
            (s.prototype.fetchNext = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  if (o.fetchingInProgress) (o.fetchingInProgress = !1), n([]);
                  else {
                    o.fetchingInProgress = !0;
                    var e = o.validateUserBuilder();
                    if (e instanceof a.CometChatException) return void t(e);
                    r.makeApiCall("users", {}, o.getNextData()).then(
                      function (e) {
                        if (
                          (e.meta &&
                            (o.total_pages = e.meta.pagination.total_pages),
                          0 < e.data.length)
                        ) {
                          o.pagination = e.meta.pagination;
                          var t = [];
                          e.data.map(function (e) {
                            t.push(i.UsersController.trasformJSONUser(e));
                          }),
                            n(t);
                        } else n([]);
                        o.fetchingInProgress = !1;
                      },
                      function (e) {
                        (o.fetchingInProgress = !1),
                          t(new a.CometChatException(e.error));
                      }
                    );
                  }
                } catch (e) {
                  (o.fetchingInProgress = !1), t(new a.CometChatException(e));
                }
              });
            }),
            (s.prototype.getNextData = function () {
              var e = {};
              if (
                ((e.per_page = this.limit),
                o.isFalsy(this.searchKeyword) ||
                  (e.searchKey = this.searchKeyword),
                o.isFalsy(this.status) || (e.status = this.status),
                o.isFalsy(this.hideBlockedUsers) || (e.hideBlockedUsers = 1),
                o.isFalsy(this.role) || (e.roles = this.role),
                o.isFalsy(this.roles) || (e.roles = this.roles),
                o.isFalsy(this.tags) || (e.tags = this.tags),
                o.isFalsy(this.friendsOnly) || (e.friendsOnly = 1),
                o.isFalsy(this.withTags) || (e.withTags = 1),
                o.isFalsy(this.UIDs) || (e.uids = this.UIDs),
                1 == this.current_page)
              )
                (e.page = this.next_page),
                  this.next_page++,
                  this.current_page++;
              else {
                if (this.next_page > this.total_pages)
                  return (e.page = this.next_page), e;
                e.page = this.next_page++;
              }
              return e;
            }),
            (s.prototype.getPreData = function () {
              var e = {};
              return (
                (e.per_page = this.limit),
                0 <= this.next_page &&
                  0 < this.next_page &&
                  (e.page = --this.next_page),
                e
              );
            }),
            (s.USER_STATUS = {
              ONLINE: c.PresenceConstatnts.STATUS.ONLINE,
              OFFLINE: c.PresenceConstatnts.STATUS.OFFLINE,
            }),
            s
          );
        })();
      t.UsersRequest = s;
      var u = (function () {
        function e() {
          this.shouldHideBlockedUsers = !1;
        }
        return (
          (e.prototype.setLimit = function (e) {
            return (this.limit = e), this;
          }),
          (e.prototype.setStatus = function (e) {
            return (this.status = e), this;
          }),
          (e.prototype.setSearchKeyword = function (e) {
            return (this.searchKeyword = e), this;
          }),
          (e.prototype.hideBlockedUsers = function (e) {
            return (this.shouldHideBlockedUsers = e), this;
          }),
          (e.prototype.setRole = function (e) {
            return (this.role = e), this;
          }),
          (e.prototype.setRoles = function (e) {
            return (this.roles = e), this;
          }),
          (e.prototype.friendsOnly = function (e) {
            return (this.showFriendsOnly = e), this;
          }),
          (e.prototype.setTags = function (e) {
            return (this.tags = e), this;
          }),
          (e.prototype.withTags = function (e) {
            return (this.showTags = e), this;
          }),
          (e.prototype.setUIDs = function (e) {
            return (this.UIDs = e), this;
          }),
          (e.prototype.build = function () {
            return (
              this.role && this.roles && this.roles.push(this.role), new s(this)
            );
          }),
          e
        );
      })();
      t.UsersRequestBuilder = u;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(3),
        s = n(0),
        r = n(1),
        i = n(13),
        a = (function () {
          function e(e) {
            (this.store = r.constants.DEFAULT_STORE),
              s.isFalsy(e) || (this.store = e),
              (this.userStore = i.createInstance({
                name: s.format(
                  r.LOCAL_STORE.STORE_STRING,
                  o.CometChat.getAppId(),
                  r.LOCAL_STORE.USERS_STORE
                ),
              })),
              this.userStore.setDriver([i.LOCALSTORAGE, i.INDEXEDDB, i.WEBSQL]);
          }
          return (
            (e.getInstance = function () {
              return (
                null == e.UserStore && (e.UserStore = new e()), e.UserStore
              );
            }),
            (e.prototype.set = function (e, t) {
              return this.userStore.setItem(e, t);
            }),
            (e.prototype.remove = function (e) {
              this.userStore.removeItem(e);
            }),
            (e.prototype.get = function (e) {
              return this.userStore.getItem(e);
            }),
            (e.prototype.clearStore = function () {
              var n = this;
              return new Promise(function (e, t) {
                n.userStore
                  .clear()
                  .then(function () {
                    e(!0);
                  })
                  .catch(function (e) {
                    t(e);
                  });
              });
            }),
            (e.prototype.clear = function (e) {}),
            (e.prototype.selectStore = function (e) {
              this.store = e;
            }),
            (e.prototype.storeUsers = function (e) {
              var t = this;
              return (
                e.map(function (e) {
                  t.set(e.getUid(), e);
                }),
                !0
              );
            }),
            (e.prototype.storeUser = function (e) {
              return this.set(e.getUid(), e), !0;
            }),
            e
          );
        })();
      t.UserStore = a;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var s = n(6),
        o = n(0),
        r = n(2),
        i = n(24),
        a = n(1),
        E = (function () {
          function e(e) {
            (this.limit = 100),
              (this.next_page = 1),
              (this.current_page = 1),
              (this.total_pages = -1),
              (this.fetchingInProgress = !1),
              (this.getUserAndGroupTags = !1),
              (this.pagination = {
                total: 0,
                count: 0,
                per_page: 0,
                current_page: 0,
                total_pages: 0,
                links: [],
              }),
              o.isFalsy(e) ||
                ((this.limit = e.limit),
                o.isFalsy(e.conversationType) ||
                  (this.conversationType = e.conversationType),
                o.isFalsy(e.getUserAndGroupTags) ||
                  (this.getUserAndGroupTags = e.getUserAndGroupTags));
          }
          return (
            (e.prototype.validateConversationBuilder = function () {
              return void 0 === this.limit
                ? new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(a.GENERAL_ERROR.METHOD_COMPULSORY),
                        "SET_LIMIT",
                        "SET_LIMIT",
                        "Set Limit"
                      )
                    )
                  )
                : isNaN(this.limit)
                ? new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                        ),
                        "SET_LIMIT",
                        "SET_LIMIT",
                        "setLimit()"
                      )
                    )
                  )
                : this.limit > a.DEFAULT_VALUES.CONVERSATION_MAX_LIMIT
                ? new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(a.GENERAL_ERROR.LIMIT_EXCEEDED),
                        "SET_LIMIT",
                        "SET_LIMIT",
                        a.DEFAULT_VALUES.CONVERSATION_MAX_LIMIT
                      )
                    )
                  )
                : this.limit < a.DEFAULT_VALUES.ZERO
                ? new r.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                        ),
                        "SET_LIMIT",
                        "SET_LIMIT",
                        "setLimit()"
                      )
                    )
                  )
                : void 0;
            }),
            (e.prototype.fetchNext = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  if (o.fetchingInProgress) (o.fetchingInProgress = !1), n([]);
                  else {
                    o.fetchingInProgress = !0;
                    var e = o.validateConversationBuilder();
                    if (e instanceof r.CometChatException) return void t(e);
                    s.makeApiCall("getConversations", {}, o.getNextData()).then(
                      function (e) {
                        if (
                          (e.meta &&
                            (o.total_pages = e.meta.pagination.total_pages),
                          0 < e.data.length)
                        ) {
                          o.pagination = e.meta.pagination;
                          var t = [];
                          e.data.map(function (e) {
                            t.push(
                              i.ConversationController.trasformJSONConversation(
                                e.conversationId,
                                e.conversationType,
                                e.lastMessage,
                                e.conversationWith,
                                e.unreadMessageCount
                              )
                            );
                          }),
                            n(t);
                        } else n([]);
                        o.fetchingInProgress = !1;
                      },
                      function (e) {
                        (o.fetchingInProgress = !1),
                          t(new r.CometChatException(e.error));
                      }
                    );
                  }
                } catch (e) {
                  (o.fetchingInProgress = !1), t(new r.CometChatException(e));
                }
              });
            }),
            (e.prototype.getNextData = function () {
              var e = {};
              if (
                ((e.per_page = this.limit),
                o.isFalsy(this.conversationType) ||
                  (e.conversationType = this.conversationType),
                o.isFalsy(this.getUserAndGroupTags) ||
                  (e.withUserAndGroupTags = this.getUserAndGroupTags),
                1 == this.current_page)
              )
                (e.page = this.next_page),
                  this.next_page++,
                  this.current_page++;
              else {
                if (this.next_page > this.total_pages)
                  return (e.page = this.next_page), e;
                e.page = this.next_page++;
              }
              return e;
            }),
            e
          );
        })();
      t.ConversationsRequest = E;
      var c = (function () {
        function e() {}
        return (
          (e.prototype.setLimit = function (e) {
            return (this.limit = e), this;
          }),
          (e.prototype.setConversationType = function (e) {
            return (this.conversationType = e), this;
          }),
          (e.prototype.withUserAndGroupTags = function (e) {
            return (
              "boolean" == typeof e && (this.getUserAndGroupTags = e), this
            );
          }),
          (e.prototype.build = function () {
            return new E(this);
          }),
          e
        );
      })();
      t.ConversationsRequestBuilder = c;
    },
    function (e, t, n) {
      "use strict";
      var d =
          (this && this.__awaiter) ||
          function (r, i, a, E) {
            return new (a || (a = Promise))(function (e, t) {
              function n(e) {
                try {
                  s(E.next(e));
                } catch (e) {
                  t(e);
                }
              }
              function o(e) {
                try {
                  s(E.throw(e));
                } catch (e) {
                  t(e);
                }
              }
              function s(t) {
                t.done
                  ? e(t.value)
                  : new a(function (e) {
                      e(t.value);
                    }).then(n, o);
              }
              s((E = E.apply(r, i || [])).next());
            });
          },
        _ =
          (this && this.__generator) ||
          function (n, o) {
            var s,
              r,
              i,
              e,
              a = {
                label: 0,
                sent: function () {
                  if (1 & i[0]) throw i[1];
                  return i[1];
                },
                trys: [],
                ops: [],
              };
            return (
              (e = { next: t(0), throw: t(1), return: t(2) }),
              "function" == typeof Symbol &&
                (e[Symbol.iterator] = function () {
                  return this;
                }),
              e
            );
            function t(t) {
              return function (e) {
                return (function (t) {
                  if (s) throw new TypeError("Generator is already executing.");
                  for (; a; )
                    try {
                      if (
                        ((s = 1),
                        r &&
                          (i =
                            2 & t[0]
                              ? r.return
                              : t[0]
                              ? r.throw || ((i = r.return) && i.call(r), 0)
                              : r.next) &&
                          !(i = i.call(r, t[1])).done)
                      )
                        return i;
                      switch (((r = 0), i && (t = [2 & t[0], i.value]), t[0])) {
                        case 0:
                        case 1:
                          i = t;
                          break;
                        case 4:
                          return a.label++, { value: t[1], done: !1 };
                        case 5:
                          a.label++, (r = t[1]), (t = [0]);
                          continue;
                        case 7:
                          (t = a.ops.pop()), a.trys.pop();
                          continue;
                        default:
                          if (
                            !(i = 0 < (i = a.trys).length && i[i.length - 1]) &&
                            (6 === t[0] || 2 === t[0])
                          ) {
                            a = 0;
                            continue;
                          }
                          if (
                            3 === t[0] &&
                            (!i || (t[1] > i[0] && t[1] < i[3]))
                          ) {
                            a.label = t[1];
                            break;
                          }
                          if (6 === t[0] && a.label < i[1]) {
                            (a.label = i[1]), (i = t);
                            break;
                          }
                          if (i && a.label < i[2]) {
                            (a.label = i[2]), a.ops.push(t);
                            break;
                          }
                          i[2] && a.ops.pop(), a.trys.pop();
                          continue;
                      }
                      t = o.call(n, a);
                    } catch (e) {
                      (t = [6, e]), (r = 0);
                    } finally {
                      s = i = 0;
                    }
                  if (5 & t[0]) throw t[1];
                  return { value: t[0] ? t[1] : void 0, done: !0 };
                })([t, e]);
              };
            }
          };
      t.__esModule = !0;
      var h = n(2),
        A = n(6),
        I = n(10),
        f = n(0),
        s = n(46),
        R = n(1),
        i = n(14),
        O = n(33),
        o = (function () {
          function e(e) {
            (this.limit = R.DEFAULT_VALUES.MSGS_LIMIT),
              (this.timestamp = 0),
              (this.id = R.DEFAULT_VALUES.DEFAULT_MSG_ID),
              (this.messageStore = s.MessagesStore.getInstance()),
              (this.endpointName = "getUserMessages"),
              (this.listId = ""),
              (this.totalPages = 0),
              (this.unread = !1),
              (this.inProgress = !1),
              (this.hideMessagesFromBlockedUsers = !1),
              (this.updatedAt = 0),
              (this.onlyUpdates = 0),
              (this.paginationMeta = {}),
              (this.hideDeletedMessages = !1),
              (this.limit = e.limit),
              (this.paginationMeta[
                R.MessageConstatnts.PAGINATION.KEYS.PER_PAGE
              ] = this.limit),
              (this.uid = e.uid),
              (this.guid = e.guid),
              (this.parentMessageId = e.parentMessageId),
              (this.timestamp = e.timestamp),
              this.timestamp &&
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.SENT_AT
                ] = this.timestamp),
              (this.id = e.id),
              this.id != R.DEFAULT_VALUES.DEFAULT_MSG_ID &&
                (this.paginationMeta[R.MessageConstatnts.PAGINATION.KEYS.ID] =
                  this.id),
              (this.hideMessagesFromBlockedUsers =
                e.HideMessagesFromBlockedUsers),
              this.hideMessagesFromBlockedUsers &&
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.HIDE_MESSAGES_FROM_BLOCKED_USER
                ] = this.hideMessagesFromBlockedUsers),
              (this.unread = e.unread),
              this.unread &&
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.UNREAD
                ] = this.unread),
              e.searchKey &&
                ((this.searchKey = e.searchKey),
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.SEARCH_KEY
                ] = this.searchKey)),
              e.onlyUpdate &&
                ((this.onlyUpdates = e.onlyUpdate),
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.ONLY_UPDATES
                ] = this.onlyUpdates)),
              e.updatedAt &&
                ((this.updatedAt = e.updatedAt),
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.UPDATED_AT
                ] = this.updatedAt)),
              e.category && (this.category = e.category),
              e.categories && (this.categories = e.categories),
              e.type && (this.type = e.type),
              e.types && (this.types = e.types),
              e.HideReplies &&
                ((this.hideReplies = e.HideReplies),
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.HIDE_REPLIES
                ] = this.hideReplies)),
              (this.hideDeletedMessages = e.HideDeletedMessages),
              this.hideDeletedMessages &&
                (this.paginationMeta[
                  R.MessageConstatnts.PAGINATION.KEYS.HIDE_DELETED_MESSAGES
                ] = this.hideDeletedMessages);
          }
          return (
            (e.prototype.fetchNext = function () {
              var e = this;
              return new Promise(function (s, r) {
                return d(e, void 0, void 0, function () {
                  var t, n, o;
                  return _(this, function (e) {
                    switch (e.label) {
                      case 0:
                        if ((e.trys.push([0, 5, , 6]), this.inProgress))
                          return (
                            (this.inProgress = !1),
                            r(
                              new h.CometChatException(
                                i.MESSAGES_REQUEST_ERRORS.REQUEST_IN_PROGRESS_ERROR
                              )
                            ),
                            [2]
                          );
                        if (((this.inProgress = !0), this.onlyUpdates)) {
                          if (0 == this.updatedAt)
                            return (
                              (this.inProgress = !1),
                              r(
                                new h.CometChatException(
                                  i.MESSAGES_REQUEST_ERRORS.NOT_ENOUGH_PARAMS
                                )
                              ),
                              [2]
                            );
                        } else if (0 == this.timestamp && 0 == this.id && 0 == this.updatedAt) return (this.inProgress = !1), r(new h.CometChatException(i.MESSAGES_REQUEST_ERRORS.NOT_ENOUGH_PARAMS)), [2];
                        (this.affix =
                          R.MessageConstatnts.PAGINATION.AFFIX.APPEND),
                          (this.paginationMeta[
                            R.MessageConstatnts.PAGINATION.KEYS.AFFIX
                          ] = this.affix),
                          (this.currentMethod =
                            R.MessageConstatnts.PAGINATION.AFFIX.APPEND),
                          (e.label = 1);
                      case 1:
                        return (
                          e.trys.push([1, 3, , 4]), [4, this.makeAPICall()]
                        );
                      case 2:
                        return (
                          (t = e.sent()), s(t), (this.inProgress = !1), [3, 4]
                        );
                      case 3:
                        return (
                          (n = e.sent()),
                          (this.inProgress = !1),
                          r(new h.CometChatException(n)),
                          [3, 4]
                        );
                      case 4:
                        return [3, 6];
                      case 5:
                        return (
                          (o = e.sent()),
                          (this.inProgress = !1),
                          r(new h.CometChatException(o)),
                          [3, 6]
                        );
                      case 6:
                        return [2];
                    }
                  });
                });
              });
            }),
            (e.prototype.fetchPrevious = function () {
              var e = this;
              return new Promise(function (s, r) {
                return d(e, void 0, void 0, function () {
                  var t, n, o;
                  return _(this, function (e) {
                    switch (e.label) {
                      case 0:
                        if ((e.trys.push([0, 5, , 6]), this.inProgress))
                          return (
                            r(
                              new h.CometChatException(
                                i.MESSAGES_REQUEST_ERRORS.REQUEST_IN_PROGRESS_ERROR
                              )
                            ),
                            (this.inProgress = !1),
                            [2]
                          );
                        if (
                          ((this.inProgress = !0),
                          this.onlyUpdates && 0 == this.updatedAt)
                        )
                          return (
                            (this.inProgress = !1),
                            r(
                              new h.CometChatException(
                                i.MESSAGES_REQUEST_ERRORS.NOT_ENOUGH_PARAMS
                              )
                            ),
                            [2]
                          );
                        (this.affix =
                          R.MessageConstatnts.PAGINATION.AFFIX.PREPEND),
                          (this.paginationMeta[
                            R.MessageConstatnts.PAGINATION.KEYS.AFFIX
                          ] = this.affix),
                          (this.currentMethod =
                            R.MessageConstatnts.PAGINATION.AFFIX.PREPEND),
                          (e.label = 1);
                      case 1:
                        return (
                          e.trys.push([1, 3, , 4]), [4, this.makeAPICall()]
                        );
                      case 2:
                        return (
                          (t = e.sent()), s(t), (this.inProgress = !1), [3, 4]
                        );
                      case 3:
                        return (
                          (n = e.sent()),
                          (this.inProgress = !1),
                          r(new h.CometChatException(n)),
                          [3, 4]
                        );
                      case 4:
                        return [3, 6];
                      case 5:
                        return (
                          (o = e.sent()),
                          (this.inProgress = !1),
                          r(new h.CometChatException(o)),
                          [3, 6]
                        );
                      case 6:
                        return [2];
                    }
                  });
                });
              });
            }),
            (e.prototype.makeAPICall = function () {
              var g = this;
              return new Promise(function (s, t) {
                try {
                  var e = g.uid;
                  if (void 0 !== e) {
                    if (
                      typeof e !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                              ),
                              "UID",
                              "UID",
                              "setUID()"
                            )
                          )
                        )
                      );
                    if (f.isFalsy(e.trim()))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(R.GENERAL_ERROR.INVALID),
                              "UID",
                              "UID",
                              "UID",
                              "UID"
                            )
                          )
                        )
                      );
                    g.uid = e.trim();
                  }
                  var n = g.guid;
                  if (void 0 !== n) {
                    if (
                      typeof n !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                              ),
                              "GUID",
                              "GUID",
                              "setGUID()"
                            )
                          )
                        )
                      );
                    if (f.isFalsy(n.trim()))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(R.GENERAL_ERROR.INVALID),
                              "GUID",
                              "GUID",
                              "GUID",
                              "GUID"
                            )
                          )
                        )
                      );
                    g.guid = n.trim();
                  }
                  var o = g.parentMessageId;
                  if (void 0 !== o) {
                    if (isNaN(o))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                              ),
                              "PARENT_MESSAGE_ID",
                              "PARENT_MESSAGE_ID",
                              "setParentMessageId()"
                            )
                          )
                        )
                      );
                    if (o < R.DEFAULT_VALUES.ZERO)
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR
                                  .PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                              ),
                              "PARENT_MESSAGE_ID",
                              "PARENT_MESSAGE_ID",
                              "setParentMessageId()"
                            )
                          )
                        )
                      );
                  }
                  var r =
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.PER_PAGE
                    ];
                  if (void 0 === r)
                    return void t(
                      new h.CometChatException(
                        JSON.parse(
                          f.format(
                            JSON.stringify(R.GENERAL_ERROR.METHOD_COMPULSORY),
                            "SET_LIMIT",
                            "SET_LIMIT",
                            "Set Limit"
                          )
                        )
                      )
                    );
                  if (isNaN(r))
                    return void t(
                      new h.CometChatException(
                        JSON.parse(
                          f.format(
                            JSON.stringify(
                              R.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                            ),
                            "SET_LIMIT",
                            "SET_LIMIT",
                            "setLimit()"
                          )
                        )
                      )
                    );
                  if (r > R.DEFAULT_VALUES.MSGS_MAX_LIMIT)
                    return void t(
                      new h.CometChatException(
                        JSON.parse(
                          f.format(
                            JSON.stringify(R.GENERAL_ERROR.LIMIT_EXCEEDED),
                            "SET_LIMIT",
                            "SET_LIMIT",
                            R.DEFAULT_VALUES.MSGS_MAX_LIMIT
                          )
                        )
                      )
                    );
                  if (r < R.DEFAULT_VALUES.ZERO)
                    return void t(
                      new h.CometChatException(
                        JSON.parse(
                          f.format(
                            JSON.stringify(
                              R.GENERAL_ERROR
                                .PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                            ),
                            "SET_LIMIT",
                            "SET_LIMIT",
                            "setLimit()"
                          )
                        )
                      )
                    );
                  var i =
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.SEARCH_KEY
                    ];
                  if (void 0 !== i) {
                    if (
                      typeof i !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                              ),
                              "SET_SEARCH_KEYWORD",
                              "SET_SEARCH_KEYWORD",
                              "setSearchKeyword()"
                            )
                          )
                        )
                      );
                    if (f.isFalsy(i.trim()))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(R.GENERAL_ERROR.INVALID),
                              "SET_SEARCH_KEYWORD",
                              "SET_SEARCH_KEYWORD",
                              "search keyword",
                              "search keyword"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.SEARCH_KEY
                    ] = i.trim();
                  }
                  var a =
                    g.paginationMeta[R.MessageConstatnts.PAGINATION.KEYS.ID];
                  if (void 0 !== a) {
                    if (isNaN(a))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                              ),
                              "MESSAGE_ID",
                              "MESSAGE_ID",
                              "setMessageId()"
                            )
                          )
                        )
                      );
                    if (a < R.DEFAULT_VALUES.ZERO)
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR
                                  .PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                              ),
                              "MESSAGE_ID",
                              "MESSAGE_ID",
                              "setMessageId()"
                            )
                          )
                        )
                      );
                  }
                  var E =
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.SENT_AT
                    ];
                  if (void 0 !== E) {
                    if (isNaN(E))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                              ),
                              "TIMESTAMP",
                              "TIMESTAMP",
                              "setTimestamp()"
                            )
                          )
                        )
                      );
                    if (E < R.DEFAULT_VALUES.ZERO)
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR
                                  .PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                              ),
                              "TIMESTAMP",
                              "TIMESTAMP",
                              "setTimestamp()"
                            )
                          )
                        )
                      );
                  }
                  var c = g.hideMessagesFromBlockedUsers;
                  if (void 0 !== c) {
                    if (
                      typeof c !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                              ),
                              "HIDE_MESSAGES_FROM_BLOCKED_USERS",
                              "HIDE_MESSAGES_FROM_BLOCKED_USERS",
                              "hideMessagesFromBlockedUsers()"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.HIDE_MESSAGES_FROM_BLOCKED_USER
                    ] = 1 == c ? 1 : 0;
                  }
                  var u = g.unread;
                  if (void 0 !== u) {
                    if (
                      typeof u !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                              ),
                              "UNREAD",
                              "UNREAD",
                              "setUnread()"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.UNREAD
                    ] = 1 == u ? 1 : 0;
                  }
                  var S = g.category;
                  if (void 0 !== S) {
                    if (
                      typeof S !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                              ),
                              "SET_CATEGORY",
                              "SET_CATEGORY",
                              "setCategory()"
                            )
                          )
                        )
                      );
                    if (f.isFalsy(S.trim()))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(R.GENERAL_ERROR.INVALID),
                              "SET_CATEGORY",
                              "SET_CATEGORY",
                              "category",
                              "category"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.CATEGORIES
                    ] = S.trim();
                  }
                  var l = g.categories;
                  if (void 0 !== l) {
                    if (!Array.isArray(l))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                              ),
                              "SET_CATEGORIES",
                              "SET_CATEGORIES",
                              "setCategories()"
                            )
                          )
                        )
                      );
                    0 < l.length &&
                      (g.paginationMeta[
                        R.MessageConstatnts.PAGINATION.KEYS.CATEGORIES
                      ] = l);
                  }
                  var p = g.type;
                  if (void 0 !== p) {
                    if (
                      typeof p !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                              ),
                              "SET_TYPE",
                              "SET_TYPE",
                              "setType()"
                            )
                          )
                        )
                      );
                    if (f.isFalsy(p.trim()))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(R.GENERAL_ERROR.INVALID),
                              "SET_TYPE",
                              "SET_TYPE",
                              "type",
                              "type"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.TYPES
                    ] = p.trim();
                  }
                  var C = g.types;
                  if (void 0 !== C) {
                    if (!Array.isArray(C))
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_AN_ARRAY
                              ),
                              "SET_TYPES",
                              "SET_TYPES",
                              "setTypes()"
                            )
                          )
                        )
                      );
                    0 < C.length &&
                      (g.paginationMeta[
                        R.MessageConstatnts.PAGINATION.KEYS.TYPES
                      ] = C);
                  }
                  var T = g.hideDeletedMessages;
                  if (void 0 !== T) {
                    if (
                      typeof T !==
                      R.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.BOOLEAN
                    )
                      return void t(
                        new h.CometChatException(
                          JSON.parse(
                            f.format(
                              JSON.stringify(
                                R.GENERAL_ERROR.PARAMETER_MUST_BE_A_BOOLEAN
                              ),
                              "HIDE_DELETED_MESSAGES",
                              "HIDE_DELETED_MESSAGES",
                              "hideDeletedMessages()"
                            )
                          )
                        )
                      );
                    g.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.HIDE_DELETED_MESSAGES
                    ] = 1 == T ? 1 : 0;
                  }
                  if ((g.createEndpoint(), g.totalPages))
                    if (1 != g.totalPages);
                    else if (g.lastAffix == g.affix) return void s([]);
                  A.makeApiCall(
                    g.endpointName,
                    { listId: g.listId },
                    g.paginationMeta
                  ).then(
                    function (t) {
                      return d(g, void 0, void 0, function () {
                        var n,
                          o = this;
                        return _(this, function (e) {
                          return (
                            (n = []),
                            t[R.ResponseConstants.RESPONSE_KEYS.KEY_META][
                              R.ResponseConstants.RESPONSE_KEYS.KEY_CURSOR
                            ].hasOwnProperty(
                              R.MessageConstatnts.PAGINATION.KEYS.ID
                            ) &&
                              0 == this.id &&
                              (this.id = parseInt(
                                t.meta.cursor[
                                  R.MessageConstatnts.PAGINATION.KEYS.ID
                                ]
                              )),
                            t[R.ResponseConstants.RESPONSE_KEYS.KEY_META][
                              R.ResponseConstants.RESPONSE_KEYS.KEY_CURSOR
                            ].hasOwnProperty(
                              R.MessageConstatnts.PAGINATION.KEYS.SENT_AT
                            ) &&
                              0 == this.timestamp &&
                              (this.timestamp =
                                t.meta.cursor[
                                  R.MessageConstatnts.PAGINATION.KEYS.SENT_AT
                                ]),
                            t.meta.pagination.hasOwnProperty("total_pages") &&
                              (this.totalPages = t.meta.pagination.total_pages),
                            t.meta.cursor.hasOwnProperty("affix") &&
                              (this.lastAffix = t.meta.cursor.affix),
                            t.data[0]
                              ? (0 < this.id &&
                                  (this.id = parseInt(t.data[0].id)),
                                0 < this.timestamp &&
                                  (this.timestamp = t.data[0].sentAt),
                                t.data.map(function (t) {
                                  O.MessageListnerMaping.getInstance()
                                    .get(
                                      R.LOCAL_STORE.KEY_MESSAGE_LISTENER_LIST
                                    )
                                    .then(
                                      function (e) {
                                        parseInt(t.id) > e &&
                                          O.MessageListnerMaping.getInstance().set(
                                            R.LOCAL_STORE
                                              .KEY_MESSAGE_LISTENER_LIST,
                                            parseInt(t.id)
                                          );
                                      },
                                      function (e) {
                                        O.MessageListnerMaping.getInstance().set(
                                          R.LOCAL_STORE
                                            .KEY_MESSAGE_LISTENER_LIST,
                                          parseInt(t.id)
                                        );
                                      }
                                    ),
                                    o.affix ==
                                    R.MessageConstatnts.PAGINATION.AFFIX.APPEND
                                      ? (o.id < parseInt(t.id) &&
                                          0 < o.id &&
                                          (o.id = parseInt(t.id)),
                                        o.timestamp < t.sentAt &&
                                          0 < o.timestamp &&
                                          (o.timestamp = t.sentAt),
                                        o.updatedAt < t.updatedAt &&
                                          0 < o.updatedAt &&
                                          (o.updatedAt = t.updatedAt))
                                      : (o.id > parseInt(t.id) &&
                                          (o.id = parseInt(t.id)),
                                        o.timestamp > t.sentAt &&
                                          (o.timestamp = t.sentAt),
                                        o.updatedAt > t.updatedAt &&
                                          (o.updatedAt = t.updatedAt)),
                                    o.id &&
                                      (o.paginationMeta[
                                        R.MessageConstatnts.PAGINATION.KEYS.ID
                                      ] = o.id),
                                    o.timestamp &&
                                      (o.paginationMeta[
                                        R.MessageConstatnts.PAGINATION.KEYS.SENT_AT
                                      ] = o.timestamp),
                                    o.updatedAt &&
                                      (o.paginationMeta[
                                        R.MessageConstatnts.PAGINATION.KEYS.UPDATED_AT
                                      ] = o.updatedAt),
                                    n.push(
                                      I.MessageController.trasformJSONMessge(t)
                                    );
                                }))
                              : (n = []),
                            s(n),
                            [2]
                          );
                        });
                      });
                    },
                    function (e) {
                      t(new h.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  t(new h.CometChatException(e));
                }
              });
            }),
            (e.prototype.createEndpoint = function () {
              this.parentMessageId
                ? ((this.endpointName = "getThreadMessages"),
                  (this.listId = this.parentMessageId.toString()),
                  this.hideReplies &&
                    ((this.hideReplies = !1),
                    delete this.paginationMeta[
                      R.MessageConstatnts.PAGINATION.KEYS.HIDE_REPLIES
                    ]))
                : (f.isFalsy(this.guid) || f.isFalsy(this.uid)) &&
                  f.isFalsy(this.guid)
                ? (f.isFalsy(this.uid)
                    ? (this.endpointName = "getMessages")
                    : (this.endpointName = "getUserMessages"),
                  (this.listId = this.uid))
                : ((this.endpointName = "getGroupMessages"),
                  (this.listId = this.guid));
            }),
            (e.prototype.makeData = function () {
              var e = {};
              (e[R.MessageConstatnts.PAGINATION.KEYS.PER_PAGE] = this.limit),
                (e[R.MessageConstatnts.PAGINATION.KEYS.AFFIX] = this.affix),
                (f.isFalsy(this.guid) || f.isFalsy(this.uid)) &&
                  f.isFalsy(this.guid) &&
                  f.isFalsy(this.uid);
            }),
            (e.prototype.getFilteredPreviousDataByReceiverId = function (t) {
              return d(this, void 0, void 0, function () {
                var n,
                  o = this;
                return _(this, function (e) {
                  switch (e.label) {
                    case 0:
                      switch (((n = []), t)) {
                        case "user":
                          return [3, 1];
                        case "group":
                          return [3, 3];
                        case "both":
                          return [3, 5];
                      }
                      return [3, 7];
                    case 1:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.uid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 2:
                      return e.sent(), [3, 9];
                    case 3:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.guid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 4:
                      e.sent(), (e.label = 5);
                    case 5:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.guid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .filter(function (e) {
                                return t[e].sender.uid == o.uid;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 6:
                      return e.sent(), [3, 9];
                    case 7:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .getAllMessages()
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 8:
                      return e.sent(), [3, 9];
                    case 9:
                      return [2, n];
                  }
                });
              });
            }),
            (e.prototype.getFilteredNextDataByReceiverId = function (t) {
              return d(this, void 0, void 0, function () {
                var n,
                  o = this;
                return _(this, function (e) {
                  switch (e.label) {
                    case 0:
                      switch (((n = []), t)) {
                        case "user":
                          return [3, 1];
                        case "group":
                          return [3, 3];
                        case "both":
                          return [3, 5];
                      }
                      return [3, 7];
                    case 1:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.uid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 2:
                      return e.sent(), [3, 9];
                    case 3:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.guid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 4:
                      e.sent(), (e.label = 5);
                    case 5:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .get(this.guid)
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .filter(function (e) {
                                return t[e].sender.uid == o.uid;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 6:
                      return e.sent(), [3, 9];
                    case 7:
                      return [
                        4,
                        s.MessagesStore.getInstance()
                          .getAllMessages()
                          .then(function (t) {
                            Object.keys(t)
                              .filter(function (e) {
                                return parseInt(e) > o.id;
                              })
                              .map(function (e) {
                                n = n.concat([t[e]]);
                              });
                          }),
                      ];
                    case 8:
                      return e.sent(), [3, 9];
                    case 9:
                      return [2, n];
                  }
                });
              });
            }),
            e
          );
        })();
      t.DefaultMessagesRequest = o;
      var r = (function () {
        function e() {
          (this.maxLimit = R.DEFAULT_VALUES.MSGS_MAX_LIMIT),
            (this.timestamp = 0),
            (this.id = R.DEFAULT_VALUES.DEFAULT_MSG_ID),
            (this.unread = !1),
            (this.HideMessagesFromBlockedUsers = !1),
            (this.onlyUpdate = 0),
            (this.HideDeletedMessages = !1);
        }
        return (
          (e.prototype.setLimit = function (e) {
            return (this.limit = e), this;
          }),
          (e.prototype.setGUID = function (e) {
            return (this.guid = e), this;
          }),
          (e.prototype.setUID = function (e) {
            return (this.uid = e), this;
          }),
          (e.prototype.setParentMessageId = function (e) {
            return (this.parentMessageId = e), this;
          }),
          (e.prototype.setTimestamp = function (e) {
            return (
              void 0 === e && (e = f.getCurrentTime()),
              (this.timestamp = e),
              this
            );
          }),
          (e.prototype.setMessageId = function (e) {
            return (
              void 0 === e && (e = R.DEFAULT_VALUES.DEFAULT_MSG_ID),
              (this.id = e),
              this
            );
          }),
          (e.prototype.setUnread = function (e) {
            return void 0 === e && (e = !1), (this.unread = e), this;
          }),
          (e.prototype.hideMessagesFromBlockedUsers = function (e) {
            return (
              void 0 === e && (e = !1),
              (this.HideMessagesFromBlockedUsers = e),
              this
            );
          }),
          (e.prototype.setSearchKeyword = function (e) {
            return (this.searchKey = e), this;
          }),
          (e.prototype.setUpdatedAfter = function (e) {
            return (this.updatedAt = e), this;
          }),
          (e.prototype.updatesOnly = function (e) {
            return e && (this.onlyUpdate = 1), this;
          }),
          (e.prototype.setCategory = function (e) {
            return (this.category = e), this;
          }),
          (e.prototype.setCategories = function (e) {
            return (this.categories = e), this;
          }),
          (e.prototype.setType = function (e) {
            return (this.type = e), this;
          }),
          (e.prototype.setTypes = function (e) {
            return (this.types = e), this;
          }),
          (e.prototype.hideReplies = function (e) {
            return (this.HideReplies = e), this;
          }),
          (e.prototype.hideDeletedMessages = function (e) {
            return (this.HideDeletedMessages = e), this;
          }),
          (e.prototype.build = function () {
            return (
              this.category &&
                this.categories &&
                this.categories.push(this.category),
              this.type && this.types && this.types.push(this.type),
              new o(this)
            );
          }),
          e
        );
      })();
      t.DefaultMessagesRequestBuilder = r;
    },
    function (e, t, n) {
      "use strict";
      var a =
        (this && this.__assign) ||
        function () {
          return (a =
            Object.assign ||
            function (e) {
              for (var t, n = 1, o = arguments.length; n < o; n++)
                for (var s in (t = arguments[n]))
                  Object.prototype.hasOwnProperty.call(t, s) && (e[s] = t[s]);
              return e;
            }).apply(this, arguments);
        };
      t.__esModule = !0;
      var o = n(3),
        E = n(0),
        c = n(1),
        s = n(13),
        r = (function () {
          function e(e) {
            (this.store = c.constants.DEFAULT_STORE),
              E.isFalsy(e) || (this.store = e),
              (this.messagesStore = s.createInstance({
                name: E.format(
                  c.LOCAL_STORE.STORE_STRING,
                  o.CometChat.getAppId(),
                  c.LOCAL_STORE.MESSAGES_STORE
                ),
              })),
              this.messagesStore.setDriver([
                s.LOCALSTORAGE,
                s.INDEXEDDB,
                s.WEBSQL,
              ]);
          }
          return (
            (e.getInstance = function () {
              return (
                null == e.MessagesStore && (e.MessagesStore = new e()),
                e.MessagesStore
              );
            }),
            (e.prototype.set = function (e, t) {
              return this.messagesStore.setItem(e, t);
            }),
            (e.prototype.remove = function (e) {
              this.messagesStore.removeItem(e);
            }),
            (e.prototype.get = function (e) {
              return this.messagesStore.getItem(e);
            }),
            (e.prototype.clearStore = function () {
              var n = this;
              return new Promise(function (e, t) {
                n.messagesStore
                  .clear()
                  .then(function () {
                    e(!0);
                  })
                  .catch(function (e) {
                    t(e);
                  });
              });
            }),
            (e.prototype.getAllMessages = function () {
              var n = this,
                o = {};
              return new Promise(function (e, t) {
                n.messagesStore
                  .iterate(function (e, t, n) {
                    t != c.constants.MSG_VER_POST &&
                      t != c.constants.MSG_VER_POST &&
                      (o = a({}, o, e));
                  })
                  .then(function () {
                    e(o);
                  });
              });
            }),
            (e.prototype.clear = function (e) {}),
            (e.prototype.selectStore = function (e) {
              this.store = e;
            }),
            (e.prototype.storeMessages = function (e) {
              var n = this;
              if (o.CometChat.user.getUid()) {
                var s = o.CometChat.user.getUid(),
                  r = {},
                  i = 0;
                return (
                  this.get(c.constants.MSG_VER_POST).then(function (o) {
                    e.map(function (e) {
                      var t;
                      if (!E.isFalsy(e)) {
                        0 == i && (i = parseInt(e.getId().toString())),
                          i > e.getId() && (i = parseInt(e.getId().toString())),
                          o < e.getId() && (o = parseInt(e.getId().toString()));
                        var n = void 0;
                        (n =
                          e.getSender() instanceof Object
                            ? e.getSender().getUid()
                            : e.getSender()),
                          e.getReceiverType() ==
                            c.MessageConstatnts.RECEIVER_TYPE.GROUP &&
                            (n = e.getReceiver()),
                          e.getSender() instanceof Object
                            ? e.getSender().getUid() == s &&
                              (n = e.getReceiver())
                            : e.getSender() == s && (n = e.getReceiver()),
                          r[n] || (r[n] = {}),
                          (r[n] = a({}, r[n], (((t = {})[e.getId()] = e), t)));
                      }
                    }),
                      n.get(c.constants.MSG_VER_PRE).then(function (e) {
                        ((0 < i && i < e) || null == e) &&
                          n.set(c.constants.MSG_VER_PRE, i);
                      }) &&
                        0 < o &&
                        n.set(c.constants.MSG_VER_POST, o),
                      Object.keys(r).map(function (t) {
                        n.get(t).then(function (e) {
                          null == e && (e = {}), n.set(t, a({}, r[t], e));
                        });
                      });
                  }),
                  !0
                );
              }
            }),
            e
          );
        })();
      t.MessagesStore = r;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var s = n(6),
        o = n(0),
        r = n(11),
        i = n(2),
        a = n(1),
        E = (function () {
          function e(e) {
            (this.next_page = 1),
              (this.current_page = 1),
              (this.total_pages = -1),
              (this.fetchingInProgress = !1),
              (this.pagination = {
                total: 0,
                count: 0,
                per_page: 0,
                current_page: 0,
                total_pages: 0,
                links: [],
              }),
              o.isFalsy(e) ||
                ((this.limit = e.limit),
                o.isFalsy(e.searchKeyword) ||
                  (this.searchKeyword = e.searchKeyword),
                o.isFalsy(e.direction) || (this.direction = e.direction));
          }
          return (
            (e.prototype.validateBlockedUsersBuilder = function () {
              if (void 0 === this.limit)
                return new i.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(a.GENERAL_ERROR.METHOD_COMPULSORY),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "Set Limit"
                    )
                  )
                );
              if (isNaN(this.limit))
                return new i.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        a.GENERAL_ERROR.PARAMETER_MUST_BE_A_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (this.limit > a.DEFAULT_VALUES.USERS_MAX_LIMIT)
                return new i.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(a.GENERAL_ERROR.LIMIT_EXCEEDED),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      a.DEFAULT_VALUES.USERS_MAX_LIMIT
                    )
                  )
                );
              if (this.limit < a.DEFAULT_VALUES.ZERO)
                return new i.CometChatException(
                  JSON.parse(
                    o.format(
                      JSON.stringify(
                        a.GENERAL_ERROR.PARAMETER_MUST_BE_A_POSITIVE_NUMBER
                      ),
                      "SET_LIMIT",
                      "SET_LIMIT",
                      "setLimit()"
                    )
                  )
                );
              if (void 0 !== this.searchKeyword) {
                if (
                  typeof this.searchKeyword !==
                  a.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new i.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "setSearchKeyword()"
                      )
                    )
                  );
                if (o.isFalsy(this.searchKeyword.trim()))
                  return new i.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(a.GENERAL_ERROR.INVALID),
                        "SET_SEARCH_KEYWORD",
                        "SET_SEARCH_KEYWORD",
                        "search keyword",
                        "search keyword"
                      )
                    )
                  );
                this.searchKeyword = this.searchKeyword.trim();
              }
              if (void 0 !== this.direction) {
                if (
                  typeof this.direction !==
                  a.COMMON_UTILITY_CONSTANTS.TYPE_CONSTANTS.STRING
                )
                  return new i.CometChatException(
                    JSON.parse(
                      o.format(
                        JSON.stringify(
                          a.GENERAL_ERROR.PARAMETER_MUST_BE_A_STRING
                        ),
                        "SET_DIRECTION",
                        "SET_DIRECTION",
                        "setDirection()"
                      )
                    )
                  );
                if (
                  this.direction !=
                    a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS.BOTH &&
                  this.direction !=
                    a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS
                      .BLOCKED_BY_ME &&
                  this.direction !=
                    a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS
                      .HAS_BLOCKED_ME
                )
                  return new i.CometChatException(
                    a.UserErrors.INVALID_DIRECTION
                  );
              }
            }),
            (e.prototype.fetchPrevious = function () {
              var o = this;
              return (
                (this.fetchingInProgress = !0),
                new Promise(function (t, n) {
                  try {
                    0 == o.next_page && (t([]), (o.fetchingInProgress = !1));
                    var e = o.validateBlockedUsersBuilder();
                    if (e instanceof i.CometChatException) return void n(e);
                    if (!o.fetchingInProgress)
                      return s
                        .makeApiCall("blockedUsersList", {}, o.getPreData())
                        .then(
                          function (e) {
                            if (
                              (e.meta &&
                                (o.total_pages = e.meta.pagination.total_pages),
                              0 < e.data.length)
                            ) {
                              o.pagination = e.meta.pagination;
                              var n = [];
                              e.data.map(function (e, t) {
                                n.push(r.UsersController.trasformJSONUser(e));
                              }),
                                t(n),
                                (o.fetchingInProgress = !1);
                            }
                            t([]), (o.fetchingInProgress = !1);
                          },
                          function (e) {
                            n(new i.CometChatException(e.error)),
                              (o.fetchingInProgress = !1);
                          }
                        );
                    t([]);
                  } catch (e) {
                    (o.fetchingInProgress = !1), n(new i.CometChatException(e));
                  }
                })
              );
            }),
            (e.prototype.fetchNext = function () {
              var o = this;
              return new Promise(function (n, t) {
                try {
                  if (o.fetchingInProgress) (o.fetchingInProgress = !1), n([]);
                  else {
                    o.fetchingInProgress = !0;
                    var e = o.validateBlockedUsersBuilder();
                    if (e instanceof i.CometChatException) return void t(e);
                    s.makeApiCall("blockedUsersList", {}, o.getNextData()).then(
                      function (e) {
                        if (
                          (e.meta &&
                            (o.total_pages = e.meta.pagination.total_pages),
                          0 < e.data.length)
                        ) {
                          o.pagination = e.meta.pagination;
                          var t = [];
                          e.data.map(function (e) {
                            t.push(r.UsersController.trasformJSONUser(e));
                          }),
                            n(t);
                        } else n([]);
                        o.fetchingInProgress = !1;
                      },
                      function (e) {
                        (o.fetchingInProgress = !1),
                          t(new i.CometChatException(e.error));
                      }
                    );
                  }
                } catch (e) {
                  (o.fetchingInProgress = !1), t(new i.CometChatException(e));
                }
              });
            }),
            (e.prototype.getNextData = function () {
              var e = {};
              if (
                ((e.per_page = this.limit),
                o.isFalsy(this.direction) || (e.direction = this.direction),
                o.isFalsy(this.searchKeyword) ||
                  (e.searchKey = this.searchKeyword),
                1 == this.current_page)
              )
                (e.page = this.next_page),
                  this.next_page++,
                  this.current_page++;
              else {
                if (this.next_page > this.total_pages)
                  return (e.page = this.next_page), e;
                e.page = this.next_page++;
              }
              return e;
            }),
            (e.prototype.getPreData = function () {
              var e = {};
              return (
                (e.per_page = this.limit),
                0 <= this.next_page &&
                  0 < this.next_page &&
                  (e.page = --this.next_page),
                e
              );
            }),
            (e.MAX_LIMIT = 2),
            (e.DEFAULT_LIMIT = 1),
            (e.directions = a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS),
            e
          );
        })();
      t.BlockedUsersRequest = E;
      var c = (function () {
        function e() {}
        return (
          (e.prototype.setLimit = function (e) {
            return (this.limit = e), this;
          }),
          (e.prototype.setSearchKeyword = function (e) {
            return (this.searchKeyword = e), this;
          }),
          (e.prototype.setDirection = function (e) {
            return (this.direction = e), this;
          }),
          (e.prototype.blockedByMe = function () {
            return (
              (this.direction =
                a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS.BLOCKED_BY_ME),
              this
            );
          }),
          (e.prototype.hasBlockedMe = function () {
            return (
              (this.direction =
                a.BlockedUsersConstants.REQUEST_KEYS.DIRECTIONS.HAS_BLOCKED_ME),
              this
            );
          }),
          (e.prototype.build = function () {
            return new E(this);
          }),
          e
        );
      })();
      t.BlockedUsersRequestBuilder = c;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = (function () {
          function t(e) {
            (this.subscriptionType = t.SUBSCRIPTION_TYPE_NONE),
              (this.roles = null),
              (this.region = o.DEFAULT_VALUES.REGION_DEFAULT),
              (this.autoJoinGroup = !0),
              (this.subscriptionType = e.subscriptionType),
              (this.roles = e.roles),
              (this.region = e.region),
              (this.autoJoinGroup = e.autoJoinGroup);
          }
          return (
            (t.prototype.getSubscriptionType = function () {
              return this.subscriptionType;
            }),
            (t.prototype.getRoles = function () {
              return this.roles;
            }),
            (t.prototype.getRegion = function () {
              return this.region;
            }),
            (t.prototype.getIsAutoJoinEnabled = function () {
              return this.autoJoinGroup;
            }),
            (t.SUBSCRIPTION_TYPE_NONE = "NONE"),
            (t.SUBSCRIPTION_TYPE_ALL_USERS = "ALL_USERS"),
            (t.SUBSCRIPTION_TYPE_ROLES = "ROLES"),
            (t.SUBSCRIPTION_TYPE_FRIENDS = "FRIENDS"),
            (t.REGION_EU = o.DEFAULT_VALUES.REGION_DEFAULT_EU),
            (t.REGION_US = o.DEFAULT_VALUES.REGION_DEFAULT_US),
            (t.REGION_IN = o.DEFAULT_VALUES.REGION_DEFAULT_IN),
            (t.REGION_PRIVATE = o.DEFAULT_VALUES.REGION_DEFAULT_PRIVATE),
            t
          );
        })();
      t.AppSettings = s;
      var r = (function () {
        function e() {
          (this.subscriptionType = s.SUBSCRIPTION_TYPE_NONE),
            (this.roles = null),
            (this.region = o.DEFAULT_VALUES.REGION_DEFAULT),
            (this.autoJoinGroup = !0);
        }
        return (
          (e.prototype.subscribePresenceForAllUsers = function () {
            return (
              (this.subscriptionType = s.SUBSCRIPTION_TYPE_ALL_USERS), this
            );
          }),
          (e.prototype.subscribePresenceForRoles = function (e) {
            return (
              (this.subscriptionType = s.SUBSCRIPTION_TYPE_ROLES),
              (this.roles = e),
              this
            );
          }),
          (e.prototype.subscribePresenceForFriends = function () {
            return (this.subscriptionType = s.SUBSCRIPTION_TYPE_FRIENDS), this;
          }),
          (e.prototype.setRegion = function (e) {
            return (
              void 0 === e && (e = o.DEFAULT_VALUES.REGION_DEFAULT),
              (this.region = e.toLowerCase()),
              this
            );
          }),
          (e.prototype.enableAutoJoinForGroups = function (e) {
            return void 0 === e && (e = !0), (this.autoJoinGroup = e), this;
          }),
          (e.prototype.build = function () {
            return new s(this);
          }),
          e
        );
      })();
      t.AppSettingsBuilder = r;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var S = n(10),
        l = n(1),
        p = n(11),
        C = n(15),
        T = n(24),
        o = n(3),
        s = n(2),
        r = n(0),
        i = (function () {
          function e() {}
          return (
            (e.processMessage = function (e) {
              try {
                return S.MessageController.trasformJSONMessge(e);
              } catch (e) {
                r.Logger.error("CometChatHelper: processMessage", e);
              }
            }),
            (e.getConversationFromMessage = function (u) {
              return new Promise(function (E, c) {
                try {
                  o.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (null !== e) {
                        var t = S.MessageController.trasformJSONMessge(u),
                          n = t.receiverType,
                          o = t.conversationId,
                          s = void 0,
                          r = S.MessageController.trasformJSONMessge(u);
                        if (n == l.MessageConstatnts.RECEIVER_TYPE.USER) {
                          var i = p.UsersController.trasformJSONUser(
                            t[l.MessageConstatnts.KEYS.SENDER]
                          );
                          s =
                            i.getUid() == e.getUid()
                              ? p.UsersController.trasformJSONUser(
                                  t[l.MessageConstatnts.KEYS.RECEIVER]
                                )
                              : i;
                        } else
                          s = C.GroupsController.trasformJSONGroup(
                            t[l.MessageConstatnts.KEYS.RECEIVER]
                          );
                        var a =
                          T.ConversationController.trasformJSONConversation(
                            o,
                            n,
                            r,
                            s,
                            0
                          );
                        E(a);
                      } else c(l.UserErrors.USER_NOT_LOGGED_IN);
                    },
                    function (e) {
                      c(new s.CometChatException(e.error));
                    }
                  );
                } catch (e) {
                  c(new s.CometChatException(e));
                }
              });
            }),
            e
          );
        })();
      t.CometChatHelper = i;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = (function () {
          function e(e) {
            (this.sessionID = e.sessionID),
              (this.defaultLayout = e.defaultLayout),
              (this.isAudioOnly = e.isAudioOnly),
              (this.region = e.region),
              (this.user = e.user),
              (this.mode = e.mode),
              (this.domain = e.domain),
              (this.ShowEndCallButton = e.ShowEndCallButton),
              (this.ShowMuteAudioButton = e.ShowMuteAudioButton),
              (this.ShowPauseVideoButton = e.ShowPauseVideoButton),
              (this.ShowScreenShareButton = e.ShowScreenShareButton),
              (this.ShowSwitchModeButton = e.ShowSwitchModeButton),
              (this.StartAudioMuted = e.StartAudioMuted),
              (this.StartVideoMuted = e.StartVideoMuted),
              (this.localizedObject = e.localizedObject),
              (this.analyticsSettings = e.analyticsSettings),
              (this.appId = e.appId),
              (this.customCSS = e.customCSS),
              (this.ShowRecordingButton = e.ShowRecordingButton),
              (this.StartRecordingOnCallStart = e.StartRecordingOnCallStart),
              (this.useLegacyUI = e.useLegacyUI);
          }
          return (
            (e.prototype.shouldUseLegacyUI = function () {
              return this.useLegacyUI;
            }),
            (e.prototype.isRecordingButtonEnabled = function () {
              return this.ShowRecordingButton;
            }),
            (e.prototype.shouldStartRecordingOnCallStart = function () {
              return this.StartRecordingOnCallStart;
            }),
            (e.prototype.getCustomCSS = function () {
              return this.customCSS;
            }),
            (e.prototype.getSessionId = function () {
              return this.sessionID;
            }),
            (e.prototype.isAudioOnlyCall = function () {
              return this.isAudioOnly;
            }),
            (e.prototype.getUser = function () {
              return this.user;
            }),
            (e.prototype.getRegion = function () {
              return this.region;
            }),
            (e.prototype.getAppId = function () {
              return this.appId;
            }),
            (e.prototype.getDomain = function () {
              return this.domain;
            }),
            (e.prototype.isDefaultLayoutEnabled = function () {
              return this.defaultLayout;
            }),
            (e.prototype.getMode = function () {
              return this.mode;
            }),
            (e.prototype.getStartWithAudioMuted = function () {
              return this.StartAudioMuted;
            }),
            (e.prototype.getStartWithVideoMuted = function () {
              return this.StartVideoMuted;
            }),
            (e.prototype.isEndCallButtonEnabled = function () {
              return this.ShowEndCallButton;
            }),
            (e.prototype.isMuteAudioButtonEnabled = function () {
              return this.ShowMuteAudioButton;
            }),
            (e.prototype.isPauseVideoButtonEnabled = function () {
              return this.ShowPauseVideoButton;
            }),
            (e.prototype.isScreenShareButtonEnabled = function () {
              return this.ShowScreenShareButton;
            }),
            (e.prototype.isModeButtonEnabled = function () {
              return this.ShowSwitchModeButton;
            }),
            (e.prototype.getLocalizedStringObject = function () {
              return this.localizedObject;
            }),
            (e.prototype.getAnalyticsSettings = function () {
              return this.analyticsSettings;
            }),
            e
          );
        })();
      t.CallSettings = s;
      var r = (function () {
        function e() {
          (this.defaultLayout = !0),
            (this.isAudioOnly = !1),
            (this.mode = o.CallConstants.CALL_MODE.DEFAULT),
            (this.ShowEndCallButton = !0),
            (this.ShowMuteAudioButton = !0),
            (this.ShowPauseVideoButton = !0),
            (this.ShowScreenShareButton = !0),
            (this.ShowSwitchModeButton = !0),
            (this.StartAudioMuted = !1),
            (this.StartVideoMuted = !1),
            (this.localizedObject = {}),
            (this.analyticsSettings = {}),
            (this.ShowRecordingButton = !1),
            (this.StartRecordingOnCallStart = !1),
            (this.useLegacyUI = !1);
        }
        return (
          (e.prototype.setSessionID = function (e) {
            return (this.sessionID = e), this;
          }),
          (e.prototype.enableDefaultLayout = function (e) {
            return (this.defaultLayout = e), this;
          }),
          (e.prototype.setIsAudioOnlyCall = function (e) {
            return (this.isAudioOnly = e), this;
          }),
          (e.prototype.setRegion = function (e) {
            return (this.region = e), this;
          }),
          (e.prototype.setDomain = function (e) {
            return (this.domain = e), this;
          }),
          (e.prototype.setUser = function (e) {
            return (this.user = e), this;
          }),
          (e.prototype.setMode = function (e) {
            return (this.mode = e), this;
          }),
          (e.prototype.showEndCallButton = function (e) {
            return (this.ShowEndCallButton = e), this;
          }),
          (e.prototype.showMuteAudioButton = function (e) {
            return (this.ShowMuteAudioButton = e), this;
          }),
          (e.prototype.showPauseVideoButton = function (e) {
            return (this.ShowPauseVideoButton = e), this;
          }),
          (e.prototype.showScreenShareButton = function (e) {
            return (this.ShowScreenShareButton = e), this;
          }),
          (e.prototype.showModeButton = function (e) {
            return (this.ShowSwitchModeButton = e), this;
          }),
          (e.prototype.setLocalizedStringObject = function (e) {
            return (this.localizedObject = e), this;
          }),
          (e.prototype.setAnalyticsSettings = function (e) {
            return (this.analyticsSettings = e), this;
          }),
          (e.prototype.setAppId = function (e) {
            return (this.appId = e), this;
          }),
          (e.prototype.startWithAudioMuted = function (e) {
            return (this.StartAudioMuted = e), this;
          }),
          (e.prototype.startWithVideoMuted = function (e) {
            return (this.StartVideoMuted = e), this;
          }),
          (e.prototype.setCustomCSS = function (e) {
            return (this.customCSS = e), this;
          }),
          (e.prototype.showRecordingButton = function (e) {
            return (this.ShowRecordingButton = e), this;
          }),
          (e.prototype.startRecordingOnCallStart = function (e) {
            return (this.StartRecordingOnCallStart = e), this;
          }),
          (e.prototype.forceLegacyUI = function (e) {
            return (this.useLegacyUI = e), this;
          }),
          (e.prototype.build = function () {
            return new s(this);
          }),
          e
        );
      })();
      t.CallSettingsBuilder = r;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = (function () {
          function e(e) {
            (this.avatar = ""), o.isFalsy(e) || (this.uid = e.toString());
          }
          return (
            (e.prototype.setUID = function (e) {
              this.uid = e ? e.toString() : "";
            }),
            (e.prototype.getUID = function () {
              return this.uid.toString();
            }),
            (e.prototype.setName = function (e) {
              this.name = e ? e.toString() : "";
            }),
            (e.prototype.getName = function () {
              return this.name.toString();
            }),
            (e.prototype.setAvatar = function (e) {
              this.avatar = e ? e.toString() : "";
            }),
            (e.prototype.getAvatar = function () {
              return this.avatar.toString();
            }),
            (e.prototype.setJWT = function (e) {
              this.jwt = e ? e.toString() : "";
            }),
            (e.prototype.getJWT = function () {
              return this.jwt.toString();
            }),
            (e.prototype.setResource = function (e) {
              this.resource = e ? e.toString() : "";
            }),
            (e.prototype.getResource = function () {
              return this.resource.toString();
            }),
            e
          );
        })();
      t.RTCUser = s;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var c = n(3),
        o = n(1),
        s = n(29),
        r = n(34),
        u = n(0),
        i = n(35),
        a = n(20),
        E = n(21),
        S = n(53),
        l = n(54),
        p = n(55),
        C = n(56),
        T = n(57),
        g = n(58),
        d = n(59),
        _ = n(22),
        h = n(26),
        A = n(25),
        I = n(5),
        f = n(60),
        R = i.ListenerHandlers.getInstance(),
        O = (function () {
          function e() {}
          return (
            (e.getInstance = function () {
              try {
                return (
                  (null != this.wsConnectionHelper &&
                    null != this.wsConnectionHelper) ||
                    (this.wsConnectionHelper = new e()),
                  this.wsConnectionHelper
                );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: getInstance", e);
              }
            }),
            (e.prototype.WSLogin = function (t, n) {
              var o = this;
              try {
                f.getWSURL().then(
                  function (e) {
                    (o.connection = new WebSocket(e)),
                      n &&
                        o.connection &&
                        o.connection.readyState == I.READY_STATE.CONNECTING &&
                        n(o.connection.readyState),
                      (o.connection.onopen = function (e) {
                        o.onOpen(e, n), o.authenticateUser(t);
                      }),
                      (o.connection.onclose = function (e) {
                        o.onClose(e, n);
                      }),
                      (o.connection.onerror = function (e) {
                        o.onError(e, n);
                      }),
                      (o.connection.onmessage = function (e) {
                        o.onMessage(e);
                      });
                  },
                  function (e) {
                    u.Logger.error("WSConnectionHelper :: WSLogin", e);
                  }
                );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: WSLogin", e);
              }
            }),
            (e.prototype.onOpen = function (e, t) {
              try {
                t && t(this.connection.readyState);
              } catch (e) {
                u.Logger.error("WSConnectionHelper: onOpen", e);
              }
            }),
            (e.prototype.onClose = function (e, t) {
              try {
                t && t(this.connection.readyState);
              } catch (e) {
                u.Logger.error("WSConnectionHelper: onClose", e);
              }
            }),
            (e.prototype.onError = function (e, t) {
              try {
                t && t(this.connection.readyState);
              } catch (e) {
                u.Logger.error("WSConnectionHelper: onError", e);
              }
            }),
            (e.prototype.onMessage = function (e) {
              if (e.data && "string" == typeof e.data)
                try {
                  var t = this.getCometChatEventFromMessage(JSON.parse(e.data));
                  if (
                    t.getDeviceId() === c.CometChat.getSessionId() &&
                    t.getType() !== I.AUTH.TYPE
                  )
                    return;
                  switch (t.getType()) {
                    case I.TYPING_INDICATOR.TYPE:
                      this.publishTypingIndicator(t);
                      break;
                    case I.RECEIPTS.TYPE:
                      this.publishReceipts(t);
                      break;
                    case I.PRESENCE.TYPE:
                      this.publishPresence(t);
                      break;
                    case I.AUTH.TYPE:
                      this.authResponseReceived(t);
                      break;
                    case I.MESSAGE.TYPE:
                      this.publishMessages(t);
                      break;
                    case I.TRANSIENT_MESSAGE.TYPE:
                      this.publishTransientMessage(t);
                      break;
                    default:
                      u.Logger.log(
                        "WSHelper: onMessage :: unknown type",
                        e.data
                      );
                  }
                } catch (e) {
                  u.Logger.error("WSHelper: onMessage", e);
                }
              else u.Logger.log("WSHelper: onMessage :: object data", e.data);
            }),
            (e.prototype.authenticateUser = function (o) {
              var s = this;
              try {
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new S.CometChatAuthEvent(
                          c.CometChat.getAppId(),
                          "",
                          "",
                          e.getUid(),
                          c.CometChat.getSessionId()
                        );
                        if (
                          (t.setAuth(o),
                          t.setPresenceSubscription(
                            c.CometChat.appSettings.getSubscriptionType()
                          ),
                          t.setDeviceId(c.CometChat.getSessionId()),
                          f.checkConnection(s.connection))
                        ) {
                          var n = t.getAsString();
                          s.connection.send(n);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: startTypingIndicator", e);
              }
            }),
            (e.prototype.authResponseReceived = function (e) {
              try {
                "200" === e.getCode() &&
                  "OK" === e.getStatus() &&
                  this.startPingingWS();
              } catch (e) {
                u.Logger.error("WSConnectionHelper: authResponseReceived", e);
              }
            }),
            (e.prototype.WSLogout = function () {
              try {
                f.checkConnection(this.connection) &&
                  (this.pingInterval && clearInterval(this.pingInterval),
                  this.connection.close(I.LOGOUT_CODE, I.LOGOUT_REASON));
              } catch (e) {
                u.Logger.error("WSConnectionHelper: WSLogout", e);
              }
            }),
            (e.prototype.startPingingWS = function () {
              var t = this;
              try {
                this.pingInterval ||
                  (this.pingInterval = setInterval(function () {
                    if (f.checkConnection(t.connection)) {
                      var e = new p.CometChatPingEvent(
                        "",
                        "",
                        "",
                        "",
                        ""
                      ).getAsString();
                      t.connection.send(e);
                    }
                  }, 15e3));
              } catch (e) {
                u.Logger.error("WSConnectionHelper: startPingingWS", e);
              }
            }),
            (e.prototype.sendOnlineEvent = function () {
              var o = this;
              try {
                f.checkConnection(this.connection) &&
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = {
                          appId: c.CometChat.getAppId(),
                          deviceId: c.CometChat.getSessionId(),
                          type: I.PRESENCE.TYPE,
                          body: {
                            action: I.PRESENCE.ACTION.ONLINE,
                            timestamp: Math.floor(new Date().getTime() / 1e3),
                            user: e,
                          },
                        };
                        if (f.checkConnection(o.connection)) {
                          var n = f.stringifyMessage(t);
                          o.connection.send(n);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: sendOnlineEvent", e);
              }
            }),
            (e.prototype.sendOfflineEvent = function () {
              var o = this;
              try {
                f.checkConnection(this.connection) &&
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = {
                          appId: c.CometChat.getAppId(),
                          deviceId: c.CometChat.getSessionId(),
                          type: I.PRESENCE.TYPE,
                          body: {
                            action: I.PRESENCE.ACTION.OFFLINE,
                            timestamp: Math.floor(new Date().getTime() / 1e3),
                            user: e,
                          },
                        };
                        if (f.checkConnection(o.connection)) {
                          var n = f.stringifyMessage(t);
                          o.connection.send(n);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: sendOfflineEvent", e);
              }
            }),
            (e.prototype.startTypingIndicator = function (s, r, i) {
              var a = this;
              try {
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new A.TypingIndicator(s, r);
                        t.setSender(e), i && t.setMetadata(i);
                        var n = new d.CometChatTypingEvent(
                          c.CometChat.getAppId(),
                          s,
                          r,
                          e.getUid(),
                          c.CometChat.getSessionId()
                        );
                        if (
                          (n.setAction(I.TYPING_INDICATOR.ACTION.STARTED),
                          n.setTypingIndicator(t),
                          f.checkConnection(a.connection))
                        ) {
                          var o = n.getAsString();
                          a.connection.send(o);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: startTypingIndicator", e);
              }
            }),
            (e.prototype.pauseTypingIndicator = function (s, r, i) {
              var a = this;
              try {
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new A.TypingIndicator(s, r);
                        t.setSender(e), i && t.setMetadata(i);
                        var n = new d.CometChatTypingEvent(
                          c.CometChat.getAppId(),
                          s,
                          r,
                          e.getUid(),
                          c.CometChat.getSessionId()
                        );
                        if (
                          (n.setAction(I.TYPING_INDICATOR.ACTION.ENDED),
                          n.setTypingIndicator(t),
                          f.checkConnection(a.connection))
                        ) {
                          var o = n.getAsString();
                          a.connection.send(o);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: pauseTypingIndicator", e);
              }
            }),
            (e.prototype.markAsRead = function (s, r, i, a) {
              var E = this;
              try {
                c.CometChat.getMode();
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new T.CometChatReceiptEvent(
                          c.CometChat.getAppId(),
                          s,
                          r,
                          e.getUid(),
                          c.CometChat.getSessionId(),
                          a
                        );
                        t.setAction(I.RECEIPTS.ACTION.READ);
                        var n = new _.MessageReceipt();
                        if (
                          (n.setSender(e),
                          n.setReceiverType(r),
                          n.setReceiver(s),
                          n.setReceiptType(
                            I.RECEIPTS.RECEIPT_TYPE.READ_RECEIPT
                          ),
                          n.setMessageId(i),
                          n.setTimestamp(
                            Math.floor(new Date().getTime() / 1e3).toString()
                          ),
                          t.setMessageReceipt(n),
                          f.checkConnection(E.connection))
                        ) {
                          var o = t.getAsString();
                          E.connection.send(o);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: markAsRead", e);
              }
            }),
            (e.prototype.markAsDelivered = function (s, r, i, a) {
              var E = this;
              try {
                c.CometChat.getMode();
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new T.CometChatReceiptEvent(
                          c.CometChat.getAppId(),
                          s,
                          r,
                          e.getUid(),
                          c.CometChat.getSessionId(),
                          a
                        );
                        t.setAction(I.RECEIPTS.ACTION.DELIVERED);
                        var n = new _.MessageReceipt();
                        if (
                          (n.setSender(e),
                          n.setReceiverType(r),
                          n.setReceiver(s),
                          n.setReceiptType(
                            I.RECEIPTS.RECEIPT_TYPE.DELIVERY_RECEIPT
                          ),
                          n.setMessageId(i),
                          n.setTimestamp(
                            Math.floor(new Date().getTime() / 1e3).toString()
                          ),
                          t.setMessageReceipt(n),
                          f.checkConnection(E.connection))
                        ) {
                          var o = t.getAsString();
                          E.connection.send(o);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: markAsDelivered", e);
              }
            }),
            (e.prototype.sendTransientMessage = function (s, r, i) {
              var a = this;
              try {
                c.CometChat.didMessagesPollingStart() ||
                  c.CometChat.getLoggedinUser().then(
                    function (e) {
                      if (e) {
                        var t = new h.TransientMessage(s, r, i);
                        t.setSender(e);
                        var n = new g.CometChatTransientEvent(
                          c.CometChat.getAppId(),
                          s,
                          r,
                          e.getUid(),
                          c.CometChat.getSessionId()
                        );
                        if (
                          (n.setTransientMessage(t),
                          f.checkConnection(a.connection))
                        ) {
                          var o = n.getAsString();
                          a.connection.send(o);
                        }
                      } else u.Logger.log("no logged-in user", "null");
                    },
                    function (e) {
                      u.Logger.log("error in fetching logged-in user", e);
                    }
                  );
              } catch (e) {
                u.Logger.error("WSConnectionHelper: sendTransientMessage", e);
              }
            }),
            (e.prototype.publishTypingIndicator = function (e) {
              try {
                var t = e.getTypingIndicator();
                switch (e.getAction()) {
                  case I.TYPING_INDICATOR.ACTION.STARTED:
                    R.messageHandlers.map(function (e) {
                      e._eventListener.onTypingStarted &&
                        e._eventListener.onTypingStarted(t);
                    }),
                      r.TypingNotificationController.addIncomingTypingStarted(
                        t
                      );
                    break;
                  case I.TYPING_INDICATOR.ACTION.ENDED:
                    R.messageHandlers.map(function (e) {
                      e._eventListener.onTypingEnded &&
                        e._eventListener.onTypingEnded(t);
                    }),
                      r.TypingNotificationController.removeIncomingTypingStarted(
                        t
                      );
                }
              } catch (e) {
                u.Logger.error("WSHelper: publishTypingIndicator", e);
              }
            }),
            (e.prototype.publishReceipts = function (e) {
              try {
                var t = e.getMessageReceipt();
                switch (e.getAction()) {
                  case I.RECEIPTS.ACTION.DELIVERED:
                    R.messageHandlers.map(function (e) {
                      e._eventListener.onMessagesDelivered &&
                        e._eventListener.onMessagesDelivered(t);
                    });
                    break;
                  case I.RECEIPTS.ACTION.READ:
                    R.messageHandlers.map(function (e) {
                      e._eventListener.onMessagesRead &&
                        e._eventListener.onMessagesRead(t);
                    });
                }
              } catch (e) {
                u.Logger.error("WSHelper: publishReceipts", e);
              }
            }),
            (e.prototype.publishPresence = function (e) {
              try {
                var t = e.getUser();
                switch ((t.setLastActiveAt(e.getTimestamp()), e.getAction())) {
                  case I.PRESENCE.ACTION.ONLINE:
                  case I.PRESENCE.ACTION.AVAILABLE:
                    t.setStatus(o.PresenceConstatnts.STATUS.ONLINE),
                      R.userHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onUserOnline) ||
                          e._eventListener.onUserOnline(t);
                      });
                    break;
                  case I.PRESENCE.ACTION.OFFLINE:
                    t.setStatus(o.PresenceConstatnts.STATUS.OFFLINE),
                      R.userHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onUserOffline) ||
                          e._eventListener.onUserOffline(t);
                      });
                }
              } catch (e) {
                u.Logger.error("WSHelper: publishPresence", e);
              }
            }),
            (e.prototype.publishMessages = function (e) {
              try {
                var t = e.getMessage();
                t instanceof E.Call
                  ? this.publishCallMessage(t)
                  : t instanceof a.Action
                  ? this.publishActionMessage(t)
                  : this.publishMessage(t);
              } catch (e) {
                u.Logger.error("WSHelper: publishMessages", e);
              }
            }),
            (e.prototype.publishMessage = function (n) {
              try {
                R.messageHandlers.map(function (e) {
                  if (e._eventListener) {
                    var t = n;
                    switch (t.getType()) {
                      case o.MessageConstatnts.TYPE.TEXT:
                        u.isFalsy(e._eventListener.onTextMessageReceived) ||
                          e._eventListener.onTextMessageReceived(n);
                        break;
                      case o.MessageConstatnts.TYPE.CUSTOM:
                        u.isFalsy(e._eventListener.onCustomMessageReceived) ||
                          e._eventListener.onCustomMessageReceived(n);
                        break;
                      default:
                        t.getCategory() == o.MessageCategory.CUSTOM
                          ? u.isFalsy(
                              e._eventListener.onCustomMessageReceived
                            ) || e._eventListener.onCustomMessageReceived(n)
                          : u.isFalsy(
                              e._eventListener.onMediaMessageReceived
                            ) || e._eventListener.onMediaMessageReceived(n);
                    }
                  }
                });
              } catch (e) {
                u.Logger.error("WSHelper: publishMessage", e);
              }
            }),
            (e.prototype.publishCallMessage = function (t) {
              try {
                var n = s.CallController.getInstance().getActiveCall();
                switch (t.getStatus()) {
                  case o.CallConstants.CALL_STATUS.INITIATED:
                    t.getReceiverType() == o.CallConstants.RECEIVER_TYPE_GROUP
                      ? R.callHandlers.map(function (e) {
                          t.getCallInitiator().getUid().toLocaleLowerCase() !=
                            c.CometChat.user.getUid().toLocaleLowerCase() &&
                            (u.isFalsy(
                              e._eventListener.onIncomingCallReceived
                            ) ||
                              e._eventListener.onIncomingCallReceived(t));
                        })
                      : R.callHandlers.map(function (e) {
                          u.isFalsy(e._eventListener.onIncomingCallReceived) ||
                            e._eventListener.onIncomingCallReceived(t);
                        });
                    break;
                  case o.CallConstants.CALL_STATUS.ONGOING:
                    t.getReceiverType() == o.CallConstants.RECEIVER_TYPE_GROUP
                      ? t.getCallInitiator().getUid().toLocaleLowerCase() ==
                          c.CometChat.user.getUid().toLocaleLowerCase() &&
                        R.callHandlers.map(function (e) {
                          u.isFalsy(e._eventListener.onOutgoingCallAccepted) ||
                            e._eventListener.onOutgoingCallAccepted(t);
                        })
                      : R.callHandlers.map(function (e) {
                          u.isFalsy(e._eventListener.onOutgoingCallAccepted) ||
                            e._eventListener.onOutgoingCallAccepted(t);
                        });
                    break;
                  case o.CallConstants.CALL_STATUS.UNANSWERED:
                    if (
                      t.getCallInitiator().getUid().toLocaleLowerCase() ==
                      c.CometChat.user.getUid().toLocaleLowerCase()
                    )
                      R.callHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onOutgoingCallRejected) ||
                          e._eventListener.onOutgoingCallRejected(t);
                      });
                    else {
                      if (null !== n && n.getSessionId() === t.getSessionId())
                        try {
                          s.CallController.getInstance().endCall();
                        } catch (e) {
                          u.Logger.error("CallError", e);
                        }
                      R.callHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onIncomingCallCancelled) ||
                          e._eventListener.onIncomingCallCancelled(t);
                      });
                    }
                    break;
                  case o.CallConstants.CALL_STATUS.REJECTED:
                    if (
                      t.getReceiverType() == o.CallConstants.RECEIVER_TYPE_GROUP
                    )
                      t.getCallInitiator().getUid().toLocaleLowerCase() ==
                        c.CometChat.user.getUid().toLocaleLowerCase() &&
                        R.callHandlers.map(function (e) {
                          u.isFalsy(e._eventListener.onOutgoingCallRejected) ||
                            e._eventListener.onOutgoingCallRejected(t);
                        });
                    else {
                      if (null !== n && n.getSessionId() === t.getSessionId())
                        try {
                          s.CallController.getInstance().endCall();
                        } catch (e) {
                          u.Logger.error("CallError", e);
                        }
                      R.callHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onOutgoingCallRejected) ||
                          e._eventListener.onOutgoingCallRejected(t);
                      });
                    }
                    break;
                  case o.CallConstants.CALL_STATUS.BUSY:
                    t.getReceiverType() == o.CallConstants.RECEIVER_TYPE_GROUP
                      ? t.getCallInitiator().getUid().toLocaleLowerCase() ==
                          c.CometChat.user.getUid().toLocaleLowerCase() &&
                        R.callHandlers.map(function (e) {
                          u.isFalsy(e._eventListener.onOutgoingCallRejected) ||
                            e._eventListener.onOutgoingCallRejected(t);
                        })
                      : R.callHandlers.map(function (e) {
                          if (
                            null !== n &&
                            n.getSessionId() === t.getSessionId()
                          )
                            try {
                              s.CallController.getInstance().endCall();
                            } catch (e) {
                              u.Logger.error("CallError", e);
                            }
                          u.isFalsy(e._eventListener.onOutgoingCallRejected) ||
                            e._eventListener.onOutgoingCallRejected(t);
                        });
                    break;
                  case o.CallConstants.CALL_STATUS.CANCELLED:
                    if (
                      (R.callHandlers.map(function (e) {
                        u.isFalsy(e._eventListener.onIncomingCallCancelled) ||
                          e._eventListener.onIncomingCallCancelled(t);
                      }),
                      null !== n && n.getSessionId() === t.getSessionId())
                    )
                      try {
                        s.CallController.getInstance().endCall();
                      } catch (e) {
                        u.Logger.error("CallError", e);
                      }
                    break;
                  case o.CallConstants.CALL_STATUS.ENDED:
                    if (
                      (s.CallController.getInstance().getCallListner() &&
                        s.CallController.getInstance()
                          .getCallListner()
                          ._eventListener.onCallEnded(t),
                      null !== n && n.getSessionId() === t.getSessionId())
                    )
                      try {
                        s.CallController.getInstance().endCall();
                      } catch (e) {
                        u.Logger.error("CallError", e);
                      }
                    break;
                  default:
                    u.Logger.log(
                      "WSHelper: publishCallMessage :: unknown status",
                      t
                    );
                }
              } catch (e) {
                u.Logger.error("WSHelper: publishCallMessage", e);
              }
            }),
            (e.prototype.publishActionMessage = function (t) {
              try {
                switch (t.getAction()) {
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_JOINED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserJoined) ||
                        e._eventListener.onUserJoined(
                          t,
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_INVITED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserJoined) ||
                        e._eventListener.onUserJoined(
                          t,
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_LEFT:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserLeft) ||
                        e._eventListener.onUserLeft(
                          t,
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_BANNED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserBanned) ||
                        e._eventListener.onUserBanned(
                          t,
                          t.getActionOn(),
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_KICKED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserKicked) ||
                        e._eventListener.onUserKicked(
                          t,
                          t.getActionOn(),
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_UNBANNED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onUserUnbanned) ||
                        e._eventListener.onUserUnbanned(
                          t,
                          t.getActionOn(),
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_SCOPE_CHANGED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onMemberScopeChanged) ||
                        e._eventListener.onMemberScopeChanged(
                          t,
                          t.getActionOn(),
                          t.getNewScope(),
                          t.getOldScope(),
                          t.getActionFor()
                        );
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MESSAGE_EDITED:
                    R.messageHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onMessageEdited) ||
                        e._eventListener.onMessageEdited(t.getActionOn());
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MESSAGE_DELETED:
                    R.messageHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onMessageDeleted) ||
                        e._eventListener.onMessageDeleted(t.getActionOn());
                    });
                    break;
                  case o.ActionConstatnts.ACTION_TYPE.TYPE_MEMBER_ADDED:
                    R.groupHandlers.map(function (e) {
                      u.isFalsy(e._eventListener.onMemberAddedToGroup) ||
                        e._eventListener.onMemberAddedToGroup(
                          t,
                          t.getActionOn(),
                          t.getActionBy(),
                          t.getActionFor()
                        );
                    });
                    break;
                  default:
                    u.Logger.log(
                      "WSHelper: publishActionMessage :: unknown action",
                      t
                    );
                }
              } catch (e) {
                u.Logger.error("WSHelper: publishActionMessage", e);
              }
            }),
            (e.prototype.publishTransientMessage = function (e) {
              try {
                var t = e.getTransientMessage();
                R.messageHandlers.map(function (e) {
                  e._eventListener.onTransientMessageReceived &&
                    e._eventListener.onTransientMessageReceived(t);
                });
              } catch (e) {
                u.Logger.error("WSHelper: publishTransientMessage", e);
              }
            }),
            (e.prototype.getCometChatEventFromMessage = function (e) {
              try {
                if (e.hasOwnProperty(I.KEYS.TYPE))
                  switch (e.type) {
                    case I.TYPING_INDICATOR.TYPE:
                      return d.CometChatTypingEvent.getTypingEventFromJSON(e);
                    case I.RECEIPTS.TYPE:
                      return T.CometChatReceiptEvent.getReceiptEventFromJSON(e);
                    case I.PRESENCE.TYPE:
                      return C.CometChatPresenceEvent.getPresenceEventFromJSON(
                        e
                      );
                    case I.MESSAGE.TYPE:
                      return l.CometChatMessageEvent.getMessageEventFromJSON(e);
                    case I.AUTH.TYPE:
                      return S.CometChatAuthEvent.getAuthEventFromJSON(e);
                    case I.TRANSIENT_MESSAGE.TYPE:
                      return g.CometChatTransientEvent.getTransientEventFromJSON(
                        e
                      );
                  }
              } catch (e) {
                u.Logger.error(
                  "WSConnectionHelper: getCometChatEventFromMessage",
                  e
                );
              }
            }),
            (e.pingMessage = { action: "ping" }),
            (e.wsConnectionHelper = new e()),
            e
          );
        })();
      t.WSConnectionHelper = O;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var S = n(5),
        r = (function (i) {
          function u(e, t, n, o, s) {
            var r = i.call(this, e, t, n, o, s) || this;
            return r.setType(S.AUTH.TYPE), r;
          }
          return (
            s(u, i),
            (u.prototype.getStatus = function () {
              return this.status;
            }),
            (u.prototype.setStatus = function (e) {
              this.status = e;
            }),
            (u.prototype.getCode = function () {
              return this.code;
            }),
            (u.prototype.setCode = function (e) {
              this.code = e;
            }),
            (u.prototype.setAuth = function (e) {
              this.auth = e;
            }),
            (u.prototype.getAuth = function () {
              return this.auth;
            }),
            (u.prototype.setPresenceSubscription = function (e) {
              this.presenceSubscription = e;
            }),
            (u.prototype.getPresenceSubscription = function () {
              return this.presenceSubscription;
            }),
            (u.prototype.setDeviceId = function (e) {
              this.deviceId = e;
            }),
            (u.prototype.getdeviceId = function () {
              return this.deviceId;
            }),
            (u.prototype.getAsString = function () {
              return JSON.stringify(this.getAsJSONObject());
            }),
            (u.prototype.getAsJSONObject = function () {
              var e = this.getCometChatEventJSON(),
                t = {};
              return (
                (t[S.KEYS.AUTH] = this.auth),
                (t[S.KEYS.DEVICE_ID] = this.deviceId),
                (t[S.KEYS.PRESENCE_SUBSCRIPTION] = this.presenceSubscription),
                (e[S.KEYS.BODY] = t),
                e
              );
            }),
            (u.getAuthEventFromJSON = function (e) {
              var t = e[S.KEYS.APP_ID],
                n = e[S.KEYS.RECEIVER],
                o = e[S.KEYS.RECEIVER_TYPE],
                s = e[S.KEYS.DEVICE_ID],
                r = e[S.KEYS.SENDER],
                i = e[S.KEYS.BODY],
                a = i[S.KEYS.STATUS],
                E = i[S.KEYS.CODE],
                c = new u(t, n, o, r, s);
              return c.setStatus(a), c.setCode(E), c;
            }),
            u
          );
        })(n(9).CometChatEvent);
      t.CometChatAuthEvent = r;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var u = n(10),
        S = n(5),
        r = (function (i) {
          function c(e, t, n, o, s) {
            var r = i.call(this, e, t, n, o, s) || this;
            return r.setType(S.MESSAGE.TYPE), r;
          }
          return (
            s(c, i),
            (c.prototype.getMessage = function () {
              return this.message;
            }),
            (c.prototype.setMessage = function (e) {
              this.message = e;
            }),
            (c.getMessageEventFromJSON = function (e) {
              var t = e[S.KEYS.APP_ID],
                n = e[S.KEYS.RECEIVER],
                o = e[S.KEYS.RECEIVER_TYPE],
                s = e[S.KEYS.DEVICE_ID],
                r = e[S.KEYS.SENDER],
                i = e[S.KEYS.BODY],
                a = new c(t, n, o, r, s),
                E = u.MessageController.trasformJSONMessge(i);
              return a.setMessage(E), a;
            }),
            c
          );
        })(n(9).CometChatEvent);
      t.CometChatMessageEvent = r;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var i = n(5),
        r = (function (r) {
          function e(e, t, n, o, s) {
            return r.call(this, e, t, n, o, s) || this;
          }
          return (
            s(e, r),
            (e.prototype.getAsString = function () {
              return JSON.stringify(this.getAsJSONObject());
            }),
            (e.prototype.getAsJSONObject = function () {
              var e = {};
              return (e[i.KEYS.ACTION] = i.KEYS.PING), e;
            }),
            e
          );
        })(n(9).CometChatEvent);
      t.CometChatPingEvent = r;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var u = n(5),
        r = n(9),
        S = n(4),
        i = (function (i) {
          function c(e, t, n, o, s) {
            var r = i.call(this, e, t, n, o, s) || this;
            return r.setType(u.PRESENCE.TYPE), r;
          }
          return (
            s(c, i),
            (c.prototype.getAction = function () {
              return this.action;
            }),
            (c.prototype.setAction = function (e) {
              this.action = e;
            }),
            (c.prototype.getUser = function () {
              return this.user;
            }),
            (c.prototype.setUser = function (e) {
              this.user = e;
            }),
            (c.prototype.getTimestamp = function () {
              return this.timestamp;
            }),
            (c.prototype.setTimestamp = function (e) {
              this.timestamp = e;
            }),
            (c.getPresenceEventFromJSON = function (e) {
              var t = e[u.KEYS.APP_ID],
                n = e[u.KEYS.DEVICE_ID],
                o = e[u.KEYS.SENDER],
                s = e[u.KEYS.BODY],
                r = s[u.KEYS.ACTION],
                i = s[u.KEYS.USER],
                a = s[u.KEYS.TIMESTAMP],
                E = new c(t, "", "", o, n);
              return (
                E.setAction(r), E.setUser(new S.User(i)), E.setTimestamp(a), E
              );
            }),
            c
          );
        })(r.CometChatEvent);
      t.CometChatPresenceEvent = i;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var C = n(5),
        r = n(9),
        T = n(22),
        g = n(4),
        i = (function (a) {
          function p(e, t, n, o, s, r) {
            var i = a.call(this, e, t, n, o, s, r) || this;
            return i.setType(C.RECEIPTS.TYPE), i;
          }
          return (
            s(p, a),
            (p.prototype.getAction = function () {
              return this.action;
            }),
            (p.prototype.setAction = function (e) {
              this.action = e;
            }),
            (p.prototype.getMessageReceipt = function () {
              return this.receipt;
            }),
            (p.prototype.setMessageReceipt = function (e) {
              this.receipt = e;
            }),
            (p.prototype.getAsString = function () {
              return JSON.stringify(this.getAsJSONObject());
            }),
            (p.prototype.getAsJSONObject = function () {
              var e = this.getCometChatEventJSON(),
                t = {};
              return (
                (t[C.KEYS.ACTION] = this.action),
                (t[C.KEYS.MESSAGE_ID] = this.receipt.getMessageId()),
                (t[C.KEYS.USER] = this.receipt.getSender()),
                (e[C.KEYS.BODY] = t),
                e
              );
            }),
            (p.getReceiptEventFromJSON = function (e) {
              var t = e[C.KEYS.APP_ID],
                n = e[C.KEYS.RECEIVER],
                o = e[C.KEYS.RECEIVER_TYPE],
                s = e[C.KEYS.DEVICE_ID],
                r = e[C.KEYS.SENDER],
                i = e[C.KEYS.BODY],
                a = i[C.KEYS.ACTION],
                E = i[C.KEYS.USER],
                c = i[C.KEYS.MESSAGE_ID],
                u = i[C.KEYS.TIMESTAMP],
                S = new p(t, n, o, r, s);
              S.setAction(a);
              var l = new T.MessageReceipt();
              return (
                l.setSender(new g.User(E)),
                l.setReceiverType(o),
                l.setReceiver(n),
                a === C.RECEIPTS.ACTION.DELIVERED &&
                  (l.setReceiptType(C.RECEIPTS.RECEIPT_TYPE.DELIVERY_RECEIPT),
                  l.setDeliveredAt(u)),
                a === C.RECEIPTS.ACTION.READ &&
                  (l.setReceiptType(C.RECEIPTS.RECEIPT_TYPE.READ_RECEIPT),
                  l.setReadAt(u)),
                l.setMessageId(c.toString()),
                l.setTimestamp(u),
                S.setMessageReceipt(l),
                S
              );
            }),
            p
          );
        })(r.CometChatEvent);
      t.CometChatReceiptEvent = i;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var l = n(5),
        r = n(9),
        p = n(26),
        C = n(4),
        i = (function (i) {
          function S(e, t, n, o, s) {
            var r = i.call(this, e, t, n, o, s) || this;
            return r.setType(l.TRANSIENT_MESSAGE.TYPE), r;
          }
          return (
            s(S, i),
            (S.prototype.getTransientMessage = function () {
              return this.transientMessage;
            }),
            (S.prototype.setTransientMessage = function (e) {
              this.transientMessage = e;
            }),
            (S.prototype.getAsString = function () {
              return JSON.stringify(this.getAsJSONObject());
            }),
            (S.prototype.getAsJSONObject = function () {
              var e = this.getCometChatEventJSON(),
                t = {};
              return (
                (t[l.KEYS.USER] = this.transientMessage.getSender()),
                this.transientMessage.getData() &&
                  (t[l.KEYS.DATA] = this.transientMessage.getData()),
                (e[l.KEYS.BODY] = t),
                e
              );
            }),
            (S.getTransientEventFromJSON = function (e) {
              var t = e[l.KEYS.APP_ID],
                n = e[l.KEYS.RECEIVER],
                o = e[l.KEYS.RECEIVER_TYPE],
                s = e[l.KEYS.DEVICE_ID],
                r = e[l.KEYS.SENDER],
                i = e[l.KEYS.BODY],
                a = i[l.KEYS.USER],
                E = i[l.KEYS.DATA],
                c = new S(t, n, o, r, s),
                u = new p.TransientMessage(n, o, E);
              return (
                u.setSender(new C.User(a)),
                i.hasOwnProperty(l.KEYS.DATA) && i[l.KEYS.DATA] && u.setData(E),
                c.setTransientMessage(u),
                c
              );
            }),
            S
          );
        })(r.CometChatEvent);
      t.CometChatTransientEvent = i;
    },
    function (e, t, n) {
      "use strict";
      var o,
        s =
          (this && this.__extends) ||
          ((o = function (e, t) {
            return (o =
              Object.setPrototypeOf ||
              ({ __proto__: [] } instanceof Array &&
                function (e, t) {
                  e.__proto__ = t;
                }) ||
              function (e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
              })(e, t);
          }),
          function (e, t) {
            function n() {
              this.constructor = e;
            }
            o(e, t),
              (e.prototype =
                null === t
                  ? Object.create(t)
                  : ((n.prototype = t.prototype), new n()));
          });
      t.__esModule = !0;
      var p = n(5),
        r = n(9),
        C = n(25),
        T = n(4),
        i = (function (i) {
          function l(e, t, n, o, s) {
            var r = i.call(this, e, t, n, o, s) || this;
            return r.setType(p.TYPING_INDICATOR.TYPE), r;
          }
          return (
            s(l, i),
            (l.prototype.getAction = function () {
              return this.action;
            }),
            (l.prototype.setAction = function (e) {
              this.action = e;
            }),
            (l.prototype.getTypingIndicator = function () {
              return this.typingIndicator;
            }),
            (l.prototype.setTypingIndicator = function (e) {
              this.typingIndicator = e;
            }),
            (l.prototype.getAsString = function () {
              return JSON.stringify(this.getAsJSONObject());
            }),
            (l.prototype.getAsJSONObject = function () {
              var e = this.getCometChatEventJSON(),
                t = {};
              return (
                (t[p.KEYS.ACTION] = this.action),
                (t[p.KEYS.USER] = this.typingIndicator.getSender()),
                this.typingIndicator.getMetadata() &&
                  (t[p.KEYS.METADATA] = this.typingIndicator.getMetadata()),
                (e[p.KEYS.BODY] = t),
                e
              );
            }),
            (l.getTypingEventFromJSON = function (e) {
              var t = e[p.KEYS.APP_ID],
                n = e[p.KEYS.RECEIVER],
                o = e[p.KEYS.RECEIVER_TYPE],
                s = e[p.KEYS.DEVICE_ID],
                r = e[p.KEYS.SENDER],
                i = e[p.KEYS.BODY],
                a = i[p.KEYS.ACTION],
                E = i[p.KEYS.USER],
                c = i[p.KEYS.METADATA],
                u = new l(t, n, o, r, s);
              u.setAction(a);
              var S = new C.TypingIndicator(n, o);
              return (
                S.setSender(new T.User(E)),
                i.hasOwnProperty(p.KEYS.METADATA) &&
                  i[p.KEYS.METADATA] &&
                  S.setMetadata(c),
                u.setTypingIndicator(S),
                u
              );
            }),
            l
          );
        })(r.CometChatEvent);
      t.CometChatTypingEvent = i;
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(1),
        s = n(0),
        r = n(5);
      (t.checkConnection = function (e) {
        return e && e.readyState === r.READY_STATE.OPEN;
      }),
        (t.stringifyMessage = function (e) {
          return JSON.stringify(e);
        }),
        (t.getWSURL = function () {
          return new Promise(function (n, e) {
            s.getAppSettings().then(
              function (e) {
                var t =
                  r.WS.protocol +
                  s.getChatHost(e) +
                  ":" +
                  e[o.APP_SETTINGS.KEYS.CHAT_WSS_PORT];
                n(t);
              },
              function (e) {
                s.Logger.error("WSHelper :: getWSURL", e);
              }
            );
          });
        });
    },
    function (e, t, n) {
      "use strict";
      t.__esModule = !0;
      var o = n(0),
        s = (function () {
          function e(e) {
            (this.id = ""),
              (this.name = ""),
              e &&
                (o.isFalsy(e.id) || (this.id = e.id),
                o.isFalsy(e.name) || (this.name = e.name));
          }
          return (
            (e.prototype.getId = function () {
              return this.id;
            }),
            (e.prototype.getName = function () {
              return this.name;
            }),
            e
          );
        })();
      t.CCExtension = s;
    },
    function (e, t, n) {
      var i = n(63),
        a = n(64);
      e.exports = function (e, t, n) {
        var o = (t && n) || 0;
        "string" == typeof e &&
          ((t = "binary" === e ? new Array(16) : null), (e = null));
        var s = (e = e || {}).random || (e.rng || i)();
        if (((s[6] = (15 & s[6]) | 64), (s[8] = (63 & s[8]) | 128), t))
          for (var r = 0; r < 16; ++r) t[o + r] = s[r];
        return t || a(s);
      };
    },
    function (e, t) {
      var n =
        ("undefined" != typeof crypto &&
          crypto.getRandomValues &&
          crypto.getRandomValues.bind(crypto)) ||
        ("undefined" != typeof msCrypto &&
          "function" == typeof window.msCrypto.getRandomValues &&
          msCrypto.getRandomValues.bind(msCrypto));
      if (n) {
        var o = new Uint8Array(16);
        e.exports = function () {
          return n(o), o;
        };
      } else {
        var s = new Array(16);
        e.exports = function () {
          for (var e, t = 0; t < 16; t++)
            0 == (3 & t) && (e = 4294967296 * Math.random()),
              (s[t] = (e >>> ((3 & t) << 3)) & 255);
          return s;
        };
      }
    },
    function (e, t) {
      for (var s = [], n = 0; n < 256; ++n)
        s[n] = (n + 256).toString(16).substr(1);
      e.exports = function (e, t) {
        var n = t || 0,
          o = s;
        return [
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
          "-",
          o[e[n++]],
          o[e[n++]],
          "-",
          o[e[n++]],
          o[e[n++]],
          "-",
          o[e[n++]],
          o[e[n++]],
          "-",
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
          o[e[n++]],
        ].join("");
      };
    },
  ]);
});
