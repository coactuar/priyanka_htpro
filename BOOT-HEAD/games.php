<?php
require_once "logincheck.php";
$curr_room = 'games';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bg.jpg" usemap="#image-map">
            <map name="image-map">
                <area alt="ExibisionHall" href="exhibitionhalls.php" coords="1225,1292,1243,1778,1499,1791,1486,1288" shape="poly">
                <!-- <area alt="Connect" title="Connect" href="#" coords="3500,1284,3478,1791,3734,1796,3747,1292" shape="poly"> -->
                <area alt="Photo Booth" title="Photo Booth" href="photobooth.php" coords="2520,1472,2184,1233" shape="rect">
            </map>
         
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>

<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>