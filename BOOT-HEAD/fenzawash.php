<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = 'd0fd4260740fe814322242b6ce5657185c909f570c41fe2ef69bb481ff66bc71';
require_once "exhibcheck.php";

$curr_room = 'fenzawash';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="fenzawash">
            <img src="https://origyn.s3.ap-south-1.amazonaws.com/fenzawash.jpg">
            <div id="back-button">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <a href="#" id="literature"></a>
            <a href="#" id="contact"></a>
            <a href="assets/resources/fenzawash_1.jpg" id="poster1" class="view"></a>
            <a href="assets/resources/fenzawash_2.jpg" id="poster2" class="view"></a>
            <a href="assets/resources/fenzawash_3.jpg" id="poster3" class="view"></a>
            <a href="https://player.vimeo.com/video/481078696" class="viewvideo" id="video1"></a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<div class="modal fade" id="literatureList" tabindex="-1" role="dialog" aria-labelledby="literatureListTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="literatureListLongTitle">Scientific Literatures</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content scroll">
                    <ul class="popuplist">
                        <li><a href="assets/resources/fenzawash_lbl1.pdf" class="viewpoppdf resdl" data-docid="ccb89d8f57b33374bc909df373a6b3cce0422318c2ac8ff032305d1b8029eb0e">FENZA WASH LBL 1</a></li>
                        <li><a href="assets/resources/fenzawash_lbl2.pdf" class="viewpoppdf resdl" data-docid="4e75116eef0e6727efb46f694da22b10c747214f17efa5f4e341f6af7cdeef72">FENZA WASH LBL 2</a></li>
                        <li><a href="assets/resources/fenzawash_lbl3.pdf" class="viewpoppdf resdl" data-docid="ae4f0ee3209a9871d3fbc380639a4fbcc913099d59756f604d909cab30b358f5">FENZA WASH LBL 3</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="contactUs" tabindex="-1" role="dialog" aria-labelledby="contactUsTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contactUsLongTitle">Contact Us</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content scroll text-center">
                    For any queries, please write to us at <a href="mailto:contactus@integracehealth.com">contactus@integracehealth.com</a>
                </div>
            </div>

        </div>
    </div>
</div>
<?php require_once "scripts.php" ?>
<script>
    $(function() {
        $(document).on('click', '#literature', function() {
            $('#literatureList').modal('show');
        });
        $(document).on('click', '#contact', function() {
            $('#contactUs').modal('show');
        });

    });
</script>
<?php require_once "exhib-script.php" ?>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>