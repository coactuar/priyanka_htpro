<?php

	
	if(!isset($_SESSION["user_empid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_empid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where emp_code='$empid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fliud bg-white navbar navbar-default">
<div class="top-banner offset-md-7 " >
    <img src="img/top-banner1.jpg" class="img-fluid float-right w-50" alt=""/> 
</div></div>

<div class="content-area">
  <div class="main-area">
  <div class="row mt-1 mb-1 ">
  <!--
        <div class="col-12 text-center">
           <img src="img/Pahal Logo.png" width=10%  alt=""/> 
        </div>
		-->
    </div>
    <div class="row mt-1 mb-1 user-info">
        <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn-logout">Logout</a>
        </div>
    </div>
    
    <div class="row mt-1 mb-5">
        <div class="col-12 col-md-5 col-lg-7">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe src="https://vimeo.com/event/830944/embed" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div>    
        </div>
        <div class="col-12 col-md-6 col-lg-4">
            <div class="question-box">
              <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10 offset-1">
                            <h6>Ask your Question:</h6>
                            <div id="message" style="display:none"></div>
                            <div class="form-group">
                               <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="6"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-10 offset-1">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_empid" name="user_empid" value="<?php echo $_SESSION['user_empid']; ?>">
                            <input type="submit" id="submitQues" class="btn-submit" value="Submit Question" alt="Submit">
                            
                        </div>
                      </div>  
                </form>
            </div>    
        </div>
        
    </div>
    
    
  </div>  
  
  
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                $('#submitQues').attr('disabled', true);
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                $('#submitQues').attr('disabled', false);
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>