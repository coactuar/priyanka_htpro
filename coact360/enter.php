<?php
require_once "logincheck.php";
$curr_room = 'entrance';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<section class="videotoplay" id="gotoentry" style="display:block;">
    <video class="videoplayer" id="gotoentryvideo" preload="auto">
        <source src="entry.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipEntry()">SKIP</a></div>
</section>
<?php require_once "scripts.php" ?>

<script>
    var entryVideo = document.getElementById("gotoentryvideo");
    entryVideo.addEventListener('ended', entryEnd, false);

    $(function() {
        gotoEntry();
    });

    function gotoEntry() {
        entryVideo.currentTime = 0;
        entryVideo.play();
    }

    function entryEnd(e) {
        location.href = "lobby.php";
    }

    function skipEntry() {
        location.href = "lobby.php";
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>