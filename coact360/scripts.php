<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="assets/js/mag-popup.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>

<script>
    var getteamchat, getchat;

    $(function() {
        $('body').addClass('loaded');
        updateEvent('<?= $userid ?>', '<?= $curr_room ?>');

        $('.view').magnificPopup({
            disableOn: 700,
            type: 'image',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });

        $('.viewvideo').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            closeBtnInside: true,

            fixedContentPos: false
        });
        $('.viewpopvideo').magnificPopup({
            type: 'iframe',
            closeBtnInside: true,
            callbacks: {
                open: function() {
                    $('#videoList').modal('hide');

                },
                close: function() {
                    $('#videoList').modal('show');
                }
            },
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                    '<div class="mfp-title">Some caption</div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'

        });
        $('.showpdf').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });
        $('.viewpoppdf').magnificPopup({
            type: 'iframe',
            callbacks: {
                open: function() {
                    $('#literatureList').modal('hide');

                },
                close: function() {
                    $('#literatureList').modal('show');
                }
            },
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                    '<div class="mfp-title">Some caption</div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'

        });

        $(document).on('click', '.show_leaderboard', function() {
            $('#leaderboard').modal('show');
        });

        $('#leaderboard').on('show.bs.modal', function(e) {
            getMyRank('<?= $userid ?>');
            getLeaderboard();
        });

        $(document).on('click', '#lRanks', function() {
            $('#pointsSystem').removeClass('active');
            $('#lRanks').addClass('active');
            $('#ranks').css('display', 'block');
            $('#points').css('display', 'none');
            getMyRank('<?= $userid ?>');
            getLeaderboard();
        });

        $(document).on('click', '#pointsSystem', function() {
            $('#lRanks').removeClass('active');
            $('#pointsSystem').addClass('active');
            $('#points').css('display', 'block');
            $('#ranks').css('display', 'none');
        });


    });

    function updateEvent(userid, loc) {

        $.ajax({
                url: 'control/update.php',
                data: {
                    action: 'updateevent',
                    userId: userid,
                    room: loc
                },
                type: 'post',
                success: function(output) {
                    //console.log(output);
                    /* if (output == '0') {
                        location.href = 'login.php';
                    } */
                }
            })
            .always(function(data) {
                setTimeout(function() {
                    updateEvent('<?= $userid ?>', '<?= $curr_room ?>');
                }, 30000);
            });
    }

    function getLeaderboard() {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'getleaderboard',
                user: '<?= $userid ?>'
            },
            type: 'post',
            success: function(output) {
                $('#conf-ranks').html(output);
            }
        });
    }

    function getMyRank(userId) {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'getpoints',
                user: userId
            },
            type: 'post',
            success: function(output) {
                console.log(output);
                $('#my-rank').html(output);
            }
        });
    }
</script>
<!-- Panellum Functions -->
<script>
    // Hot spot creation function
    function hotspot(hotSpotDiv, args) {
        hotSpotDiv.classList.add('tool-tip');
        var span = document.createElement('span');
        span.innerHTML = args;
        hotSpotDiv.appendChild(span);
    }

    function hotspot_white(hotSpotDiv, args) {
        hotSpotDiv.classList.add('tool-tip-white');
        var span = document.createElement('span');
        span.innerHTML = args;
        hotSpotDiv.appendChild(span);
    }

    function hotspot_green(hotSpotDiv, args) {
        hotSpotDiv.classList.add('tool-tip-green');
        var span = document.createElement('span');
        span.innerHTML = args;
        hotSpotDiv.appendChild(span);
        var div = document.createElement('div');
        div.classList.add('dot')
        hotSpotDiv.appendChild(div);

    }

    function showContent(hotSpotDiv, args) {
        //alert(args);
        $.magnificPopup.open({
            items: {
                src: args
            },
            type: 'iframe',
            closeBtnInside: true,
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                    '<div class="mfp-title">Some caption</div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'


        }, 0);
    }
</script>