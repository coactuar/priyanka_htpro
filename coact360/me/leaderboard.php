<?php
require_once '../functions.php';
require_once 'logincheck.php';
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>
<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12">
            <div id="lb">
            </div>
        </div>
    </div>
</div>
<?php
require_once 'scripts.php';
?>

<script>
    $(function() {
        getLeaderboard();
    });

    function getLeaderboard() {

        $.ajax({
            url: '../control/lb.php',
            data: {
                action: 'getfulllb',
            },
            type: 'post',
            success: function(response) {
                $('#lb').html(response);
            }
        });
    }
</script>
<?php
require_once 'footer.php';
?>