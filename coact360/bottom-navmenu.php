<nav class="navbar bottom-nav">
  <ul class="nav mr-auto ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="plenary.php"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Plenary</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="streamwiseplan.php" title="Go To Streamwise Plan"><i class="fas fa-location-arrow"></i><span class="hide-menu">Streamwise Plan</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="focusgroup.php"><i class="fas fa-network-wired"></i><span class="hide-menu">Focus Group</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="resourcecenter.php" title="Go To Resource Center"><i class="fa fa-box-open"></i><span class="hide-menu">Resource Center</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link show_leaderboard" href="#"><i class="fas fa-trophy"></i><span class="hide-menu">Leaderboard</span></a>
      </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>