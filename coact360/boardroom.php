<?php
require_once "logincheck.php";
$curr_room = 'westwing';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">

        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/board_room.jpg",
        "autoLoad": true,
        //"title": "Board Room",
        "pitch": 0,
        "yaw": 8,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [{
                "pitch": 1,
                "yaw": 6.7,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Wincenter Kickoff 2021",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "kickoff.php",
            },
            {
                "pitch": -2.8,
                "yaw": 164.8,
                "cssClass": "dot",
            },
            {
                "pitch": -2.8,
                "yaw": -164,
                "cssClass": "dot",
            }
        ]
    });
    // Make buttons work
    document.getElementById('pan-up').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() + 10);
    });
    document.getElementById('pan-down').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() - 10);
    });
    document.getElementById('pan-left').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() - 10);
    });
    document.getElementById('pan-right').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() + 10);
    });
    document.getElementById('zoom-in').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() - 10);
    });
    document.getElementById('zoom-out').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() + 10);
    });



    function hs01handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.pdf'
            },
            type: 'iframe'


        }, 0);
    }

    function hs02handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.html'
            },
            type: 'iframe'


        }, 0);
    }

    function hs03handler() {
        alert('Way to Enter Conference Room');
    }
</script>

<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>