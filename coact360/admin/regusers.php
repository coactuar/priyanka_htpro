<?php
require_once "../functions.php";
$title = 'Capgemini';

$member = new User();
$list = $member->getAllMemberList();

$i = 0;
$data = array();
$ev = new Event();

foreach ($list as $user) {
  $data[$i]['Name'] = $user['firstname']. ' ' .$user['lastname'];
  $data[$i]['E-mail ID'] = $user['emailid'];

  $i++;
}
$filename = $title . "_users.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
