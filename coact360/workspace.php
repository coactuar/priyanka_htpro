<?php
require_once "logincheck.php";
$curr_room = 'engagement';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/west_wing.jpg",
        "autoLoad": true,
        //"title": "Work Space",
        "pitch": 0,
        "yaw": 12,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [{
                "pitch": -1,
                "yaw": -34.6,
                "cssClass": "hotspot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "opp_response.php",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Opportunity and Response"
            },
            {
                "pitch": -1,
                "yaw": 37.4,
                "cssClass": "hotspot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "content_management.php",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Content and KM"
            },
            {
                "pitch": -1.4,
                "yaw": 55.9,
                "cssClass": "hotspot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "cve.php",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Client Visit Experience"
            },
            {
                "pitch": -1.45,
                "yaw": 127.3,
                "cssClass": "hotspot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "research.php",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Sales Research"
            },
            {
                "pitch": 4.3,
                "yaw": -123.2,
                "cssClass": "hotspot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "sales_ops.php",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Sales Reporting and Operations"
            }
        ]
    });
    // Make buttons work
    document.getElementById('pan-up').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() + 10);
    });
    document.getElementById('pan-down').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() - 10);
    });
    document.getElementById('pan-left').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() - 10);
    });
    document.getElementById('pan-right').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() + 10);
    });
    document.getElementById('zoom-in').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() - 10);
    });
    document.getElementById('zoom-out').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() + 10);
    });



    function hs01handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.pdf'
            },
            type: 'iframe'


        }, 0);
    }

    function hs02handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.html'
            },
            type: 'iframe'


        }, 0);
    }

    function hs03handler() {
        alert('Way to Enter Conference Room');
    }
</script>

<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>