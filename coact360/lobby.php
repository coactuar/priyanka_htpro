<?php
require_once "logincheck.php";
$curr_room = 'lobby';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<section class="videotoplay" id="gotoboardroom" style="display:none;">
    <video class="videoplayer" id="gotoboardroomvideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipBR()">SKIP</a></div>
</section>
<section class="videotoplay" id="gotoengroom" style="display:none;">
    <video class="videoplayer" id="gotoengroomvideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipEng()">SKIP</a></div>
</section>
<section class="videotoplay" id="gotobreakoutroom" style="display:none;">
    <video class="videoplayer" id="gotobreakoutroomvideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipBrR()">SKIP</a></div>
</section>
<?php require_once "scripts.php" ?>
<script>
    var boardroomVideo = document.getElementById("gotoboardroomvideo");
    boardroomVideo.addEventListener('ended', boardroomEnd, false);
    var engroomVideo = document.getElementById("gotoengroomvideo");
    engroomVideo.addEventListener('ended', engroomEnd, false);
    var breakoutroomVideo = document.getElementById("gotobreakoutroomvideo");
    breakoutroomVideo.addEventListener('ended', breakoutroomEnd, false);

    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/Lobby.jpg",
        "autoLoad": true,
        //"title": "Main Lobby",
        "pitch": 0,
        "yaw": 0,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [
            // {
            //     "pitch": 10,
            //     "yaw": -26,
            //     "cssClass": "hotspot",
            //     "clickHandlerFunc": gotoBoardRoom,
            //     "clickHandlerArgs": '',
            //     "createTooltipFunc": hotspot,
            //     "createTooltipArgs": "Plenary"
            // },
            // {
            //     "pitch": 5,
            //     "yaw": -26,
            //     "cssClass": "hotspot",
            //     "clickHandlerFunc": gotoEngagement,
            //     "clickHandlerArgs": '',
            //     "createTooltipFunc": hotspot,
            //     "createTooltipArgs": "Streamwise Plan"
            // },
            // {
            //     "pitch": 0,
            //     "yaw": -26,
            //     "cssClass": "hotspot",
            //     "clickHandlerFunc": gotoBreakoutRoom,
            //     "clickHandlerArgs": '',
            //     "createTooltipFunc": hotspot,
            //     "createTooltipArgs": "Focus Group"
            // },
            {
                "pitch": -2,
                "yaw": -150.7,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": 'sample.pdf',

            },

        ]
    });
    // Make buttons work
    document.getElementById('pan-up').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() + 10);
    });
    document.getElementById('pan-down').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() - 10);
    });
    document.getElementById('pan-left').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() - 10);
    });
    document.getElementById('pan-right').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() + 10);
    });
    document.getElementById('zoom-in').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() - 10);
    });
    document.getElementById('zoom-out').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() + 10);
    });

    // function skipBR() {
    //     location.href = 'plenary.php';
    // }

    // function skipEng() {
    //     location.href = 'streamwiseplan.php';
    // }

    // function skipBrR() {
    //     location.href = 'focusgroup.php';
    // }

    function boardroomEnd(e) {
        location.href = 'plenary.php';
    }

    function engroomEnd(e) {
        location.href = 'streamwiseplan.php';
    }

    function breakoutroomEnd(e) {
        location.href = 'focusgroup.php';
    }

    function gotoBoardRoom() {
        $('#gotoboardroom').css('display', 'block');
        boardroomVideo.currentTime = 0;
        boardroomVideo.play();
    }

    function gotoEngagement() {
        $('#gotoengroom').css('display', 'block');
        engroomVideo.currentTime = 0;
        engroomVideo.play();
    }

    function gotoBreakoutRoom() {
        $('#gotobreakoutroom').css('display', 'block');
        breakoutroomVideo.currentTime = 0;
        breakoutroomVideo.play();
    }
</script>

<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>