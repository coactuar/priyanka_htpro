<nav class="navbar bottom-nav">
  <ul class="nav mx-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="auditorium.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="youthconnect.php" title=""><i class="fas fa-network-wired"></i><span class="hide-menu">Youth Connect</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="bcsjourney.php" title=""><i class="fa fa-road"></i><span class="hide-menu">BCS Journey</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="exhibithall.php"><i class="fas fa-border-all"></i><span class="hide-menu">Exhibit Hall</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="selfiezone.php"><i class="fas fa-camera"></i><span class="hide-menu">Selfie Zone</span></a>
    </li>
 
    <li class="nav-item">
      <a class="nav-link" href="cometchatlogin.php"><i class="fas fa-camera"></i><span class="hide-menu" target="_blank">Talk To Us</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>
<div id="helplines" style="position: absolute;
    bottom: 10px;
    right: 0px;
    text-align: center;
    color: #333;
    width: 11.5%;
    font-size: 0.7rem;">
  For assistance:<br>
  <i class="fas fa-phone-square-alt"></i> +917314-855-655

</div>