<?php
require_once "functions.php";

if (isset($_POST['userEmail'])) {
    $email = '';
    $email_error = '';

    if (empty($_POST['userEmail'])) {
        $email_error = 'Please enter Email ID';
    } else {
        if (!filter_var($_POST['userEmail'], FILTER_VALIDATE_EMAIL)) {
            $email_error = 'Please enter a valid Email ID';
        } else {
            $email = $_POST['userEmail'];
        }
    }

    if ($email_error == '') {
        $user = new User();
        $user->__set('emailid', $email);
        $login = $user->userLogin();
        //var_dump($login);
        $reg_status = $login['status'];
        if ($reg_status == "error") {
            $error = $login['message'];
            $data = array(
                'error' => $error
            );
        } else {
            $data = array(
                'success' => true,
                'email' => $_SESSION['email']

            );
        }
    } else {
        $data = array(
            'email_error' => $email_error
        );
    }

    echo json_encode($data);
}
