<?php
require_once "logincheck.php";
$curr_room = 'youthconnect';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/youthconnect.jpg">
            <a href="https://us02web.zoom.us/j/87220856066?pwd=TitobGNVVDltQVJ5dlhBcDNwNU5YUT09" target="_blank" id="youthConnect">
                <div class="indicator d-6"></div>
            </a>


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>