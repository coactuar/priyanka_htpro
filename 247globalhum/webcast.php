<?php
	require_once "config.php";

	if(!isset($_SESSION["employ_id"]))
	{
		header("location: ./");
		exit;
	}

	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $employ_id=$_SESSION["employ_id"];
           // $location=$_SESSION["location"];
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where employ_id='$employ_id' and eventname='$eventname'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
			   
            //unset($_SESSION['name']);
            unset($_SESSION['employ_id']);
           // unset($_SESSION['location']);
            header("location: ./");
            exit;
        }

    }

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>[24].ai</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body class="video">
<div class="top">
       <!-- <img src="img/top-banner.jpg"  class="img-fluid" alt=""/> -->
</div>

<div class="container-fluid">    
    <div class="row">
	    <div class="col-12 col-md-12 text-center">
            <div class="container-fluid">
                <nav class="navbar">
                    <img class="img24" src="img/247.png" alt=""/>   
                    <img class="img24" src="img/next.png" alt=""/> 
                </nav> 
            </div>
            <div class="container-fluid text-center">
                <img src="img/videotxt.png" alt="hum" class="hum">
            </div>   
        </div>
        <div class="container mt-5">
            <div class="row">
            <div class="col-md-1"></div>

                <div class="col-12 col-md-10 text-center videoborder">
                    <div class="embed-responsive embed-responsive-16by9 mt-2">
                        <iframe src="https://player.vimeo.com/video/631917586?h=733e3940b5" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="GPJ RALLY 2018 X_1"></iframe>
                    </div>
                </div>
                <div class="col-md-1" style="color:aliceblue">
                    <div class="contaioner">
                        <div class="row">
                            <div class="col-md-6">
                            Hello <?php echo $_SESSION['name']; ?>!<br>
                            </div>
                            <div class="col-md-6" >
                    
                                <!-- Hello <?php echo $_SESSION['name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a> -->
                                <a href="?action=logout" id="logoutbtn" class="btn btn-bg videologout">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         async:false,
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);



</script>

</body>
</html>