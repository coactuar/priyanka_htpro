<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
   // source:"https://player.vimeo.com/video/631917586?h=733e3940b5",
		//source:"https://player.vimeo.com/video/631917586?h=733e3940b5&amp;title=0&amp;byline=0&amp;portrait=0&amp;speed=0&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
		//source="https://player.vimeo.com/video/631917586?h=733e3940b5&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
    
    
    source: "https://player.vimeo.com/video/631917586?h=733e3940b5",
		autoPlay:true,
		loop: true,
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
			   3: '1080p',
              2: '720p', // 500kbps
              1: '480p', // 500kbps
              0: '360p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        poster: "img/poster.jpg",
        width: "100%",
        height: "100%",
        mediacontrol: { buttons: "#00b2dd"}
    });
    
    //player.play();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>