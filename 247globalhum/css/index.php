<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BFIL</title>
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>


<div class="container "> 

	
    <div class="row ">
<div class="col-12 d-xl-none text-center d-md-none mt-3">
<!-- <img src="img/Ai/RED BG SEQ/24.png" class="img-fluid " alt=""> -->

</div>
<div class="col-12 d-xl-none d-md-none">
<!-- <img src="img/Ai/RED BG SEQ/24.png" alt="" class=" img-fluid w-100"> -->

</div>

        <div class="col-12 col-md-3 offset-md-9 fixed-bottom mb-2">
	
            <form id="login-form" method="post">
			
           
              <div id="login-message"></div>
              <!-- <div class="input-group ">
                <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
              </div> -->
			       <div class="input-group mt-1">
                <input type="text" class="form-control" placeholder="Email ID" aria-label="emplyid" aria-describedby="basic-addon1" name="emplyid" id="emplyid" required>
              </div>
              <!-- <div class="input-group mt-1 mb-1">
                 <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="Location" required>
              </div> -->
              
              <div class="input-group">
                <button class="mt-4 btn btn-block" type="submit">Login</button>
              </div>
            </form>
        </div>
    </div>
	
</div>

<script src="js/jquery.min.js"></script>
<script src="js/mag-popup.js"></script>

<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#country').val() == '-1')
    {
        alert('Please select country');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
	  console.log(data);
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your email is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'webcast.php';   
      }
      
  });
  
  return false;
});

});

</script>


</body>
</html>