<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>[24].ai</title>
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>


<div class="container-fluid"> 
  <div class="row ">
  <div class="container-fluid">
      <nav class="navbar">
          <img class="img24" src="img/247.png" alt=""/>   
          <img class="img24" src="img/next.png" alt=""/> 
      </nav> 
  </div>
  <div class="container-fluid hum text-center">
    <img src="img/HUM_middle.png" alt="hum" width="300" height="200">
  </div><br>
  <div class="container-fluid text-center mt-2">
  <img src="img/HUM_text.png" class="txt12" width="50%" >
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
        <div class="col-12 col-md-6 text-center mb-2">
            <form id="login-form" method="post">
              <div id="login-message"></div>  
			       <div class="input-group">
                <input type="text" class="form-control" placeholder="User Name" aria-label="emplyid" aria-describedby="basic-addon1" name="emplyid" id="emplyid" required>
              </div>
              <div class="input-group mt-2">
                <input type="password" class="form-control" placeholder="Password" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
              </div>
              <!-- <div class="input-group mt-1 mb-1">
                 <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="Location" required>
              </div> -->
              
              <div class="input-group">
                <button class="mt-4 btn btn-block" id="submitlogin" type="submit">Submit</button>
              </div>
            </form>
        </div>
        <div class="col-md-3"></div>
      </div>
    </div>
    </div>
	
</div>

<script src="js/jquery.min.js"></script>
<script src="js/mag-popup.js"></script>

<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#country').val() == '-1')
    {
        alert('Please select country');
        return false;
    }
  
  if($('#emplyid').val() == 'globalhum2021' && $('#name').val()=='247247' ){
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
	  console.log(data);
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your email is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'webcast.php';   
      }
      
  });
}else{
  $('#login-message').text('Invalid password or email');

}
  return false;
});

});

</script>


</body>
</html>