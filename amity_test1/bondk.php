<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = '913234bfe40b6433e81ceb7573bdd9b9c069ad2c08d89d3beb33bdc351ed5954';
require_once "exhibcheck.php";
$curr_room = 'bondk';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<style>
table td, table tr, table th{
background: transparent !important;
}

.txtcolor{
    color:black;
}

table.dataTable thead .sorting_asc {
    background-image: url("") !important;
}

.backbtn{
    float:left;
   background-color: black;
   padding:3px;
}

.h2{
    color:black
}
.border{
    border-color:black;
}
.bg-image{
    background-image: url('assets/img/listbg.jpg');
    background-size: cover;  
    
}
</style>
<body>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
       
        <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white">
        <div id="back-button" class="backbtn">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
        </div>
        <h1 class="mb-3 h2">POSTER LISTS</h1>
       
        <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Breakout Room</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>  
                  <div>
                    <a href="assets/img/Clickable poster session/2nd row last right/2.Sample Posters - Narender Kumar.pdf" class="viewpoppdf btn btn-primary btn-sm" role="button" aria-pressed="true">Primary link</a>
                  </div> 
                </td>
                <td><a href="https://www.google.com/" class="btn btn-primary btn-sm" role="button" target="_blank" aria-pressed="true">Primary link</a></td>     
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                
            </tr>
            <tr>
                <td>Michael Bruce</td>
                <td>Javascript Developer</td>
                
            </tr>
            <tr>
            <td>
            <a href="assets/img/Clickable poster session/2nd row last right/2.Sample Posters - Narender Kumar.pdf" class="viewpoppdf">
                <div style="height:100%;width:100%;color:black;">
                Donna Snider
                </div>
            </a>
                </td>
                <td>Customer Support</td>
               
            </tr>
            <tr>
                <td>  
                  <div>
                    <a href="assets/img/Clickable poster session/2nd row last right/2.Sample Posters - Narender Kumar.pdf" class="viewpoppdf btn btn-primary btn-sm" role="button" aria-pressed="true">Primary link</a>
                  </div> 
                </td>
                <td><a href="https://www.google.com/" class="btn btn-primary btn-sm" role="button" target="_blank" aria-pressed="true">Primaary link</a></td>     
            </tr>
            <tr>
                <td>Donna Snider</td>
                <td>Customer Support</td>
               
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                
            </tr>
        </tfoot>
    </table>
        </div>


        
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>


<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>

<script>
    $(document).ready(function() {
    $('#example').DataTable();
     } );
</script>
</body>