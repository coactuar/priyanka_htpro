<nav class="navbar bottom-nav">
  <ul class="nav mr-auto ml-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="auditorium.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="exhibitionhalls.php" title="Go To Exhibition Halls"><i class="fa fa-box-open"></i><span class="hide-menu">Exhibition Halls</span></a>
    </li>
    <li> <a class="" href="lounge.php" title="Networking Lounge"><i class="fas fa-network-wired">
          <div id="chat-message"></div>
        </i><span class="hide-menu">Networking Lounge</span></a></li>
    <li class="nav-item">
      <a class="nav-link" href="photobooth.php"><i class="fas fa-camera"></i><span class="hide-menu">Photo Booth</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>