<?php
require_once "../functions.php";
$title = 'CARBS';

$member = new User();
$list = $member->getOnlineMembers('');
//var_dump($list);

$i = 0;
$data = array();
$ev = new Event();
if (!empty($list)) {
  foreach ($list as $user) {
    $state = '';
    $city = '';
    if ($user['state'] != '0') {
      $state =  $ev->getState($user['state']);
    }
    if ($user['city'] != '0') {
      $city =  $ev->getCity($user['city']);
    }

    $data[$i]['Name'] = $user['first_name'] . ' ' . $user['last_name'];
    $data[$i]['E-mail ID'] = $user['emailid'];
    $data[$i]['Mobile No.'] = $user['phone_num'];

    $i++;
  }
}
$filename = $title . "_onlineusers.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
