<?php
require_once "../functions/config.php";

$sql = "SELECT `first_name`,`last_name`,`emailid`,`phone_num`, states.name as state,cities.name as city,`updates`,`division`,`reg_date`  FROM `tbl_users`,  states, cities where  tbl_users.state=states.id and tbl_users.city=cities.id order by reg_date desc";  
$setRec = mysqli_query($link, $sql); 
$columnHeader = '';  
$columnHeader = "#" . "\t". "First Name" . "\t" . "Last Name" . "\t" . "Email ID" . "\t"."Mobile No." . "\t". "State" . "\t"."City" . "\t". "Speciality" . "\t". "Division names" . "\t". "Registered On" ."\t";  
$setData = '';  
  $i = 1;
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = '"'.$i.'"' . "\t";  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
    //echo $rowData.'<br>';
    $i = $i + 1;
}  

  
$file = 'Userlist.xls';  
header("Content-Type: application/octet-stream");  
header("Content-Disposition: attachment; filename=".$file);  
header("Pragma: no-cache");  
header("Expires: 0");  


echo ucwords($columnHeader) . "\n" . $setData . "\n";  

?>