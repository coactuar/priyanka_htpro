<nav class="navbar">
  <ul class="nav ms-auto">
    <li class="nav-item border-end border-secondary">
      <ul id="attendance">
        <li>
          <div id="visitorCount"><b>Visitors:</b> 1 </div>
        </li>
        <li>
          <div id="onlineCount"><b>Online:</b> 1</div>
        </li>
        <li>
          <div id="onpageCount"><b>On this page:</b> 1</div>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#"> Dear User!</a>
    </li>
  </ul>
</nav>