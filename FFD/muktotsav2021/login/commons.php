<!--Helpdesk-->
<?php
$file = $_SERVER["SCRIPT_NAME"];
$break = Explode('/', $file);
$pfile = $break[count($break) - 1];
?>
<div id="talktous" class="popup-dialog">
  <div class="popup-content">
    <div id="chat_team" class="team_chat_box">
      <div class="chat_history scroll" data-touser="team" id="chat_history_team"></div>
      <form>
        <div class="form-group">
          <input name="chat_message_team" id="chat_message_team" rows="1" class="input form-control sendmsg" autocomplete="off">
        </div>
        <div class="form-group mt-2 text-left">
          <button type="button" name="send_teamchat" class="send_teamchat btn btn-primary" data-src="<?php echo $pfile ?>" data-to="team" data-from="<?php echo $userid ?>">Send</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="attendees-chat"></div>