<script src="assets/js/jquery-latest.js"></script>
<script src="assets/js/mag-popup.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    var getteamchat, getchat;

    $(function() {
        $('body').addClass('loaded');
        /* setTimeout(function() {
        }, 1000); */

        updateEvent('<?= $userid ?>', '<?= $curr_room ?>');
        updateAttendance();
        /*setInterval(function() {
            checkfornewchat('<?= $userid ?>');
        }, 60000); */

        $('.view').magnificPopup({
            disableOn: 700,
            type: 'image',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });

//         $('.open').magnificPopup({
//             disableOn: 700,
//   type: 'inline',
//   midClick: true,
//   mainClass: 'mfp-fade',
//   removalDelay: 160,
//             preloader: false,
//             closeBtnInside: true,

//             fixedContentPos: false
// });

$('#resource').on('click', function() {
                $('#resourcesList').modal('show');
            });

        $('.viewvideo').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            closeBtnInside: true,

            fixedContentPos: false
        });

        $('.showpdf').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });
        $('.viewpoppdf').magnificPopup({
            type: 'iframe',
            callbacks: {
                open: function() {
                    $('#resourcesList').modal('hide');
                },
                close: function() {
                    $('#resourcesList').modal('show');
                }
            },
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                    '<div class="mfp-title"></div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'

        });

        $('.resdl').on('click', function() {

            var res_id = $(this).data('docid');
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateFileDLCount',
                    resId: res_id,
                    userId: '<?= $userid ?>'
                },
                type: 'post',
                success: function() {
                    //console.log(data);
                }
            });

        });

    });

    function updateAttendance() {
        updateVisitors();
        updateOnline();
        updateOnPage();
    }

    function updateVisitors() {
        $.ajax({
                url: 'control/update.php',
                data: {
                    action: 'updatevisitors',
                },
                type: 'post',
                success: function(output) {
                    //console.log(output);
                    $('#visitorCount').html(output);
                }
            })
            .always(function(data) {
                setTimeout(function() {
                    updateVisitors();
                }, 600000);
            });
    }

    function updateOnline() {
        $.ajax({
                url: 'control/update.php',
                data: {
                    action: 'updateonline',
                },
                type: 'post',
                success: function(output) {
                    //console.log(output);
                    $('#onlineCount').html(output);
                }
            })
            .always(function(data) {
                setTimeout(function() {
                    updateOnline();
                }, 60000);
            });
    }

    function updateOnPage() {
        $.ajax({
                url: 'control/update.php',
                data: {
                    action: 'updateonpage',
                    page: '<?= $curr_room ?>'
                },
                type: 'post',
                success: function(output) {
                    //console.log(output);
                    $('#onpageCount').html(output);
                }
            })
            .always(function(data) {
                setTimeout(function() {
                    updateOnPage();
                }, 60000);
            });
    }

    function updateEvent(userid, loc) {

        $.ajax({
                url: 'control/update.php',
                data: {
                    action: 'updateevent',
                    userId: userid,
                    room: loc
                },
                type: 'post',
                success: function(output) {
                    //console.log(output);
                    if (output == '0') {
                        location.href = './';
                    }
                }
            })
            .always(function(data) {
                setTimeout(function() {
                    updateEvent('<?= $userid ?>', '<?= $curr_room ?>');
                }, 30000);
            });
    }

    function getOnlineAttendees(keyword, pageNum) {
        $.ajax({
            url: 'control/users.php',
            data: {
                action: 'getonlineattendees',
                key: keyword,
                pagenum: pageNum,
                userId: '<?= $userid ?>'
            },
            type: 'post',
            success: function(response) {
                $("#attendeeList").html(response);
            }
        });
    }

    function getAttendeesChat(to_id) {
        $.ajax({
            url: 'control/chat.php',
            data: {
                action: 'getAttendeesChat',
                to: to_id
            },
            type: 'post',
            success: function(response) {
                $("#attendees-list-chat").html(response);
            }
        });

    }

    function getSharedCards(to_id) {
        $.ajax({
            url: 'control/users.php',
            data: {
                action: 'getsharedcards',
                to: to_id
            },
            type: 'post',
            success: function(response) {
                $("#cards-shared").html(response);
            }
        });

    }

    function getTeamChatHistory(to_user_id, from_user_id) {
        $.ajax({
            url: 'control/chat.php',
            data: {
                action: 'getteamchathistory',
                to: to_user_id,
                from: from_user_id,
                userId: '<?= $userid ?>'
            },
            type: 'post',
            success: function(response) {
                var tar = '#chat_history_' + to_user_id;
                var curr_hist = $(tar).html();
                if (response !== curr_hist) {
                    //console.log(response);
                    //console.log(curr_hist);
                    $(tar).html(response);
                    var myDiv = document.getElementById('chat_history_' + to_user_id);
                    myDiv.scrollTop = myDiv.scrollHeight;
                } else {
                    //console.log('no new msg');
                }

            }
        });
    }

    function make_chat_box(to_user_id, from_user_id) {
        var modal_content = '<div id="chat_' + to_user_id + '" class="user_dialog attendee_chat_box" title="Chat">';
        modal_content += '<div style="height:225px; border:0px solid #ccc; background-color:#ccc; overflow-y:auto; margin-bottom:15px;" class="chat_history scroll" data-touser="' + to_user_id + '" id="chat_history_' + to_user_id + '">';
        modal_content += '</div>';
        modal_content += '<div class="form-group">';
        modal_content += '<textarea name="chat_message_' + to_user_id + '" id="chat_message_' + to_user_id + '" rows="1" class="form-control" required></textarea>';
        modal_content += '</div><div class="form-group text-left">';
        modal_content += '<button type="button" name="send_chat" class="send_chat btn btn-primary" data-to="' + to_user_id + '" data-from="' + from_user_id + '" class="btn btn-info">Send</button></div></div>';
        $('#attendees-chat').html(modal_content);
    }

    function getChatHistory(to_user_id, from_user_id) {
        $.ajax({
            url: "control/users.php",
            data: {
                action: 'getchathistory',
                to: to_user_id,
                from: from_user_id,
                userId: '<?= $userid ?>'
            },
            type: 'post',
            success: function(response) {
                var tar = '#chat_history_' + to_user_id;
                var curr_hist = $(tar).html();
                if (response !== curr_hist) {
                    $('#chat_history_' + to_user_id).html(response);
                    var myDiv = document.getElementById('chat_history_' + to_user_id);
                    myDiv.scrollTop = myDiv.scrollHeight;
                }

            }
        });
    }

    function checkfornewchat(to_id) {
        $.ajax({
            url: "control/chat.php",
            data: {
                action: 'checknewchat',
                to: to_id
            },
            type: 'post',
            success: function(response) {
                if (response > 0) {
                    $('#chat-message').html('<span class="badge badge-danger">' + response + '</span>').css('display', 'block');
                } else {
                    $('#chat-message').html('').fadeOut();

                }

            }
        });
    }

    function getLeaderboard() {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'getleaderboard',
                user: ''
            },
            type: 'post',
            success: function(output) {
                $('#conf-ranks').html(output);
            }
        });
    }

    function getMyRank(userId) {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'getpoints',
                user: userId
            },
            type: 'post',
            success: function(output) {
                console.log(output);
                $('#my-rank').html(output);
            }
        });
    }
</script>