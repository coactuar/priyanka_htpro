<?php
require_once "logincheck.php";
$curr_room = 'lobby';
$curr_session = "Lobby";
$date = getdate();
//var_dump($date);
$day_img = '';
if ($date['mon'] == '9') {
    if ($date['mday'] == '23') {
        $day_img = '23sep.jpg';
    }
    if ($date['mday'] == '24') {
        $day_img = '24sep.jpg';
    }
    if ($date['mday'] == '25') {
        $day_img = '25sep.jpg';
    }
    if ($date['mday'] == '26') {
        $day_img = '26sep.jpg';
    }
    if ($date['mday'] == '27') {
        $day_img = '27sep.jpg';
    }
    if ($date['mday'] == '28') {
        $day_img = '28sep.jpg';
    }
}
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/img/Overview Final -1.jpg">
            <div id="lobbyVideo">
                <iframe src="" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
            </div>
            <!-- <a href="#" id="lobbyVideo" onclick="javascript:alert('Will Activate Shortly')">
            </a>-->
            <a href="auditorium.php" id="enterYC">
                <div class="indicator d-1"></div>
            </a>
            <a href="bcsjourney.php" id="enterHall">
                <div class="indicator d-2"></div>
            </a>
            <a href="exhibithall.php" id="enterSZ">
                <div class="indicator d-3"></div>
            </a>
            <a href="youthconnect.php"  id="enterAudi">
                <div class="indicator d-0"></div>
            </a>
            <a href="selfiezone.php" id="enterBcs">
                <div class="indicator d-2"></div>
            </a>
           <a href="assets/resources/Schedule.pdf" data-docid="2651306eff4222195ac248112d34fe7ddfd3b8e685a8b0685c3b1186caadbc95" class="showpdf resdl" id="showAgenda">
                <!-- <div class="indicator d-6"></div> -->
            </a>
            <a href="auditorium.php" data-docid="1ff2261bbc1d9997d62e4e51d9002383ba41ce8f259a0b18fbab6e24f14170c7" class=" resdl" id="banner01">
                <!-- <div class="indicator d-6"></div> -->
            </a>
            <a href="exhibithall.php" data-docid="b4461de2694ab2221fa3819f633e60531c1ba7f5818327acfcee643954289c72" class=" resdl" id="banner02">
                <!-- <div class="indicator d-6"></div> -->
            </a>
            </a>
            <a href="bcsjourney.php" data-docid="416dd176a7af6b29912541dc66e84940628499940e67427da86cb7fa394ef7ee" class=" resdl" id="banner03">
                <!-- <div class="indicator d-6"></div> -->
            </a>
            <a href="youthconnect.php" data-docid="da4a04edd33cd773243f35fd57b411b5da13846d9bf8d45510012b26a605f5eb" class=" resdl" id="banner04">
                <!-- <div class="indicator d-6"></div> -->
            </a>
          <a href="assets/img/Entrance Wall final.jpg" data-docid="54129688187e6595a9dc5eb0e377178c76bd9ade056ac2c99985c380713806e9" class="view resdl" id="banner05">
                <!-- <div class="indicator d-6"></div> -->
            </a>
            <a href="selfiezone.php" data-docid="d6e48e311a00a245f3ae209ec364dd4df0e7eb321856bbcae8d0072b0c1dfc02" class=" resdl" id="banner06">
                <!-- <div class="indicator d-6"></div> -->
            </a>
               <!-- <a href="assets/resources/banner-07.jpg" data-docid="9615d0a60ebbd7b52326641633afe508aae88ab241c5bddc19442f6ba6de1250" class="view resdl" id="banner07">
 
            </a> -->
            <!-- <audio id="track">
  <source src="assets/videos/Overview Audio.mp4 " type="audio/mpeg" />
</audio>

<div id="player-container" id="banner07">
  <div id="play-pause" class="play">Play</div>
</div> -->
            <!-- <a id="ppbutton1" id="banner07" class="view resdl"  class="ppbutton fa fa-play" data-src="https://kolber.github.io/audiojs/demos/mp3/01-dead-wrong-intro.mp3"></a> -->

<!-- <a id="ppbutton2" class="ppbutton fa fa-play" data-src="https://kolber.github.io/audiojs/demos/mp3/02-juicy-r.mp3"></a>

<a id="ppbutton3" class="ppbutton fa fa-play" data-src="https://www.ee.columbia.edu/~dpwe/sounds/instruments/flute-C6.wav"></a> -->

            <!-- <?php if ($day_img != '') { ?>
                <a href="assets/img/<?= $day_img ?>" data-docid="45ef4356ff6995b4989a9f0f8d6b4de7c0f807bce0464518582371420a372976" class="view resdl" id="banner08">
                    <img src="assets/img/<?= $day_img ?>" alt="">
                <?php } else { ?>
                    <a href="assets/resources/banner-08.jpg" data-docid="45ef4356ff6995b4989a9f0f8d6b4de7c0f807bce0464518582371420a372976" class="view resdl" id="banner08">
                    <?php } ?>
                    <div class="indicator d-6"></div>
                    </a> -->


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php";            ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<section class="videotoplay" id="gotoaudi" style="display:none;">
    <iframe id="gotoaudivideo" src="https://player.vimeo.com/video/610895828?h=67bba02c3a&autoplay=0&controls=0" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

    <a href="auditorium.php" class="skip">SKIP</a>
</section>

<?php require_once "scripts.php" ?>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>

</script>
<script>
    function enterAudi() {
        $('#content').css('display', 'none');
        $('#gotoaudi').css('display', 'block');
        var iframe = document.querySelector('iframe#gotoaudivideo');
        var player = new Vimeo.Player(iframe);
        player.on('ended', function(data) {
            location.href = "auditorium.php";
        });
        player.play();

    }
</script>
<script>
    var clicked_id;
var audio_var = new Audio();

$(".ppbutton").on("click", function () {
  var datasrc = $(this).attr("data-src");
  clicked_id = $(this).attr("id");
  console.log(clicked_id);
  audio_var.pause();

  $(".ppbutton")
    .not(this)
    .each(function () {
      $(this).removeClass("fa-pause");
      $(this).addClass("fa-play");
    });

  if ($(this).hasClass("fa-play")) {
    console.log("play_click");
    audio_var.src = datasrc;
    $(this).removeClass("fa-play");
    $(this).addClass("fa-pause");
    console.log(audio_var);
    audio_var.play();
  } else {
    console.log("pause_click");
    $(this).removeClass("fa-pause");
    $(this).addClass("fa-play");
    console.log(audio_var);
    audio_var.pause();
    //audio_var.src='';
    //audio_var.load();
    console.log(audio_var);
  }
});

audio_var.onended = function () {
  $("#" + clicked_id).removeClass("fa-pause");
  $("#" + clicked_id).addClass("fa-play");
};

</script>


<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>
