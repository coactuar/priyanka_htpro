<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <style>
      body{
          background-image: url(img/bgbg.png);
          /* background-position:  bottom; */

          background-repeat: no-repeat;
          background-size: cover;
      }
      .xyz{
          margin-top:150px;
          /* margin-right:40px; */
      }
      .starbg{
        width:450px; 
        height:450px;
       

      }
      .number{
          height:50px;
          width:550px;
          margin-top:30px;
      }
      .btn{
          height:50px;
          width:170px;
          font-size:25px;
          font-weight: bolder;
      }
      .abc{
        margin-top: 160px;
      }
      h3{
          color:white;
      }
      .head{
        width:550px;
      }

      @media screen and (max-width: 1024px) {
        .number{
          height:50px;
          width:450px;
          margin-top:30px;
      }
      .head{
        width:450px;
      }
      .xyz{
          margin-top:50px;
          /* margin-right:40px; */
      }
    }
    @media screen and (max-width: 768px) {
          .starbg {
        width: 352px;
        height: 423px;
    }
    .number{
      width:360px;
    }
      .head{
        width:350px;
      }
    }
  </style>
  <body>
      
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 xyz">
          <div class="row">
            <div class="col-md-6">
              <img src="img/starbg.png" alt="Girl in a jacket" class="starbg">
            </div>
            <div class="col-md-6 abc">
              <img src="img/heading1.png" alt="Girl in a jacket" class="head"><br>
                <input type="number" name="number" class="number"><br><br>
                <input type="submit" value="LOGIN" class="btn btn-primary">
            </div>
          </div>
      </div>
    </div>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>