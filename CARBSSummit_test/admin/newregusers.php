<?php
require_once "../functions.php";
$title = 'CARBS';

$member = new User();
$list = $member->getAllNewMemberList();
//var_dump($list);

$i = 0;
$data = array();
$ev = new Event();

foreach ($list as $user) {
  $state = '';
  $city = '';
  if ($user['state'] != '0') {
    $state =  $ev->getState($user['state']);
  }
  if ($user['city'] != '0') {
    $city =  $ev->getCity($user['city']);
  }

  $data[$i]['Name'] = $user['first_name'] . ' ' . $user['last_name'];
  $data[$i]['E-mail ID'] = $user['emailid'];
  $data[$i]['Phone No.'] = $user['phone_num'];
  $data[$i]['State'] = $state;
  $data[$i]['City'] = $city;
  $data[$i]['Speciality'] = $user['speciality'];
  $data[$i]['Time of Registration'] = $user['reg_date'];
  $data[$i]['Last Login'] = $user['login_date'];
  $data[$i]['Last Logout'] = $user['logout_date'];

  $i++;
}
$filename = $title . "_users.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
