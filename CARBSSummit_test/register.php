<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$fname = '';
$lname = '';
$emailid = '';
$mobile = '';
$state = 0;
$city = 0;
$speciality = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Phone No. is required';
    }
    if ($_POST['state'] == '0') {
        $errors['state'] = 'State is required';
    }
    if ($_POST['city'] == '0') {
        $errors['city'] = 'City is required';
    }
    if ($_POST['speciality'] == '0') {
        $errors['spec'] = 'Speciality is required';
    }

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];
    $speciality = $_POST['speciality'];
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }

    if (count($errors) == 0) {
        $newuser = new User();

        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('state', $state);
        $newuser->__set('city', $city);
        $newuser->__set('speciality', $speciality);

        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
            $speciality = '';
            $state = 0;
            $city = 0;
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $event_title ?></title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <div class="container reg-content">
        <div class="row">
            <div class="col-12 col-lg-9 mx-auto">
                <div class="row pt-4">
                    <div class="col-4 col-md-3 p-3 text-center">
                        <img src="assets/img/endorsed-by.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-4 col-md-2 offset-md-2 text-center p-3">
                        <img src="assets/img/logo-carbs.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-4 col-md-3 offset-md-2 text-center p-3">
                        <img src="assets/img/logo-aace.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-6 col-md-6 col-lg-4 mx-auto">
                <img src="assets/img/CARBs.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="register-wrapper mx-4 p-3 p-md-0">
            <div class="row">
                <div class="col-12 col-md-8 mx-auto p-2">
                    <h5 class="text-center my-2">Register for the Second virtual symosium of CARBS Summit</h5>
                    <?php
                    if (count($errors) > 0) : ?>
                        <div class="alert alert-danger alert-msg">
                            <ul class="list-unstyled">
                                <?php foreach ($errors as $error) : ?>
                                    <li>
                                        <?php echo $error; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if ($succ != '') { ?>
                        <div class="alert alert-success alert-msg">
                            <?= $succ ?>
                        </div>
                    <?php } ?>
                    <form method="POST" class="mt-4">
                        <div class="row mt-2">
                            <div class="col-12 col-md-6">
                                <label>First Name</label>
                                <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off">
                            </div>
                            <div class="col-12 col-md-6">
                                <label>Last Name</label>
                                <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                            <div class="col-12">
                                <label>Email ID</label>
                                <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $emailid; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                            <div class="col-12 col-md-6">
                                <label>Phone No.</label>
                                <input type="number" id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                            <div class="col-12 col-md-6">
                                <label>State</label>
                                <div id="states">
                                    <select class="input" id="state" name="state" value="<?= $state ?>" onChange="updateCity()">
                                        <option value="0">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                            <div class="col-12 col-md-6">
                                <label>City</label>
                                <div id="cities">
                                    <select class="input" id="city" name="city">
                                        <option value="0">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <label>Speciality</label>
                                <div id="cities">
                                    <select class="input" id="speciality" name="speciality">
                                        <option value="0">Select Speciality</option>
                                        <option value="Endocrinologist" <?= $speciality === 'Endocrinologist' ? 'selected' : '' ?>>Endocrinologist</option>
                                        <option value="Cardiologist" <?= $speciality === 'Cardiologist' ? 'selected' : '' ?>>Cardiologist</option>
                                        <option value="Diabetologist" <?= $speciality === 'Diabetologist' ? 'selected' : '' ?>>Diabetologist</option>
                                        <option value="Nephrologist" <?= $speciality === 'Nephrologist' ? 'selected' : '' ?>>Nephrologist</option>
                                        <option value="Consulting Physician" <?= $speciality === 'Consulting Physician' ? 'selected' : '' ?>>Consulting Physician</option>
                                        <option value="General Physician" <?= $speciality === 'General Physician' ? 'selected' : '' ?>>General Physician</option>
                                        <option value="Other" <?= $speciality === 'Other' ? 'selected' : '' ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4 mb-3">
                            <div class="col-12">
                                <input type="image" src="assets/img/btn-register.png" value="Submit" />
                                <a href="./" class="form-cancel"><img src="assets/img/btn-cancel.png" alt="" /></a>
                                <br><br>
                                If already registered, <a href="login.php">click here</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="assets/img/bottom-banner.png" class="img-fluid" alt="" />
                </div>
            </div>
        </div>
    </div>


    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(function() {
            updateState();
        });

        function updateState() {
            var c = '101'; //$('#country').val();
            if (c != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getstates',
                        country: c
                    },
                    type: 'post',
                    success: function(response) {
                        $("#states").html(response);
                    }
                });
            }
        }

        function updateCity() {
            var s = $('#state').val();
            if (s != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getcities',
                        state: s
                    },
                    type: 'post',
                    success: function(response) {
                        $("#cities").html(response);
                    }
                });
            }
        }
    </script>

    <?php require_once 'ga.php';  ?>
    <?php require_once 'footer.php';  ?>