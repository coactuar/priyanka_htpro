<?php
require_once "logincheck.php";
$curr_room = 'photobooth';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<link rel="stylesheet" href="assets/css/cam.css">
<div class="page-content">
    <div id="content" class="photobooth">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="photobooth">
            <img src="assets/img/photobooth.jpg">
            <div id="photobooth">
                <div id="cam-panel" class="camera-panel">
                    <div class="photo-jacket-container">
                        <img class="photo-jacket" src="assets/img/booth-01.png" />
                    </div>
                    <div id="cam-ui" class="layout-01">
                        <div id="cam-feed">
                            <video id="cam-feed-video" playsinline autoplay></video>
                            <canvas class="cam-prev"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div id="photobooth-actions">
                <div class="d-flex justify-content-center" id="before-capture" style="display: none;">
                    <button type="button" class="btn btn-capture" id="cam-btn"><i class="fas fa-camera"></i></button>
                </div>
                <div class="d-flex justify-content-center" id="after-capture" data-html2canvas-ignore style="display: none!important;">
                    <button type="button" class="btn btn-retry" id="retake-btn"><i class="fas fa-undo-alt"></i></button>
                    <!-- <button type="button" class="btn btn-save" id="save-btn"><i class="fas fa-save"></i></button> -->
                    <button type="button" onclick="myfunctionName('save')" class="btn btn-save" id="save-btn"><i class="fas fa-save"></i></button>
                </div>
            </div>
            <div id='photobooth-options'>
                <div class="d-flex flex-column">
                    <div class="w-100 bg-dark text-white text-center p-2">
                        Select Layout
                    </div>
                    <div class="w-100 text-center p-2">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" onclick="updPB('01')"> Take Selfie - Layout 01</a>
                            </li>
                            <li>
                                <a href="#" onclick="updPB('02')"> Take Selfie - Layout 02</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="options" class="text-center mt-4 mt-sm-2">
            <div id="social" style="display: none;">
                <a href="#" id="facebook" target="_blank"><i class="fab fa-facebook-square fa-3x"></i></a>
                <a href="#" id="linkedin" target="_blank"><i class="fab fa-linkedin fa-3x"></i></a>
                <a href="#" id="twitter" target="_blank"><i class="fab fa-twitter-square fa-3x" aria-hidden="true"></i></a>
            </div>
        </div>

        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/CameraController.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.2/FileSaver.js"></script>
<script type="text/javascript" src="assets/js/html2canvas.min.js"></script>
<script>
    $(function() {
        initCamera();
    });

function myfunctionName(a)
{

		afterCapture.style.cssText = "display:none !important";
  beforeCapture.style.cssText = "display:none !important";
  window.scrollTo(0, 0);
  html2canvas(document.querySelector(".camera-panel"), {
    backgroundColor: "#ffffff",
  }).then((c) => {
    c.toBlob(function (blob) {
      saveAs(blob, "selfie.jpg");
      beforeCapture.style.cssText = "display:none !important";
      afterCapture.style.cssText = "display:flex !important";
    });
    var dataURL = c.toDataURL();

    $.ajax({
      type: "POST",
      url: "control/event.php",
      data: {
        action: "updateVideoView",
        vidId: "100",
        userId: "<?= $userid ?>",
      },
    })
      .done(function (o) {
        console.log("saved");
      })
      .success(function (response) {
        console.log(response);
      });

    $.ajax({
      type: "POST",
      url: "savetoserver.php",
      data: {
        imgBase64: dataURL,
      },
    })
      .done(function (o) {
        console.log("saved");
      })
      .success(function (response) {
        console.log(response);
        document
          .getElementById("facebook")
          .setAttribute(
            "href",
            "https://www.facebook.com/sharer.php?u=" + response + "&quote="
          );
        document
          .getElementById("linkedin")
          .setAttribute(
            "href",
            "https://www.linkedin.com/shareArticle?mini=true&url=" +
              response +
              "&summary=%20&source=LinkedIn"
          );
        document
          .getElementById("twitter")
          .setAttribute(
            "href",
            "https://twitter.com/intent/tweet?url=" + response + "&text="
          );
        $("#social").fadeIn();
      });
  });
		
}

    function updPB(opt) {
        $('.photo-jacket').attr('src', 'assets/img/booth-' + opt + '.png');
        $('#cam-ui').removeClass().addClass('layout-' + opt);
    }
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>