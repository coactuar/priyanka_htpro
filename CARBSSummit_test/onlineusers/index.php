<?php
require_once '../functions.php';
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>
<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12">
            <div id="users">
            </div>
        </div>
    </div>

</div>




<?php
require_once 'scripts.php';
?>

<script>
    $(function() {
        getUserList();
    });


    function getUserList(pageNum) {

        $.ajax({
            url: '../control/users.php',
            data: {
                action: 'getonlineusers',
            },
            type: 'post',
            success: function(response) {
                $('#users').html(response);
            }
        });
    }
</script>
<?php
require_once 'footer.php';
?>