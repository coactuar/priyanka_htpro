/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Create viewer.
var viewer = new Marzipano.Viewer(document.getElementById('pano'));

// Create source.
var source = Marzipano.ImageUrlSource.fromString(
  "http://localhost/coact360demo2/01.jpg"
  //"//localhost/_events/OnConf/Events/Coact/Capgemini/360demov2/01.jpg"
);

// Create geometry.
var geometry = new Marzipano.EquirectGeometry([{ width: 5000 }]);

// Create view.
var limiter = Marzipano.RectilinearView.limit.traditional(1024, 100*Math.PI/180);
var view = new Marzipano.RectilinearView({ yaw: Math.PI }, limiter);

// Get the view values

var yaw = view.yaw();
var pitch = view.pitch();
var fov = view.fov();      // fov is horizontal
/*var vfov = view.vfov();
var hfov = view.hfov();    // same as view.fov()*/


// Change the values

view.setYaw(0 * Math.PI/180);
view.setPitch(0 * Math.PI/180);
view.setFov(120 * Math.PI/180);

// Create scene.
var scene = viewer.createScene({
  source: source,
  geometry: geometry,
  view: view,
  pinFirstLevel: true
});

// Display scene.
scene.switchTo();

var autorotate = Marzipano.autorotate({
  yawSpeed: 0.1,         // Yaw rotation speed
  targetPitch: 0,        // Pitch value to converge to
  targetFov: 0.1 * Math.PI/2   // Fov value to converge to
});

// Start autorotation immediately
//viewer.startMovement(autorotate);

// Get the hotspot container for scene.
var container = scene.hotspotContainer();
/*container.createHotspot(document.getElementById('iframespot'), { yaw: 0.5335, pitch: -0.102 },
  { perspective: { radius: 1640, extraTransforms: "rotateX(5deg)" }});*/
container.createHotspot(document.getElementById('iframeselect'), { yaw: -0.35, pitch: -0.239 });
container.createHotspot(document.querySelector("#playVideo"), { yaw: -2.645, pitch: 0.010 });

container.createHotspot(document.getElementById('poster01'), { yaw: 1.142, pitch: 0.108 },
  { perspective: { radius: 1640, extraTransforms: "rotateX(-4deg) rotateY(-24deg) rotateZ(-2deg)" }});

container.createHotspot(document.getElementById('brand'), { yaw: 0, pitch: 0.150 },
  { perspective: { radius: 1000, extraTransforms: "rotateX(0deg) rotateY(0deg) rotateZ(0deg)" }});