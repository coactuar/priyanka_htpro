

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Capgemini - Demo</title>
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />
    <style>@-ms-viewport { width: device-width; }</style>
    <link rel="stylesheet" href="//www.marzipano.net/demos/common/reset.css">
    <link rel="stylesheet" href="//www.marzipano.net/demos/common/hint.css">
    <link rel="stylesheet" href="magnific-popup.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="preloader"></div>
<div id="pano"></div>
<div id="brand">
  <div id="logo">

  </div>
 
</div>
<div id="poster01">
  <img src="admin/uploads/poster.jpg" alt=""/>
</div>

<!--<div id="iframespot">
  <img src="sample.jpg" alt=""/>
</div>-->
<ul id="iframeselect">
  <li><a href="#">Plenary</a></li>
  <li><a href="#">Streamwise Plan</a></li>
  <li><a href="#">Focus Group</a></li>
</ul>
<div id="playVideo">
    <a href="https://player.vimeo.com/video/512478145" class="hotspot viewvideo">
      <div class="out"></div>
      <div class="in"></div>
    </a>
</div>
<script src="jquery.min.js"></script>
<script src="jquery.magnific-popup.min.js"></script>
<script src="marzipano.js"></script>
<script src="main.js"></script>
<script>
$(function() {
   setTimeout(function(){
    $('.preloader').fadeOut('slow');
   }, 1000);
});

$('.viewvideo').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    closeBtnInside: true,

    fixedContentPos: false
});
</script>
<script>
  function getpics()
{
    $.ajax({
        url: 'new.php',
        //data: {action: 'getquestions'},
        type: 'post',
        success: function(response) {
          $("#logo").html(response);
          
            
        }
    });
    
}
getpics();
setInterval(function(){ getpics(); }, 6000);
</script>


</script>
</body>
</html>