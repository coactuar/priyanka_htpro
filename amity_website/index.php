<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>SBCI 2021</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/images/Logo_icon.png" rel="icon">
    <link href="assets/images/Logo_icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">


</head>
<style>
   
.m-0 {
    padding: 0;
    margin: 0 0 0 0 !important;
}


#clockdiv{
	font-family: sans-serif;
	color: #fff;
	display: inline-block;
	font-weight: 100;
	text-align: center;
	font-size: 30px;
}

#clockdiv > div{
	padding: 10px;
	border-radius: 3px;
	background: #00BF96;
	display: inline-block;
}

#clockdiv div > span{
	padding: 15px;
	border-radius: 3px;
	background: #00816A;
	display: inline-block;
}

.smalltext{
	padding-top: 5px;
	font-size: 16px;
}

#gennovaimg{
    margin-left:19%;
}

#kewauneeimg{
    width: 25%;
    height: 55%;
    margin-left:15px;
}

#mpimg{
    width: 10%; 
    height: 55%;
    margin: 0px 18px
}

#eppendorfimg{
    width: 18%;
    height: 55%;
}
#scieximg{
    width: 13%;
    height: 55%;
}
.plh{
  text-align: center;
}

.annual{
    text-align: left;
}

.yellow{
    color: yellow;
}

/* #navbar{
   margin-right:0px;
} */

@media only screen and (max-width: 768px) {
    #gennovaimg{
        /* margin-left: 19%; */
        width: 200px;
    }
}

</style>
<body>

    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center ">
        <div class="container-fluid container-xxl d-flex align-items-center">

            <div id="logo" class="me-auto">
                <!-- Uncomment below if you prefer to use a text logo -->
                <!-- <h1><a href="index.html">The<span>Event</span></a></h1>-->
                <a href="http://sbcihq.in/" target="_blank" class="scrollto"><img src="assets/images/logo_right.png" alt="" width="100%" title=""></a>
            </div>

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">About</a></li>
                    <li><a class="nav-link scrollto" href="#speakers">Speakers</a></li>
                    <li><a class="nav-link scrollto" href="#schedule">Programs</a></li>
                    <li><a class="nav-link scrollto" href="#commite">Organizing committee</a></li>
                    <!-- <li><a class="nav-link scrollto" href="Amity Final Navigation Video.mp4">Event Nav</a></li> -->
                    <!-- <li><a class="nav-link scrollto" href="https://coact.live/amity_test/">Meeting Link</a></li> -->
                    <li><a class="nav-link scrollto" target="_blank" href="#bg_reg">Registration</a></li>
                    
                    <!-- <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li> -->
                    <li><a class="nav-link scrollto" href="#supporters">Sponsors</a></li>
                   
                    <!-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
          <ul>
            <li><a href="#">Drop Down 1</a></li>
            <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
              <ul>
                <li><a href="#">Deep Drop Down 1</a></li>
                <li><a href="#">Deep Drop Down 2</a></li>
                <li><a href="#">Deep Drop Down 3</a></li>
                <li><a href="#">Deep Drop Down 4</a></li>
                <li><a href="#">Deep Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="#">Drop Down 2</a></li>
            <li><a href="#">Drop Down 3</a></li>
            <li><a href="#">Drop Down 4</a></li>
          </ul>
        </li> -->
                    <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- .navbar -->
            <a class="buy-tickets scrollto" target="_blank" href="https://www.amity.edu/gurugram/"><img src="assets/images/PNG Logo.png" alt="" width="150px" srcset=""></a>

        </div>
    </header>
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
            <div class="row">
                <div class="col-md-6 mt-2">
                    <a class="btn btn-warning mt-2" href="https://90thsbciauh.live/login/" role="button">CLICK HERE TO JOIN VIRTUAL PLATFORM</a>
                    <!-- <p class="m-0">Register Soon!!! Early bird registration closing on <span class="blink_me yellow"> 30.11.2021 </span> (Fee INR 500+GST)</p>
                    <p class="m-0">Registration fee after 30.11.2021  will be INR 1000+GST</p> -->
                </div>


                <div class="col-md-4 mt-2">
                    <p style="font-size: 15px;" class="">To know more about <br>
                Virtual Platform Featurs & Functionality <br>
                    <!-- <a href="http://" target="_blank" rel="noopener noreferrer">click here</a> To Watch Video  -->
                    
                    <a class="popup-vimeo" target="_blank" data-toggle="modal" data-target="#exampleModal">click here</a> To Watch Video <br>
                    <span style="border-radius: 40px;" class="blink_me btn btn-md btn-danger" data-toggle="modal" data-target="#exampleModal">Virtual Platform Tour </span> </p>
                   
                    <!-- <p class="m-0">Submission:  </p> -->
                </div>
                <div class="col-md-2 mt-2">
                    <!-- <p class="m-0">POSTER SUBMISSION IS NOW CLOSED</p> -->
                    <a href="90th SBC(I) 2021 Abstract Book.pdf" class="btn btn-info m-0" download>DOWNLOAD ABSTRACT
                        <!-- <img alt="PDF" width="104"  height="42"> -->
                    </a>
                    <!-- <p class="m-0">Submission: <span class="blink_me"> 30.11.2021 </span> </p> -->
                </div>
                <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a> -->
                <!-- <a href="#about" class="about-btn scrollto">About The Event</a> -->

                <br>
                <img class ="heroimg m-1" src="assets/images/Picture_text.jpg" width="100%" alt="" srcset="">
            </div>
        </div>
    </section>
    <!-- End Hero Section -->

    <main id="main">
    <!-- <button type="button" class="btn btn-primary" >
  Launch demo modal
</button> -->
<div class="modal fade" id="exampleModal" style="position: absolute;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <!-- <video src="Amity Final Navigation Video.mp4" width="400px"></video> -->
       <video id="" width="100%" controls poster="video/thumb.jpg">
          <source src="Amity Final Navigation Video.mp4" type="video/mp4">
          <!-- <source src="video/Login_via_Lynda_dot_com.ogg" type="video/ogg"> -->
          Your browser does not support the video tag.
        </video>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
        <!-- ======= About Section ======= -->
        <section id="about">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <!-- <div class="col-lg-5">
                        <h2>About The Event</h2>
                        <p>Sed nam ut dolor qui repellendus iusto odit. Possimus inventore eveniet accusamus error amet eius aut accusantium et. Non odit consequatur repudiandae sequi ea odio molestiae. Enim possimus sunt inventore in est ut optio sequi
                            unde.
                        </p>
                    </div> -->
                    <div class="col-lg-4 col-md-3 col-sm-3 text-left">
                        <h3 class="ml-2 text-center">Platinum Partner</h3>
                     <a href="https://gennova.bio/" target="_blank" rel="noopener noreferrer"><img src="assets/images/logo_below.png" id="gennovaimg" alt="" srcset=""></a>   
            
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-7 text-center">
                        <h3 class="ml-2">Gold Partner</h3>
                       
                     <a href="https://www.kewaunee.in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/Kewaunee (General).jpg" id="kewauneeimg" alt="" srcset=""></a>   
                     <a href="https://www.mpbio.com/in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/MP logo.jpg" id="mpimg" alt="" srcset=""></a>   
                     <a href="https://www.eppendorf.com/IN-en/" target="_blank" rel="noopener noreferrer"><img src="assets/images/eppendorf_logo_web.jpg" id="eppendorfimg" alt="" srcset=""></a>  
                     <a href="https://sciex.com/applications" target="_blank" rel="noopener noreferrer"><img src="assets/images/SCIEX Logo 2019.png" id="scieximg" alt="" srcset=""></a>  

            
                    </div>
                    <!-- <div class="col-lg-2">
                        <h3>When</h3>
                        <p>Monday to Wednesday<br>10-12 December</p>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- End About Section -->
        <section class=" ">
            <div class="container">
                <h3 class="text-center mt-2 " style="color: #194880; text-align: justify;">About Us</h3>
                <!-- <hr class="hr"> -->
                <img src="assets/images/side.png.jpg" class="w-100" alt="">
                <div class="">
                    <!-- <h2>About</h2> -->

                    <p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #052a57; font-size: 20px  ;">The Society of Biological Chemists (SBC), India is one of the oldest scientific fraternities in India. The SBC(I) has more than 4000 members at present, including senior scientists in the field of life sciences, young researchers and students. SBC(I) headquarter is at IISc, Bangalore. The society also includes large numbers of local chapters across the country organizing local activities like seminars/workshops/conferences. Traditionally, the most significant activity of SBC(I) is its annual meeting hosted by different research labs/universities across the country. </p>
                    <p class="" style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #052a57; font-size: 20px  ;">
                        This year's annual meeting will be the 90<sup>th</sup>  event of the society. This 90<sup>th</sup> annual meeting under the theme “Metabolism to Drug Discovery: Where Chemistry and Biology Unite” will be organized by Amity University Haryana (AUH) between 16<sup>th</sup> and 19<sup>th</sup> December 2021. Amity University is one of the top private universities in India with several national and global campuses spread across all continents providing quality education close to 2 lac students. The Manesar (Gurugram), Haryana
                        campus is situated on 110 acres of lush green valley amongst Aravalli hills. <br> <br>The Amity Institute of Biotechnology (AIB), and Amity Institute of Integrative Sciences and Health (AIISH) are working hand in hand to successfully organize this year’s annual meeting. Due to the continuing COVID-19 pandemic, the meeting will be held entirely on a virtual platform. It is unfortunate that the attendees will miss the visit to the beautiful green campus of AUH, however, the virtual platform will attempt to provide you with a similar experience. The halls and lobbies of the virtual platform will be customised to provide the attendees a feeling of visiting the AUH.

                        <!-- <br> Amity Institute of Integrative Sciences and Health (AIISH) <br> Amity
                      Institute of Biotechnology (AIB) <br> Amity University Haryana, <br> Amity Education Valley <br> Gurugram - 122413, India -->


                    </p>
                    <p class="text-center mt-4">
                        <button class="btn btn-lg btn-success border  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/aib/" target="_blank" class="text-white "  rel="noopener noreferrer">Amity Institute of Biotechnology</a></button>
                        <button class="btn btn-lg btn-success border ml-md-5  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/central-instrument-research-facility.aspx" target="_blank" class="text-white" rel="noopener noreferrer">Central Instrument Research Facility</a></button>

                        <button class="btn btn-lg btn-success border ml-md-5  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/lipidomics-research-facility.aspx" target="_blank" class="text-white" rel="noopener noreferrer">Amity Lipidomics Research Facility</a></button>


                    </p>

                   
                    <!-- <button class="btn btn-lg btn-primary  " data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">Watch Video</button> -->
                    <!-- <button class="btn btn-lg btn-info   " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="#popupVideo" data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline">Watch Video</a></button>
                   -->
                    <div class="container">
  <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#form">
    See Modal with Form
  </button>   -->
  <!-- <p class="text-center">
   <button class="btn btn-lg btn-primary" class="btn btn-danger" data-toggle="modal" data-target="#form" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">Watch Video</button> 
  </p>
</div> -->

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <!-- <h5 class="modal-title" id="exampleModalLabel">Create Account</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
        <div class="modal-body">
         
    </div>
  </div>
</div>
                </p>

                    <!-- <div class=" content offset-4">


                      <ul>
                          <li>
                              <i class="bx bx-link-alt "></i>
                              <div>
                                  <h5>
                                      <a href="https://www.amity.edu/gurugram/aib/" target="_blank" rel="noopener noreferrer">https://www.amity.edu/gurugram/aib/</a>
                                  </h5>

                              </div>
                          </li>
                          <li>

                          </li>
                      </ul>


                  </div> -->




        </section>

        <section id="speakers">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Special Lectures</h2>   
                </div>
                <div class="row">
                <div class="col-lg-3 col-sm-2">
                    <!-- <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker/SBC_sekhar.jpg" alt="Speaker 1" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Shekhar Mande, Ph.D</a></h3>

                                    <div class="social">
                                    
                                        <p>DG, CSIR</p>
                                    </div>
                                </div>
                            </div> -->
                    </div>
                     <div class="col-lg-3 col-md-4">
                    <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker/SBC_sekhar.jpg" alt="Speaker 1" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Shekhar Mande, Ph.D</a></h3>

                                    <div class="social">
                                    
                                        <p>DG, CSIR</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                    <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker/SBC_Ram1.jpg" alt="Speaker 1" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Ram Vishwakarma, Ph.D</a></h3>

                                    <div class="social">
                                    
                                        <p>Advisor, CSIR and Chairman, Covid Strategy Group of CSIR</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- <div class="col-lg-3 col-md-4">
                    <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker/SBC_DNRao.jpg" alt="Speaker 1" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">D.N. Rao, Ph.D</a></h3>

                                    <div class="social">
                                    
                                        <p>IISc, Bangalore</p>
                                    </div>
                                </div>
                            </div>
                    </div> -->
                  
                </div>
            </div>

        </section>
              
        <!-- ======= Speakers Section ======= -->
        <section id="speakers">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Plenary speakers</h2>
                   
                </div>
                

                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_GPT.png" alt="Speaker 2" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">G. P. Talwar, Ph.D</a></h3>

                                <div class="social">
                                 
                                    <p>Director, Talwar Research Foundation</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Pad1.png" alt="Speaker 3" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">G. Padmanabhan, Ph.D</a></h3>

                                <div class="social">
                                 
                                    <p>Chancellor, Central University of Tamilnadu </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Bjrao1.png" alt="Speaker 3" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">B.J. Rao, Ph.D.</a></h3>

                                <div class="social">
                                  
                                    <p>Vice Chancellor, University of Hyderabad </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_DJch1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">D.J. Chattopadhyay, Ph.D</a></h3>

                                <div class="social">
                                 
                                    <p>Vice Chancellor, Sister Nivedita University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_Nag1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">V. Nagaraja, Ph.D</a></h3>

                                <div class="social">
                                  
                                    <p>Professor, IISc, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Par1.png" alt="Speaker 6" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Partha P. Majumder, Ph.D</a></h3>

                                <div class="social">
                                  
                                    <p>Professor, NIBMG, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Sank1.png" alt="Speaker 6" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">R. Sankarnarayanan, Ph.D</a></h3>

                                <div class="social">
                                  
                                    <p>Professor, CCMB, Hyderabad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_Gai1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Gaiti Hasan, Ph.D.</a></h3>

                                <div class="social">
                                   
                                    <p>Professor, NCBS, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_San1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sanjay Singh, Ph.D</a></h3>

                                <div class="social">
                                   
                                    <p>CEO, Gennova Biopharma</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_Anu1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                  Anurag Agrawal, Ph.D
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>Director, IGIB, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_Var1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">R. Varadarajan, Ph.D</a></h3>

                                <div class="social">
                                   
                                    <p>Professor, IISc, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section id="speakerss">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Speakers</h2>
                   
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Anuradha Chaudhary</a></h3>

                                <div class="social">
                                 
                                    <p>VB Patel Chest Clinic</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Ashwani Kumar</a></h3>

                                <div class="social">
                                 
                                    <p>IMTECH, Chandigarh</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rajiv Kumar</a></h3>

                                <div class="social">
                                 
                                    <p>BHU, Varanasi </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Adline Princy Salomon</a></h3>

                                <div class="social">
                                  
                                    <p>Sashtra University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Riddhiman Dhar</a></h3>

                                <div class="social">
                                 
                                    <p>IIT-Kharagpur</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">K Satyamoorthy</a></h3>

                                <div class="social">
                                  
                                    <p>Manipal University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Shaida Andrabi</a></h3>

                                <div class="social">
                                  
                                    <p>Kashmir University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Mohit K Jolly</a></h3>

                                <div class="social">
                                  
                                    <p>IISc, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Gargi Bagchi</a></h3>

                                <div class="social">
                                   
                                    <p>AUH, Gurugram</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Prathibha Ranganathan</a></h3>

                                <div class="social">
                                   
                                    <p>CHG, Bengaluru</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                  Ullas-Kolthur Seetaram
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>TIFR, Mumbai</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Kaustuv Datta</a></h3>

                                <div class="social">
                                   
                                    <p>DU SOUTH CAMPUS</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Arvind Ramanathan</a></h3>

                                <div class="social">
                                 
                                    <p>InStem, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Girish Ratnaparkhi</a></h3>
                                <div class="social">
                                    <p>IISER Pune</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">K. M. Sinha</a></h3>

                                <div class="social">
                                    <p>AUH, Gurugram </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Amirul Islam Mallick</a></h3>

                                <div class="social"> 
                                    <p>IISER, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Varsha Singh</a></h3>

                                <div class="social"> 
                                    <p>IISc, Bangaluru</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sobhan Sen</a></h3>

                                <div class="social">
                                  
                                    <p>JNU, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sudipta Basu</a></h3>

                                <div class="social">
                                  
                                    <p>IIT-Gandhinagar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">K Subramaniam</a></h3>

                                <div class="social">
                                  
                                    <p>IIT Chennai</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rupasri Ain</a></h3>

                                <div class="social">
                                   
                                    <p>IICB, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Anindya Ghosh Roy</a></h3>

                                <div class="social">
                                   
                                    <p>NBRC, Manesar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                Debojyoti Chakraborty
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>IGIB, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Monalisa Mukherjee</a></h3>

                                <div class="social">
                                   
                                    <p>AUUP, Noida</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rachna Chabba</a></h3>

                                <div class="social">
                                 
                                    <p>IISER Mohali</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Praveen Vemula</a></h3>

                                <div class="social">
                                 
                                    <p>InStem, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Swasti Raychaudhuri</a></h3>

                                <div class="social">
                                 
                                    <p>CCMB, Hyderabad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Siddesh S Kamat</a></h3>

                                <div class="social">
                                  
                                    <p>IISER, Pune</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Mausumi Mutsuddi</a></h3>

                                <div class="social">
                                 
                                    <p>BHU, Varanasi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rashna Bhandari</a></h3>

                                <div class="social">
                                  
                                    <p>CDFD, Hyderabad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Oishee Chakrabarti</a></h3>

                                <div class="social">
                                  
                                    <p>SINP, Kolkatas</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Aneesh kumar AG</a></h3>

                                <div class="social">
                                  
                                    <p>NII,New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rituparna Sinha Roy</a></h3>

                                <div class="social">
                                   
                                    <p>IISER Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rai Ajit Srivastava</a></h3>

                                <div class="social">
                                   
                                    <p>Wayne State University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                Ruchi Anand
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>IIT-Bombay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Dhirendra Katti</a></h3>

                                <div class="social">
                                   
                                    <p>IIT Kanpur</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Nitin Choudhary</a></h3>
                                <div class="social">
                                    <p>IIT Guwahati</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Vijay K Choudhary</a></h3>
                                <div class="social"> 
                                    <p>DU South campus</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Avinash Sonawane</a></h3>

                                <div class="social">                                
                                    <p>IIT Indore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Amit Awasthi</a></h3>

                                <div class="social">
                                  
                                    <p>THSTI, Faridabad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Jyothilaxmi Vadassery</a></h3>

                                <div class="social">
                                 
                                    <p>NIPGR, New delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Shyam Kumar Masakapalli</a></h3>

                                <div class="social">
                                  
                                    <p>IIT-Mandi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sumit Ghosh</a></h3>

                                <div class="social">
                                  
                                    <p>CIMAP, Lucknow</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Dhiraj Kumar</a></h3>

                                <div class="social">
                                  
                                    <p>ICGEB, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Ramesha Thimmappa</a></h3>

                                <div class="social">
                                   
                                    <p>AUUP, Noida</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sheetal Gandotra</a></h3>

                                <div class="social">
                                   
                                    <p>IGIB, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                Hiya Ghosh
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>NCBS, Bengaluru</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Karthik Raman</a></h3>

                                <div class="social">
                                   
                                    <p>IIT-Chennai</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">K B Hari Kumar</a></h3>
                                <div class="social">
                                    <p>RGCB, Thiruvananthapuram</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Ritu Kulshreshtha</a></h3>
                                <div class="social"> 
                                    <p>IIT Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Debabrata Biswas</a></h3>

                                <div class="social">                                
                                    <p>IICB, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Saikrishnan Kayarat</a></h3>

                                <div class="social">
                                  
                                    <p>IISER Pune</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Tavpritesh Sethi</a></h3>

                                <div class="social">
                                 
                                    <p>IIT Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Saikat Chakraborty</a></h3>

                                <div class="social">
                                  
                                    <p>IICB, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Rakesh S. Laishram</a></h3>

                                <div class="social">
                                  
                                    <p>RGCB, Thiruvananthapuram</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="300">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Tapas Manna</a></h3>

                                <div class="social">
                                  
                                    <p>IISER Thiruvananthapuram</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Kaustuv Sanyal</a></h3>

                                <div class="social">
                                   
                                    <p>JNCASR, Bengaluru</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Chandrima Das</a></h3>

                                <div class="social">
                                   
                                    <p>SINP, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                Senjuti Sinharoy
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>NIPGR , New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Tina Mukherjee</a></h3>

                                <div class="social">
                                   
                                    <p>Instem, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                      
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="200">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sam Mathew</a></h3>

                                <div class="social">
                                   
                                    <p>RCB, Faridabad</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-4">
                        <div class="speakerss" data-aos="fade-up" data-aos-delay="100">
                            <img src="#" alt="" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                Souvik Bhattacharjee
                                  </a></h3>

                                <div class="social">
                                 
                                    <p>JNU, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        
                    </div> 
                </div>
            </div>

        </section>

      
        <!-- End Speakers Section -->

        <!-- ======= Schedule Section ======= -->
        <section id="schedule" class="section-with-bg">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Event Schedule</h2>
                    <!-- <p>Here is our event schedule</p> -->
                </div>

                <ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
                    <li class="nav-item">
                        <a class="nav-link active" href="#day-1" role="tab" data-bs-toggle="tab">Day 1 (16.12.2021)</a>
                    </li>
                    <li class="nav-item mb-2">
                        <a class="nav-link" href="#day-2"  role="tab" data-bs-toggle="tab">Day 2 (17.12.2021)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#day-3"  role="tab" data-bs-toggle="tab">Day 3 (18.12.2021)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#day-4" role="tab" data-bs-toggle="tab">Day 4 (19.12.2021)</a>
                    </li>
                </ul>

                <!-- <h3 class="sub-heading">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius necessitatibus voluptatem quis labore perspiciatis quia.</h3> -->

                <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

                    <!-- Schdule Day 1 -->
                    <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 00 PM– 4:30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                            </div> -->
                                <h4>Inauguration of the meeting<span></span></h4>
                                <!-- <p>Facere provident incidunt quos voluptas.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4:30 PM – 5: 00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/images/SBC_sekhar.jpg" alt="Shekhar Mande, Ph.D DG, CSIR">
                            </div> -->
                                <h4>Special lecture (Shekhar Mande, DG, CSIR)<span></span></h4>
                                <p>CSIR for new Atma Nirbhar Bharat</p>
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM – 5: 30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4>Special lecture (Ram Vishwakarma,  Advisor, CSIR and Chairman, Covid Strategy Group of CSIR)<span></span></h4>
                                <p>Covid Mitigation Strategies of CSIR</p>
                            </div>
                        </div>

                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>11:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div>
                                <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                                <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div>
                                <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                                <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                    <!-- End Schdule Day 1 -->

                    <!-- Schdule Day 2 -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 11:00 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                            </div> -->
                                <h4>History of Biochemistry </h4>
                                <p>4 talks x 30 minutes each</p>
                                <p><strong>Chair: Umesh Varshney</strong></p>
                                <p><strong> GP Talwar, Talwar Research Foundation, New Delhi:</strong><br> Evolution of Biochemistry in Medical Institutions
                                    <br>
                                    <strong>G Padmanabhan, CUTN, Thiruvarur:</strong><br> Known Molecules: New Applications Stand on Shoulders of Giants<br>
                                    <strong>B.J. Rao, Hyd University, Hyderabad:</strong><br> Title TBD <br>
                                    <strong>D.J. Chattopadhyay, SNU, Kolkata:</strong><br> History of Biochemistry-Eastern India
                                </p>
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:00 AM-11:30 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4>P. B. Rama Rao Memorial Award Lecture</h4>
                                <p><strong>Chair: Rajendra Prasad</strong></p>
                                <p><strong>Samir K. Maji, IIT Bombay:</strong> Title TBD</p>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM-11:40 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4> Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:40 AM- 1:20 PM </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                            </div> -->
                            <!-- <span>Auditorium 01</span> -->
                                <span>Auditorium 01<br> Session 1 (5 talks x 20 min)</span>
                                    <h4 style="text-decoration:underline"> Microbiology and infectious disease biology I</h4>
                                    <p><strong>Chair: Abhik Saha</strong></p>
                                    <p><strong>Anuradha Chaudhary, VB Patel Chest Clinic, New Delhi:</strong><br> Antifungal resistance in Candida auris-an emerging threat</p>

                                    <p> <strong>Ashwani Kumar, IMTECH, Chandigarh:</strong><br> M tuberculosis biofilms in lungs protect resident bacilli from the host immune system and antimycobacterial agents</p>
                                    <p><strong>Rajiv Kumar, BHU, Varanasi:</strong><br> Targeting Type-1 IFNs to improve anti- parasitic immunity </p>
                                    <p><strong>Adline Princy Salomon, Sashtra University, Thanjavur: </strong><br>Novel 4-(Benzylamino) cyclohexyl 2-hydroxycinnamate hybrid drug: A next
                                    generation anti-virulence drug?
                                    </p>
                                    <p><strong>Riddhiman Dhar, IIT-Kharagpur:</strong><br>Transcription factor binding activity is the primary driver of noise in gene expression</p>
                            </div>
                          
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:40 AM- 1:20 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                            </div> -->
                                <!-- <span>Auditorium 02</span> -->
                                <span> Auditorium 02 <br>Session 2 (5 talks x 20 min) </span>
                                    <h4 style="text-decoration:underline">Cancer Biology I</h4>
                                    <p><strong>Chair: Bushra Ateeq</strong></p>
                            <p><strong>K Satyamoorthy, Manipal University, Manipal:</strong><br>DNA methylation and miRNA cross-talks during cervical cancer progression </p>
                            <p><strong>Shaida Andrabi, Kashmir University, Srinagar:</strong><br>Identification of novel kinases that overcome mitotic arrest and promote resistance against anti-cancer and anti-mitotic drugs</p>
                            <p><strong>Ritu Kulshreshtha, IIT Delhi:</strong><br>Deciphering the significance of EzH2:microRNA interactions in glioblastoma</p>
                            <p><strong>Gargi Bagchi, AUH, Gurugram:</strong><br>Novel pathways and molecular targets in prostate cancer</p>
                            <p><strong>Prathibha Ranganathan, CHG, Bengaluru:</strong><br>Understanding Castrate Resistant Prostate Cancer- A Transcriptome study</p>
                            
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:20 PM-3:00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                            </div> -->
                                <h4>Lunch and Mid-Day Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>2:30 PM to 3:00 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                            </div> -->
                                <h4>SBC (I) EC Meeting</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:00 PM – 4:00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                            </div> -->
                                <h4>Annual GB Meeting </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4:15 PM – 5:00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                            </div> -->
                                <p><strong>Chair: V. Nagaraja </strong></p>
                                <h4>Plenary lecture 1 (Partha Pratim Majumder, NIBMG, Kolkata) </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM– 6:00 PM </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> -->
                                <!-- <span>Auditorium 01</span> -->
                                <span> Auditorium 01 <br>Session 3 (3 talks x 20 min) </span>
                                    <h4 style="text-decoration:underline">Development and aging</h4>
                                    <p><strong>Chair: K. Subramaniam </strong></p>
                                    <p><strong> Anindya Ghosh Roy, NBRC, Manesar:</strong><br>Wnt signaling establishes the microtubule polarity in neuron through regulation of Kinesin-13</p>
                                    <p><strong> Rupasri Ain, IICB, Kolkata:</strong><br>COX6B2-directed mitochondrial function is pivotal in equipoising trophoblast stem cell self-renewal and differentiation</p>
                                    <p><strong>K. Subramaniam, IIT, Chennai:</strong><br>PLP-1 and gene silencing in the C. elegans germ line</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM – 6:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <!-- <span>Auditorium 02</span> -->
                               <span> Auditorium 02 <br>Session 4 (3 talks x 20 min)</span> 
                               <h4 style="text-decoration:underline"> Translational research and drug discovery I</h4>
                                    <p><strong>Chair: Pawan Dhar </strong></p>
                                    <p><strong> Praveen Vemula, InStem Bangalore:</strong><br>Biomimicry as a New Path for Drug Discovery: Metabolites Inspired Drug Development for the Treatment of Inflammatory Bowel Diseases.</p>
                                    <p><strong> Debojyoti Chakraborty, IGIB, Delhi:</strong><br>(Clustered regularly interspaced short palindromic repeats-CRISPR associated)</p>
                                    <p><strong> Monalisa Mukherjee, AUUP, Noida:</strong><br>Emergence of Heptazine-Based Graphitic Carbon Nitride within Hydrogel Nanocomposites for Scarless Healing of Burn Wounds</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>6:00 PM – 6:10 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div> -->
                                <h4>Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>6:10 PM – 7:30 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div> -->
                                <!-- <span>Auditorium 01</span> -->
                                <span> Auditorium 01 <br>Session 5 (4 talks x 20 min) </span>
                                <h4 style="text-decoration:underline">Microbiology and infectious disease biology II </h4>
                                <p><strong>Chair: Anil K. Tyagi</strong></p>
                                    <p><strong> Rachna Chaba, IISER Mohali :</strong><br>Elucidating the intricate interconnection between long-chain fatty acid metabolism and envelope homeostasis in Escherichia coli.</p>
                                    <p><strong> K.M. Sinha, AUH, Gurugram :</strong><br>Cyclic di-AMP mediates DNA damage repair in Mycobacterium</p>
                                    <p><strong> Amirul Islam Mallick, IISER Kolkata :</strong><br>A suicidal predator: Cost of bacterial predation via Type VI secretion system under environmental stress</p>
                                    <p><strong> Varsha Singh, IISc, Bangalore :</strong><br>Bacterial odors: A new twist in the tale of pattern recognition in animals</p>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>6:10 PM – 7:30 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                          </div> -->
                          <!-- <span>Auditorium 02</span> -->
                                <span>Auditorium 02 <br>Session 6 (4 talks x 20 min)</span>
                                    <h4 style="text-decoration:underline">Translational research and drug discovery II </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                                    <p><strong>Chair: Avinash Bajaj </strong></p>
                                    <p><strong> Sobhan Sen, JNU, New Delhi :</strong><br>A New Family of Solvatochromic Fluorescent Probes for Spectroscopy and Imaging of Lipid Membrane</p>
                                    <p><strong>Sudipta Basu, IIT Gandhinagar :</strong><br>Image and Impair Cellular Powerhouse</p>
                                    <p><strong> Rituparna Sinha Roy, IISER Kolkata :</strong><br>Emerging approaches for designing peptide based therapeutics</p>
                                    <p><strong> Ruchi Anand, IIT Bombay :</strong><br>Strategies to Combat Antibiotic Resistance</p>
                            </div>
                        </div>
                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>10:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                                </div>
                                <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                                <p>Facere provident incidunt quos voluptas.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>11:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div>
                                <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                                <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div>
                                <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                                <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                    <!-- End Schdule Day 2 -->

                    <!-- Schdule Day 3-->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 9:45 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div> --><p><strong>Chair: Vinay Nandicoori  </strong></p>
                                <h4>Plenary lecture 2 (V. Nagaraja, IISc, Bangalore) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                           
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:45 AM - 10:45 AM    </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> --><span>Auditorium 01 <br>Session 7 (3 talks x 20 min) </span>
                                <h4 style="text-decoration:underline">Translational research and drug discovery III</h4>
                                    <p><strong>Chair: Sobhan Sen </strong></p>
                                    <p><strong>Rai Ajit Srivastava, Espervita Therapeutics, USA:</strong><br>Discovery, mechanism of action, 
                                    and development of dual inhibitors of fatty acids and cholesterol synthesis for the treatment of
                                     cardiometabolic disease and NASH: A journey from discovery to FDA approval</p>
                                    <p><strong>Dhirendra Katti, IIT Kanpur:</strong><br>A synergistic combination of niclosamide and doxorubicin 
                                    as an efficacious and translatable therapy for all clinical subtypes of breast cancer</p>
                                    <p><strong>Nitin Chaudhary, IIT Guahati:</strong><br>Engineering native sequences into hydrogelators for biomedical applications</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:45 AM - 10:45 AM    </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <span>Auditorium 02 <br>Session 8 (3 talks x 20 min) </span>
                                <h4 style="text-decoration:underline">Metabolism and immunology I </h4>
                                   <p><strong>Chair: Dhiraj Kumar</strong></p>
                                    <p><strong> Vijay K. Chaudhary, DU South Campus, New Delhi :</strong><br>Phage-Displayed Human Antibody 
                                    Library - A resource for discovery of next-generation antibody therapeutics</p>
                                    <p><strong>Avinash Sonawane, IIT Indore:</strong><br>Role of ion channels in host immunity to Mycobacterium tuberculosis early and late infection</p>
                                    <p><strong>Amit Awasthi, THSTI, Faridabad:</strong><br>Salt triggers anti-tumor Immunity through modulation of gut microbiota</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:45 AM-11:00 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div> -->
                                <h4> Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:00 AM – 1:00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div> --><p><strong>Chair: Sudhanshu Vrati</strong></p>
                                <span>Session 9 (4 talks x 30 min)</span>
                                <h4 style="text-decoration:underline">Covid 19: An Indian perspective </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                                 <p><strong> Anurag Agrawal, IGIB, New Delhi:</strong><br>COVID19: Genome Surveillance</p>
                                    <p><strong> Sanjay Singh, Gennova Biopharma, Pune:</strong><br>Title TBD </p>
                                    <p><strong>R. Varadarajan, IISc, Bangalore :</strong><br>Immunogen design for COVID-19</p>
                                    <p><strong>Lalith Kishore, CCAMP, Bangalore:</strong><br>Title TBD</p>
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:00 PM - 2:00  PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                          </div> -->
                                <h4>Lunch and Mid-Day Break </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>2:00 PM – 5:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                        </div> -->
                                <h4>Poster session</h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM – 5:30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                      </div> -->
                                <h4>M Shadaksharaswamy Endowment Lecture Award </h4>
                                <p><strong>Chair: Rajendra Prasad</strong></p>
                                <p><strong>Suman K. Dhar, JNU, New Delhi:</strong><br>Unique biology and possible interventions for therapy against human malaria parasite Plasmodium falciparum</p>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <!-- <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM - 5:30 PM  </time></div>
                            <div class="col-md-9">
                                <div class="speaker">
                        <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                    </div>
                                <h4>Award lecture</h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:30 PM – 5:40 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                  </div> -->
                                <h4>Break</h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM   </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> --><span>Auditorium 01 <br>Session 10 (4 talks x 20 min)</span>
                                <h4 style="text-decoration:underline">Natural products and plant metabolites </h4>
                                    <p><strong>Chair: Supriya Chakraborty</strong></p>
                                    <p><strong> Jyothilakshmi Vadassery, NIPGR, New Delhi:</strong><br>Endophytic fungi Piriformospora indica recruits host-derived putrescine for growth promotion in plants</p>
                                    <p><strong> Shyam Kumar Masakapalli, IIT-Mandi:</strong><br>Phytochemical treasures from the Himalayas - profiling, pathway mapping to drug discovery</p>
                                    <p><strong> Sumit Ghosh, CIMAP, Lucknow:</strong><br>Unraveling the biosynthetic enzymes of medicinal diterpenes in kalmegh</p>
                                    <p><strong> Ramesha Thimmappa, AUUP, Noida:</strong><br>Probing hidden functional diversity in oxidosqualene cyclases for novel triterpene discovery and metabolic engineering.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <span>Auditorium 02 <br>Session 11 (4 talks x 20 min)</span>
                                <h4 style="text-decoration:underline">Metabolism and immunology II</h4>
                                <p><strong>Chair: Suman Dhar</strong></p>
                                    <p><strong>Dhiraj kumar, ICGEB, New Delhi:</strong><br>Molecular interface between human innate immune cells and Mycobacterium tuberculosis</p>
                                    <p><strong>Sheetal Gandotra, IGIB, New Delhi:</strong><br>Lipid droplets in macrophages: circuit breakers or amplifiers of inflammation</p>
                                    <p><strong>Girish Ratnaparkhi, IISER Pune:</strong><br>SUMOylation of Jun fine-tunes the Drosophila gut immune response</p>
                                    <p><strong>Karthik Raman, IIT-Chennai:</strong><br>Unravelling microbial interactions in the gut microbiome through computational approaches</p>
                            </div>
                        </div>
                        <!-- 
                        <div class="row schedule-item">
                            <div class="col-md-"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div>
 -->
                    </div>
                    <!-- End Schdule Day 4 -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-4">

                    <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 9:30 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                        </div> -->
                                <h4>C. R. Krishna Murti Award Lecture </h4>
                                <p><strong>Chair: Rajendra Prasad</strong></p>
                                <p><strong> Kausik Chattopadhyay, IISER Mohali:</strong><br>The (w)hole story of the b-barrel pore-formation mechanism of Vibrio cholerae cytolysin</p>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:30 AM – 10: 15 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                        </div> -->
                                <p><strong>Chair: Rakesh Bhatnagar </strong></p>
                                <h4>Plenary lecture 3 (R. Sankarnarayanan, CCMB, Hyderabad) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:15 AM – 11:15 AM </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                        </div> -->
                            <span>Auditorium 01 <br> Session 12 (3 talks x 20 min)</span>
                                <h4 style="text-decoration:underline">Cancer Biology II</h4>
                                    <p><strong>Chair: Benubrata Das</strong></p>
                                    <p><strong>KB Harikumar, RGCB, Trivandam:</strong><br>Targeting tumor and stromal crosstalk for pancreatic cancer therapy</p>
                                  
                                    <p><strong>Debabrata Biswas, IICB, Kolkata:</strong><br>Human FKBP5 negatively regulates transcription through inhibition of P-TEFb complex formation</p>
                                    <p><strong>Mohit K Jolly, IISc, Bangalore:</strong><br>Systems-level network modeling deciphers the master regulators of phenotypic plasticity and heterogeneity in melanoma </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:15 AM – 11:15 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                        </div> -->
                                <span>Auditorium 02 <br> Session 13 (3 talks x 20 min)</span>
                                <h4 style="text-decoration:underline"> Biochemistry at the digital era</h4>
                                    <p><strong>Chair: R. Shankarnarayan</strong></p>
                                    <p><strong> Tavpritesh Sethi, IIT Delhi:</strong><br>Data Fusion and AI for Pandemic Response</p>
                                    <p><strong>Saikrishnan Kayarat, IISER Pune:</strong><br>Mechanism of the ATP-dependent restriction endonuclease SauUSI that makes medically relevant Staphylococcus aureus strains untransformable</p>
                                    <p><strong>Saikat Chakraborty, IICB, Kolkata:</strong><br>Protein-protein interaction analysis: Sequence, Structure, and Network perspectives</p>
                            </div>
                        </div>



                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:15 AM-11:30 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                      </div> -->
                                <h4>Break </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM - 1:10 PM </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                      </div> --><span>Auditorium 01 <br>Session 14 (5 talks x 20 min) </span>
                                <h4 style="text-decoration:underline">Molecular biology and metabolism I</h4>
                                    <p><strong>Chair: Arnab Mukhopadhyay</strong></p>
                                    <p><strong>Swasti Raychaudhuri, CCMB, Hyderabad:</strong><br>The good, the bad, and the ugly states of α-SYNUCLEIN Inclusions</p>
                                    <p><strong>Kaustuv Sanyal, JNCASR, Bangalore:</strong><br>Centromere specification</p>
                                    <p><strong>Tapas Manna, IISER Trivandam:</strong><br>Molecular Drivers of Chromosomal Stability in Cancer Cells</p>
                                    <p><strong>Rakesh S. Laishram, RGCB:</strong><br>Processing at the 3’-untranslated RNA: Implications in cardiac gene regulation</p>                                   
                                    <p><strong>Ullas-Kolthur Seetaram, TIFR:</strong><br>Title TBD</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM - 1:10 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                      </div> -->
                                <span>Auditorium 02 <br>Session 15 (5 talks x 20 min)</span>
                                <h4 style="text-decoration:underline"> Molecular cell signaling I</h4>
                                    <p><strong>Chair: Rashna Bhandari</strong></p>
                                    <p><strong>Souvik Bhattacharjee, JNU, New Delhi:</strong><br>GETting the Plasmodium falciparum proteins thERe by their TAil</p>
                                    <p><strong>Senjuti Sinharoy, NIPGR, New Delhi:</strong><br>Divergent evolution and chemistry drive symbiotic nitrogen fixation in peanut</p>
                                    <p><strong>Tina Mukherjee, InStem, Bangalore:</strong><br>Myeloid cells: sensors and regulators of animal physiology</p>
                                    <p><strong>Sam Mathew, RCB, Faridabad:</strong><br>The Wnt Signaling Pathway as a Therapeutic Target to Treat RhabdomyosarcomaTumors</p>                                   
                                    <p><strong>Kaustav Datta, DU South Campus, New Delhi:</strong><br>Regulation of mitochondrial gene expression in response to nutritional cues in Saccharomyces cerevisiae: Tale of accessory factor involved in mitochondrial translation elongation</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:10 PM – 2:15 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                        <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                    </div> -->
                                <h4>Lunch and Mid-Day Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>2:15 PM – 3:45 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                  </div> -->
                                <h4> Poster session </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:45 PM – 4:30 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                  </div> --><p><strong>Chair: K. Natarajan</strong></p>
                                <h4> Plenary lecture 4 (Gaiti Hasan, NCBS) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 30 PM – 5:50 PM     </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                      </div> --><span> Auditorium 01 <br>Session 16 (4 talks x 20 min)</span>
                                <h4 style="text-decoration:underline">Molecular biology and metabolism II</h4>
                                    <p><strong>Chair: Ullas Kolthur Seetaram</strong></p>
                                    <p><strong>Arvind Ramanathan, InStem, Bangalore:</strong><br>Title TBD</p>
                                    <p><strong>Chandrima Das, SINP:</strong><br>Histone readers in stress adaptation through metabolic reprogramming: Implications in human diseases</p>
                                    <p><strong>Siddhesh S Kamat, IISER Pune:</strong><br>Mapping sphingolipid pathways during phagocytosis</p>
                                    <p><strong>Aneeshkumar AG, NII, New Delhi:</strong><br>Systemic ablation of vitamin D receptor leads to skeletal muscle glycogen storage disorder in mice</p>  
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 30 PM – 5:50 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                      </div> -->
                                <span>Auditorium 02 <br>Session 17 (4 talks x 20 min) </span>
                                <h4 style="text-decoration:underline"> Molecular cell signaling II</h4>
                                    <p><strong>Chair: Chinmay Mukhopadhyay</strong></p>
                                    <p><strong>Mausumi Mutsuddi, BHU, Varanasi:</strong><br>Unraveling the Molecular Mechanisms of Human Neurological Disorders: Drosophila degenerates for a good cause</p>
                                    <p><strong>Rashna Bhandari, CDFD, Hyderabad:</strong><br>A high energy phosphate jump – from pyrophospho-inositol to pyrophospho-serine</p>
                                    <p><strong>Hiya Ghosh, NCBS, Bangalore:</strong><br>Maintaining order in the adult brain: making new neurons and keeping well the adult ones</p>
                                    <p><strong>Oishee Chakrabarti, SINP, Kolkata: :</strong><br>Organellar dynamics regulate cellular surveillance</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:50 PM – 6:00 PM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div> -->
                                <h4>Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>6:00 PM - 6:30 PM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> -->
                                <h4>valedictory session</span>
                                </h4>
                                <!-- <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p> -->
                            </div>
                        </div>
                        <!-- 
                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                                </div>
                                <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                                <p>Facere provident incidunt quos voluptas.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>
 -->
                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                </div>

            </div>

        </section>
        <!-- End Schedule Section -->

        <!-- ======= Venue Section ======= -->
        <!-- <section id="venue">

            <div class="container-fluid" data-aos="fade-up">

                <div class="section-header">
                    <h2>Event Venue</h2>
                    <p>Event venue location info and gallery</p>
                </div>

                <div class="row g-0">
                    <div class="col-lg-6 venue-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0"
                            style="border:0" allowfullscreen></iframe>
                    </div>

                    <div class="col-lg-6 venue-info">
                        <div class="row justify-content-center">
                            <div class="col-11 col-lg-8 position-relative">
                                <h3>Downtown Conference Center, New York</h3>
                                <p>Iste nobis eum sapiente sunt enim dolores labore accusantium autem. Cumque beatae ipsam. Est quae sit qui voluptatem corporis velit. Qui maxime accusamus possimus. Consequatur sequi et ea suscipit enim nesciunt quia velit.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container-fluid venue-gallery-container" data-aos="fade-up" data-aos-delay="100">
                <div class="row g-0">

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/1.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/1.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/2.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/2.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/3.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/3.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/4.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/4.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/5.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/5.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/6.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/6.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/7.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/7.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/8.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/8.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </section> -->
        <!-- End Venue Section -->

        <!-- ======= Hotels Section ======= -->
        <!-- <section id="hotels" class="section-with-bg">

            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Hotels</h2>
                    <p>Her are some nearby hotels</p>
                </div>

                <div class="row" data-aos="fade-up" data-aos-delay="100">

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/1.jpg" alt="Hotel 1" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 1</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                            </div>
                            <p>0.4 Mile from the Venue</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/2.jpg" alt="Hotel 2" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 2</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill-half-full"></i>
                            </div>
                            <p>0.5 Mile from the Venue</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/3.jpg" alt="Hotel 3" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 3</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                            </div>
                            <p>0.6 Mile from the Venue</p>
                        </div>
                    </div>

                </div>
            </div>

        </section> -->
        <!-- End Hotels Section -->

        <!-- ======= Gallery Section ======= -->
        <!-- <section id="gallery">

            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Gallery</h2>
                    <p>Check our gallery from the recent events</p>
                </div>
            </div>

            <div class="gallery-slider swiper-container">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/1.jpg" class="gallery-lightbox"><img src="assets/img/gallery/1.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/2.jpg" class="gallery-lightbox"><img src="assets/img/gallery/2.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/3.jpg" class="gallery-lightbox"><img src="assets/img/gallery/3.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/4.jpg" class="gallery-lightbox"><img src="assets/img/gallery/4.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/5.jpg" class="gallery-lightbox"><img src="assets/img/gallery/5.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/6.jpg" class="gallery-lightbox"><img src="assets/img/gallery/6.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/7.jpg" class="gallery-lightbox"><img src="assets/img/gallery/7.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/8.jpg" class="gallery-lightbox"><img src="assets/img/gallery/8.jpg" class="img-fluid" alt=""></a>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>

        </section> -->
        <!-- End Gallery Section -->
        <section id="commite">
            <div class="container-fluid ">
                <div class="row ">
                    <div class="col-12 mt-4">
                        <h3 class="text-center " style="color: aliceblue;">
                            ORGANIZING COMMITTEE
                        </h3>
                        <!-- <h3 class="text-left mt-2 ">
                          90th Annual meeting, 2021.
                      </h3> -->
                    </div>
                    <div class="col-md-5 col-12 offset-md-2 ">
                        <h5 style="font-size: 25px; color: aliceblue;">Patron </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Ashutosh Sharma, Secretary, DST.
                            </li>
                            <li>
                                G. Padmanaban, President, NASI.
                            </li>
                            <li>
                                Shekhar Mande, Director General, CSIR.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            National Advisory Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Hari Mishra, BARC, Mumbai.
                            </li>
                            <li>
                                K. Natarajan, JNU, New Delhi.
                            </li>
                            <li>
                                L.S. Shashidhara, Ashoka University, Sonepat, IISER, Pune.
                            </li>
                            <li>
                                Rakesh Bhatnagar, Amity University Rajasthan, Jaipur.
                            </li>
                            <li>
                                Sanjay Singh, Gennova Biopharmaceuticals Ltd, Pune.
                            </li>
                            <li>
                                Subhra Chakraborty, NIPGR, New Delhi.
                            </li>
                            <li>
                                Tapas K. Kundu, CDRI, Lucknow.
                            </li>
                            <li>
                                V. Nagaraja, IISc, Bangalore.
                            </li>
                            <li>
                            Vijay K. Chaudhary, DU, South Campus, New Delhi.
                            </li>

                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            National Scientific Programme Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                          	Abhik Saha, Presidency University, Kolkata.
                            </li>
                            <li>
                            Arnab Mulkhopadhyay, NII, New Delhi.
                            </li>
                            <li>
                           	Avinash Bajaj, RCB, Faridabad.
                            </li>
                            <li>
                           	Benubrata Das, IACS, Kolkata.
                            </li>
                            <li>
                            Bushra Ateeq, IIT, Kanpur.
                            </li>
                            <li>
                           	Chandrima Das, SINP, Kolkata.
                            </li>
                            <li>
                           	Ellora Sen, NBRC, Manesar.
                            </li>
                            <li>
                           	Jitendra Thakur, ICGEB, New Delhi.
                            </li>
                            <li>
                           	Pawan Dhar, JNU, New Delhi.
                            </li>
                            <li>
                           	Supriya Chakraborty, JNU, New Delhi.
                            </li>
                            <!-- <li>
                                Ellora Sen, NBRC.
                            </li> -->
                            <li>
                            Vinay K. Nandicoori, CCMB, Hyderabad.
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-5 col-12 ">

                        <!-- <h5 class="mt-2">
                          Organizing Committee (Amity University Haryana)
                      </h5> -->
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Convenor
                        </h5>
                        <ul style="font-size: 20px;">
                            <li class="">Rajendra Prasad.</li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Organizing Secretary.
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Machiavelli Singh.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Programme Coordinators
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>Kaustav Bandyopadhyay.</li>
                            <li>
                                Ujjaini Dasgupta.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Treasurer
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Nitai Debnath.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Publication Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Saif Hameed.
                            </li>
                            <li>
                                Zeeshan Fatima.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Virtual Poster Management Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Munindra Ruwali.
                            </li>
                            <li>
                                Sumistha Das.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Virtual Scientific Session Management Committee
                        </h5>
                        <ul style="font-size: 17px;">
                            <li>Amresh Prakash.</li>
                            <li>Atanu Banerjee.</li>
                            <li> Chandramani Pathak.</li>
                            <li>Deepa Suhag.</li>
                            <li> Gargi Bagchi.</li>
                            <li>Jinny Tomar.</li>
                            <li>Ravi Dutta Sharma.</li>
                            <li>Sangeeta Kumari.</li>
                            <li>
                                Vaibhav Kapuria.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


        </section>

        
        <section class="bg_reg mt-2" id="bg_reg">
            <div class="container">
            <div class="row">
    <div class="col-md-12 col-12 col-xl-12">
<h1 class="text-white">
Registration
</h1>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
All prospective participants are expected to be members of the Society of Biological Chemists. Those who are not currently the members, it is advisable to take the SBC membership prior to registering for the conference.
<br>
The registration to the conference is mandatory to attend all the Virtual Sessions and to be eligible to present research work in the form of posters or short talks. It is mandatory to complete the registration process and the payment of the registration fee, i.e. INR 500 latest by Nov 15th, 2021. 
</p>

<h3 class="text-white">Registration Fee includes:</h3>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
-Unique login information allowing registrants to participate in meeting activities. <br>
-All live oral presentations including plenary and concurrent sessions. <br>
-Live poster presentations with one-on-one interaction with the presenter.<br>
-PDF of the abstract book.<br>
-Snaps at the Photobooth

</p>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
Registration fees are being charged to cover custom platform development, testing, subscription costs and licence for the conference application (Zoom/MS teams), creating and maintaining a website, Live technical support including support for dry runs, conference hosting, and other administrative costs.
</p>
<h1 class="text-white">
Abstract Submission guidelines:
</h1>
<h4 class="text-white">
Abstract preparation guidelines (A template is provided separately):
</h4>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
1.	The abstract should be prepared in MS Word in .doc or .docx on A4-sized page format. <br>
2.	Total printable word count should not exceed 250 words (excluding title, author names and address). Also please make sure the full abstract does NOT go beyond one page.<br>
3.	Abstract should be written in Times New Roman font.<br>
4.	Abstract text title, author names, author address and images should be centre-aligned. The main abstract text should be justified.<br>
5.	Font size should be 12 points for the main text and 16 point for the title.<br>
6.	Presenting author names should be underlined. Corresponding author name should be marked with an asterisk (*) in superscript.<br>
7.	Email address of presenting author and the corresponding author should be provided.<br>
8.	All addresses should be italicized.

</p>
<h1 class="text-white">
Poster submission guidelines:
</h1>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
Please provide a A4 sized poster (PDF only). The size should not be more than 5 mb.</p>
<div class="col-md-12 col-12 col-xl-12 text-center">
                      <a target="_blank" href="https://www.amity.edu/gurugram/sbci2021" rel="noopener noreferrer"><button class="btn btn-lg btn-success border" >Register Now</button></a>      
         
                            </div>
                            <p class="mt-3" style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
                            You can submit your abstract and/or poster after registration. Further instructions will be sent to your registered email. </p>
    </div>
</div>

            </div>
                <div class="container-fluid "  id="important_note1">

                    <div class="row">
                        <div class="col-12">
                            <h1  class="text-center text-white">
                            IMPORTANT DATES:
                            </h1>
                        </div>
                        <div class="col-md-4 col-xl-4 col-12 offset-md-2 ">
                            <h3 class="text-white">
                            REGISTRATION OPEN 
                            </h3>
                            <h3 class="text-white">
                            REGISTRATION FEE 	
                            </h3>
                            <h4 class="text-white">
                            ABSTRACT/POSTER SUBMISSION DEADLINE 	
                            </h4>   
                            <h4 class="text-white">
                            LATE REGISTRATION FEE WITHOUT ABSTRACT 
                            </h4>   
                            <h3 class="text-white">
                            FINAL REGISTRATION CLOSES 	
                            </h3>              
                        </div>
                        <div class="col-md-6 col-xl-6 col-12 mt-2 ">
                            <h3 class="text-white">
                            – 5th September 2021
                          
                            </h3>
                            <h3 class="text-white">
                            - INR 500
                            </h3>
                            <h3 class="text-white">
                            - 30th November 2021
                            </h3>
                            <h3 class="text-white">
                            – INR 1000
                            </h3>
                            <h3 class="text-white">
                            – 1st December 2021
                            </h3>
                        </div>

                           
                            <!-- <div class="col-md-12 col-12 col-xl-12 text-center">
                      <a target="_blank" href="https://www.amity.edu/gurugram/sbci2021" rel="noopener noreferrer"><button class="btn btn-lg btn-success border" >Register Now</button></a>      
         
                            </div> -->
                            
                                      </div>

                                      
                </div>

                <section id="important_note" class="d-md-none d-xl-none d-sm-block">
            <div class="container-fluid">

<div class="row">
    <div class="col-md-12 text-center">
        <img src="assets/images/AMity.png" class="img-fluid"   alt="" srcset="">
    </div>
    
    <!-- <div class="col-12">
        <h1  class="text-center text-white">
        IMPORTANT DATES:
        </h1>
    </div>
    <div class="col-md-6 col-xl-6 offset-md-4 col-12 ">
<p class="text-white">
REGISTRATION OPEN   <span class="text_margin" >
– 5th September 2021
</span>
    </p>
<p class="text-white">
REGISTRATION FEE 	<span class="text_margin">
- INR 500
</span>
</p>
<p class="text-white">
ABSTRACT/POSTER SUBMISSION DEADLINE 	 <span class="text_margin">
- 15th November 2021
</span>
</p>   
<p class="text-white">
LATE REGISTRATION FEE WITHOUT ABSTRACT <span class="text_margin">
– INR 1000
</span>
</p>   
<p class="text-white">
FINAL REGISTRATION CLOSES 	 <span class="text_margin">
– 1st December 2021
</span>
</p>               -->
</div>
            </section> 
            </section>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center col-12">
            <h1>Countdown Timer</h1>
<div id="clockdiv">
  <div>
    <span id="day"></span>
    <div class="smalltext">Days</div>
  </div>
  <div>
    <span id="hour"></span>
    <div class="smalltext">Hours</div>
  </div>
  <div>
    <span id="minute"></span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span id="second"></span>
    <div class="smalltext">Seconds</div>
  </div>
</div>

            </div>
        </div>
    </div>
</section>
           
        <!-- ======= Supporters Section ======= -->
        <section id="supporters" class="section-with-bg">

            <div class="container-fluid" data-aos="fade-up">
                <div class="section-header">
                    <h2>Sponsors</h2>
                </div>

                <div class="row no-gutters supporters-wrap clearfix" data-aos="zoom-in" data-aos-delay="100">
                    <div class="col-lg-1 col-md-1"></div>
                    <div class="col-lg-2 col-md-2 col-xs-6">
                        <div class="supporter-logo">
                        <a href="https://gennova.bio/" target="_blank" rel="noopener noreferrer"><img src="assets/images/logo_below.png" class="img-fluid" alt="" srcset=""></a> 
                            <!-- <img src="assets/images/logo_below.png" class="img-fluid" alt=""> -->
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-xs-6">
                        <div class="supporter-logo">
                        <a href="https://www.kewaunee.in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/Kewaunee (General).jpg"  class="img-fluid" alt="" srcset=""></a>  
                            <!-- <img src="assets/images/Kewaunee (General).jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-xs-6">
                        <div class="supporter-logo" style="padding:30px">
                        <a href="https://www.mpbio.com/in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/MP logo.jpg" class="img-fluid" alt="" srcset=""></a>
                            <!-- <img src="assets/images/MP logo.jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-xs-6">
                        <div class="supporter-logo">
                          <a href="https://www.eppendorf.com/IN-en/" target="_blank" rel="noopener noreferrer"><img src="assets/images/eppendorf_logo_web.jpg" class="img-fluid" alt="" srcset=""></a>   

                            <!-- <img src="assets/images/eppendorf_logo_web.jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-xs-6">
                        <div class="supporter-logo">
                          <a href="https://sciex.com/applications" target="_blank" rel="noopener noreferrer"><img src="assets/images/SCIEX Logo 2019.png" class="img-fluid" alt="" srcset=""></a>   

                            <!-- <img src="assets/images/eppendorf_logo_web.jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1"></div>
                    

                    <!-- <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/5.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/6.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/7.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/8.png" class="img-fluid" alt="">
                        </div>
                    </div> -->

                </div>

            </div>

        </section>
    

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="section-bg">

            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Contact Us</h2>
                    <!-- <p>Nihil officia ut sint molestiae tenetur.</p> -->
                </div>

                <div class="row contact-info">

                    <div class="col-md-4">
                        <div class="contact-address">
                            <i class="bi bi-geo-alt"></i>
                            <h3>Address</h3>
                            <address> 
                              Amity Institute of Integrative Sciences and Health (AIISH)<br> Amity Institute of Biotechnology (AIB)<br> Amity University Haryana, <br> Amity Education Valley<br> Gurugram - 122413, India
                              <br>
                              <strong>Phone:</strong> <br>
                            +91-9996633376 <br>+91-8800275382 <br>
                              <strong>Email:</strong><a href="mailto:amitysbci2021@gmail.com" target="_blank"> amitysbci2021@gmail.com</a> <br>
                          </address>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-phone">
                            <i class="bi bi-phone"></i>
                            <h3>Phone Number</h3>
                            <p><br>
                            +91-9996633376 <br>+91-8800275382 </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-email">
                            <i class="bi bi-envelope"></i>
                            <h3>Email</h3>
                            <p><a href="mailto:amitysbci2021@gmail.com" target="_blank">amitysbci2021@gmail.com</a></p>
                        </div>
                    </div>

                </div>

                <div class="form">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                            </div>
                            <div class="form-group col-md-6 mt-3 mt-md-0">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>

            </div>
        </section>
        <!-- End Contact Section -->

    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <img src="assets/images/PNG Logo.png" alt="TheEvenet">
                        <p>

                            Amity Institute of Integrative Sciences and Health (AIISH)<br> Amity Institute of Biotechnology (AIB)<br> Amity University Haryana, <br> Amity Education Valley<br> Gurugram - 122413, India
                            <br>

                        </p>
                    </div>

                    <!-- <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div> -->

                    <div class="col-lg-6 col-md-6 footer-links">
                        <h4>SBCI 2021</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#hero">Home</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#about">About us</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#speakers">Speakers</li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#commite">Organizing committee  </a></li>
                            <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="#bg_reg">Registration
                            </a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#supporters">Sponsors </a></li>
                        </ul>

              
                  
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>

                            <strong>Phone:</strong> <br>
                            +91-9996633376 <br>+91-8800275382 
                            <strong>Email:</strong>amitysbci2021@gmail.com<br>
                        
                        </p>

                        <div class="social-links">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>Coact</strong>. All Rights Reserved
            </div>

        </div>
    </footer>
    <!-- End  Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
<script>


const day=document.getElementById('day')
const hour=document.getElementById('hour')
const minute=document.getElementById('minute')
const second=document.getElementById('second')

setInterval(() => {
    

const currDate=Date.now()
const wishDate=new Date("16 december 2021 00:00:00")
const diff=(wishDate-currDate)
const dd=(Math.floor(diff/1000/60/60/24))
const hh=(Math.floor(diff/1000/60/60)%24)
const mm=(Math.floor(diff/1000/60)%60)
const ss=(Math.floor(diff/1000)%60)
day.innerHTML=dd<10?"0"+dd:dd
hour.innerHTML=hh<10?"0"+hh:hh
minute.innerHTML=mm<10?"0"+mm:mm
second.innerHTML=ss<10?"0"+ss:ss
if (dd>100) {
    document.getElementById('day-box').style.marginRight="30px"
}
}, 1000);



    function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}





// function initializeClock(id, endtime) {
//   var clock = document.getElementById(id);
//   var daysSpan = clock.querySelector('.days');
//   var hoursSpan = clock.querySelector('.hours');
//   var minutesSpan = clock.querySelector('.minutes');
//   var secondsSpan = clock.querySelector('.seconds');

//   function updateClock() {
//     var t = getTimeRemaining(endtime);

//     daysSpan.innerHTML = t.days;
//     hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
//     minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
//     secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

//     if (t.total <= 0) {
//       clearInterval(timeinterval);
//     }
//   }

//   updateClock();
//   var timeinterval = setInterval(updateClock, 1000);
// }

// var deadline = new Date(Date.parse(new Date()) +  224 * 7 *60 *60 *1000);
// initializeClock('clockdiv', deadline);

$(function() {
    $('.popup-youtube, .popup-vimeo').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
});
</script>
</body>

</html>