<?php
class Event
{
  private $ds;

  private $video_id;
  private $user_id;
  private $video_title;
  private $video_url;

  private $vidtable = 'tbl_videos';
  private $vidviewtable = 'tbl_videosviews';

  private $usertable = 'tbl_users';

  function __construct()
  {
    $this->ds = new DataSource();
  }
  public function __get($property)
  {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value)
  {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }

    return $this;
  }


  public function getCountries()
  {
    $query = 'Select * from countries order by name';
    $paramType = '';
    $paramValue = array();

    $list = $this->ds->select($query, $paramType, $paramValue);
    return $list;
  }

  public function getStates($cntry)
  {
    $query = 'Select * from states where country_id=? order by name';
    $paramType = 's';
    $paramValue = array($cntry);

    $list = $this->ds->select($query, $paramType, $paramValue);
    return $list;
  }

  public function getCities($state)
  {
    $query = 'Select * from cities where state_id=? order by name';
    $paramType = 's';
    $paramValue = array($state);

    $list = $this->ds->select($query, $paramType, $paramValue);
    return $list;
  }

  public function getCountry($country)
  {
    $query = 'Select name from countries where id=?';
    $paramType = 's';
    $paramValue = array($country);

    $country = $this->ds->select($query, $paramType, $paramValue);
    return $country[0]['name'];
  }

  public function getState($state)
  {
    $query = 'Select name from states where id=?';
    $paramType = 's';
    $paramValue = array($state);

    $state = $this->ds->select($query, $paramType, $paramValue);
    return $state[0]['name'];
  }

  public function getCity($city)
  {
    $query = 'Select name from cities where id=?';
    $paramType = 's';
    $paramValue = array($city);

    $city = $this->ds->select($query, $paramType, $paramValue);
    return $city[0]['name'];
  }

  public function getVideos()
  {
    $query = 'Select video_id, video_title, views from ' . $this->vidtable . ' order by video_title';
    $paramType = '';
    $paramValue = array();

    $videos = $this->ds->select($query, $paramType, $paramValue);

    return $videos;
  }

  public function getVideo()
  {
    $query = 'Select video_title, video_url, views from ' . $this->vidtable . ' where video_id=?';
    $paramType = 's';
    $paramValue = array($this->video_id);

    $video = $this->ds->select($query, $paramType, $paramValue);

    return $video;
  }

  public function updateVideoView()
  {
    $video = $this->getVideo();
    $views = $video[0]['views'] + 1;

    $query = 'Update ' . $this->vidtable . ' set views=? where video_id = ?';
    $paramType = 'ss';
    $paramValue = array(
      $views,
      $this->video_id
    );

    $this->ds->execute($query, $paramType, $paramValue);

    $viewtime   = date('Y/m/d H:i:s');
    $query = "Insert into " . $this->vidviewtable . "(video_id, user_id, view_time) values(?, ?, ?)";
    $paramType = 'sss';
    $paramValue = array(
      $this->video_id,
      $this->user_id,
      $viewtime
    );

    $viewid = $this->ds->insert($query, $paramType, $paramValue);
    return $viewid;
  }

  public function getVideoViewers()
  {
    $query = 'Select distinct(' . $this->vidviewtable . '.user_id ), first_name, last_name, emailid from ' . $this->vidviewtable . ', ' . $this->usertable . ' where video_id=? and ' . $this->vidviewtable . '.user_id = ' . $this->usertable . '.userid';
    $paramType = 's';
    $paramValue = array($this->video_id);

    $viewerList = $this->ds->select($query, $paramType, $paramValue);

    return $viewerList;
  }
}
