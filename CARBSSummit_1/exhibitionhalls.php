<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/exhibitionhall.jpg">
            <div id="exhVideo">
                <iframe src="https://player.vimeo.com/video/577914632?autoplay=1&loop=1&muted=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
            </div>
            <!-- <a href="?autoplay=1" id="exhVideo" class="viewvideo vidview" data-vidid="1234">
                <div class="indicator d-6"></div>
            </a> -->


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    $(function() {
        $('.vidview').on('click', function() {
            var vid_id = $(this).data('vidid');
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId: '<?= $userid ?>'
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>