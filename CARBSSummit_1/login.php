<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  if ($_POST['emailid'] === '') {
    $errors['email'] = 'Email ID is required';
  }

  $emailid = $_POST['emailid'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $event_title ?></title>
  <link rel="stylesheet" href="assets/css/normalize.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/all.min.css">
  <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
  <div class="container reg-content">
    <div class="row">
      <div class="col-12 col-lg-9 mx-auto">
        <div class="row pt-4">
          <div class="col-4 col-md-3 p-3 text-center">
            <img src="assets/img/endorsed-by.png" class="img-fluid" alt="">
          </div>
          <div class="col-4 col-md-2 offset-md-2 text-center p-3">
            <img src="assets/img/logo-carbs.png" class="img-fluid" alt="">
          </div>
          <div class="col-4 col-md-3 offset-md-2 text-center p-3">
            <img src="assets/img/logo-aace.png" class="img-fluid" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-6 col-md-6 col-lg-4 mx-auto">
        <img src="assets/img/CARBs.png" class="img-fluid" alt="">
      </div>
    </div>
    <div class="register-wrapper mx-4">
      <div class="row">
        <div class="col-12 col-md-8 mx-auto p-2">
          <h5 class="text-center my-2">Log in to the Second virtual symosium of CARBS Summit</h5>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 mx-auto p-2">
          <?php
          if (count($errors) > 0) : ?>
            <div class="alert alert-danger alert-msg">
              <ul class="list-unstyled">
                <?php foreach ($errors as $error) : ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>
          <?php if ($succ != '') { ?>
            <div class="alert alert-success alert-msg">
              <?= $succ ?>
            </div>
          <?php } ?>
          <form method="POST" class="mt-4">
            <div class="row mt-3 mb-1">
              <div class="col-12">
                <label>Email ID</label>
                <input type="text" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
              </div>
            </div>
            <div class="row mt-4 mb-3">
              <div class="col-12">
                <input type="image" src="assets/img/btn-login.png" value="Submit" />
                <br><br>
                If not registered, <a href="register.php">click here</a>
              </div>
            </div>
        </div>
        </form>
      </div>
      <div class="row">
        <div class="col-12">
          <img src="assets/img/bottom-banner.png" class="img-fluid" alt="" />
        </div>
      </div>
    </div>
  </div>


  </div>
  <?php require_once 'ga.php';  ?>
  <?php require_once 'footer.php';  ?>