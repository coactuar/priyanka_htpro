<?php

use Genseer\User;

require_once '../lib/config.php';
require_once '../functions.php';


if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {

            /* Get name of attendee*/
        case 'getAttendeeName':

            $user_id = $_POST["user"];
            $user = new User();
            $name = $user->getMemberName($user_id);

            echo $name;

            break;

            /* Count no. of online attendees*/
        case 'getliveattcount':

            $user = new User();
            $total_live_attendees = $user->getLiveAttendeesCount();
            if ($total_live_attendees == '0') {
                $total_live_attendees = 1;
            }

            echo '<b>Online Attendees:</b> ' . $total_live_attendees;

            break;

            /* Count no. of online attendees on current page*/
        case 'getonpageatt':
            $curr_room = $_POST['room'];
            $user = new User();
            $total_onthispage = $user->getPageAttendeesCount($curr_room);
            if ($total_onthispage == '0') {
                $total_onthispage = 1;
            }

            echo '<b>In this room:</b> ' . $total_onthispage;

            break;

            /* Update logout time and current room location of user*/
        case 'updateevent':

            $curr_room = $_POST['room'];
            $id = $_SESSION["user_id"];

            $user = new User();
            $curr_status = $user->updateMemberLoginStatus($id, $curr_room);

            echo $curr_status;

            break;

            /*Get list of online attendees*/
        case 'getonlineattendees':

            $you = $_SESSION["user_emailid"];
            $keyword = $_POST['key'];

            $online = new User();
            $list = $online->getonlinemembers($keyword);

            if (!empty($list)) {
                foreach ($list as $user) {
                    //echo $user['first_name'];
?>
                    <div class="attendee">
                        <div class="name"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                        <div class="desig"><?php echo $user['designation']; ?></div>
                        <div class="company"><?php echo $user['company']; ?></div>
                        <?php if ($user['emailid'] != $you) { ?>
                            <div class="attendee-actions">
                                <button type="button" class="btn-chat" data-to="<?php echo $user['id']; ?>" data-from="<?php echo $_SESSION['user_id']; ?>"><i class="far fa-comment-alt"></i></button>
                                <button type="button" class="btn-email" data-to="<?php echo $user['id']; ?>" data-from="<?php echo $_SESSION['user_id']; ?>"><i class="far fa-envelope"></i></button>
                            </div>
                        <?php } ?>
                    </div>
<?php
                }
            } else {
                echo 'There is no attendee matching your search criteria';
            }

            break;

            /*send email to attendee*/
        case 'sendEmail':

            $user_to_id = $_POST["to"];
            $user_from_id = $_POST["from"];
            $message = $_POST["msg"];

            $user = new User();
            $curr_status = $user->sendEmail($user_from_id, $user_to_id, $message);

            echo 'succ';

            break;

            /* get list of  downloads done by attendee*/
        case 'getAttendeesDown':

            $userid = $_POST['to'];

            $userDown = new User();
            $downloads = $userDown->getMemberDownloads($userid);

            if (!empty($downloads)) {
                $output = "<ul>";
                foreach ($downloads as $dl) {
                    $output .= "<li>" . $dl['resource_title'] . "</li>";
                }
                $output .= "</ul>";

                echo $output;
            } else {
                echo 'You have not downloaded anything yet.';
            }


            break;

            /* get list of  videos viewed by attendee*/
        case 'getAttendeesVideos':

            $userid = $_POST['to'];

            $userVid = new User();
            $videos = $userVid->getMemberVideos($userid);

            if (!empty($videos)) {
                $output = "<ul>";
                foreach ($videos as $vid) {
                    $output .= "<li>" . $vid['video_title'] . "</li>";
                }
                $output .= "</ul>";

                echo $output;
            } else {
                echo 'You have not viewed any video yet.';
            }


            break;
    } //switch

}


?>