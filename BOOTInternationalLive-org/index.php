<?php
include('commons/header.php');
?>
<?php
//use Genseer\User;
require_once "model/config.php";
require_once 'model/functions.php';

$loginEmail = '';
$errors = [];

$succ = false;

if (isset($_POST['mainlogin-btn'])) {
  if (empty($_POST['loginEmail'])) {
    $errors['email'] = 'Email ID is required';
  }

  $loginEmail = $_POST['loginEmail'];

  if (count($errors) === 0) {

    $member = new User();
    $response = $member->loginMember($loginEmail);
    $errors['msg'] = $response;
  }
}
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $event_title; ?></title>
  <link rel="stylesheet" href="assects/css/bootstrap.min.css">
  <link rel="stylesheet" href="assects/css/all.min.css">
  <link rel="stylesheet" href="assects/css/styles.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
  <div class="container-fluid mx-auto">

  <div id="login-area">
      <div class="row">
        <div class="col-12 col-md-6 col-lg-7 ">
          <div class="login-bg mb-3">
            <img src="assects/img/login-banner.png" alt="" class="img-fluid" />
          </div>
          <div id="loginform-area">
            If already registered, login here:
            <form method="post">
              <?php
              if (count($errors) > 0) : ?>
                <div class="alert alert-danger alert-msg">
                  <ul class="list-errors">
                    <?php foreach ($errors as $error) : ?>
                      <li>
                        <?php echo $error; ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif;
              ?>
              <div class="form-group">
                <!-- <label for="loginEmail">Enter your Email ID</label> -->
                <input type="text" name="loginEmail" placeholder="Enter Email ID" class="input" value="<?php echo $loginEmail; ?>">
              </div>
              <div class="form-group">
                <input type="submit" name="mainlogin-btn" id="btnLogin" class="btn btn-login" value="">
              </div>
            </form>
          </div>

        </div>
        <div class="col-12 col-md-6 col-lg-5 text-center">
        <iframe class="" src="https://player.vimeo.com/video/536240918?autoplay=1&amp;loop=1&amp;muted=1 " width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>


        </div>
      </div>
    </div>
  </div>
  <div id="code">IPL/O/BR/09042021</div>



  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $(document).ready(function() {
      /*$('#message').hide();
    $("#loginForm").submit(function(event){
		submitForm();
		return false;
	});*/

    });

    function submitForm() {
      $.ajax({
        type: "POST",
        url: "chklogin.php",
        cache: false,
        data: $('form#loginForm').serialize(),
        success: function(response) {
          if (response == 'login') {
            $("#login-modal").modal('hide');
            location.href = 'lobby.php';
          } else if (response == '0') {
            $('#message').text('You are not registered').removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          } else {
            $('#message').text(response).removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          }
        },
        error: function() {
          alert("Error");
        }
      });
    }
  </script>
 
</body>

</html>