<?php 
$timezone = 'Asia/Kolkata';
date_default_timezone_set($timezone);

	/*Security*/
	define('SECRET_KEY', 'thisisareallylongsecretkey');
	define('API_KEY', 'maheshkiapikey');
	
	/*Data Type*/
	define('BOOLEAN', 	'1');
	define('INTEGER', 	'2');
	define('STRING', 	'3');

	/*Error Codes*/
	define('REQUEST_METHOD_NOT_VALID',		        100);
	define('REQUEST_CONTENTTYPE_NOT_VALID',	        101);
	define('REQUEST_NOT_VALID', 			        102);
    define('VALIDATE_PARAMETER_REQUIRED', 			103);
	define('VALIDATE_PARAMETER_DATATYPE', 			104);
	define('API_NAME_REQUIRED', 					105);
	define('API_PARAM_REQUIRED', 					106);
	define('API_DOES_NOT_EXIST', 					107);
	define('INVALID_USER',       					108);
	define('USER_NOT_ACTIVE', 						109);
	define('USER_NOT_VERIFIED',						110);

	define('SUCCESS_RESPONSE', 						200);
	define('ERROR_RESPONSE', 						202);

	/*Server Errors*/

	define('JWT_PROCESSING_ERROR',					300);
	define('ATHORIZATION_HEADER_NOT_FOUND',			301);
	define('ACCESS_TOKEN_ERRORS',					302);
	define('INVALID_API_KEY', 						303);	
	define('MAIL_NOT_SENT', 						304);	
    
    
    /*Mail*/
    define('MAIL_HOST', 	    					'mail.genseer.com');
    define('MAIL_USERNAME', 						'webinars@genseer.com');
    define('MAIL_PWD', 			        			'GensWebinars@#$1234');
    define('MAIL_NAME', 				    		'Genseer Webinars');
    define('MAIL_VERIFYURL', 						'http://localhost:4200/confirm-email/');
    
    /*$mail_host = 'mail.genseer.com';
    $mail_username = 'webinars@genseer.com';
    $mail_pwd = 'GensWebinars@#$1234';
    $mail_name = 'Genseer Webinars';
    $mail_verifyurl = 'https://webinars.genseer.com/verify.php';
    */

function throwError($code, $message){
    header("Access-Control-Allow-Origin: *");
    header("content-type: application/json");
    $errMsg = json_encode(['response' => ['status'=>$code, 'message'=>$message]]);
    return $errMsg;
    exit;

}

function returnResponse($code, $data){
    header("content-type: application/json");
    header("Access-Control-Allow-Origin: *");
    $response = json_encode(['response' => ['status'=>$code, "result" => $data]]);
    echo $response;
    exit;

}
