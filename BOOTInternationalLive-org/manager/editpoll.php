<?php
require_once "../controls/sesAdminCheck.php";
require_once "../functions.php";

$poll_id=0;
$poll = new Poll();
if(isset($_GET['p'])){
    $poll_id = $_GET['p'];
    $valid = $poll->isValidPoll($poll_id);
    
    if(!$valid)
    {
        header('location: polls.php');
    }
}
$curr_poll = $poll->getPoll($poll_id);
$errors = [];
$succ = false;


$sessid = $curr_poll[0]['session_id'];
$pollques = $curr_poll[0]['poll_question'];
$opt1 = $curr_poll[0]['poll_opt1'];
$opt2 = $curr_poll[0]['poll_opt2'];
$opt3 = $curr_poll[0]['poll_opt3'];
$opt4 = $curr_poll[0]['poll_opt4'];
$ans = $curr_poll[0]['correct_ans'];

if(isset($_POST['updques-btn'])){
    
  if (($_POST['sessionid'] == '0')) {
        $errors['session'] = 'Select Session';
  }
  if (empty($_POST['pollques'])) {
        $errors['pollques'] = 'Poll Question is required';
  }
  if (empty($_POST['opt1'])) {
        $errors['opt1'] = 'Option 1 is required';
  }
  if (empty($_POST['opt2'])) {
        $errors['opt2'] = 'Option 2 is required';
  }
  if (($_POST['corrans'] == '0')) {
        $errors['ans'] = 'Select Correct Answer';
  }
    
  $sess = $_POST['sessionid'];
  $pollques = $_POST['pollques'];
  $opt1 = $_POST['opt1'];
  $opt2 = $_POST['opt2'];
  if(isset($_POST['opt3'])){
    $opt3 = $_POST['opt3'];
  }
  if(isset($_POST['opt4'])){
    $opt4 = $_POST['opt4'];
  }
  $ans = $_POST['corrans'];
  
  
  if(count($errors) == 0){  
    $poll = new Poll();
    $editPoll = $poll->editPoll($poll_id);
    if($editPoll > 0){
        $succ = true;
    }
  }
}
?>
<!doctype html>
<html> 
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Edit Poll Question</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-md bg-light">
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="users.php">Registered Users</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="sessions.php">Webcast Sessions</a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="polls.php">Polls</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div>
</nav>
<div class="container-fluid bg-white color-grey">
   
   <div class="row mt-2">
        <div class="col-12 col-md-8 offset-md-2">
            <h6>Edit Poll Question</h6>
            <?php
                if (count($errors) > 0): ?>
                <div class="alert alert-danger">
                  <ul>
                  <?php foreach ($errors as $error): ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                  <?php endforeach;?>
                  </ul>
                </div>
              <?php endif;
              ?>
              <?php
                if ($succ){ 
              ?>
                <div id="registration-confirmation">
                      <div class="alert alert-success">
                      Poll Question has been updated successfully!
                      </div>
                      
                  <a href="polls.php">Continue to Poll Questions List</a> 
                  </div>
                
              <?php 
                }
                else{
              ?>
            <div id="poll-message"></div>
            <form id="add-poll" method="post" action="">
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="corrans">Select Session<sup class="req">*</sup></label>
                      <select id="sessionid" name="sessionid" required class="input">
                          <option value="0">Select Session</option>
                          <?php
                            $sess = new Session();
                            $sessList = $sess->getSessionList();
                            if(!empty($sessList)){
                                foreach($sessList as $session){
                          ?>
                          <option value="<?php echo $session['session_id']; ?>" <?php if($sessid == $session['session_id']){ echo 'selected'; }?> ><?php echo $session['session_title']; ?></option>
                          <?php
                                }
                            }
                          ?>
                      </select>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <label for="pollques">Poll Question<sup class="req">*</sup></label>
                <textarea class="input" id="pollques" name="pollques" rows="3" required><?php echo $pollques; ?></textarea>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt1">Option 1<sup class="req">*</sup></label>
                      <input type="text" class="input" id="opt1" name="opt1" value="<?php echo $opt1; ?>" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt2">Option 2<sup class="req">*</sup></label>
                      <input type="text" class="input" id="opt2" name="opt2" value="<?php echo $opt2; ?>"  required>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt3">Option 3</label>
                      <input type="text" class="input" id="opt3" name="opt3"  value="<?php echo $opt3; ?>" >
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="opt4">Option 4</label>
                      <input type="text" class="input" id="opt4" name="opt4"  value="<?php echo $opt4; ?>" >
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="corrans">Correct Answer<sup class="req">*</sup></label>
                      <select id="corrans" name="corrans" required class="input">
                          <option value="0">Select Correct Answer</option>
                          <option value="opt1" <?php if($ans == 'opt1'){ echo 'selected'; }?>>Option 1</option>
                          <option value="opt2" <?php if($ans == 'opt2'){ echo 'selected'; }?>>Option 2</option>
                          <option value="opt3" <?php if($ans == 'opt3'){ echo 'selected'; }?>>Option 3</option>
                          <option value="opt4" <?php if($ans == 'opt4'){ echo 'selected'; }?>>Option 4</option>
                      </select>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                    <sup class="req">*</sup> marked fields are reqqired.<br>
                        <label for="">&nbsp;</label>
                      <input type="submit" name="updques-btn" id="submit" class="form-submit btn-submit" value="Update">
                    </div>
                </div>
              </div>
            </form>
            <?php } ?>
        </div>
   </div>
    
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>