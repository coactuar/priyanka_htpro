<?php
require_once "../controls/sesAdminCheck.php";
require_once "../functions.php";

$errors = [];
$succ = false;

$audi = '0';
$title = '';
$short = '';
$long = '';
$start = '';
$day = 'day1';
$url = '';

if (isset($_POST['addsession-btn'])) {

  if (($_POST['audi'] == '0')) {
    $errors['audi'] = 'Select Auditorium';
  }
  if (empty($_POST['title'])) {
    $errors['title'] = 'Session Title required';
  }


  $audi = $_POST['audi'];
  $title = $_POST['title'];
  if (isset($_POST['shortdes'])) {
    $short = $_POST['shortdes'];
  }
  if (isset($_POST['longdes'])) {
    $long = $_POST['longdes'];
  }
  $start = $_POST['start_time'];
  $day = $_POST['day'];
  if (isset($_POST['webcasturl'])) {
    $url = $_POST['webcasturl'];
  }


  if (count($errors) == 0) {
    $session = new Session();
    $addSession = $session->addSession();
    if ($addSession > 0) {
      $succ = true;
    }
  }
}

?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Add Session</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
  <nav class="navbar navbar-expand-md bg-light">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="users.php">Registered Users</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="sessions.php">Webcast Sessions</a>
        </li>

      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>
  <div class="container-fluid bg-white color-grey">
    <div class="row mt-1">
      <div class="col-12 col-md-8 offset-md-2">

        <div id="addsession-area" class="form">
          <h2>Add a Session</h2>
          <?php
          if (count($errors) > 0) : ?>
            <div class="alert alert-danger">
              <ul>
                <?php foreach ($errors as $error) : ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif;
          ?>
          <?php
          if ($succ) {
          ?>
            <div id="registration-confirmation">
              <div class="alert alert-success">
                Session has been added successfully!
              </div>
              <br>
              <a href="addsession.php">Add another Session</a><br>
              <a href="sessions.php">Continue to Session List</a>
            </div>

          <?php
          } else {
          ?>
            <form action="" method="post" role="form">
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>Start Time<sup class="req">*</sup></label>
                    <?php
                    $sdate = date('Y-m-d\TH:i', strtotime($start));
                    ?>
                    <input type="datetime-local" id="start_time" name="start_time" class="input" value="<?php if ($start != '') {
                                                                                                          echo $sdate;
                                                                                                        } ?>" required>
                    <?php //if($start !='') {echo date_format($start,"d-m-y H:i");} 
                    ?>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>Select Day</label>
                    <select name="day" id="day" class="input" required>
                      <option value="day1">Day 1</option>
                      <option value="day2">Day 2</option>
                      <option value="day2">Day 3</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <label>Select Auditorium<sup class="req">*</sup></label>
                    <select name="audi" id="audi" class="input" required>
                      <option value="0">Select Auditorium</option>
                      <?php
                      $audi = new Session();
                      $audiList = $audi->getAudiList();
                      if (!empty($audiList)) {
                        foreach ($audiList as $auditorium) {
                      ?>
                          <option value="<?php echo $auditorium['audi_id']; ?>"><?php echo $auditorium['audi_name']; ?></option>
                      <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-8">
                  <div class="form-group">
                    <label>Session Title<sup class="req">*</sup></label>
                    <input type="text" id="title" name="title" class="input" value="<?php echo $title; ?>" autocomplete="off" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label>Description (Short)</label>
                    <textarea type="text" id="shortdes" name="shortdes" rows="2" class="input" value="<?php echo $short; ?>" autocomplete="off"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label>Description (Long)</label>
                    <textarea type="text" id="longdes" name="longdes" rows="6" class="input" value="<?php echo $long; ?>" autocomplete="off"></textarea>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-12 col-md-12">
                  <div class="form-group">
                    <label>Webcast URL</label>
                    <textarea type="text" id="webcasturl" name="webcasturl" rows="1" class="input" value="<?php echo $url; ?>" autocomplete="off"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-12">
                  <div class="form-group">
                    <input type="submit" name="addsession-btn" id="submit" class="form-submit btn-submit" value="Add Session" />
                    <a href="sessions.php" class="btn-cancel">Cancel</a>
                  </div>
                </div>
              </div>
            </form>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

</body>

</html>