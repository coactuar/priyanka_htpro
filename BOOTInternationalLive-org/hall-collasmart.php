<?php
require_once "controls/sesCheck.php";
require_once "functions.php";

$exhibit_hall_id = 'collasmart';
$exhibitor_id = 'fbf5f764f8e3098d2a905b5c9705fe4a71c7a354a5fed927';

$halls = new Hall();
$verify = $halls->verifyHallId($exhibitor_id);
if (empty($verify)) {
    header('location: exhibition-halls');
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Collasmart :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

</head>

<body>
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <div class="navbar-collapse">

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area">
                <div id="background-image" class="exhibit-hall-godrej-bg" style="background-image:url(img/hall-collasmart.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item back-button">
                        <button class="btn btn-back" onclick="history.back()"><i class="fas fa-arrow-alt-circle-left"></i> Back</button>
                    </div>
                    <div class="overlay-item collasmart-tv-area">

                    </div>

                    <a class="overlay-item collasmart-samples-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subSampleReq"></a>
                    <a class="overlay-item collasmart-products-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subProductsReq"></a>
                    <a class="overlay-item collasmart-literature-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subLitReq"></a>

                    <a class="overlay-item collasmart-brodl-area dl-brochure res-dl" data-docid="409c74f17c17c7ed777ace91ff2c702c5f0eee18ff933c50" href="resources/collasmart-brochure.pdf" target="_blank" download data-userid="<?php echo $_SESSION['user_id']; ?>" id="dlBro"></a>

                    <div class="overlay-item collasmart-banner-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/collasmart-a.jpg"></a>
                        </div>
                    </div>


                </div>
                <?php require_once "bottom-navmenu.php" ?>



            </div>
        </div>

    </div>

    <?php require_once "commons.php" ?>
    <?php require_once "hallcommons.php" ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.popupoverlay.js"></script>
    <script type="text/javascript" src="lightbox/html5lightbox.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>

    <script data-loc="<?php echo $exhibitor_id; ?>" src="js/site.js?v=2"></script>
    <script data-hall="<?php echo $exhibitor_id; ?>" src="js/exhibit-hall.js"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    <script>


    </script>

</body>

</html>