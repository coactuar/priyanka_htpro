<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Amity gallery final render Edit 200.jpg">
            <a href="assets/img/Clickable poster session/1st row 9th right last/poster_1.jpg" id="exhVideo" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/1st row left top/poster2.jpg" id="exhVideo1" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row 5th centre/poster3.jpg" id="exhVideo2" class="view" >
                <div class="indicator d-6"></div>
            </a>
             <a href="assets/img/Clickable poster session/2nd row 8th centre/poster4.jpg" id="exhVideo3" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo4" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo5" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo6" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo7" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo8" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo9" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo10" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo11" class="view" >
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="assets/resources/2.Sample Posters - KB_RGM_new grant  -  Compatibility Mode.pdf" id="exhVideo" class="showpdf"></a> -->

            <a href="bondk.php" id="bondk">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="colsmartA.php" id="colsmartA">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="lizolid.php" id="lizolid">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="stiloz.php" id="stiloz">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamp.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <div id="next-button">
                <a href="posterseession.php"><i class="fas fa-arrow-alt-circle-right"></i> Next</a>
            </div> -->
        </div>
     
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>