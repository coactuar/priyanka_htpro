<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>SBCI 2021</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/images/Logo_icon.png" rel="icon">
    <link href="assets/images/Logo_icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">


</head>
<style>
   
.m-0 {
    padding: 0;
    margin: 0 0 0 0 !important;
}


#clockdiv{
	font-family: sans-serif;
	color: #fff;
	display: inline-block;
	font-weight: 100;
	text-align: center;
	font-size: 30px;
}

#clockdiv > div{
	padding: 10px;
	border-radius: 3px;
	background: #00BF96;
	display: inline-block;
}

#clockdiv div > span{
	padding: 15px;
	border-radius: 3px;
	background: #00816A;
	display: inline-block;
}

.smalltext{
	padding-top: 5px;
	font-size: 16px;
}

#gennovaimg{
    margin-left:19%;
}

#kewauneeimg{
    width: 30%;
    height: 55%;
    margin-left:5px;
}

#mpimg{
    width: 10%; 
    height: 55%;
    margin: 0px 18px
}

#eppendorfimg{
    width: 30%;
    height: 55%;
}
.plh{
  text-align: center;
}

.annual{
    text-align: left;
}

.yellow{
    color:yellow;
}
@media only screen and (max-width: 768px) {
    #gennovaimg{
    margin-left:15%;
}
}


</style>
<body>

    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center ">
        <div class="container-fluid container-xxl d-flex align-items-center">

            <div id="logo" class="me-auto">
                <!-- Uncomment below if you prefer to use a text logo -->
                <!-- <h1><a href="index.html">The<span>Event</span></a></h1>-->
                <a href="http://sbcihq.in/" target="_blank" class="scrollto"><img src="assets/images/logo_right.png" alt="" width="100%" title=""></a>
            </div>

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">About</a></li>
                    <li><a class="nav-link scrollto" href="#speakers">Speakers</a></li>
                    <li><a class="nav-link scrollto" href="#schedule">Programs</a></li>
                    <li><a class="nav-link scrollto" href="#commite">Organizing committee</a></li>
                    <li><a class="nav-link scrollto" target="_blank" href="#bg_reg">Registration</a></li>
                    <!-- <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li> -->
                    <li><a class="nav-link scrollto" href="#supporters">Sponsors</a></li>
                    <!-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
          <ul>
            <li><a href="#">Drop Down 1</a></li>
            <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
              <ul>
                <li><a href="#">Deep Drop Down 1</a></li>
                <li><a href="#">Deep Drop Down 2</a></li>
                <li><a href="#">Deep Drop Down 3</a></li>
                <li><a href="#">Deep Drop Down 4</a></li>
                <li><a href="#">Deep Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="#">Drop Down 2</a></li>
            <li><a href="#">Drop Down 3</a></li>
            <li><a href="#">Drop Down 4</a></li>
          </ul>
        </li> -->
                    <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- .navbar -->
            <a class="buy-tickets scrollto" target="_blank" href="https://www.amity.edu/gurugram/"><img src="assets/images/PNG Logo.png" alt="" width="150px" srcset=""></a>

        </div>
    </header>
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
            <div class="heroleft">
                <p class="m-0">Registration Soon!!! Early bird registration closing on <span class="blink_me yellow"> 15.11.2021</span> (Fee INR 500+GST)</p>
                <p class="m-0">Registration fee after 15.11.2021 will be INR 1000+GST</p>
            </div>
            <div class="heroright m-0">
                <p class="m-0">Last date of poster</p>
                <p class="m-0">Submission: <span class="blink_me"> 15.11.2021</span> </p>
            </div>
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a> -->
            <!-- <a href="#about" class="about-btn scrollto">About The Event</a> -->
            <img class ="heroimg" src="assets/images/Picture_text.jpg" width="100%" alt="" srcset="">
        </div>
    </section>
    <!-- End Hero Section -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <!-- <div class="col-lg-5">
                        <h2>About The Event</h2>
                        <p>Sed nam ut dolor qui repellendus iusto odit. Possimus inventore eveniet accusamus error amet eius aut accusantium et. Non odit consequatur repudiandae sequi ea odio molestiae. Enim possimus sunt inventore in est ut optio sequi
                            unde.
                        </p>
                    </div> -->
                    <div class="col-lg-4 col-md-4 col-sm-3 text-left">
                        <h3 class="ml-2 text-center">Platinum Partner</h3>
                     <a href="https://gennova.bio/" target="_blank" rel="noopener noreferrer"><img src="assets/images/logo_below.png" id="gennovaimg" alt="" srcset=""></a>   
            
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-7 text-center">
                        <h3 class="ml-2">Gold Partner</h3>
                       
                     <a href="https://www.kewaunee.in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/Kewaunee (General).jpg" id="kewauneeimg" alt="" srcset=""></a>   
                     <a href="https://www.mpbio.com/in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/MP logo.jpg" id="mpimg" alt="" srcset=""></a>   
                     <a href="https://www.eppendorf.com/IN-en/" target="_blank" rel="noopener noreferrer"><img src="assets/images/eppendorf_logo_web.jpg" id="eppendorfimg" alt="" srcset=""></a>   

            
                    </div>
                    <!-- <div class="col-lg-2">
                        <h3>When</h3>
                        <p>Monday to Wednesday<br>10-12 December</p>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- End About Section -->
        <section class=" ">
            <div class="container">
                <h3 class="text-center mt-2 " style="color: #194880; text-align: justify;">About Us</h3>
                <!-- <hr class="hr"> -->
                <img src="assets/images/side.png.jpg" class="w-100" alt="">
                <div class="">
                    <!-- <h2>About</h2> -->

                    <p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #052a57; font-size: 20px  ;">The Society of Biological Chemists (SBC), India is one of the oldest scientific fraternities in India. The SBC(I) has more than 4000 members at present, including senior scientists in the field of life sciences, young researchers and students. SBC(I) headquarter is at IISc, Bangalore. The society also includes large numbers of local chapters across the country organizing local activities like seminars/workshops/conferences. Traditionally, the most significant activity of SBC(I) is its annual meeting hosted by different research labs/universities across the country. </p>
                    <p class="" style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #052a57; font-size: 20px  ;">
                        This year's annual meeting will be the 90<sup>th</sup>  event of the society. This 90<sup>th</sup> annual meeting under the theme “Metabolism to Drug Discovery: Where Chemistry and Biology Unite” will be organized by Amity University Haryana (AUH) between 16<sup>th</sup> and 19<sup>th</sup> December 2021. Amity University is one of the top private universities in India with several national and global campuses spread across all continents providing quality education close to 2 lac students. The Manesar (Gurugram), Haryana
                        campus is situated on 110 acres of lush green valley amongst Aravalli hills. <br> <br>The Amity Institute of Biotechnology (AIB), and Amity Institute of Integrative Sciences and Health (AIISH) are working hand in hand to successfully organize this year’s annual meeting. Due to the continuing COVID-19 pandemic, the meeting will be held entirely on a virtual platform. It is unfortunate that the attendees will miss the visit to the beautiful green campus of AUH, however, the virtual platform will attempt to provide you with a similar experience. The halls and lobbies of the virtual platform will be customised to provide the attendees a feeling of visiting the AUH.

                        <!-- <br> Amity Institute of Integrative Sciences and Health (AIISH) <br> Amity
                      Institute of Biotechnology (AIB) <br> Amity University Haryana, <br> Amity Education Valley <br> Gurugram - 122413, India -->


                    </p>
                    <p class="text-center mt-4">
                        <button class="btn btn-lg btn-success border  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/aib/" target="_blank" class="text-white "  rel="noopener noreferrer">Amity Institute of Biotechnology</a></button>
                        <button class="btn btn-lg btn-success border ml-md-5  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/central-instrument-research-facility.aspx" target="_blank" class="text-white" rel="noopener noreferrer">Central Instrument Research Facility</a></button>

                        <button class="btn btn-lg btn-success border ml-md-5  " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="https://www.amity.edu/gurugram/lipidomics-research-facility.aspx" target="_blank" class="text-white" rel="noopener noreferrer">Amity Lipidomics Research Facility</a></button>


                    </p>

                   
                    <!-- <button class="btn btn-lg btn-primary  " data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">Watch Video</button> -->
                    <!-- <button class="btn btn-lg btn-info   " style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;"><a href="#popupVideo" data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline">Watch Video</a></button>
                   -->
                    <div class="container">
  <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#form">
    See Modal with Form
  </button>   -->
  <!-- <p class="text-center">
   <button class="btn btn-lg btn-primary" class="btn btn-danger" data-toggle="modal" data-target="#form" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">Watch Video</button> 
  </p>
</div> -->

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <!-- <h5 class="modal-title" id="exampleModalLabel">Create Account</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
        <div class="modal-body">
         
    </div>
  </div>
</div>
                </p>

                    <!-- <div class=" content offset-4">


                      <ul>
                          <li>
                              <i class="bx bx-link-alt "></i>
                              <div>
                                  <h5>
                                      <a href="https://www.amity.edu/gurugram/aib/" target="_blank" rel="noopener noreferrer">https://www.amity.edu/gurugram/aib/</a>
                                  </h5>

                              </div>
                          </li>
                          <li>

                          </li>
                      </ul>


                  </div> -->




        </section>
        <!-- ======= Speakers Section ======= -->
        <section id="speakers">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Confirmed Speakers</h2>
                    <!-- <p>Here are some of our speakers</p> -->
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_Vijay1.png" alt="Speaker 1" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">K. Vijayraghavan, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Principal Scientific Advisor, Govt. of India</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_GPT.png" alt="Speaker 2" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">G. P. Talwar, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Director, Talwar Research Foundation</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Pad1.png" alt="Speaker 3" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">G. Padmanabhan, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Chancellor, Central University of Tamilnadu </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Bjrao1.png" alt="Speaker 3" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">B.J. Rao, Ph.D.</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Vice Chancellor, University of Hyderabad </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_DJch1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">D.J. Chattopadhyay, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Vice Chancellor, Sister Nivedita University</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_Nag1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">V. Nagaraja, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Professor, IISc, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Par1.png" alt="Speaker 6" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Partha P. Majumder, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Professor, NIBMG, Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/images/speaker_bg/SBC_Sank1.png" alt="Speaker 6" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">R. Sankarnarayanan, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Professor, CCMB, Hyderabad</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_Gai1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Gaiti Hasan, Ph.D.</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Professor, NCBS, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_San1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">Sanjay Singh, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>CEO, Gennova Biopharma</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="100">
                            <img src="assets/images/speaker_bg/SBC_Anu1.png" alt="Speaker 4" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">
                                  Anurag Agrawal, Ph.D
                                  </a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Director, IGIB, New Delhi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="speaker" data-aos="fade-up" data-aos-delay="200">
                            <img src="assets/images/speaker_bg/SBC_Var1.png" alt="Speaker 5" class="img-fluid">
                            <div class="details">
                                <h3><a href="speaker-details.html">R. Varadarajan, Ph.D</a></h3>

                                <div class="social">
                                    <!-- <a href=""><i class="bi bi-twitter"></i></a>
                                    <a href=""><i class="bi bi-facebook"></i></a>
                                    <a href=""><i class="bi bi-instagram"></i></a>
                                    <a href=""><i class="bi bi-linkedin"></i></a> -->
                                    <p>Professor, IISc, Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- End Speakers Section -->

        <!-- ======= Schedule Section ======= -->
        <section id="schedule" class="section-with-bg">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Event Schedule</h2>
                    <!-- <p>Here is our event schedule</p> -->
                </div>

                <ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
                    <li class="nav-item">
                        <a class="nav-link active" href="#day-1" role="tab" data-bs-toggle="tab">Day 1</a>
                    </li>
                    <li class="nav-item mb-2">
                        <a class="nav-link" href="#day-2"  role="tab" data-bs-toggle="tab">Day 2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#day-3"  role="tab" data-bs-toggle="tab">Day 3</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#day-4" role="tab" data-bs-toggle="tab">Day 4</a>
                    </li>
                </ul>

                <!-- <h3 class="sub-heading">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius necessitatibus voluptatem quis labore perspiciatis quia.</h3> -->

                <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

                    <!-- Schdule Day 1 -->
                    <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 00 PM– 4:30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                            </div> -->
                                <h4>Meet and greet <span></span></h4>
                                <!-- <p>Facere provident incidunt quos voluptas.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4:30 PM – 5: 15 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4>Special lecture <span></span></h4>
                                <p>K. Vijayraghavan, PSA to Prime Minister of India</p>
                            </div>
                        </div>

                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>11:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div>
                                <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                                <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div>
                                <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                                <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                    <!-- End Schdule Day 1 -->

                    <!-- Schdule Day 2 -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 11:00 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                            </div> -->
                                <h4>History of Biochemistry </h4>
                                <p>4 talks x 30 minutes each</p>
                                <p>G.P. Talwar, Talwar Research Foundation, New Delhi <br>
G. Padmanabhan, CUTN, Thiruvarur <br>
B.J. Rao, Hyd University, Hyderabad <br>
D.J. Chattopadhyay, SNU, Kolkata
</p>
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:00 AM-11:30 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4>Award Lecture</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM-11:40 AM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                            </div> -->
                                <h4> Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:40 AM- 1:20 PM </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                            </div> --><span>Auditorium 01</span>
                                <h4> Session 1 (Scientific session 5 talks x 20 min) Microbiology and infectious disease biology I</h4>
<p>Anuradha Chaudhary, VB Patel Chest Clinic, New Delhi <br>
 Ashwani Kumar, IMTECH, Chandigarh <br>
Rajiv Kumar, BHU, Varanasi <br> 
Adline Princy Salomon, Sashtra University, Thanjavur <br>
Riddhiman Dhar, IIT-Kharagpur
</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:40 AM- 1:20 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                            </div> -->
                                <span>Auditorium 02</span>
                                <h4>Session 2 (Scientific session 5 talks x 20 min) Cancer Biology I</h4>
                            <p>K Satyamoorthy, Manipal University, Manipal <br>
Shaida Andrabi, Kashmir University, Srinagar <br>
Mohit K Jolly, IISc, Bangalore <br>
Gargi Bagchi, AUH, Gurugram <br>
Prathibha Ranganathan, CHG, Bengaluru
</p>
                            
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:20 PM-3:45 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                            </div> -->
                                <h4>Lunch and Mid-Day Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:45 PM – 4:30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                            </div> -->
                                <h4>Plenary lecture 1 (Partha Pratim Majumder, NIBMG, Kolkata) </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4:30 PM– 5:30 PM   </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> --><span>Auditorium 01</span>
                                <h4>Session 3 (Scientific session 3 talks x 20 min) Development and aging</h4>
                                <p>
                            Anindya Ghosh Roy, NBRC, Manesar <br>
                            Rupasri Ain, IICB, Kolkata  <br>
K. Subramaniam, IIT, Chennai 

                            </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4:30 PM – 5:30 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <span>Auditorium 02</span>
                                <h4>Session 4 (Scientific session 3 talks x 20 min) Translational research and drug discovery I</h4>
                       <p>
                       Praveen Vemula, InStem Bangalore <br>
            Debojyoti Chakraborty, IGIB, Delhi	 <br>
Monalisa Mukherjee, AUUP, Noida

                       </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:30 PM – 5:40 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div> -->
                                <h4>Short Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div> -->
                                <span>Auditorium 01</span>
                                <h4>Session 5 (Scientific session 4 talks x 20 min) Microbiology and infectious disease biology II </h4>
                             <p>
                             Rachna Chaba, IISER Mohali		 <br>
K.M. Sinha, AUH, Gurugram <br>
Amirul Islam Mallick, IISER Kolkata <br>
Varsha Singh, IISc, Bangalore 

                             </p>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                          </div> -->
                          <span>Auditorium 02</span>
                                <h4>Session 6 (Scientific session 4 talks x 20 min) Translational research and drug discovery II </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                          <p>

                          Sobhan Sen, JNU, New Delhi <br>
 Sudipta Basu, IIT Gandhinagar <br>
Rituparna Sinha Roy, IISER Kolkata <br>
Ruchi Anand, IIT Bombay 

                          </p>
                            </div>
                        </div>
                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>10:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                                </div>
                                <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                                <p>Facere provident incidunt quos voluptas.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>11:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div>
                                <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                                <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div>
                                <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                                <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                    <!-- End Schdule Day 2 -->

                    <!-- Schdule Day 4-->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 9:45 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div> -->
                                <h4>Plenary lecture 2 (V. Nagaraja, IISc, Bangalore) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                           
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:45 AM - 10:45 AM    </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> --><span>Auditorium 01</span>
                                <h4>Session 7 (Scientific session 3 talks x 20 min) Translational research and drug discovery II</h4>
<p>
Rai Ajit Srivastava, Espervita Therapeutics, USA <br>
 Dhirendra Katti, IIT Kanpur <br>
Nitin Chaudhary, IIT Guahati

</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:45 AM - 10:45 AM    </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <span>Auditorium 02</span>
                                <h4> Session 8 (Scientific session 3 talks x 20 min) Metabolism and immunology I </h4>
                            <p>
                            Vijay K. Chaudhary, DU South Campus, New Delhi <br>
Avinash Sonawane, IIT Indore <br>
Amit Awasthi, THSTI, Faridabad

                            </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:45 AM-11:00 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div> -->
                                <h4> Break</h4>
                                <!-- <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:00 AM – 1:00 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div> -->
                                <h4> Session 9 (Scientific session 4talks x 30 min) Covid 19: An Indian perspective </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            <p>
                            Anurag Agrawal, IGIB, New Delhi <br>
Sanjay Singh, Gennova Biopharma, Pune <br>
R. Varadarajan, IISc, Bangalore <br>
Lalith Kishore, CCAMP, Bangalore


                            </p>

                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:00 PM - 3:30  PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                              <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                          </div> -->
                                <h4>Lunch and Mid-Day Break </h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:30 PM – 5:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                        </div> -->
                                <h4>Poster session</h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM – 5:30 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                      </div> -->
                                <h4>Award lecture</h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>
                        <!-- <div class="row schedule-item">
                            <div class="col-md-3"><time>5:00 PM - 5:30 PM  </time></div>
                            <div class="col-md-9">
                                <div class="speaker">
                        <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                    </div>
                                <h4>Award lecture</h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:30 PM – 5:40 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                  </div> -->
                                <h4>Short Break</h4>
                                <!-- <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM   </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> --><span>Auditorium 01</span>
                                <h4>Session 10 (Scientific session 4 talks x 20 min) Natural products and plant metabolites </h4>
<p>
Jyothilakshmi Vadassery, NIPGR, New Delhi <br>
            Shyam Kumar Masakapalli, IIT-Mandi <br>
                 Sumit Ghosh, CIMAP, Lucknow <br>
Ramesha Thimmappa, AUUP, Noida

</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:40 PM – 7:00 PM   </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div> -->
                                <span>Auditorium 02</span>
                                <h4>Session 11 (Scientific session 4 talks x 20 min) Metabolism and immunology II</h4>
                           
                           <p>
                           Dhiraj kumar, ICGEB, New Delhi <br>
Sheetal Gandotra, IGIB, New Delhi <br>
Hiya Ghosh, NCBS, Bangalore <br>
Karthik Raman, IIT-Chennai


                           </p>
                            </div>
                        </div>
                        <!-- 
                        <div class="row schedule-item">
                            <div class="col-md-"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div>
 -->
                    </div>
                    <!-- End Schdule Day 2 -->
                    <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-4">

                    <div class="row schedule-item">
                            <div class="col-md-3"><time>9:00 AM – 9:45 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                        </div> -->
                                <h4>Plenary lecture 3 </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>9:30 AM – 10: 15 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                        </div> -->
                                <h4>Plenary lecture 3 (R. Sankarnarayanan, CCMB, Hyderabad) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:15 AM – 11:15 AM      </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                        </div> --><span>Auditorium 01</span>
                                <h4> Session 12 (Scientific session 3 talks x 20 min) Cancer Biology II</h4>
<p>
KB Harikumar, RGCB, Trivandam <br>
Ritu Kulshreshtha, IIT Delhi <br>
Debabrata Biswas, IICB, Kolkata

</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>10:15 AM – 11:15 AM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                            <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                        </div> -->
                                <span>Auditorium 02</span>
                                <h4> Session 13 (Scientific session 3 talks x 20 min) Biochemistry at the digital era</h4>
                            <p>
                            Tavpritesh Sethi, IIT Delhi <br>
                Saikrishnan Kayarat, IISER Pune		 <br>
Saikat Chakraborty, IICB, Kolkata

                            </p>
                            </div>
                        </div>



                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:15 AM-11:30 AM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                      </div> -->
                                <h4>Short Break </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM - 1:10 PM    </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                      </div> --><span>Auditorium 01</span>
                                <h4> Session 14 (Scientific session 5 talks x 20 min) Molecular biology and metabolism I</h4>
<p>
Kaustuv Sanyal, JNCASR, Bangalore	 <br>
                   Tapas Manna, IISER Trivandam <br>
         Rakesh S. Laishram, RGCB	<br>	
              Chandrima Das, SINP	 <br>	
               Ullas-Kolthur Seetaram, TIFR

</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>11:30 AM - 1:10 PM    </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                      </div> -->
                                <span>Auditorium 02</span>
                                <h4> Session 15 (Scientific session 5 talks x 20 min) Molecular cell signaling I</h4>
                          <p>
                          Suman Dhar, JNU, New Delhi  <br>
Senjuti Sinharoy, NIPGR, New Delhi <br>
Tina Mukherjee, InStem, Bangalore <br>
Sam Mathew, RCB, Faridabad <br>
Kaustav Datta, DU South Campus, New Delhi

                          </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>1:10 PM – 3:00 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                        <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                    </div> -->
                                <h4>Lunch and Mid-Day Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:00 PM – 3:45 PM </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                  </div> -->
                                <h4> Poster session </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>3:45 PM – 4:30 PM  </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                      <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                  </div> -->
                                <h4> Plenary lecture 4 (Gaiti Hasan, NCBS) </h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <h4 class="text-center mt-2">Auditorium 01 & Auditorium 02 </h4>
                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 30 PM – 5:50 PM     </time> </div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                      </div> --><span>Auditorium 01</span>
                                <h4> Session 16 (Scientific session 4 talks x 20 min) Molecular biology and metabolism II</h4>
<p>
Arvind Ramanathan, InStem, Bangalore <br>
  Swasti Raychaudhuri, CCMB, Hyderabad <br> 
Siddhesh S Kamat, IISER Pune <br>
Aneeshkumar AG, NII, New Delhi

</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>4: 30 PM – 5:50 PM     </time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                          <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                      </div> -->
                                <span>Auditorium 02</span>
                                <h4> Session 17 (Scientific session 4 talks x 20 min) Molecular cell signaling II</h4>
                            <p>
                            Mausumi Mutsuddi, BHU, Varanasi  <br>
Girish Ratnaparkhi,IISER,pune<br>
                Rashna Bhandari, CDFD, Hyderabad	 <br>
       Oishee Chakrabarti, SINP, Kolkata	 <br>

                            </p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>5:50 PM – 6:00 PM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/2.jpg" alt="Hubert Hirthe">
                                </div> -->
                                <h4>Short Break</h4>
                                <!-- <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p> -->
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-3"><time>6:00 PM - 7:00 PM</time></div>
                            <div class="col-md-9">
                                <!-- <div class="speaker">
                                    <img src="assets/img/speakers/3.jpg" alt="Cole Emmerich">
                                </div> -->
                                <h4>valedictory session</span>
                                </h4>
                                <!-- <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p> -->
                            </div>
                        </div>
                        <!-- 
                        <div class="row schedule-item">
                            <div class="col-md-2"><time>12:00 AM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/1.jpg" alt="Brenden Legros">
                                </div>
                                <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                                <p>Facere provident incidunt quos voluptas.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>02:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/4.jpg" alt="Jack Christiansen">
                                </div>
                                <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                                <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                            </div>
                        </div>
 -->
                        <!-- <div class="row schedule-item">
                            <div class="col-md-2"><time>03:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/5.jpg" alt="Alejandrin Littel">
                                </div>
                                <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                                <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                            </div>
                        </div>

                        <div class="row schedule-item">
                            <div class="col-md-2"><time>04:00 PM</time></div>
                            <div class="col-md-10">
                                <div class="speaker">
                                    <img src="assets/img/speakers/6.jpg" alt="Willow Trantow">
                                </div>
                                <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                                <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                            </div>
                        </div> -->

                    </div>
                </div>

            </div>

        </section>
        <!-- End Schedule Section -->

        <!-- ======= Venue Section ======= -->
        <!-- <section id="venue">

            <div class="container-fluid" data-aos="fade-up">

                <div class="section-header">
                    <h2>Event Venue</h2>
                    <p>Event venue location info and gallery</p>
                </div>

                <div class="row g-0">
                    <div class="col-lg-6 venue-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0"
                            style="border:0" allowfullscreen></iframe>
                    </div>

                    <div class="col-lg-6 venue-info">
                        <div class="row justify-content-center">
                            <div class="col-11 col-lg-8 position-relative">
                                <h3>Downtown Conference Center, New York</h3>
                                <p>Iste nobis eum sapiente sunt enim dolores labore accusantium autem. Cumque beatae ipsam. Est quae sit qui voluptatem corporis velit. Qui maxime accusamus possimus. Consequatur sequi et ea suscipit enim nesciunt quia velit.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container-fluid venue-gallery-container" data-aos="fade-up" data-aos-delay="100">
                <div class="row g-0">

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/1.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/1.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/2.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/2.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/3.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/3.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/4.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/4.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/5.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/5.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/6.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/6.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/7.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/7.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="venue-gallery">
                            <a href="assets/img/venue-gallery/8.jpg" class="glightbox" data-gall="venue-gallery">
                                <img src="assets/img/venue-gallery/8.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </section> -->
        <!-- End Venue Section -->

        <!-- ======= Hotels Section ======= -->
        <!-- <section id="hotels" class="section-with-bg">

            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Hotels</h2>
                    <p>Her are some nearby hotels</p>
                </div>

                <div class="row" data-aos="fade-up" data-aos-delay="100">

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/1.jpg" alt="Hotel 1" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 1</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                            </div>
                            <p>0.4 Mile from the Venue</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/2.jpg" alt="Hotel 2" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 2</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill-half-full"></i>
                            </div>
                            <p>0.5 Mile from the Venue</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="hotel">
                            <div class="hotel-img">
                                <img src="assets/img/hotels/3.jpg" alt="Hotel 3" class="img-fluid">
                            </div>
                            <h3><a href="#">Hotel 3</a></h3>
                            <div class="stars">
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                                <i class="bi bi-star-fill"></i>
                            </div>
                            <p>0.6 Mile from the Venue</p>
                        </div>
                    </div>

                </div>
            </div>

        </section> -->
        <!-- End Hotels Section -->

        <!-- ======= Gallery Section ======= -->
        <!-- <section id="gallery">

            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Gallery</h2>
                    <p>Check our gallery from the recent events</p>
                </div>
            </div>

            <div class="gallery-slider swiper-container">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/1.jpg" class="gallery-lightbox"><img src="assets/img/gallery/1.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/2.jpg" class="gallery-lightbox"><img src="assets/img/gallery/2.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/3.jpg" class="gallery-lightbox"><img src="assets/img/gallery/3.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/4.jpg" class="gallery-lightbox"><img src="assets/img/gallery/4.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/5.jpg" class="gallery-lightbox"><img src="assets/img/gallery/5.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/6.jpg" class="gallery-lightbox"><img src="assets/img/gallery/6.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/7.jpg" class="gallery-lightbox"><img src="assets/img/gallery/7.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="assets/img/gallery/8.jpg" class="gallery-lightbox"><img src="assets/img/gallery/8.jpg" class="img-fluid" alt=""></a>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>

        </section> -->
        <!-- End Gallery Section -->
        <section id="commite">
            <div class="container-fluid ">
                <div class="row ">
                    <div class="col-12 mt-4">
                        <h3 class="text-center " style="color: aliceblue;">
                            ORGANIZING COMMITTEE
                        </h3>
                        <!-- <h3 class="text-left mt-2 ">
                          90th Annual meeting, 2021.
                      </h3> -->
                    </div>
                    <div class="col-md-5 col-12 offset-md-2 ">
                        <h5 style="font-size: 25px; color: aliceblue;">Patron </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Ashutosh Sharma, Secretary, DST.
                            </li>
                            <li>
                                G. Padmanaban, President, NASI.
                            </li>
                            <li>
                                Shekhar Mande, Director General, CSIR.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            National Advisory Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Hari Mishra, BARC, Mumbai.
                            </li>
                            <li>
                                K. Natarajan, JNU, New Delhi.
                            </li>
                            <li>
                                L.S. Shashidhara, Ashoka University, Sonepat, IISER, Pune.
                            </li>
                            <li>
                                Rakesh Bhatnagar, Amity University Rajasthan, Jaipur.
                            </li>
                            <li>
                                Sanjay Singh, Gennova Biopharmaceuticals Ltd, Pune.
                            </li>
                            <li>
                                Subhra Chakraborty, NIPGR, New Delhi.
                            </li>
                            <li>
                                Tapas K. Kundu, CDRI, Lucknow.
                            </li>
                            <li>
                                V. Nagaraja, IISc, Bangalore.
                            </li>
                            <li>
                            Vijay K. Chaudhary, DU, South Campus, New Delhi.
                            </li>

                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            National Scientific Programme Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                          	Abhik Saha, Presidency University, Kolkata.
                            </li>
                            <li>
                            Arnab Mulkhopadhyay, NII, New Delhi.
                            </li>
                            <li>
                           	Avinash Bajaj, RCB, Faridabad.
                            </li>
                            <li>
                           	Benubrata Das, IACS, Kolkata.
                            </li>
                            <li>
                            Bushra Ateeq, IIT, Kanpur.
                            </li>
                            <li>
                           	Chandrima Das, SINP, Kolkata.
                            </li>
                            <li>
                           	Ellora Sen, NBRC, Manesar.
                            </li>
                            <li>
                           	Jitendra Thakur, ICGEB, New Delhi.
                            </li>
                            <li>
                           	Pawan Dhar, JNU, New Delhi.
                            </li>
                            <li>
                           	Supriya Chakraborty, JNU, New Delhi.
                            </li>
                            <!-- <li>
                                Ellora Sen, NBRC.
                            </li> -->
                            <li>
                            Vinay K. Nandicoori, CCMB, Hyderabad.
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-5 col-12 ">

                        <!-- <h5 class="mt-2">
                          Organizing Committee (Amity University Haryana)
                      </h5> -->
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Convenor
                        </h5>
                        <ul style="font-size: 20px;">
                            <li class="">Rajendra Prasad.</li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Organizing Secretary.
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Machiavelli Singh.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Programme Coordinators
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>Kaustav Bandyopadhyay.</li>
                            <li>
                                Ujjaini Dasgupta.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Treasurer
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Nitai Debnath.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Publication Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Saif Hameed.
                            </li>
                            <li>
                                Zeeshan Fatima.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Virtual Poster Management Committee
                        </h5>
                        <ul style="font-size: 20px;">
                            <li>
                                Munindra Ruwali.
                            </li>
                            <li>
                                Sumistha Das.
                            </li>
                        </ul>
                        <h5 style="font-size: 25px;  color: aliceblue;">
                            Virtual Scientific Session Management Committee
                        </h5>
                        <ul style="font-size: 17px;">
                            <li>Amresh Prakash.</li>
                            <li>Atanu Banerjee.</li>
                            <li> Chandramani Pathak.</li>
                            <li>Deepa Suhag.</li>
                            <li> Gargi Bagchi.</li>
                            <li>Jinny Tomar.</li>
                            <li>Ravi Dutta Sharma.</li>
                            <li>Sangeeta Kumari.</li>
                            <li>
                                Vaibhav Kapuria.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


        </section>

        
        <section class="bg_reg mt-2" id="bg_reg">
            <div class="container">
            <div class="row">
    <div class="col-md-12 col-12 col-xl-12">
<h1 class="text-white">
Registration
</h1>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
All prospective participants are expected to be members of the Society of Biological Chemists. Those who are not currently the members, it is advisable to take the SBC membership prior to registering for the conference.
<br>
The registration to the conference is mandatory to attend all the Virtual Sessions and to be eligible to present research work in the form of posters or short talks. It is mandatory to complete the registration process and the payment of the registration fee, i.e. INR 500 latest by Nov 15th, 2021. 
</p>

<h3 class="text-white">Registration Fee includes:</h3>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
-Unique login information allowing registrants to participate in meeting activities. <br>
-All live oral presentations including plenary and concurrent sessions. <br>
-Live poster presentations with one-on-one interaction with the presenter.<br>
-PDF of the abstract book.<br>
-Snaps at the Photobooth

</p>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
Registration fees are being charged to cover custom platform development, testing, subscription costs and licence for the conference application (Zoom/MS teams), creating and maintaining a website, Live technical support including support for dry runs, conference hosting, and other administrative costs.
</p>
<h1 class="text-white">
Abstract Submission guidelines:
</h1>
<h4 class="text-white">
Abstract preparation guidelines (A template is provided separately):
</h4>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
1.	The abstract should be prepared in MS Word in .doc or .docx on A4-sized page format. <br>
2.	Total printable word count should not exceed 250 words (excluding title, author names and address). Also please make sure the full abstract does NOT go beyond one page.<br>
3.	Abstract should be written in Times New Roman font.<br>
4.	Abstract text title, author names, author address and images should be centre-aligned. The main abstract text should be justified.<br>
5.	Font size should be 12 points for the main text and 16 point for the title.<br>
6.	Presenting author names should be underlined. Corresponding author name should be marked with an asterisk (*) in superscript.<br>
7.	Email address of presenting author and the corresponding author should be provided.<br>
8.	All addresses should be italicized.

</p>
<h1 class="text-white">
Poster submission guidelines:
</h1>
<p style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
Please provide a A4 sized poster (PDF only). The size should not be more than 5 mb.</p>
<div class="col-md-12 col-12 col-xl-12 text-center">
                      <a target="_blank" href="https://www.amity.edu/gurugram/sbci2021" rel="noopener noreferrer"><button class="btn btn-lg btn-success border" >Register Now</button></a>      
         
                            </div>
                            <p class="mt-3" style="text-align: justify; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: white; font-size: 20px  ;">
                            You can submit your abstract and/or poster after registration. Further instructions will be sent to your registered email. </p>
    </div>
</div>

            </div>
                <div class="container-fluid "  id="important_note1">

                    <div class="row">
                        <div class="col-12">
                            <h1  class="text-center text-white">
                            IMPORTANT DATES:
                            </h1>
                        </div>
                        <div class="col-md-4 col-xl-4 col-12 offset-md-2 ">
<h3 class="text-white">
REGISTRATION OPEN 
</h3>
<h3 class="text-white">
REGISTRATION FEE 	
</h3>
<h4 class="text-white">
ABSTRACT/POSTER SUBMISSION DEADLINE 	
</h4>   
<h4 class="text-white">
LATE REGISTRATION FEE WITHOUT ABSTRACT 
</h4>   
<h3 class="text-white">
FINAL REGISTRATION CLOSES 	
</h3>              
</div>
                        <div class="col-md-6 col-xl-6 col-12 mt-2 ">
                            <h3 class="text-white">
                            – 5th September 2021
                            </h3>
<h3 class="text-white">
- INR 500
</h3>
<h3 class="text-white">
- 15th November 2021
</h3>
<h3 class="text-white">
– INR 1000
</h3>
<h3 class="text-white">
– 1st December 2021
</h3>
                            </div>

                           
                            <!-- <div class="col-md-12 col-12 col-xl-12 text-center">
                      <a target="_blank" href="https://www.amity.edu/gurugram/sbci2021" rel="noopener noreferrer"><button class="btn btn-lg btn-success border" >Register Now</button></a>      
         
                            </div> -->
                            
                                      </div>

                                      
                </div>

                <section id="important_note" class="d-md-none d-xl-none d-sm-block">
            <div class="container-fluid">

<div class="row">
    <div class="col-md-12 text-center">
        <img src="assets/images/AMity.png" class="img-fluid"   alt="" srcset="">
    </div>
    
    <!-- <div class="col-12">
        <h1  class="text-center text-white">
        IMPORTANT DATES:
        </h1>
    </div>
    <div class="col-md-6 col-xl-6 offset-md-4 col-12 ">
<p class="text-white">
REGISTRATION OPEN   <span class="text_margin" >
– 5th September 2021
</span>
    </p>
<p class="text-white">
REGISTRATION FEE 	<span class="text_margin">
- INR 500
</span>
</p>
<p class="text-white">
ABSTRACT/POSTER SUBMISSION DEADLINE 	 <span class="text_margin">
- 15th November 2021
</span>
</p>   
<p class="text-white">
LATE REGISTRATION FEE WITHOUT ABSTRACT <span class="text_margin">
– INR 1000
</span>
</p>   
<p class="text-white">
FINAL REGISTRATION CLOSES 	 <span class="text_margin">
– 1st December 2021
</span>
</p>               -->
</div>
            </section> 
            </section>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center col-12">
            <h1>Countdown Timer</h1>
<div id="clockdiv">
  <div>
    <span id="day"></span>
    <div class="smalltext">Days</div>
  </div>
  <div>
    <span id="hour"></span>
    <div class="smalltext">Hours</div>
  </div>
  <div>
    <span id="minute"></span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span id="second"></span>
    <div class="smalltext">Seconds</div>
  </div>
</div>

            </div>
        </div>
    </div>
</section>
           
        <!-- ======= Supporters Section ======= -->
        <section id="supporters" class="section-with-bg">

            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h2>Sponsors</h2>
                </div>

                <div class="row no-gutters supporters-wrap clearfix" data-aos="zoom-in" data-aos-delay="100">

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                          
                            <img src="assets/images/logo_below.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                        <a href="https://www.kewaunee.in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/Kewaunee (General).jpg"  class="img-fluid" alt="" srcset=""></a>  
                            <!-- <img src="assets/images/Kewaunee (General).jpg" class="img-fluid" alt=""> -->
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                        <a href="https://www.mpbio.com/in/" target="_blank" rel="noopener noreferrer"><img src="assets/images/MP logo.jpg" alt="" srcset=""></a>
                            <img src="assets/images/MP logo.jpg" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/images/eppendorf_logo_web.jpg" class="img-fluid" alt="">
                        </div>
                    </div>

                    <!-- <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/5.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/6.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/7.png" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="supporter-logo">
                            <img src="assets/img/supporters/8.png" class="img-fluid" alt="">
                        </div>
                    </div> -->

                </div>

            </div>

        </section>
    

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="section-bg">

            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Contact Us</h2>
                    <!-- <p>Nihil officia ut sint molestiae tenetur.</p> -->
                </div>

                <div class="row contact-info">

                    <div class="col-md-4">
                        <div class="contact-address">
                            <i class="bi bi-geo-alt"></i>
                            <h3>Address</h3>
                            <address> 
                              Amity Institute of Integrative Sciences and Health (AIISH)<br> Amity Institute of Biotechnology (AIB)<br> Amity University Haryana, <br> Amity Education Valley<br> Gurugram - 122413, India
                              <br>
                              <strong>Phone:</strong> <br>
                            +91-9996633376 <br>+91-8800275382 <br>
                              <strong>Email:</strong><a href="mailto:amitysbci2021@gmail.com" target="_blank"> amitysbci2021@gmail.com</a> <br>
                          </address>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-phone">
                            <i class="bi bi-phone"></i>
                            <h3>Phone Number</h3>
                            <p><br>
                            +91-9996633376 <br>+91-8800275382 </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-email">
                            <i class="bi bi-envelope"></i>
                            <h3>Email</h3>
                            <p><a href="mailto:amitysbci2021@gmail.com" target="_blank">amitysbci2021@gmail.com</a></p>
                        </div>
                    </div>

                </div>

                <div class="form">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                            </div>
                            <div class="form-group col-md-6 mt-3 mt-md-0">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>

            </div>
        </section>
        <!-- End Contact Section -->

    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <img src="assets/images/PNG Logo.png" alt="TheEvenet">
                        <p>

                            Amity Institute of Integrative Sciences and Health (AIISH)<br> Amity Institute of Biotechnology (AIB)<br> Amity University Haryana, <br> Amity Education Valley<br> Gurugram - 122413, India
                            <br>

                        </p>
                    </div>

                    <!-- <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div> -->

                    <div class="col-lg-6 col-md-6 footer-links">
                        <h4>SBCI 2021</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#hero">Home</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#about">About us</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#speakers">Speakers</li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#commite">Organizing committee  </a></li>
                            <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="#bg_reg">Registration
                            </a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#supporters">Sponsors </a></li>
                        </ul>

              
                  
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>

                            <strong>Phone:</strong> <br>
                            +91-9996633376 <br>+91-8800275382 
                            <strong>Email:</strong>amitysbci2021@gmail.com<br>
                        
                        </p>

                        <div class="social-links">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>Coact</strong>. All Rights Reserved
            </div>

        </div>
    </footer>
    <!-- End  Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
<script>


const day=document.getElementById('day')
const hour=document.getElementById('hour')
const minute=document.getElementById('minute')
const second=document.getElementById('second')

setInterval(() => {
    

const currDate=Date.now()
const wishDate=new Date("16 december 2021 00:00:00")
const diff=(wishDate-currDate)
const dd=(Math.floor(diff/1000/60/60/24))
const hh=(Math.floor(diff/1000/60/60)%24)
const mm=(Math.floor(diff/1000/60)%60)
const ss=(Math.floor(diff/1000)%60)
day.innerHTML=dd<10?"0"+dd:dd
hour.innerHTML=hh<10?"0"+hh:hh
minute.innerHTML=mm<10?"0"+mm:mm
second.innerHTML=ss<10?"0"+ss:ss
if (dd>100) {
    document.getElementById('day-box').style.marginRight="30px"
}
}, 1000);



    function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

// function initializeClock(id, endtime) {
//   var clock = document.getElementById(id);
//   var daysSpan = clock.querySelector('.days');
//   var hoursSpan = clock.querySelector('.hours');
//   var minutesSpan = clock.querySelector('.minutes');
//   var secondsSpan = clock.querySelector('.seconds');

//   function updateClock() {
//     var t = getTimeRemaining(endtime);

//     daysSpan.innerHTML = t.days;
//     hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
//     minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
//     secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

//     if (t.total <= 0) {
//       clearInterval(timeinterval);
//     }
//   }

//   updateClock();
//   var timeinterval = setInterval(updateClock, 1000);
// }

// var deadline = new Date(Date.parse(new Date()) +  224 * 7 *60 *60 *1000);
// initializeClock('clockdiv', deadline);
</script>
</body>

</html>