<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>[24]7.ai</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
  <style>
  .bg-1 { 
    background-image: url("img/Ai/RED BG SEQ/H1.png");/* Green */
     background-repeat: no-repeat; /* Do not repeat the image */
     background-size:100% 100%;
     background-color: black;
     /* height: 500px; */
  }
  .navbar img{
    height: 20px;
    margin: 20px;
  }

  #empName{
    background-color:white;
  }

  </style>
</head>
<body class="bg-1">
  <div class="container-fluid">
    <nav class="navbar">
      <img src="img/Ai/RED BG SEQ/24.png" alt=""/>   
    </nav> 
  </div>
  <div class="container-fluid hum text-center">
    <img src="img/Ai/RED BG SEQ/Hum Orange.png" alt="hum" width="150" height="150">
  </div><br>
  <div class="container-fluid text-center">
  <img src="img/Ai/RED BG SEQ/Sentance.png"  width="80%" >
  </div>
  <div class="content-area">
		<div id="loginForm">
			<form id="login-form" method="post" role="form">
        

          <div id="login-message"></div>
          <div class="input-group">
              <input type="text" class="form-control" src="img/Ai/RED BG SEQ/Username.png" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
          </div>
          <!-- <div class="input-group">
              <input type="image" class="form-control" src="img/Ai/RED BG SEQ/.png" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
          </div> -->
          <div class="input-group">
              <input type="image" class="form-control" src="img/Ai/RED BG SEQ/HUM2.png" placeholder="Login" name="empName" autocomplete="off" required>
              <!-- <button id="login" class="login-button" src="img/Ai/RED BG SEQ/Mid.png" type="submit">Login</button> -->
          </div>
          <div class="offset-4">
              <p style="font-size:24px" id="demo"></p>
          </div>
			</form> 
		</div> 
	</div>
      
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
